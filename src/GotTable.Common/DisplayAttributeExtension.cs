﻿using System;

namespace GotTable.Common
{
    public static class DisplayAttributeExtension
    {
        public static string GetDisplayName(this Enum enumVal)
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(true);
            return (attributes.Length > 0) ? ((System.ComponentModel.DataAnnotations.DisplayAttribute)attributes[0]).Name : null;
        }

        public static string GetPromptString(this Enum enumVal)
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(true);
            return (attributes.Length > 0) ? ((System.ComponentModel.DataAnnotations.DisplayAttribute)attributes[0]).Prompt : null;
        }
    }
}
