﻿using System;


namespace GotTable.Common
{
    [Serializable]
    public sealed class EnumDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
