﻿using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum NotificationStatusType : int
        {
            [Display(Name = "Queued")]
            Queued = 1,

            [Display(Name = "Running")]
            Running = 2,

            [Display(Name = "Completed")]
            Completed = 3,

            [Display(Name = "Exception")]
            Exception = 4
        };
    }
}
