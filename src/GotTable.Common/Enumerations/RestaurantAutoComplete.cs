﻿namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum RestaurantAutoComplete : int
        {
            Location = 1,
            Cuisine = 2,
            RestaurantName = 3
        };
    }
}
