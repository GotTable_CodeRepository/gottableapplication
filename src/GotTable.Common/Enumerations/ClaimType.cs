﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GotTable.Common.Enumerations
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Enumeration
    {
        /// <summary>
        /// 
        /// </summary>
        public enum ClaimType : int
        {
            UserId = 1,
            Name = 2,
            Role = 3,
            Email = 4,
            CityId = 5,
            EnableRestaurantLogin = 6,
            RestaurantId = 7,
            RestaurantName = 8,
            RestaurantAddress = 9,
            DineInActive = 10,
            DeliveryActive = 11,
            TakeawayActive = 12
        }
    }
}
