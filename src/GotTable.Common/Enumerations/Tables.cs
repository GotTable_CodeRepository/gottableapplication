﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum Tables : int
        {
            [Display(Name = "Table For 2 Person")]
            TableFor2 = 1,
            [Display(Name = "Table For 4 Person")]
            TableFor4 = 2,
            [Display(Name = "Table For 6 Person")]
            TableFor6 = 3,
            [Display(Name = "Table For 8 Person")]
            TableFor8 = 4,
            [Display(Name = "Table For 10 Person")]
            TableFor10 = 5,
            [Display(Name = "Table For 12 Person")]
            TableFor12 = 6
        };
    }
}
