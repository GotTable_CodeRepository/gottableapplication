﻿
namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum OTPTransaction : int
        {
            TakeawayBooking = 1,
            DeliveryBooking = 2,
            UserRegistration = 3,
            DineInBooking = 4
        };
    }
}
