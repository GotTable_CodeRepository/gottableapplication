﻿using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum Device : int
        {
            [Display(Name = "IOS")]
            IOS = 1,
            [Display(Name = "Android")]
            Android = 2,
            [Display(Name = "Windows App")]
            WindowsApp = 3
        };
    }
}
