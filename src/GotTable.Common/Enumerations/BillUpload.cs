﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum BillUpload : int
        {
            [Display(Name = "Pending")]
            Pending = 1,

            [Display(Name = "InProcessing")]
            InProcessing = 2,

            [Display(Name = "Error")]
            Error = 3,

            [Display(Name = "Done")]
            Done = 4,
        };
    }
}
