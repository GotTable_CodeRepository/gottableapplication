﻿
using System.ComponentModel.DataAnnotations;

namespace GotTable.Common.Enumerations
{
    public partial class Enumeration
    {
        public enum AdminType : int
        {
            [Display(Name = "Super Admin")]
            SuperAdmin = 1,

            [Display(Name = "Sales Admin")]
            SalesAdmin = 2,

            [Display(Name = "Account Admin")]
            AccountAdmin = 3,

            [Display(Name = "Application Support")]
            ApplicationSupport = 4,

            [Display(Name = "Restaurant Admin")]
            HotelAdmin = 6
        };
    }
}
