﻿using System;

namespace GotTable.Common
{
    public static class BookingDateTimeExtension
    {
        public static long ToDateTimeInteger(this DateTime bookingDateTime)
        {
            return Convert.ToInt64(bookingDateTime.ToString(GotTableCommanConstant.BookingDateTimeIntegerFormat));
        }

        public static long ToLockInDateTimeInteger(this DateTime bookingDateTime)
        {
            return Convert.ToInt64(bookingDateTime.AddMinutes(-45).ToString(GotTableCommanConstant.BookingDateTimeIntegerFormat));
        }

        public static long ToLockOutDateTimeInteger(this DateTime bookingDateTime)
        {
            return Convert.ToInt64(bookingDateTime.AddMinutes(120).ToString(GotTableCommanConstant.BookingDateTimeIntegerFormat));
        }

        public static int ToInteger(this string bookingTime)
        {
            return Convert.ToInt16(bookingTime.Replace(":", ""));
        }
    }
}
