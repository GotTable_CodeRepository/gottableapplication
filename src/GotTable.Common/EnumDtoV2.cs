﻿using System;

namespace GotTable.Common
{
    public sealed class EnumDtoV2<T>
    {
        public T Id { get; set; }

        public string Name { get; set; }
    }
}
