﻿using System;

namespace GotTable.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class GotTableCommanConstant
    {
        /// <summary>
        /// BookingDateTimeIntFormat (Integer) 
        /// </summary>
        public const string BookingDateTimeIntegerFormat = "yyyyMMddHHmm";

        /// <summary>
        /// BookingDateFormat
        /// </summary>
        public const string BookingDateFormat = "MM-dd-yyyy";

        /// <summary>
        /// BookingTimeFormat
        /// </summary>
        public const string BookingTimeFormat = "HH:mm";

        /// <summary>
        /// BookingDateTimeIntFormat (Integer) 
        /// </summary>
        public const string BookingDateTimeFormat = "MM-dd-yyyy HH:mm";
    }
}
