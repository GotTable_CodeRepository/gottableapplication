﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class DineInStatusList
    {
        private DineInStatusList()
        {

        }

        public static List<EnumDto> Get()
        {
            var dineInStatusList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.DineInStatusType));
            for (int i = 2; i <= items.Count() - 1; i++)
            {
                dineInStatusList.Add(new EnumDto()
                {
                    Id = i,
                    Name = Enum.GetName(typeof(Enumeration.DineInStatusType), i)
                });
            }
            return dineInStatusList;
        }
    }
}
