﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.Common.List
{
    public class NotificationTypeList
    {
        private NotificationTypeList()
        {

        }

        public static List<EnumDto> Get(bool getAll)
        {
            var notificationTypeList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.NotificationType));
            for (int i = 1; i <= items.Count(); i++)
            {
                notificationTypeList.Add(new EnumDto()
                {
                    Id = i,
                    Name = Enum.GetName(typeof(Enumeration.NotificationType), i)
                });
            }
            return notificationTypeList;
        }
    }
}
