﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class TableList
    {
        private TableList()
        {

        }

        public static List<EnumDto> Get()
        {
            var tableList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.Tables));
            for (int i = 1; i <= items.Count(); i++)
            {
                tableList.Add(new EnumDto()
                {
                    Id = i,
                    Name = Enum.GetName(typeof(Enumeration.Tables), i)
                });
            }
            return tableList;
        }
    }
}
