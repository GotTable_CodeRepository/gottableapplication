﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class WeekDayList
    {
        private WeekDayList()
        {

        }

        public static List<EnumDto> Get()
        {
            var daysList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.WeekDays));
            for (int i = 1; i <= items.Count(); i++)
            {
                daysList.Add(new EnumDto()
                {
                    Id = i,
                    Name = Enum.GetName(typeof(Enumeration.WeekDays), i)
                });
            }
            return daysList;
        }
    }
}
