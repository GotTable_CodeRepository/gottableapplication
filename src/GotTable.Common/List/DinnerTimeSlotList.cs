﻿using System;
using System.Collections.Generic;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class DinnerTimeSlotList
    {
        private DinnerTimeSlotList()
        {

        }

        public static List<EnumDto> Get()
        {
            var dinnerSlot = new List<EnumDto>();

            dinnerSlot.Add(new EnumDto() { Name = "00:00" });
            dinnerSlot.Add(new EnumDto() { Name = "04:30 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "05:00 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "05:30 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "06:00 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "06:30 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "07:00 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "07:30 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "08:00 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "08:30 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "09:00 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "09:30 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "10:00 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "10:30 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "11:00 PM" });
            dinnerSlot.Add(new EnumDto() { Name = "11:30 PM" });

            return dinnerSlot;
        }
    }
}
