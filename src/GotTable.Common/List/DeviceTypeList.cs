﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.Common.List
{
    public sealed class DeviceTypeList
    {
        private DeviceTypeList()
        {

        }

        public static List<EnumDto> Get()
        {
            var enumList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.Device));
            for (int i = 1; i <= items.Count(); i++)
            {
                enumList.Add(new EnumDto()
                {
                    Id = i,
                    Name = Enum.GetName(typeof(Enumeration.Device), i)
                });
            }
            return enumList;
        }
    }
}
