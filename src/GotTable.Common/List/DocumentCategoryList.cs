﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class DocumentCategoryList
    {
        private DocumentCategoryList()
        {

        }

        public static List<EnumDto> Get()
        {
            var list = new List<EnumDto>();
            var items = Enum.GetValues(typeof(Enumeration.ImageCategory));
            foreach (var item in items)
            {
                list.Add(new EnumDto()
                {
                    Id = (int)item,
                    Name = item.ToString()
                });
            }
            return list;
        }
    }
}
