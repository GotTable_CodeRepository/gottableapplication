﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class UserTypeList
    {
        private UserTypeList()
        {

        }

        public static List<EnumDto> Get()
        {
            var enumList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.AdminType));
            for (int i = 1; i <= items.Count(); i++)
            {
                if ((Enumeration.AdminType)i == Enumeration.AdminType.SalesAdmin || (Enumeration.AdminType)i == Enumeration.AdminType.AccountAdmin)
                {
                    enumList.Add(new EnumDto()
                    {
                        Id = i,
                        Name = Enum.GetName(typeof(Enumeration.AdminType), i)
                    });
                }
            }
            return enumList;
        }
    }
}
