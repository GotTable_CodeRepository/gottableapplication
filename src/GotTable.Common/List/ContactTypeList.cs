﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class ContactTypeList
    {
        private ContactTypeList()
        {

        }

        public static List<EnumDto> Get(bool getAll)
        {
            var contactTypeList = new List<EnumDto>();
            var items = Enum.GetValues(typeof(Enumeration.ContactType));
            foreach (var item in items)
            {
                contactTypeList.Add(new EnumDto()
                {
                    Id = (int)item,
                    Name = item.ToString()
                });
            }
            if (!getAll)
            {
                return contactTypeList.Where(x => x.Id != (int)Enumeration.ContactType.GotTableRepersentative).ToList();
            }
            return contactTypeList;
        }
    }
}
