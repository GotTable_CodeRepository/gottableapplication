﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;


namespace GotTable.Common.List
{
    [Serializable]
    public sealed class PrescriptionMethodList
    {
        private PrescriptionMethodList()
        {

        }

        public static List<EnumDto> Get()
        {
            var enumList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.PrescriptionTypes));
            for (int i = 1; i <= items.Count(); i++)
            {
                enumList.Add(new EnumDto()
                {
                    Id = i,
                    Name = Enum.GetName(typeof(Enumeration.PrescriptionTypes), i)
                });
            }
            return enumList;
        }
    }
}
