﻿using System;
using System.Collections.Generic;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class LunchTimeSlotList
    {
        private LunchTimeSlotList()
        {

        }

        public static List<EnumDto> Get()
        {
            var lunchSlot = new List<EnumDto>();

            lunchSlot.Add(new EnumDto() { Name = "00:00" });
            lunchSlot.Add(new EnumDto() { Name = "06:00 AM" });
            lunchSlot.Add(new EnumDto() { Name = "06:30 AM" });
            lunchSlot.Add(new EnumDto() { Name = "07:00 AM" });
            lunchSlot.Add(new EnumDto() { Name = "07:30 AM" });
            lunchSlot.Add(new EnumDto() { Name = "08:00 AM" });
            lunchSlot.Add(new EnumDto() { Name = "08:30 AM" });
            lunchSlot.Add(new EnumDto() { Name = "09:00 AM" });
            lunchSlot.Add(new EnumDto() { Name = "09:30 AM" });
            lunchSlot.Add(new EnumDto() { Name = "10:00 AM" });
            lunchSlot.Add(new EnumDto() { Name = "10:30 AM" });
            lunchSlot.Add(new EnumDto() { Name = "11:00 AM" });
            lunchSlot.Add(new EnumDto() { Name = "11:30 AM" });
            lunchSlot.Add(new EnumDto() { Name = "12:00 PM" });
            lunchSlot.Add(new EnumDto() { Name = "12:30 PM" });
            lunchSlot.Add(new EnumDto() { Name = "01:00 PM" });
            lunchSlot.Add(new EnumDto() { Name = "01:30 PM" });
            lunchSlot.Add(new EnumDto() { Name = "02:00 PM" });
            lunchSlot.Add(new EnumDto() { Name = "02:30 PM" });
            lunchSlot.Add(new EnumDto() { Name = "03:00 PM" });
            lunchSlot.Add(new EnumDto() { Name = "03:30 PM" });
            lunchSlot.Add(new EnumDto() { Name = "04:00 PM" });
            lunchSlot.Add(new EnumDto() { Name = "04:30 PM" });

            return lunchSlot;
        }
    }
}
