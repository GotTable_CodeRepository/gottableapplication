﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class PaymentMethodList
    {
        private PaymentMethodList()
        {

        }

        public static List<EnumDto> Get()
        {
            var enumList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.PaymentMethod));
            for (int i = 1; i <= items.Count(); i++)
            {
                enumList.Add(new EnumDto()
                {
                    Id = i,
                    Name = ((Enumeration.PaymentMethod)i).ToString()
                });
            }
            return enumList;
        }
    }
}
