﻿using System.Collections.Generic;

namespace GotTable.Common.List
{
    public sealed class StatusList
    {
        private StatusList()
        {

        }

        public static List<EnumDto> Get()
        {
            var statusList = new List<EnumDto>();
            statusList.Add(new EnumDto()
            {
                Id = 0,
                Name = "False"
            });
            statusList.Add(new EnumDto()
            {
                Id = 1,
                Name = "True"
            });
            return statusList;
        }
    }
}
