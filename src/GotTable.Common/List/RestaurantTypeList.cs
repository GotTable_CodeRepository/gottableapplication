﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;


namespace GotTable.Common.List
{
    [Serializable]
    public sealed class RestaurantTypeList
    {
        private RestaurantTypeList()
        {

        }

        public static List<EnumDto> Get()
        {
            var categoryList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.RestaurantTypes));
            for (int i = 1; i <= items.Count(); i++)
            {
                categoryList.Add(new EnumDto()
                {
                    Id = i,
                    Name = Enum.GetName(typeof(Enumeration.RestaurantTypes), i)
                });
            }
            return categoryList;
        }
    }
}
