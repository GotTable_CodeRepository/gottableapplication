﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.Common.List
{
    [Serializable]
    public sealed class DeliveryandTakeawayStatusList
    {
        private DeliveryandTakeawayStatusList()
        {

        }

        public static List<EnumDto> Get()
        {
            var statusList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.DeliveryandTakeawayStatusType));
            for (int i = 2; i <= items.Count() - 1; i++)
            {
                statusList.Add(new EnumDto()
                {
                    Id = i,
                    Name = Enum.GetName(typeof(Enumeration.DineInStatusType), i)
                });
            }
            return statusList;
        }
    }
}
