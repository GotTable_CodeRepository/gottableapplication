﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;


namespace GotTable.Common.List
{
    [Serializable]
    public sealed class OfferTyeList
    {
        private OfferTyeList()
        {

        }

        public static List<EnumDto> Get()
        {
            var offerTypeList = new List<EnumDto>();
            var items = Enum.GetNames(typeof(Enumeration.OfferType));
            for (int i = 1; i <= items.Count(); i++)
            {
                offerTypeList.Add(new EnumDto()
                {
                    Id = i,
                    Name = Enum.GetName(typeof(Enumeration.OfferType), i)
                });
            }
            return offerTypeList;
        }
    }
}
