﻿using System;
using Elasticsearch.Net;
using Nest;

namespace GotTable.Search
{
    [Serializable]
    public class RestaurantService
    {
        public void Add()
        {
            var uris = new Uri[] { new Uri("http://localhost:9200") };

            var connectionPool = new StaticConnectionPool(uris);
            var settings = new ConnectionSettings(connectionPool);

            var client = new ElasticClient(settings);
        }
    }
}
