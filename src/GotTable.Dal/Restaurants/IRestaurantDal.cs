﻿
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Restaurants
{
    public interface IRestaurantDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        Task<List<RestaurantInfoDto>> FetchList(decimal? cityId = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<RestaurantDto> Fetch(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="adminId"></param>
        /// <returns></returns>
        Task<RestaurantInfoDto> FetchInfo(decimal? restaurantId, decimal? adminId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(RestaurantDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(RestaurantDto dto);

        /// <summary>
        /// 
        /// </summary> 
        /// <param name="restaurantName"></param>
        /// <param name="cityName"></param>
        /// <param name="addressLine1"></param>
        /// <returns></returns>
        Task<bool> IsExist(string restaurantName, string cityName, string addressLine1);
    }
}
