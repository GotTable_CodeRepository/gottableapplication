﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.Restaurants.PipelineRestaurants
{
    [Serializable]
    public class RestaurantInfoDto
    {
        public decimal RestaurantId { get; protected internal set; }

        public string RestaurantName { get; protected internal set; }

        public string Line1 { get; protected internal set; }

        public string Line2 { get; protected internal set; }

        public string City { get; protected internal set; }

        public string State { get; protected internal set; }

        public decimal Zip { get; protected internal set; }

        public decimal? SalesAdminPersonId { get; protected internal set; }

        public string SalesAdminName { get; protected internal set; }

        public string SalesAdminEmailAddress { get; protected internal set; }

        public decimal? AccountAdminId { get; protected internal set; }

        public string AccountAdminName { get; protected internal set; }

        public string AccountAdminEmailAddress { get; protected internal set; }

        public bool? OpenForAccountAdmin { get; protected internal set; }

        public Enumeration.RestaurantCategories Category { get; protected internal set; }

        public int ContactCount { get; protected internal set; }

        public int CuisineCount { get; protected internal set; }

        public int TablesCount { get; protected internal set; }

        public int MenuCount { get; protected internal set; }

        public int TimingCount { get; protected internal set; }
    }
}
