﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Restaurants.ApplicationRestaurants
{
    public interface IAutoCompleteDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryExpression"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<IEnumerable<AutoCompleteDto>> FetchList(string queryExpression = "", Enumeration.RestaurantAutoComplete? type = null);
    }
}
