﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;

namespace GotTable.Dal.Restaurants.ApplicationRestaurants
{
    public sealed class ListCriteria
    {
        public ListCriteria(double currentLatitude = default, double currentLongitude = default, int currentPage = 1, int pageSize = 10, Enumeration.RestaurantTypes? restaurantType = default, List<decimal> cuisineIds = default, string queryExpression = default, Enumeration.OfferType? offerType = default, int areaCoverage = 50, int? restaurantCategoryId = default, List<int> offerCategories = default, List<decimal> localtyId = default)
        {
            CurrentLatitude = currentLatitude;
            CurrentLongitude = currentLongitude;
            CurrentPage = currentPage;
            PageSize = pageSize;
            RestaurantType = restaurantType;
            CuisineIds = cuisineIds;
            QueryExpression = queryExpression;
            OfferType = offerType;
            AreaCoverage = areaCoverage;
            RestaurantCategoryId = restaurantCategoryId;
            OfferCategories = offerCategories;
            LocaltyId = localtyId;
        }

        public double CurrentLatitude { get; set; }

        public double CurrentLongitude { get; set; }

        public int CurrentPage { get; set; } // 1

        public int PageSize { get; set; } // 10

        public Enumeration.RestaurantTypes? RestaurantType { get; set; }

        public List<decimal> CuisineIds { get; set; }

        public string QueryExpression { get; set; }

        public Enumeration.OfferType? OfferType { get; set; }

        public int AreaCoverage { get; set; }

        public int? RestaurantCategoryId { get; set; }

        public List<int> OfferCategories { get; set; }

        public List<decimal> LocaltyId { get; set; }
    }
}
