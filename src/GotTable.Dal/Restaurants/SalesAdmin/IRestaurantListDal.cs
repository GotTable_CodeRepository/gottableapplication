﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Restaurants.SalesAdmin
{
    public interface IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="restaurantCategory"></param>
        /// <param name="restaurantType"></param>
        /// <param name="searchExpression"></param>
        /// <param name="restaurantOwnerEmailAddress"></param>
        /// <param name="expiredRestaurant"></param>
        /// <param name="inactiveRestaurant"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RestaurantInfoDto>> FetchList(decimal userId, Enumeration.RestaurantCategories? restaurantCategory = null, Enumeration.RestaurantTypes? restaurantType = null, string searchExpression = "", string restaurantOwnerEmailAddress = "", bool? expiredRestaurant = null, bool? inactiveRestaurant = null, int currentPage = 1, int pageSize = 10);
    }
}
