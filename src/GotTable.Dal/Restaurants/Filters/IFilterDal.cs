﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Restaurants.Filters
{
    public interface IFilterDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<FilterDto>> FetchList();
    }
}
