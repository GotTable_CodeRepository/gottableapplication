﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Restaurants.AccountAdmin
{
    public interface IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountAdminPersonId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RestaurantInfoDto>> FetchList(decimal accountAdminPersonId, int currentPage = 1, int pageSize = 10);
    }
}
