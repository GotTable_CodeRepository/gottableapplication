﻿
using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.Restaurants
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class RestaurantInfoDto
    {
        #region Direct members
        public decimal BranchId { get; protected internal set; }

        public string Name { get; protected internal set; }

        public string TagLine { get; protected internal set; }

        public string AddressLine1 { get; protected internal set; }

        public string AddressLine2 { get; protected internal set; }

        public string City { get; protected internal set; }

        public string State { get; protected internal set; }

        public decimal Zip { get; protected internal set; }

        public string Latitude { get; protected internal set; }

        public string Longitude { get; protected internal set; }

        public bool? IsActive { get; protected internal set; }

        public decimal? RestaurantAdminPersonId { get; protected internal set; }

        public decimal? SalesAdminPersonId { get; protected internal set; }

        public decimal? AccountAdminPersonId { get; protected internal set; }

        public bool? OpenforAccountAdmin { get; protected internal set; }

        public DateTime CreatedDate { get; protected internal set; }

        public string SEOKeyword { get; protected internal set; }

        public string SEOTitle { get; protected internal set; }

        public string SEODescription { get; protected internal set; }

        public Enumeration.RestaurantCategories? Category { get; protected internal set; }

        public string Description { get; protected internal set; }

        public int? CostForTwo { get; protected internal set; }

        public decimal? LocalityId1 { get; protected internal set; }

        public decimal? LocalityId2 { get; protected internal set; }

        public decimal? LocalityId3 { get; protected internal set; }

        public decimal? LocalityId4 { get; protected internal set; }

        #endregion

        #region Virtual members

        public string SalesAdminName { get; protected internal set; }

        public string SalesAdminEmailAddress { get; protected internal set; }

        public string AccountAdminName { get; protected internal set; }

        public string AccountAdminEmailAddress { get; protected internal set; }

        public string RestaurantAdminEmailAddress { get; protected internal set; }

        public string RestaurantAdminPassword { get; protected internal set; }

        public string RestaurantAdminName { get; protected internal set; }

        public string LocalityName1 { get; protected internal set; }

        public string LocalityName2 { get; protected internal set; }

        public string LocalityName3 { get; protected internal set; }

        public string LocalityName4 { get; protected internal set; }

        public bool DineInActive { get; protected internal set; }

        public bool DeliveryActive { get; protected internal set; }

        public bool TakeawayActive { get; protected internal set; }

        #endregion
    }
}
