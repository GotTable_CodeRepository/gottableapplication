﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.Restaurants
{
    [Serializable]
    public sealed class RestaurantDto
    {
        #region Direct members
        public decimal BranchId { get; set; }

        public string Name { get; set; }

        public string TagLine { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public bool? IsActive { get; set; }

        public decimal? AdminId { get; set; }

        public decimal? SalesAdminPersonId { get; set; }

        public decimal? AccountAdminPersonId { get; set; }

        public bool? OpenforAccountAdmin { get; set; }

        public DateTime CreatedDate { get; set; }

        public string SEOKeyword { get; set; }

        public string SEOTitle { get; set; }

        public string SEODescription { get; set; }

        public Enumeration.RestaurantCategories? Category { get; set; }

        public string Description { get; set; }

        public int? CostForTwo { get; set; }

        public decimal? LocalityId1 { get; set; }

        public string LocalityName1 { get; set; }

        public decimal? LocalityId2 { get; set; }

        public string LocalityName2 { get; set; }

        public decimal? LocalityId3 { get; set; }

        public string LocalityName3 { get; set; }

        public decimal? LocalityId4 { get; set; }

        public string LocalityName4 { get; set; }

        #endregion

        #region Virtual members

        public string SalesAdminName { get; set; }

        public string SalesAdminEmailAddress { get; set; }

        public string AccountAdminName { get; set; }

        public string AccountAdminEmailAddress { get; set; }

        public int ContactCount { get; set; }

        public int CuisineCount { get; set; }

        public int TableCount { get; set; }

        public int TimingCount { get; set; }

        public string AdminEmailAddress { get; set; }

        public string AdminName { get; set; }

        #endregion
    }
}
