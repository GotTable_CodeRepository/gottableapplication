﻿
namespace GotTable.Dal.ApplicationLogs
{
    /// <summary>
    /// ApplicationLogDto
    /// </summary>
    public sealed class ApplicationLogDto
    {
        public int Id { get; set; }

        public int TypeId { get; set; }

        public string ControllerName { get; set; }

        public string DeviceId { get; set; }

        public int DeviceTypeId { get; set; }

        public string Url { get; set; }

        public string Params { get; set; }

        public int UserId { get; set; }

        public int StatusCode { get; set; }

        public string Error { get; set; }
    }
}
