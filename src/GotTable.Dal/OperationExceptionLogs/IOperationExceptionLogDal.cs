﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.OperationExceptionLogs
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOperationExceptionLogDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<OperationExceptionLogDto>> FetchList(int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionLogId"></param>
        /// <returns></returns>
        Task<OperationExceptionLogDto> Fetch(Guid exceptionLogId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(OperationExceptionLogDto dto);
    }
}
