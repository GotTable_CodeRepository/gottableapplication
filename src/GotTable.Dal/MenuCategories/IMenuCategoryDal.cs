﻿
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.MenuCategories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMenuCategoryDal
    {

        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<IEnumerable<MenuCategoryDto>> FetchList(int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<MenuCategoryDto> Fetch(string name);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        Task<MenuCategoryDto> Fetch(decimal categoryId);
    }
}
