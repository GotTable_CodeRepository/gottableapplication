﻿
using System;

namespace GotTable.Dal.MenuCategories
{
    /// <summary>
    /// MenuCategoryDto
    /// </summary>
    [Serializable]
    public sealed class MenuCategoryDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
