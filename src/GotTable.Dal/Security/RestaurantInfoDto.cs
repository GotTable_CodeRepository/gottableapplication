﻿
namespace GotTable.Dal.Security
{
    /// <summary>
    /// 
    /// </summary>
    public class RestaurantInfoDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal Id { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string Address { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public bool DineInActive { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public bool DeliveryActive { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public bool TakeawayActive { get; protected internal set; }
    }
}
