﻿
using System;
using System.Threading.Tasks;

namespace GotTable.Dal.Security
{
    public interface IApplicationIdentityDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<ApplicationIdentityDto> Get(string userName, string password);
    }
}
