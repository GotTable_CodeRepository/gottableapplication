﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.Dal.Security
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class ApplicationIdentityDto
    {
        public decimal Id { get; set; }
        
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public Enumeration.AdminType AdminType { get; set; }

        public string EmailAddress { get; set; }

        public bool EnableRestaurantLogin { get; set; }

        public decimal RestaurantId { get; set; }

        public string RestaurantName { get; set; }

        public string RestaurantAddress { get; set; }

        public bool DineInActive { get; set; }

        public bool DeliveryActive { get; set; }

        public bool TakeawayActive { get; set; }
    }
}
