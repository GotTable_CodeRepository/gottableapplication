﻿
using System;

namespace GotTable.Dal.PushNotifications
{
    /// <summary>
    /// PushNotificationDto
    /// </summary>
    [Serializable]
    public sealed class PushNotificationDto
    {
        public int NotificationId { get; set; }

        public decimal UserId { get; set; }

        public decimal? CityId { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public int NotificationTypeId { get; set; }

        public int CategoryTypeId { get; set; }

        public int OfferTypeId { get; set; }

        public DateTime ScheduleTime { get; set; }

        public bool Active { get; set; }

        public bool Completed { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public string OfferTypeName { get; set; }

        public string CategoryTypeName { get; set; }

        public string CityName { get; set; }

        public string NotificationType { get; set; }

        public string Extension { get; set; }

        public int? SuccessCount { get; set; }

        public int? FailureCount { get; set; }

        public string AndroidPayLoad { get; set; }

        public string IosPayLoad { get; set; }

        public int? StatusId { get; set; }
    }
}
