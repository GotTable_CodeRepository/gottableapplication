﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantDocuments
{
    public interface IDocumentDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="imageCategoryId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<DocumentDto>> FetchList(decimal restaurantId, int? imageCategoryId = default, int currentPage = default, int pageSize = default);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantDocumentId"></param>
        /// <returns></returns>
        Task<DocumentDto> Fetch(int restaurantDocumentId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(DocumentDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Update(DocumentDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        Task Delete(int documentId);
    }
}
