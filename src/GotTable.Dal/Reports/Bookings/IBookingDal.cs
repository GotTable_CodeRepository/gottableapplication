﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Reports.Bookings
{
    /// <summary>
    /// 
    /// </summary>
    public interface IBookingDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingDto>> FetchList(DateTime bookingDate, int currentPage = 0, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingStartDate"></param>
        /// <param name="bookingEndDate"></param>
        /// <param name="restaurantName"></param>
        /// <param name="restaurantCategory"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BookingDto>> FetchList(DateTime bookingStartDate, DateTime bookingEndDate, string restaurantName, string restaurantCategory, int currentPage = 0, int pageSize = 10);
    }
}
