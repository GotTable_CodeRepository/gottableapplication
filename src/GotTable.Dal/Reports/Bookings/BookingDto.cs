﻿using System;

namespace GotTable.Dal.Reports.Bookings
{
    /// <summary>
    /// BookingDto
    /// </summary>
    [Serializable]
    public sealed class BookingDto
    {
        public decimal PhoneNumber { get; set; }

        public string BookingType { get; set; }

        public decimal BranchId { get; set; }

        public string BranchName { get; set; }

        public string BranchAddress { get; set; }

        public string BranchCity { get; set; }

        public string BranchState { get; set; }

        public decimal BranchZipCode { get; set; }

        public string UserName { get; set; }

        public DateTime BookingDate { get; set; }

        public string BookingTime { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
