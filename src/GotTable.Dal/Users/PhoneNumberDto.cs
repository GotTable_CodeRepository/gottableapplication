﻿using System;

namespace GotTable.Dal.Users
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class PhoneNumberDto
    {
        public decimal Id { get; set; }

        public decimal UserId { get; set; }

        public decimal TypeId { get; set; }

        public decimal Value { get; set; }

        public bool IsActive { get; set; }

        public bool IsVerified { get; set; }

        public DateTime? VerificationDate { get; set; }
    }
}
