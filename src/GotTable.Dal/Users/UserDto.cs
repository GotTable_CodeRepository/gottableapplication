﻿using System;

namespace GotTable.Dal.Users
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class UserDto
    {
        public decimal UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int TypeId { get; set; }

        public string TypeName { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public int? PrefixId { get; set; }

        public string PrefixName { get; set; }

        public int? GenderId { get; set; }

        public string GenderName { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsEmailVerified { get; set; }

        public bool? IsEmailOptedForCommunication { get; set; }

        public int? RewardCount { get; set; }

        public decimal? DoubleTheDealBookingId { get; set; }
    }
}
