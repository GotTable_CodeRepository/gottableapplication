﻿using System;

namespace GotTable.Dal.Communications.Message.Content
{
    /// <summary>
    /// ContentDto
    /// </summary>
    [Serializable]
    public class ContentDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal ContentTypeId { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public string Message { get; protected internal set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; protected internal set; }
    }
}
