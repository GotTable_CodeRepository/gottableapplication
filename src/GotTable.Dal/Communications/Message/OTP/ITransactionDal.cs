﻿using System.Threading.Tasks;

namespace GotTable.Dal.Communications.Message.OTP
{
    /// <summary>
    /// ITransactionDal
    /// </summary>
    public interface ITransactionDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        Task<TransactionDto> Fetch(decimal transactionId);

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(TransactionDto dto);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        Task Update(TransactionDto dto);
    }
}
