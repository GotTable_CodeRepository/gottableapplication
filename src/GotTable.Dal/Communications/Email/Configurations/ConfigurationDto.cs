﻿using System;

namespace GotTable.Dal.Communications.Email.Configurations
{
    /// <summary>
    /// ConfigurationDto
    /// </summary>
    [Serializable]
    public class ConfigurationDto
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal Id { get; protected internal set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string HostName { get; protected internal set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int PortNumber { get; protected internal set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsSSL { get; protected internal set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsTLS { get; protected internal set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; protected internal set; }
    }
}
