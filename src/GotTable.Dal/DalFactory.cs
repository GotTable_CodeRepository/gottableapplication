﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("GotTable.DalEF")]
namespace GotTable.Dal
{
    public static class DalFactory
    {
        public static T Create<T>()
        {
            string assemblyName = typeof(T).Assembly.FullName.Replace(".Dal", ".DalEF");
            string typeName = typeof(T).Namespace.Replace(".Dal.", ".DalEF.") + "." + typeof(T).Name.Substring(1, typeof(T).Name.Length - 1);
            var obj = Activator.CreateInstance(assemblyName, typeName);
            return (T)obj.Unwrap();
        }
    }
}
