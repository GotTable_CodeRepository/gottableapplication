﻿using System;

namespace GotTable.Dal.RestaurantRatings
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class AvgRatingDto
    {
        public int Rating { get; set; }
        
        public int AmbienceRating { get; set; }
        
        public int FoodRating { get; set; }
        
        public int MusicRating { get; set; }
        
        public int PriceRating { get; set; }
        
        public int ServiceRating { get; set; }
    }
}
