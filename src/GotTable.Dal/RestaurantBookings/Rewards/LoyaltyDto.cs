﻿using System;

namespace GotTable.Dal.RestaurantBookings.Rewards
{
    [Serializable]
    public class LoyaltyDto
    {
        public decimal RewardId { get; set; }

        public decimal UserId { get; set; }

        public decimal BookingId { get; set; }

        public decimal BranchId { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
