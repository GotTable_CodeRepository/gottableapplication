﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantBookings.Rewards
{
    public interface ILoyaltyDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<LoyaltyDto>> FetchList(decimal restaurantId, decimal userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(LoyaltyDto dto);
    }
}
