﻿
using System;
using System.Collections.Generic;

namespace GotTable.Dal.RestaurantBookings
{
    [Serializable]
    public class DineInListItemDto
    {
        #region Booking

        public decimal BookingId { get; set; }

        public DateTime BookingDate { get; set; }

        public string BookingTime { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? CurrentStatusId { get; set; }

        public decimal? OfferId { get; set; }

        public string OfferName { get; set; }

        public string PromoCode { get; set; }

        public decimal TableId { get; set; }

        public int BillUploadStatus { get; set; }

        public int RewardPoint { get; set; }

        #endregion

        #region Branch

        public decimal BranchId { get; set; }

        public string BranchName { get; set; }

        public string BranchAddress { get; set; }

        public string BranchCity { get; set; }

        public string BranchState { get; set; }

        public decimal BranchZipCode { get; set; }

        #endregion

        #region Contact

        public List<ContactListItemDto> ContactList { get; set; }

        #endregion
    }
}
