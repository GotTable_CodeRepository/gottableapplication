﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantBookings.DeliveryandTakeaway
{
    public interface IDeliveryandTakeawayDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        Task<DeliveryandTakeawayDto> Fetch(decimal bookingId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(DeliveryandTakeawayDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(DeliveryandTakeawayStatusDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="bookingId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task Insert(DeliveryAddressDto dto, decimal bookingId, decimal userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtos"></param>
        /// <param name="bookingId"></param>
        Task Insert(List<DeliveryandTakeawayCartIttemDto> dtos, decimal bookingId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<bool> MarkAsRead(decimal restaurantId);
    }
}
