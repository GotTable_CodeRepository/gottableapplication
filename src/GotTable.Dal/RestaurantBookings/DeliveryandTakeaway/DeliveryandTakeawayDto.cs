﻿using System;
using System.Collections.Generic;

namespace GotTable.Dal.RestaurantBookings.DeliveryandTakeaway
{
    [Serializable]
    public class DeliveryandTakeawayDto
    {
        public decimal BookingId { get; set; }

        public decimal BranchId { get; set; }

        public decimal UserId { get; set; }

        public decimal? OfferId { get; set; }

        public string OfferTitle { get; set; }

        public string OfferDescription { get; set; }

        public int BookingTypeId { get; set; }

        public DateTime BookingDate { get; set; }

        public string BookingTime { get; set; }

        public decimal PhoneNumber { get; set; }

        public int CurrentStatusId { get; set; }

        public decimal CartAmount { get; set; }

        public decimal CentralGST { get; set; }

        public decimal StateGST { get; set; }

        public decimal ServiceCharges { get; set; }

        public decimal DeliveryCharges { get; set; }

        public decimal PackingCharges { get; set; }

        public string Comment { get; set; }

        public string PromoCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool IsRead { get; set; }

        public List<DeliveryandTakeawayCartIttemDto> ItemList { get; set; }

        public List<DeliveryandTakeawayStatusDto> StatusList { get; set; }

        public DeliveryAddressDto DeliveryAddress { get; set; }
    }
}
