﻿using System;

namespace GotTable.Dal.RestaurantBookings.DeliveryandTakeaway
{
    [Serializable]
    public class DeliveryandTakeawayCartIttemDto
    {
        public decimal ItemId { get; set; }

        public decimal MenuId { get; set; }

        public string MenuName { get; set; }

        public string MenuDescription { get; set; }

        public decimal Cost { get; set; }

        public int Quantity { get; set; }

        public string Remark { get; set; }
    }
}
