﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantBookings.BillUploads
{
    public interface IBillUploadDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadStatus"></param>
        /// <param name="emailAddress"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BillUploadListItemDto>> FetchList(Enumeration.BillUpload? uploadStatus = null, string emailAddress = "", string phoneNumber = "", int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        Task<BillUploadDto> Fetch(int bookingId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(BillUploadDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Update(BillUploadDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        Task Delete(int bookingId);
    }
}
