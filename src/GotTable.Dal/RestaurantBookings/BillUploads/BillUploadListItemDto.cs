﻿using System;

namespace GotTable.Dal.RestaurantBookings.BillUploads
{
    [Serializable]
    public class BillUploadListItemDto
    {
        public decimal UserId { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public decimal PhoneNumber { get; set; }


        public decimal BookingId { get; set; }

        public string BookingDate { get; set; }

        public string RestaurantName { get; set; }

        public string RestaurantAddress { get; set; }


        public double Amount { get; set; }

        public string ImagePath { get; set; }

        public DateTime? UploadedDate { get; set; }

        public decimal? AdminId { get; set; }

        public string AdminName { get; set; }
    }
}
