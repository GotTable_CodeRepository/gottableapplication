﻿using GotTable.Common;
using System;

namespace GotTable.Dal.RestaurantBookings
{
    public class BookingInfoDto
    {
        public decimal BookingId
        {
            get;
            protected internal set;
        }

        public int BookingTypeId
        {
            get;
            protected internal set;
        }

        public string BookingTypeName
        {
            get;
            protected internal set;
        }

        public DateTime BookingDate
        {
            get;
            protected internal set;
        }

        public string BookingTime
        {
            get;
            protected internal set;
        }

        public decimal BranchId
        {
            get;
            protected internal set;
        }

        public string BranchName
        {
            get;
            protected internal set;
        }
        public decimal? OfferId
        {
            get;
            protected internal set;
        }

        public string OfferTitle
        {
            get;
            protected internal set;
        }

        public string OfferDescription
        {
            get;
            protected internal set;
        }

        public decimal PhoneNumber
        {
            get;
            protected internal set;
        }

        public int? CurrentStatusId
        {
            get;
            protected internal set;
        }

        public string CurrentStatusName
        {
            get;
            protected internal set;
        }

        public string Comment
        {
            get;
            protected internal set;
        }

        public string PromoCode
        {
            get;
            protected internal set;
        }

        public DateTime CreatedDate
        {
            get;
            protected internal set;
        }
        public bool? IsRead
        {
            get;
            protected internal set;
        }

        public decimal UserId
        {
            get;
            protected internal set;
        }

        public string UserName
        {
            get;
            protected internal set;
        }

        public string UserEmail
        {
            get;
            protected internal set;
        }

        public decimal TableId
        {
            get;
            protected internal set;
        }

        public string TableName
        {
            get;
            protected internal set;
        }

        public int RewardPoint
        {
            get;
            protected internal set;
        }

        public DateTime? BookingDateTime
        {
            get
            {
                if (this.BookingDate != null && !string.IsNullOrWhiteSpace(this.BookingTime))
                {
                    return DateTime.ParseExact(this.BookingDate.ToString(Common.GotTableCommanConstant.BookingDateFormat) + " " + this.BookingTime, GotTableCommanConstant.BookingDateTimeFormat, null);
                }
                return null;
            }
        }

        public long? LockInDateTime
        {
            get
            {
                return this.BookingDateTime?.ToLockInDateTimeInteger();
            }
        }

        public long? LockOutDateTime
        {
            get
            {
                return this.BookingDateTime?.ToLockOutDateTimeInteger();
            }
        }
    }
}
