﻿using System;

namespace GotTable.Dal.RestaurantBookings
{
    [Serializable]
    public class DeliveryItemDto
    {
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Landmark { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }
    }
}
