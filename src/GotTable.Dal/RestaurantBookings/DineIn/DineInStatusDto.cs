﻿using System;

namespace GotTable.Dal.RestaurantBookings.DineIn
{
    [Serializable]
    public class DineInStatusDto
    {
        #region Active property

        public decimal Id { get; set; }

        public decimal BookingId { get; set; }

        public decimal? UserId { get; set; }

        public decimal? AdminId { get; set; }

        public decimal StatusId { get; set; }

        public bool IsAdminUser { get; set; }

        public string Comment { get; set; }

        public DateTime CreatedDate { get; set; }

        #endregion

        #region Connected property

        public string StatusName { get; set; }

        public string UserName { get; set; }

        public string UserEmailAddress { get; set; }

        public string AdminName { get; set; }

        public string AdminEmailAddress { get; set; }

        #endregion
    }
}
