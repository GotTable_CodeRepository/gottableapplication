﻿using System;
using System.Collections.Generic;

namespace GotTable.Dal.RestaurantBookings.DineIn
{
    [Serializable]
    public sealed class DineInDto
    {
        public decimal Id { get; set; }

        public decimal BranchId { get; set; }

        public decimal UserId { get; set; }

        public decimal TableId { get; set; }

        public string TableName { get; set; }

        public decimal PhoneNumber { get; set; }

        public decimal? OfferId { get; set; }

        public int? OfferTypeId { get; set; }

        public string OfferTitle { get; set; }

        public string OfferDescription { get; set; }

        public string PromoCode { get; set; }

        public DateTime BookingDate { get; set; }

        public string BookingTime { get; set; }

        public string Comment { get; set; }

        public int? CurrentStatusId { get; set; }

        public DateTime CreatedDate { get; set; }

        public bool? IsRead { get; set; }

        public List<DineInStatusDto> StatusList { get; set; }

        public bool RewardApplied { get; set; }

        public int BillUploadStatus { get; set; }

        public int RewardPoint { get; set; }

        public bool? DoubleTheDealActive { get; set; }
    }
}
