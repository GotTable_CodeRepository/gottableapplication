﻿using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantBookings.DineIn
{
    public interface IDineInDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        Task<DineInDto> Fetch(decimal bookingId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(DineInDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task Insert(DineInStatusDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        Task<bool> MarkAsRead(decimal bookingId);
    }
}
