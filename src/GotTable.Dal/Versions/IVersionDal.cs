﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Versions
{
    /// <summary>
    /// IVersionDal
    /// </summary>
    public interface IVersionDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<VersionDto>> FetchList(int currentPage = 0, int pageSize = 10);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="versionId"></param>
        /// <returns></returns>
        Task<VersionDto> Fetch(int versionId);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="versionId"></param>
        /// <param name="typeId"></param>
        /// <returns></returns>
        Task<VersionDto> Fetch(string versionId, int typeId);

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(VersionDto dto);

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        Task Update(VersionDto dto);
    }
}
