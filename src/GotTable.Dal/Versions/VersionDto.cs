﻿using System;

namespace GotTable.Dal.Versions
{
    [Serializable]
    public class VersionDto
    {
        public int VersionId { get; set; }

        public string Number { get; set; }

        public int TypeId { get; set; }

        public string TypeName { get; set; }

        public bool Active { get; set; }

        public string Message { get; set; }

        public int AttachedDeviceCount { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid SyncName { get; set; }

        public Guid SyncPassword { get; set; }
    }
}
