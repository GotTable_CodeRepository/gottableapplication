﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.RestaurantTimings
{
    public interface ITimingDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<List<TimingDto>> FetchList(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantTypeId"></param>
        /// <returns></returns>
        Task<List<TimingDto>> Fetch(decimal restaurantId, int restaurantTypeId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Insert(TimingDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        Task Update(TimingDto dto);
    }
}
