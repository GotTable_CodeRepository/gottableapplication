﻿
using System;

namespace GotTable.Dal.Cuisines
{
    /// <summary>
    /// CuisineDto
    /// </summary>
    [Serializable]
    public class CuisineDto
    {
        public decimal Id { get; protected internal set; }

        public string Name { get; protected internal set; }

        public int? CategoryId { get; protected internal set; }

        public string CategoryName { get; protected internal set; }

        public bool IsActive { get; protected internal set; }

        public int DisplayLevel { get; protected internal set; }

        public int EngagedRestaurant { get; protected internal set; }
    }
}
