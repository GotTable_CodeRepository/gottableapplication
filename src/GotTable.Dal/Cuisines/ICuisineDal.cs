﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Dal.Cuisines
{
    /// <summary>
    /// ICuisineDal
    /// </summary>
    public interface ICuisineDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<IEnumerable<CuisineDto>> FetchList(int currentPage = 1, int pageSize = 10);

        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task<IEnumerable<CuisineDto>> FetchList(int currentPage = 1, int pageSize = 10, bool status = false);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="cuisineId"></param>
        /// <returns></returns>
        Task<CuisineDto> Fetch(decimal cuisineId);

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="cuisineName"></param>
        /// <returns></returns>
        Task<CuisineDto> Fetch(string cuisineName);
    }
}
