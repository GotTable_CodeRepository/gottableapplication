﻿using GotTable.API.Model.UserAddresses;
using GotTable.API.Model.UserPhoneNumbers;
using GotTable.API.Model.Users;
using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Users;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/login/")]
    public sealed class LoginController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPhoneNumberList phoneNumberList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="phoneNumberList"></param>
        public LoginController(IUser user, IOperationExceptionLog operationExceptionLog, IPhoneNumberList phoneNumberList)
        {
            this.user = user;
            this.operationExceptionLog = operationExceptionLog;
            this.phoneNumberList = phoneNumberList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailaddress"></param>
        /// <param name="password"></param>
        /// <param name="phonenumber"></param>
        /// <param name="deviceid"></param>
        /// <param name="devicetype"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(string emailaddress, string password, string phonenumber = default, string deviceid = default, string devicetype = default)
        {
            var model = new UserResponse<UserDetailModel>()
            {
                UserDetail = new UserDetailModel()
            };
            if (string.IsNullOrEmpty(emailaddress) || string.IsNullOrEmpty(password))
            {
                model.ErrorMessage = "Email address and password cannot be blank";
                return new BadRequestObjectResult(model.ErrorMessage);
            }
            try
            {
                var userDAO = await user.Get(emailaddress, password);
                if (userDAO == null || userDAO.UserId == 0)
                {
                    model.ErrorMessage = "Invalid credentials";
                    return new BadRequestObjectResult(model.ErrorMessage);
                }
                if (!userDAO.IsActive)
                {
                    model.ErrorMessage = "Your credentials has been locked.";
                    return new BadRequestObjectResult(model.ErrorMessage);
                }
                if (userDAO.UserType == Enumeration.UserType.Guest.ToString())
                {
                    userDAO.UserType = Enumeration.UserType.EndUser.ToString();
                    await user.Save(userDAO);
                }

                model.ErrorMessage = string.Empty;
                model.UserDetail.Password = userDAO.Password;
                model.UserDetail.UserId = userDAO.UserId.ToString("G29");
                model.UserDetail.UserType = userDAO.UserType;
                model.UserDetail.FirstName = userDAO.FirstName;
                model.UserDetail.Gender = userDAO.Gender;
                model.UserDetail.Prefix = userDAO.Prefix;
                model.UserDetail.LastName = userDAO.LastName;
                model.UserDetail.EmailAddress = userDAO.EmailAddress;

                model.UserDetail.PhoneNumberList = new System.Collections.Generic.List<PhoneNumberModel>();
                model.UserDetail.AddressList = new System.Collections.Generic.List<AddressModel>();
                foreach (var phoneNumber in await phoneNumberList.Get(userDAO.UserId))
                {
                    model.UserDetail.PhoneNumberList.Add(new PhoneNumberModel()
                    {
                        UserId = phoneNumber.UserId.ToString("G29"),
                        NumberId = phoneNumber.Id.ToString("G29"),
                        Type = phoneNumber.Type,
                        Value = phoneNumber.Value.ToString("G29")
                    });
                }
                return new OkObjectResult(model);
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                model.ErrorMessage = exception.Message;
                model.UserDetail = new UserDetailModel();
                return new BadRequestObjectResult(model.ErrorMessage);
            }
        }
    }
}
