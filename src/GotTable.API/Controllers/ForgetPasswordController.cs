﻿using GotTable.DAO.Communications.Email;
using GotTable.Library.Communications.Email;
using GotTable.Library.Communications.Email.EmailTypes;
using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [AllowAnonymous]
    [Route("api/forgetpassword/")]
    public sealed class ForgetPasswordController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IEmailTransmission<ForgetPassword, ForgetPasswordDAO> emailTransmission;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="emailTransmission"></param>
        public ForgetPasswordController(IOperationExceptionLog operationExceptionLog, IEmailTransmission<ForgetPassword, ForgetPasswordDAO> emailTransmission)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.emailTransmission = emailTransmission;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailaddress"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(string emailaddress)
        {
            if (string.IsNullOrEmpty(emailaddress))
            {
                return new BadRequestObjectResult(new { message = "Missing emailaddress" });
            }
            try
            {
                var forgetPassword = new ForgetPasswordDAO()
                {
                    ConfirmationMessage = string.Empty,
                    UserId = 0,
                    EmailAddress = emailaddress,
                };

                await emailTransmission.Execute(forgetPassword);

                if (forgetPassword.Status)
                {
                    return new OkObjectResult(new { message = forgetPassword.ConfirmationMessage });
                }
                else
                {
                    return new BadRequestObjectResult(new { message = forgetPassword.ErrorMessage });
                }
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                return new BadRequestObjectResult(new { message = exception.Message });
            }
        }
    }
}
