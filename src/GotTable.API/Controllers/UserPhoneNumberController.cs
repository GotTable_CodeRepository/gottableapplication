﻿using GotTable.API.Model;
using GotTable.API.Model.UserPhoneNumbers;
using GotTable.API.Model.Users;
using GotTable.DAO.Users;
using GotTable.Library.Users;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/userphonenumber/")]
    public sealed class UserPhoneNumberController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPhoneNumberList phoneNumberList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="phoneNumberList"></param>
        public UserPhoneNumberController(IUser user, IPhoneNumberList phoneNumberList)
        {
            this.user = user;
            this.phoneNumberList = phoneNumberList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(Request<PhoneNumberModel> request)
        {
            var model = new UserResponse<UserDetailModel>
            {
                UserDetail = new UserDetailModel()
            };
            if (string.IsNullOrEmpty(request.Detail.NumberId))
            {
                model.ErrorMessage = "AddressId can not be blank";
                return new BadRequestObjectResult(model);
            }
            if (string.IsNullOrEmpty(request.Detail.Value))
            {
                model.ErrorMessage = "City name can not be blank";
                return new BadRequestObjectResult(model);
            }
            if (string.IsNullOrEmpty(request.Detail.UserId))
            {
                model.ErrorMessage = "Missing UserId";
                return new BadRequestObjectResult(model);
            }

            var phoneNumber = new PhoneNumberDAO()
            {
                Id = decimal.Parse(request.Detail.NumberId),
                isActive = true,
                Type = "Cell",
                UserId = decimal.Parse(request.Detail.UserId),
                Value = decimal.Parse(request.Detail.Value)
            };

            if (phoneNumber.Id == 0)
            {
                await user.New(phoneNumber);
            }

            var userDAO = await user.Get(phoneNumber.UserId);

            model.ErrorMessage = string.Empty;
            model.UserDetail.Password = userDAO.Password;
            model.UserDetail.UserId = userDAO.UserId.ToString("G29");
            model.UserDetail.UserType = userDAO.UserType;
            model.UserDetail.FirstName = userDAO.FirstName;
            model.UserDetail.Gender = userDAO.Gender;
            model.UserDetail.Prefix = userDAO.Prefix;
            model.UserDetail.LastName = userDAO.LastName;
            model.UserDetail.EmailAddress = userDAO.EmailAddress;

            foreach (var item in await phoneNumberList.Get(userDAO.UserId))
            {
                model.UserDetail.PhoneNumberList.Add(new PhoneNumberModel()
                {
                    UserId = item.UserId.ToString("G29"),
                    NumberId = item.Id.ToString("G29"),
                    Type = item.Type,
                    Value = item.Value.ToString("G29")
                });
            }

            return new OkObjectResult(model);
        }
    }
}
