﻿using GotTable.Library.Restaurants;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/restaurantautocompletelist/")]
    public sealed class RestaurantAutoCompleteListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IAutoCompleteList autoCompleteList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="autoCompleteList"></param>
        public RestaurantAutoCompleteListController(IAutoCompleteList autoCompleteList)
        {
            this.autoCompleteList = autoCompleteList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryExpression"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(string queryExpression)
        {
            var model = await autoCompleteList.Get(queryExpression);
            return new OkObjectResult(model);
        }
    }
}
