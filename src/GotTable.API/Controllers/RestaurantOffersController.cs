﻿using GotTable.API.Model.RestaurantOffers;
using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantOffers;
using GotTable.Library.RestaurantOffers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/restaurantoffers/")]
    public sealed class RestaurantOffersController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOfferList offerList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerList"></param>
        public RestaurantOffersController(IOfferList offerList)
        {
            this.offerList = offerList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resturantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="offerType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(decimal? resturantId = null, Enumeration.RestaurantTypes? restaurantType = null, Enumeration.OfferType? offerType = null, int currentPage = 0, int pageSize = 10)
        {
            if (resturantId == null)
            {
                return new BadRequestObjectResult(new { errorMessage = "Missing restaurantId" });
            }
            var offerDAOs = await offerList.Get(resturantId, restaurantType, null, offerType, null, null, true, currentPage, pageSize);
            var model = MapObjectToModel(offerDAOs, offerType);
            return new OkObjectResult(model);
        }

        #region Helpers method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerList"></param>
        /// <param name="offerType"></param>
        /// <returns></returns>
        internal List<OfferListModel> MapObjectToModel(List<BranchOfferDAO> offerList, Enumeration.OfferType? offerType)
        {
            var model = new List<OfferListModel>();

            if (offerType == null)
            {
                model.Add(new OfferListModel()
                {
                    Title = Enumeration.OfferType.Todays.ToString(),
                    List = offerList.Where(x => x.IsActive == true && x.TypeId == (int)Enumeration.OfferType.Todays).Select(item => new OfferModel()
                    {
                        Description = item.Description,
                        EndDate = item.EndDate.ToString(),
                        Name = item.Name,
                        OfferId = item.Id.ToString(),
                        StartDate = item.StartDate.ToString(),
                        Tag = string.Empty
                    }).ToList()
                });
                model.Add(new OfferListModel()
                {
                    Title = Enumeration.OfferType.Special.ToString(),
                    List = offerList.Where(x => x.IsActive == true && x.TypeId == (int)Enumeration.OfferType.Special).Select(item => new OfferModel()
                    {
                        Description = item.Description,
                        EndDate = item.EndDate.ToString(),
                        Name = item.Name,
                        OfferId = item.Id.ToString(),
                        StartDate = item.StartDate.ToString(),
                        Tag = string.Empty
                    }).ToList()
                });
            }
            else
            {
                model.Add(new OfferListModel()
                {
                    Title = offerType.ToString(),
                    List = offerList.Where(x => x.IsActive == true && x.TypeId == (int)offerType).Select(item => new OfferModel()
                    {
                        Description = item.Description,
                        EndDate = item.EndDate.ToString(),
                        Name = item.Name,
                        OfferId = item.Id.ToString(),
                        StartDate = item.StartDate.ToString(),
                        Tag = string.Empty
                    }).ToList()
                });
            }
            return model;
        }

        #endregion
    }
}
