﻿using GotTable.API.Model;
using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/fileupload/")]
    public sealed class FileUploadController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public FileUploadController(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(FileUploadModel model)
        {
            try
            {
                string errorMessage = "Invalid model properties, Please check";
                if (model.Content == null && string.IsNullOrEmpty(model.StringContent))
                {
                    return new BadRequestObjectResult(new { Status = 0, ErrorMessage = errorMessage, FilePath = string.Empty });
                }
                if (string.IsNullOrEmpty(model.FileExtension))
                {
                    return new BadRequestObjectResult(new { Status = 0, ErrorMessage = errorMessage, FilePath = string.Empty });
                }
                if (string.IsNullOrEmpty(model.FileName))
                {
                    return new BadRequestObjectResult(new { Status = 0, ErrorMessage = errorMessage, FilePath = string.Empty });
                }
                if (string.IsNullOrEmpty(model.FileName))
                {
                    return new BadRequestObjectResult(new { Status = 0, ErrorMessage = errorMessage, FilePath = string.Empty });
                }
                if (model.Content == null && model.StringContent != null)
                {
                    model.Content = Convert.FromBase64String(model.StringContent);
                }
                //string directoryPath = HttpContext.Current.Server.MapPath("~/Uploads/" + model.UploadType + @"//");
                //string filePath = directoryPath + model.FileName;
                //File.WriteAllBytes(filePath, model.Content);
                //string imagePath = @"/Uploads/" + model.UploadType + @"/" + model.FileName;
                return new OkObjectResult( new { Status = 1, FilePath = string.Empty, ErrorMessage = string.Empty });
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                return new BadRequestObjectResult(new { Status = 0, FilePath = string.Empty, ErrorMessage = exception.Message });
            }
        }
    }
}
