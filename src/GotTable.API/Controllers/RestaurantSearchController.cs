﻿using GotTable.API.Model.Images;
using GotTable.API.Model.Restaurants;
using GotTable.API.Model.Resturants;
using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantDocuments;
using GotTable.Library.Restaurants;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/restaurant/")]
    public class RestaurantSearchController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        /// <summary>
        /// 
        /// </summary>
        private readonly IApplicationRestaurantList applicationRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="applicationRestaurantList"></param>
        /// <param name="operationExceptionLog"></param>
        public RestaurantSearchController(IDocument document, IApplicationRestaurantList applicationRestaurantList, IOperationExceptionLog operationExceptionLog)
        {
            this.document = document;
            this.applicationRestaurantList = applicationRestaurantList;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cuisineids"></param>
        /// <param name="deviceid"></param>
        /// <param name="devicetype"></param>
        /// <param name="restaurantType"></param>
        /// <param name="restaurantCategoryId"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="todaysoffer"></param>
        /// <param name="restaurantName"></param>
        /// <param name="specialoffer"></param>
        /// <param name="areacoverage"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Route("GetSearch")]
        [HttpGet]
        public async Task<IActionResult> GetSearch(Enumeration.RestaurantTypes restaurantType, string cuisineids = "", string deviceid = default, string devicetype = default, int? restaurantCategoryId = null, double latitude = 0.0, double longitude = 0.0, string todaysoffer = default, string restaurantName = default, string specialoffer = default, int areacoverage = 0, int currentPage = 0, int pageSize = 10)
        {
            var model = new ResturantListResponse<List<RestaurantModel>>
            {
                RestaurantList = new List<RestaurantModel>(),
                ErrorMessage = string.Empty
            };
            try
            {
                if (latitude == 0.0)
                {
                    model.ErrorMessage = "Missing latitude";
                    return new BadRequestObjectResult(model);
                }
                if (longitude == 0.0)
                {
                    model.ErrorMessage = "Missing longitude";
                    return new BadRequestObjectResult(model);
                }

                var offerType = specialoffer == default && todaysoffer == default ? null : todaysoffer == "yes" ? Enumeration.OfferType.Todays : specialoffer == "yes" ? Enumeration.OfferType.Special : (Enumeration.OfferType?)null;

                List<decimal> cuisineidList = null;
                if (!string.IsNullOrEmpty(cuisineids))
                {
                    cuisineidList = new List<decimal>()
                    {
                        decimal.Parse(cuisineids)
                    };
                }

                var restaurantListDAOS = await applicationRestaurantList.Get(latitude, longitude, restaurantType, cuisineidList, areacoverage, offerType, restaurantName, currentPage, pageSize, restaurantCategoryId, null, null);

                foreach (var restaurant in restaurantListDAOS)
                {
                    model.RestaurantList.Add(new RestaurantModel()
                    {
                        Distance = restaurant.Distance.ToString("G29"),
                        Id = restaurant.Id.ToString("G29"),
                        Latitude = restaurant.Latitude,
                        Longitude = restaurant.Longitude,
                        AddressLine1 = restaurant.AddressLine1,
                        AddressLine2 = restaurant.AddressLine2,
                        City = restaurant.City,
                        State = restaurant.State,
                        Zip = restaurant.ZipCode.ToString("G29"),
                        Name = restaurant.Name,
                        Rating = 0,
                        CuisineNames = restaurant.CuisineNames,
                        OfferName = restaurant.OfferName,
                        Tag = restaurant.SelectedTag.ToString(),
                        IsOfferAvailable = restaurant.IsOfferAvailable,
                        Images = await MapObjectToModel(restaurant.Id, true),
                        RestaurantTag = restaurant.RestaurantTag,
                        ActiveOffers = restaurant.ActiveOffers,
                        AmbienceRating = restaurant.AmbienceRating,
                        CostForTwo = restaurant.CostForTwo,
                        Description = restaurant.Description,
                        FoodRating = restaurant.FoodRating,
                        MusicRating = restaurant.MusicRating,
                        PriceRating = restaurant.PriceRating,
                        ServiceRating = restaurant.ServiceRating
                    });
                }
                return new OkObjectResult(model);
            }
            catch (Exception exceptionLog)
            {
                _ = await operationExceptionLog.New(exceptionLog);
                model.ErrorMessage = exceptionLog.Message;
                return new BadRequestObjectResult(model);
            }
        }

        #region Helper method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        private async Task<List<ImageModel>> MapObjectToModel(decimal restaurantId, bool listType)
        {
            var imageModels = new List<ImageModel>();
            var restaurantImages = await document.Get(restaurantId);
            if (listType)
            {
                if (restaurantImages.FrontImage != null && restaurantImages.FrontImage.FilePath != null)
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "FrontLogo",
                        Url = restaurantImages.FrontImage.FilePath
                    });
                }
                else
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "FrontLogo",
                        Url = "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg"
                    });
                }
            }
            else
            {
                if (restaurantImages.BackImage != null && restaurantImages.BackImage.FilePath != null)
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "InsideLogo",
                        Url = restaurantImages.BackImage.FilePath
                    });
                }
                else
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "InsideLogo",
                        Url = "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg"
                    });
                }
            }
            return imageModels;
        }

        #endregion
    }
}
