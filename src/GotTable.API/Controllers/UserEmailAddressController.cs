﻿using GotTable.Library.Users;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/useremailaddress/")]
    public sealed class UserEmailAddressController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public UserEmailAddressController(IUser user)
        {
            this.user = user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(string emailAddress)
        {
            if (emailAddress.Trim() == "")
            {
                return new BadRequestObjectResult(new { message = "missing Email Address" });
            }
            var userDAO = await user.Get(emailAddress);
            if (userDAO.UserId != 0)
            {
                return new OkObjectResult(new { message = "yes" });
            }
            else
            {
                return new OkObjectResult(new { message = "no" });
            }
        }
    }
}
