﻿using GotTable.API.Model.RestaurantTables;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantTables;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/tableavailablility/")]
    public sealed class TableAvailabilityController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ITableList tableList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTable branchTable;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTable"></param>
        /// <param name="tableList"></param>
        /// <param name="operationExceptionLog"></param>
        public TableAvailabilityController(IBranchTable branchTable, ITableList tableList, IOperationExceptionLog operationExceptionLog)
        {
            this.branchTable = branchTable;
            this.tableList = tableList;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="bookingTime"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(decimal restaurantId = default, string bookingDate = default, string bookingTime = default)
        {
            var model = new ResturantTablesResponse<List<TableModel>>();
            try
            {
                if (restaurantId == default)
                {
                    model.ErrorMessage = "missing restaurantId";
                    return new BadRequestObjectResult(model);
                }
                if (string.IsNullOrEmpty(bookingDate))
                {
                    model.ErrorMessage = "missing booking date";
                    return new BadRequestObjectResult(model);
                }
                if (string.IsNullOrEmpty(bookingTime))
                {
                    model.ErrorMessage = "missing booking time";
                    return new BadRequestObjectResult(model);
                }
                var dtBookingDate = DateTime.ParseExact(bookingDate, "MM-dd-yyyy", null);
                model.ErrorMessage = string.Empty;
                model.TableList = new List<TableModel>();
                foreach (var table in await tableList.Get(restaurantId, 0, 10))
                {
                    model.TableList.Add(new TableModel()
                    {
                        Id = table.Id.ToString("G29"),
                        Name = table.SelectedTable,
                        Total = table.Active ? Convert.ToInt16(table.Value) : 0,
                        Vacant = await branchTable.CheckAvailablity(table, dtBookingDate, bookingTime)
                    });
                }
                return new OkObjectResult(model);
            }
            catch (Exception ee)
            {
                await operationExceptionLog.New(ee);
                model.ErrorMessage = ee.Message.ToString();
                return new BadRequestObjectResult(model);
            }
        }
    }
}
