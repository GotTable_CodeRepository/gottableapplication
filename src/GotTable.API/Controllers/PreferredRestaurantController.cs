﻿using GotTable.API.Model.PreferredRestaurants;
using GotTable.DAO.Restaurants.PreferredRestaurants;
using GotTable.Library.RestaurantDocuments;
using GotTable.Library.Restaurants;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/preferredrestaurant/")]
    public sealed class PreferredRestaurantController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPreferredRestaurantList preferredRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurantList"></param>
        /// <param name="document"></param>
        public PreferredRestaurantController(IPreferredRestaurantList preferredRestaurantList, IDocument document)
        {
            this.document = document;
            this.preferredRestaurantList = preferredRestaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(double latitude, double longitude, int currentPage = 0, int pageSize = 10)
        {
            var model = new PreferredRestaurantListModel<List<PreferredRestaurantModel>>
            {
                ErrorMessage = string.Empty
            };
            if (latitude == 0.0)
            {
                model.ErrorMessage = "Invalid request, Please check.!";
                return BadRequest(model);
            }
            if (longitude == 0.0)
            {
                model.ErrorMessage = "Invalid request, Please check.!";
                return BadRequest(model);
            }
            model.RestaurantList = new List<PreferredRestaurantModel>();
            var listModel = await preferredRestaurantList.Get(latitude, longitude, currentPage, pageSize);
            model.RestaurantList = await MapObjectToModel(listModel);
            return Ok(model);
        }

        #region Helpers method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurants"></param>
        /// <returns></returns>
        private async Task<List<PreferredRestaurantModel>> MapObjectToModel(List<PreferredRestaurantInfoDAO> preferredRestaurants)
        {
            var model = new List<PreferredRestaurantModel>();

            foreach (var restaurantItem in preferredRestaurants)
            {
                var restaurantImages = await document.Get(restaurantItem.RestaurantId);
                model.Add(new PreferredRestaurantModel()
                {
                    Address = restaurantItem.Address,
                    City = restaurantItem.City,
                    Distance = restaurantItem.Distance.Value.ToString("G29"),
                    Latitude = restaurantItem.Latitude,
                    Longitude = restaurantItem.Longitude,
                    Name = restaurantItem.Name,
                    RestaurantId = restaurantItem.RestaurantId.ToString("G29"),
                    State = restaurantItem.State,
                    Zipcode = restaurantItem.RestaurantId.ToString("G29"),
                    SEODescription = restaurantItem.SEODescription,
                    SEOKeyword = restaurantItem.SEOKeyword,
                    SEOTitle = restaurantItem.SEOTitle,
                    TagLine = restaurantItem.TagLine,
                    ImageUrl = restaurantImages.FrontImage != null && restaurantImages.FrontImage.FilePath != null ? restaurantImages.FrontImage.FilePath : "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg",
                    Rating = restaurantItem.Rating
                });
            }

            return model;
        }

        #endregion
    }
}
