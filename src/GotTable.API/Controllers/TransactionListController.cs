﻿using GotTable.API.Model.RestaurantBookings;
using GotTable.API.Model.UserBookings;
using GotTable.DAO.Bookings.Users;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Library.Users;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/transactionlist/")]
    public sealed class TransactionListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IUserBookingList userBookingList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="userBookingList"></param>
        public TransactionListController(IUser user, IOperationExceptionLog operationExceptionLog, IUserBookingList userBookingList)
        {
            this.user = user;
            this.operationExceptionLog = operationExceptionLog;
            this.userBookingList = userBookingList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="activeorder"></param>
        /// <param name="pastOrder"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(decimal UserId = 0, bool? activeorder = null, bool? pastOrder = null, int currentPage = 0, int pageSize = 10)
        {
            var model = new TransactionList<UserBookingModel>();
            if (UserId == 0)
            {
                model.ErrorMessage = "missing userId";
                return new BadRequestObjectResult(model);
            }

            var userDAO = await user.Get(UserId);
            if (userDAO.UserType == null || userDAO.UserType.ToLower() != "enduser")
            {
                model.ErrorMessage = "Invalid userId";
                return new BadRequestObjectResult(model);
            }

            var bookingList = await userBookingList.Get(UserId, currentPage, pageSize);
            var bookingModel = MapModel2Object(bookingList);
            model.ErrorMessage = string.Empty;
            model.TransactionDetail = bookingModel;
            return new OkObjectResult(model);
        }

        #region MapObjectToModel
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userBooking"></param>
        /// <returns></returns>
        internal UserBookingModel MapModel2Object(UserBookingDAO userBooking)
        {
            string userBookingInString = JsonConvert.SerializeObject(userBooking);
            return JsonConvert.DeserializeObject<UserBookingModel>(userBookingInString);
        }
        #endregion
    }
}
