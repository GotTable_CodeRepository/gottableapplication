﻿using GotTable.API.Model.ApplicationSettings;
using GotTable.Library.ApplicationSettings;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// ApplicationSettingController
    /// </summary>
    [Route("api/applicationsetting/")]

    public sealed class ApplicationSettingController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IApplicationSetting applicationSetting;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationSetting"></param>
        public ApplicationSettingController(IApplicationSetting applicationSetting)
        {
            this.applicationSetting = applicationSetting;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        [Route("get")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var model = new ApplicationSettingModel();
            var applicationSettingDAO = await applicationSetting.Get();
            model.AdminApplicationBaseUrl = applicationSettingDAO.AdminApplicationBaseUrl;
            model.UserApplicationBaseUrl = applicationSettingDAO.UserApplicationBaseUrl;
            model.DineInActive = applicationSettingDAO.DineInActive;
            model.DeliveryActive = applicationSettingDAO.DeliveryActive;
            model.TakeawayActive = applicationSettingDAO.TakeawayActive;
            return new ObjectResult(model);
        }
    }
}
