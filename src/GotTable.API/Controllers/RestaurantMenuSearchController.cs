﻿using GotTable.API.Model.Images;
using GotTable.API.Model.RestaurantMenus;
using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantMenus;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/restaurantmenusearch/")]
    public sealed class RestaurantMenuSearchController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMenuList menuList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="menuList"></param>
        public RestaurantMenuSearchController(IOperationExceptionLog operationExceptionLog, IMenuList menuList)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.menuList = menuList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(decimal restaurantId, Enumeration.RestaurantTypes? restaurantType = null, int currentPage = 0, int pageSize = 10)
        {
            var model = new ResturantMenuModel<List<MenuModel>>();
            if (restaurantId == 0)
            {
                model.ErrorMessage = "missing restaurantId";
                return new BadRequestObjectResult(model);
            }
            if (restaurantType == null && restaurantType == Enumeration.RestaurantTypes.DineIn)
            {
                model.ErrorMessage = "missing restaurantType";
                return new BadRequestObjectResult(model);
            }

            model.List = new List<MenuModel>();
            var menuDAOs = await menuList.Get(restaurantId, restaurantType, currentPage, pageSize);
            foreach (var menuItem in menuDAOs)
            {
                model.List.Add(new MenuModel()
                {
                    CategoryName = menuItem.SelectedCategory,
                    MenuId = menuItem.Id.ToString("G29"),
                    Name = menuItem.Name,
                    Price = menuItem.Cost,
                    Description = menuItem.Description,
                    Logo = new ImageModel()
                    {
                        Name = menuItem.MenuLogo.Name,
                        Url = menuItem.MenuLogo.Url ?? "http://67.209.123.59/images/logo.png"
                    }
                });
            }

            model.ErrorMessage = string.Empty;
            return new OkObjectResult(model);
        }
    }
}
