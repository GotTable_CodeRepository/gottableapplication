﻿using GotTable.API.Model;
using GotTable.API.Model.RestaurantBookings;
using GotTable.API.Model.RestaurantBookings.DeliveryandTakeaway;
using GotTable.Common.Enumerations;
using GotTable.DAO.Bookings.DeliveryandTakeaway;
using GotTable.DAO.Users;
using GotTable.Library.Communications.Message.Type2Factors;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Library.RestaurantMenus;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/deliveryconfirmation/")]
    public sealed class DeliveryConfirmationController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDeliveryandTakeaway deliveryandTakeaway;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchMenu branchMenu;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deliveryandTakeaway"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="branchMenu"></param>
        /// <param name="oTPCommand"></param>
        public DeliveryConfirmationController(IDeliveryandTakeaway deliveryandTakeaway, IOperationExceptionLog operationExceptionLog, IBranchMenu branchMenu, IOTPCommand oTPCommand)
        {
            this.deliveryandTakeaway = deliveryandTakeaway;
            this.operationExceptionLog = operationExceptionLog;
            this.branchMenu = branchMenu;
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(Request<DeliveryandTakeawayBookingModel> request)
        {
            var bookingModel = new BookingResponse
            {
                BookingId = "0",
                BookingType = Enumeration.BookingType.Delivery.ToString(),
                ConfirmationMessage = string.Empty,
                ErrorMessage = string.Empty
            };

            if (string.IsNullOrEmpty(request.Detail.TransactionId))
            {
                bookingModel.ErrorMessage = "Invalid request, missing transactionId";
                return new BadRequestObjectResult(bookingModel);
            }
            if (string.IsNullOrEmpty(request.Detail.OtpNumber))
            {
                bookingModel.ErrorMessage = "Invalid request, missing OTP number";
                return new BadRequestObjectResult(bookingModel);
            }

            try
            {
                var otpDetail = await oTPCommand.Get(Convert.ToDecimal(request.Detail.TransactionId));
                if (otpDetail == null)
                {
                    bookingModel.ErrorMessage = "Invalid request, Please check OTP number.";
                    return new BadRequestObjectResult(bookingModel);
                }
                if (otpDetail.OTP != request.Detail.OtpNumber)
                {
                    bookingModel.ErrorMessage = "Invalid request, Please check OTP number.";
                    return new BadRequestObjectResult(bookingModel);
                }
                if (otpDetail.TransactionId == 0)
                {
                    bookingModel.ErrorMessage = "Invalid request, Please check OTP number.";
                    return new BadRequestObjectResult(bookingModel);
                }
                if (otpDetail.IsUsed)
                {
                    bookingModel.ErrorMessage = "Invalid request, Please check OTP number.";
                    return new BadRequestObjectResult(bookingModel);
                }

                var dao = new DeliveryandTakeawayDAO()
                {
                    BookingDate = DateTime.ParseExact(request.Detail.BookingDate, "MM-dd-yyyy", null),
                    BookingTime = request.Detail.BookingTime,
                    BookingId = String.IsNullOrEmpty(request.Detail.BookingId) ? 0 : decimal.Parse(request.Detail.BookingId),
                    BookingTypeId = (int)Enumeration.BookingType.Delivery,
                    BranchId = String.IsNullOrEmpty(request.Detail.RestaurantId) ? 0 : decimal.Parse(request.Detail.RestaurantId),
                    OfferId = String.IsNullOrEmpty(request.Detail.OfferId) ? 0 : decimal.Parse(request.Detail.OfferId),
                    PhoneNumber = request.Detail.PhoneNumber,
                    CentralGST = String.IsNullOrEmpty(request.Detail.CentralGst) ? 0 : decimal.Parse(request.Detail.CentralGst),
                    StateGST = String.IsNullOrEmpty(request.Detail.StateGst) ? 0 : decimal.Parse(request.Detail.StateGst),
                    UserId = String.IsNullOrEmpty(request.Detail.UserId) ? 0 : decimal.Parse(request.Detail.UserId),
                    CartItem = await MapObject2Model(request.Detail.CartItem),
                    AddressDetail = MapObject2Model(request.Detail.AddressDetail),
                    DeliveryCharges = String.IsNullOrEmpty(request.Detail.DecimalCharges) ? 0 : decimal.Parse(request.Detail.DecimalCharges),
                    PromoCode = request.Detail.PromoCode,
                    UserDetail = new UserDetailDAO()
                    {
                        FirstName = request.Detail.FirstName,
                        LastName = request.Detail.LastName,
                        EmailAddress = request.Detail.EmailAddress,
                        Gender = Enumeration.Gender.NotDefined.ToString(),
                        Prefix = Enumeration.Prefix.NotDefined.ToString(),
                        UserType = Enumeration.UserType.Guest.ToString(),
                        IsActive = true,
                        IsEmailVerified = false,
                        IsEmailOptedForCommunication = false
                    }
                };

                await deliveryandTakeaway.New(dao);

                if (!dao.Status)
                {
                    bookingModel.ErrorMessage = dao.ErrorMessage;
                    return new BadRequestObjectResult(bookingModel);
                }
                bookingModel.ConfirmationMessage = dao.ConfirmationMessage;
                bookingModel.BookingId = dao.BookingId.ToString("G29");
                otpDetail.IsUsed = true;
                await oTPCommand.Save(otpDetail);

                return new OkObjectResult(bookingModel);
            }
            catch (Exception exception)
            {
                bookingModel.ErrorMessage = exception.Message.ToString();
                await operationExceptionLog.New(exception);
                return new BadRequestObjectResult(bookingModel);
            }
        }

        #region Helper methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private async Task<List<DeliveryandTakeawayCartItemDAO>> MapObject2Model(List<CartModel> request)
        {
            var model = new List<DeliveryandTakeawayCartItemDAO>();
            foreach (var cartItem in request)
            {
                var branchMenuDAO = await branchMenu.Get(decimal.Parse(cartItem.MenuId));
                model.Add(new DeliveryandTakeawayCartItemDAO()
                {
                    MenuId = String.IsNullOrEmpty(cartItem.MenuId) ? 0 : decimal.Parse(cartItem.MenuId),
                    Quantity = cartItem.Quantity,
                    ItemId = 0,
                    MenuName = branchMenuDAO.Name,
                    Price = branchMenuDAO.Cost
                });
            }
            return model;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private DeliveryAddressDAO MapObject2Model(DeliveryModel request)
        {
            return new DeliveryAddressDAO()
            {
                AddressLine1 = request.AddressLine1,
                AddressLine2 = request.AddressLine2 ?? string.Empty,
                City = request.City,
                Id = 0,
                Landmark = request.LandMark,
                Latitude = request.Latitude,
                Longitude = request.Longitude,
                State = request.State,
                Zip = String.IsNullOrEmpty(request.Zip) ? 0 : decimal.Parse(request.Zip)
            };
        }

        #endregion
    }
}
