﻿using GotTable.API.Model.RestaurantCategories;
using GotTable.Common;
using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/restaurantcategorylist/")]
    public sealed class RestaurantCategoryListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public RestaurantCategoryListController(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var model = MapListToModel(Common.List.RestaurantCategoryList.Get());
                return new OkObjectResult(model);
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                return new BadRequestObjectResult(exception.Message);
            }
        }

        #region Helper method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        internal List<RestaurantCategoryModel> MapListToModel(List<EnumDto> list)
        {
            var returnlist = new List<RestaurantCategoryModel>();
            foreach (var item in list)
            {
                returnlist.Add(new RestaurantCategoryModel()
                {
                    Id = item.Id,
                    Name = item.Name.Replace("_", " ")
                });
            }
            return returnlist;
        }

        #endregion
    }
}
