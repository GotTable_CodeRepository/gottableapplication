﻿using GotTable.DalEF.Localities;
using GotTable.Library.Localities;
using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/localtylist/")]
    [Serializable]
    public sealed class LocaltyListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        public readonly ILocaltyList localtyList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="localtyList"></param>
        /// <param name="operationExceptionLog"></param>
        public LocaltyListController(ILocaltyList localtyList, IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.localtyList = localtyList;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="active"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(bool? active = null, int? cityId = null)
        {
            var model = new List<LocaltyModel>();
            var localtyDAOs = await localtyList.Get(active);
            localtyDAOs.ToList().ForEach(item =>
                model.Add(new LocaltyModel()
                {
                    Id = int.Parse(item.Id.ToString()),
                    Name = item.Name
                }));
            return new OkObjectResult(model);
        }
    }
}
