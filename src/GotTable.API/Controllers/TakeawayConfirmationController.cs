﻿using GotTable.API.Model;
using GotTable.API.Model.RestaurantBookings;
using GotTable.API.Model.RestaurantBookings.DeliveryandTakeaway;
using GotTable.Common.Enumerations;
using GotTable.DAO.Bookings.DeliveryandTakeaway;
using GotTable.DAO.Users;
using GotTable.Library.Communications.Message.Type2Factors;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Library.RestaurantMenus;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/takeawayconfirmation/")]
    public sealed class TakeawayConfirmationController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDeliveryandTakeaway deliveryandTakeaway;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchMenu branchMenu;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deliveryandTakeaway"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="branchMenu"></param>
        /// <param name="oTPCommand"></param>
        public TakeawayConfirmationController(IDeliveryandTakeaway deliveryandTakeaway, IOperationExceptionLog operationExceptionLog, IBranchMenu branchMenu, IOTPCommand oTPCommand)
        {
            this.deliveryandTakeaway = deliveryandTakeaway;
            this.operationExceptionLog = operationExceptionLog;
            this.branchMenu = branchMenu;
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(Request<DeliveryandTakeawayBookingModel> request)
        {
            var model = new BookingResponse
            {
                BookingId = "0",
                BookingType = Enumeration.BookingType.Takeaway.ToString(),
                ConfirmationMessage = string.Empty,
                ErrorMessage = string.Empty
            };

            if (string.IsNullOrEmpty(request.Detail.TransactionId))
            {
                model.ErrorMessage = "Invalid request, missing transactionId";
                return new BadRequestObjectResult(model);
            }
            if (string.IsNullOrEmpty(request.Detail.OtpNumber))
            {
                model.ErrorMessage = "Invalid request, missing OTP number";
                return new BadRequestObjectResult(model);
            }

            try
            {
                var otpDetail = await oTPCommand.Get(Convert.ToDecimal(request.Detail.TransactionId));
                if (otpDetail == null)
                {
                    model.ErrorMessage = "Invalid OTP";
                    return new BadRequestObjectResult(model);
                }
                if (otpDetail.TransactionId == 0)
                {
                    model.ErrorMessage = "Invalid OTP";
                    return new BadRequestObjectResult(model);
                }
                if (otpDetail.IsUsed)
                {
                    model.ErrorMessage = "This OTP is already used";
                    return new BadRequestObjectResult(model);
                }
                if (otpDetail.OTP != request.Detail.OtpNumber)
                {
                    model.ErrorMessage = "Invalid OTP";
                    return new BadRequestObjectResult(model);
                }

                var dao = new DeliveryandTakeawayDAO()
                {
                    BookingDate = DateTime.ParseExact(request.Detail.BookingDate, "MM-dd-yyyy", null),
                    BookingTime = request.Detail.BookingTime,
                    BookingId = request.Detail.BookingId == "" ? 0 : decimal.Parse(request.Detail.BookingId),
                    BookingTypeId = (int)Enumeration.BookingType.Takeaway,
                    BranchId = request.Detail.RestaurantId == "" ? 0 : decimal.Parse(request.Detail.RestaurantId),
                    OfferId = request.Detail.OfferId == "" ? 0 : decimal.Parse(request.Detail.OfferId),
                    PhoneNumber = request.Detail.PhoneNumber,
                    StateGST = String.IsNullOrEmpty(request.Detail.StateGst) ? 0 : decimal.Parse(request.Detail.StateGst),
                    CentralGST = String.IsNullOrEmpty(request.Detail.CentralGst) ? 0 : decimal.Parse(request.Detail.CentralGst),
                    UserId = String.IsNullOrEmpty(request.Detail.UserId) ? 0 : decimal.Parse(request.Detail.UserId),
                    CartItem = await MapObject2Model(request.Detail.CartItem),
                    PromoCode = request.Detail.PromoCode ?? string.Empty,
                    UserDetail = new UserDetailDAO()
                    {
                        FirstName = request.Detail.FirstName,
                        LastName = request.Detail.LastName,
                        EmailAddress = request.Detail.EmailAddress,
                        Gender = Enumeration.Gender.NotDefined.ToString(),
                        Prefix = Enumeration.Prefix.NotDefined.ToString(),
                        UserType = Enumeration.UserType.Guest.ToString(),
                        IsActive = true
                    }
                };

                await deliveryandTakeaway.New(dao);

                if (!dao.Status)
                {
                    model.ErrorMessage = dao.ErrorMessage;
                    return new BadRequestObjectResult(model);
                }
                model.BookingId = dao.BookingId.ToString("G29");
                model.ConfirmationMessage = dao.ConfirmationMessage;
                otpDetail.IsUsed = true;
                await oTPCommand.Save(otpDetail);
                return new OkObjectResult(model);
            }
            catch (Exception ee)
            {
                model.ErrorMessage = ee.Message.ToString();
                await operationExceptionLog.New(ee);
                return new BadRequestObjectResult(model);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private async Task<List<DeliveryandTakeawayCartItemDAO>> MapObject2Model(List<CartModel> request)
        {
            var model = new List<DeliveryandTakeawayCartItemDAO>();
            foreach (var cartItem in request)
            {
                var menuItem = await branchMenu.Get(decimal.Parse(cartItem.MenuId));
                model.Add(new DeliveryandTakeawayCartItemDAO()
                {
                    MenuId = string.IsNullOrEmpty(cartItem.MenuId) ? 0 : decimal.Parse(cartItem.MenuId),
                    Quantity = cartItem.Quantity,
                    ItemId = 0,
                    MenuName = menuItem.Name,
                    Price = menuItem.Cost
                });
            }
            return model;
        }
    }
}
