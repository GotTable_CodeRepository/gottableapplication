﻿using GotTable.API.Model.UserPasswords;
using GotTable.Library.Users;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/userpassword/")]
    public sealed class UserPasswordController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        public UserPasswordController(IUser user)
        {
            this.user = user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(UserPasswordModel request)
        {
            if (string.IsNullOrEmpty(request.UserId))
            {
                return new BadRequestObjectResult(new { message = "Missing userid" });
            }
            if (string.IsNullOrEmpty(request.OldPassword) || string.IsNullOrEmpty(request.NewPassword))
            {
                return new BadRequestObjectResult(new { message = "Missing password" });
            }
            if (request.NewPassword.Length <= 5)
            {
                return new BadRequestObjectResult(new { message = "Password should be of 6 characters atleast" });
            }
            var userDAO = await user.Get(decimal.Parse(request.UserId));
            if (userDAO != null && userDAO.UserId == 0)
            {
                return new BadRequestObjectResult(new { message = "User does not exists." });
            }
            if (userDAO.UserType.ToLower() != "enduser")
            {
                return new BadRequestObjectResult(new { message = "User does not exists." });
            }
            if (userDAO.Password != request.OldPassword)
            {
                return new BadRequestObjectResult(new { message = "Old password and new password are not same." });
            }
            userDAO.Password = request.NewPassword;
            await user.Save(userDAO);
            return new OkObjectResult(new { message = "Password changed succesfully" });
        }
    }
}
