﻿using GotTable.API.Model.OTPTransactions;
using GotTable.DAO.Communications.Message;
using GotTable.Library.Communications.Message.Type2Factors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [AllowAnonymous]
    [Route("api/otptransaction/")]
    public sealed class OTPTransactionController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="oTPCommand"></param>
        public OTPTransactionController(IOTPCommand oTPCommand)
        {
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(RequestOTPModel request)
        {
            if (String.IsNullOrEmpty(request.ExternalId))
            {
                return BadRequest(new { message = "missing ExternalId" });
            }
            if (request.PhoneNumber == 0)
            {
                return BadRequest(new { message = "missing phone number" });
            }
            if (request.Type == "")
            {
                return BadRequest(new { message = "missing type" });
            }
            var otpDetail = new OTPDetailDAO()
            {
                TransactionId = 0,
                IsUsed = false,
                OTP = string.Empty,
                PhoneNumber = request.PhoneNumber,
                Type = request.Type,
                ExternalId = String.IsNullOrEmpty(request.ExternalId) ? 0 : decimal.Parse(request.ExternalId),
                CreatedDate = DateTime.Now
            };
            await oTPCommand.New(otpDetail);
            if (otpDetail.TransactionId > 0)
            {
                return new OkObjectResult(new { transactionid = otpDetail.TransactionId.ToString("G29") });
            }
            else
            {
                return new BadRequestObjectResult(new { message = "Issue" });
            }
        }
    }
}
