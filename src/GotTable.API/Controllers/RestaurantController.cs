﻿using GotTable.API.Model.Cuisines.V1;
using GotTable.API.Model.Images;
using GotTable.API.Model.RestaurantAmenities;
using GotTable.API.Model.RestaurantContacts;
using GotTable.API.Model.RestaurantReviews;
using GotTable.API.Model.Restaurants;
using GotTable.API.Model.RestaurantTimings;
using GotTable.API.Model.Resturants;
using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantAmenities;
using GotTable.Library.RestaurantConfigurations;
using GotTable.Library.RestaurantContacts;
using GotTable.Library.RestaurantCuisines;
using GotTable.Library.RestaurantDocuments;
using GotTable.Library.RestaurantRatings;
using GotTable.Library.Restaurants;
using GotTable.Library.RestaurantTimings;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class RestaurantController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IRatingList ratingList;

        /// <summary>
        /// 
        /// </summary>
        private readonly ITimingList timingList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IContactList contactList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchConfiguration branchConfiguration;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        /// <summary>
        /// 
        /// </summary>
        private readonly IAmenityList amenityList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IApplicationRestaurantList applicationRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="ratingList"></param>
        /// <param name="timingList"></param>
        /// <param name="branchConfiguration"></param>
        /// <param name="cuisineList"></param>
        /// <param name="document"></param>
        /// <param name="contactList"></param>
        /// <param name="applicationRestaurantList"></param>
        /// <param name="amenityList"></param>
        public RestaurantController(IOperationExceptionLog operationExceptionLog, IRatingList ratingList, ITimingList timingList, IBranchConfiguration branchConfiguration, ICuisineList cuisineList, IDocument document, IContactList contactList, IApplicationRestaurantList applicationRestaurantList, IAmenityList amenityList)
        {
            this.branchConfiguration = branchConfiguration;
            this.ratingList = ratingList;
            this.timingList = timingList;
            this.operationExceptionLog = operationExceptionLog;
            this.cuisineList = cuisineList;
            this.document = document;
            this.contactList = contactList;
            this.applicationRestaurantList = applicationRestaurantList;
            this.amenityList = amenityList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="deviceType"></param>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        [Route("api/restaurant/get")]
        [HttpGet]
        public async Task<IActionResult> Get(Enumeration.RestaurantTypes restaurantType, string deviceId = default, string deviceType = default, decimal restaurantId = default, double latitude = default, double longitude = default)
        {
            var restaurantModel = new ResturantDetailResponse<ResturantDetailModel>
            {
                ResturantDetail = new ResturantDetailModel()
            };
            try
            {
                if (restaurantId == default)
                {
                    restaurantModel.ErrorMessage = "Missing restaurantId, Invalid request";
                    return new BadRequestObjectResult(restaurantModel);
                }

                if (restaurantType == default)
                {
                    restaurantModel.ErrorMessage = "Missing resturant type, Invalid request";
                    return new BadRequestObjectResult(restaurantModel);
                }

                if (latitude == default)
                {
                    restaurantModel.ErrorMessage = "Missing latitude, Invalid request";
                    return new BadRequestObjectResult(restaurantModel);
                }

                if (longitude == default)
                {
                    restaurantModel.ErrorMessage = "Missing longitude, Invalid request";
                    return new BadRequestObjectResult(restaurantModel);
                }

                var resuturantList = await applicationRestaurantList.Get(latitude, longitude, restaurantId, restaurantType);

                if (resuturantList != null)
                {
                    restaurantModel.ErrorMessage = string.Empty;
                    foreach (var item in resuturantList)
                    {
                        restaurantModel.ResturantDetail.Id = item.Id.ToString("G29");
                        restaurantModel.ResturantDetail.AddressLine1 = item.AddressLine1;
                        restaurantModel.ResturantDetail.AddressLine2 = item.AddressLine2;
                        restaurantModel.ResturantDetail.City = item.City;
                        restaurantModel.ResturantDetail.Latitude = item.Latitude;
                        restaurantModel.ResturantDetail.Longitude = item.Longitude;
                        restaurantModel.ResturantDetail.Name = item.Name;
                        restaurantModel.ResturantDetail.State = item.State;
                        restaurantModel.ResturantDetail.Zip = item.ZipCode.ToString("G29");
                        restaurantModel.ResturantDetail.Rating = item.AvgRating;
                        restaurantModel.ResturantDetail.AmbienceRating = item.AmbienceRating;
                        restaurantModel.ResturantDetail.FoodRating = item.FoodRating;
                        restaurantModel.ResturantDetail.ServiceRating = item.ServiceRating;
                        restaurantModel.ResturantDetail.PriceRating = item.PriceRating;
                        restaurantModel.ResturantDetail.MusicRating = item.MusicRating;
                        restaurantModel.ResturantDetail.Distance = item.Distance.ToString("G29");
                        restaurantModel.ResturantDetail.IsOfferAvailable = item.IsOfferAvailable;
                        restaurantModel.ResturantDetail.RestaurantTag = item.RestaurantTag;
                        restaurantModel.ResturantDetail.SEODescription = item.SEODescription;
                        restaurantModel.ResturantDetail.SEOKeyword = item.SEOKeyword;
                        restaurantModel.ResturantDetail.SEOTitle = item.SEOTitle;
                        restaurantModel.ResturantDetail.Description = item.Description;
                        restaurantModel.ResturantDetail.CostForTwo = item.CostForTwo;
                        restaurantModel.ResturantDetail.Tag = item.SelectedTag.ToString();
                         
                        var configuration = await branchConfiguration.Get(item.Id);
                        if(configuration != null)
                        {
                            if (restaurantType == Enumeration.RestaurantTypes.Delivery)
                            {
                                restaurantModel.ResturantDetail.CentralGST = configuration.CentralGST.Value.ToString("G29");
                                restaurantModel.ResturantDetail.StateGST = configuration.StateGST.Value.ToString("G29");
                                restaurantModel.ResturantDetail.DeliveryCharges = configuration.DeliveryCharges.Value.ToString("G29");
                                restaurantModel.ResturantDetail.StandByTime = configuration.DeliveryStandBy;
                            }
                            else if (restaurantType == Enumeration.RestaurantTypes.Takeaway)
                            {
                                restaurantModel.ResturantDetail.CentralGST = configuration.CentralGST.Value.ToString("G29");
                                restaurantModel.ResturantDetail.StateGST = configuration.StateGST.Value.ToString("G29");
                                restaurantModel.ResturantDetail.StandByTime = configuration.TakeawayStandBy;
                                restaurantModel.ResturantDetail.DeliveryCharges = string.Empty;
                            }
                            else
                            {
                                restaurantModel.ResturantDetail.CentralGST = string.Empty;
                                restaurantModel.ResturantDetail.StateGST = string.Empty;
                                restaurantModel.ResturantDetail.DeliveryCharges = string.Empty;
                                restaurantModel.ResturantDetail.StandByTime = string.Empty;
                            }
                        }

                        var cuisines = await cuisineList.Get(item.Id, activeCuisines: true);
                        restaurantModel.ResturantDetail.Cuisines = cuisines.Select(m => new CuisineModel()
                        {
                            Id = m.Id.ToString("G29"),
                            Name = m.SelectedCuisine
                        }).ToList();

                        var contacts = await contactList.Get(item.Id);
                        restaurantModel.ResturantDetail.Contacts = contacts.OrderBy(x => x.SelectedTypeId).Select(m => new ContactModel()
                        {
                            EmailAddress = m.EmailAddress,
                            Id = m.Id.ToString("G29"),
                            Name = m.Name,
                            PhoneNumber = Convert.ToInt64(m.PhoneNumber)
                        }).ToList();

                        var ratingDAO = await ratingList.Get(restaurantId);
                        restaurantModel.ResturantDetail.Reviews = ratingDAO.Select(ratingItem => new ReviewModel(
                            userId: ratingItem.UserId.ToString("G29"),
                            userName: ratingItem.UserName,
                            userEmailAddress: ratingItem.EmailAddress,
                            priceRating: ratingItem.PriceRating ?? 0,
                            musicRating: ratingItem.MusicRating ?? 0,
                            serviceRating: ratingItem.ServiceRating ?? 0,
                            foodRating: ratingItem.FoodRating ?? 0,
                            ambienceRating: ratingItem.AmbienceRating ?? 0,
                            rating: ratingItem.Rating ?? 0,
                            title: ratingItem.Title,
                            comment: ratingItem.Comment,
                            createdDate : ratingItem.CreationDate.Value.ToString("dd-MMM-yyyy hh:mm")
                        )).ToList();

                        var timingDAO = await timingList.Get(item.Id, Enumeration.RestaurantTypes.DineIn == restaurantType, Enumeration.RestaurantTypes.Delivery == restaurantType, Enumeration.RestaurantTypes.Takeaway == restaurantType);
                        restaurantModel.ResturantDetail.Timings = timingDAO.DefaultIfEmpty().FirstOrDefault().DayTiming.Select(m => new TimingModel()
                        {
                            DinnerEndTime = m.DinnerEndTimeIn24HourFormat,
                            DinnerStartTime = m.DinnerStartTimeIn24HourFormat,
                            Name = m.SelectedDay.ToString(),
                            IsClosed = m.IsClosed,
                            LunchStartTime = m.LunchStartTimeIn24HourFormat,
                            LunchEndTime = m.LunchEndTimeIn24HourFormat
                        }).ToList();

                        var amenities = await amenityList.Get(item.Id);
                        restaurantModel.ResturantDetail.Amenities = amenities.Select(amenity => new AmenityModel()
                        {
                            AmenityId = amenity.AmenityId,
                            Name = amenity.AmenityName
                        }).ToList();

                        restaurantModel.ResturantDetail.Images = await MapObjectToModel(item.Id, false);
                    }
                }
                return new OkObjectResult(restaurantModel);
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                restaurantModel.ErrorMessage = exception.Message;
                return new BadRequestObjectResult(restaurantModel);
            }
        }

        #region Helper method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        internal async Task<List<ImageModel>> MapObjectToModel(decimal restaurantId, bool listType)
        {
            var imageModels = new List<ImageModel>();
            var restaurantImages = await document.Get(restaurantId);
            if (listType)
            {
                if (restaurantImages.FrontImage != null && restaurantImages.FrontImage.FilePath != null)
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "FrontLogo",
                        Url = restaurantImages.FrontImage.FilePath
                    });
                }
                else
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "FrontLogo",
                        Url = "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg"
                    });
                }
            }
            else
            {
                if (restaurantImages.BackImage != null && restaurantImages.BackImage.FilePath != null)
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "InsideLogo",
                        Url = restaurantImages.BackImage.FilePath
                    });
                }
                else
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "InsideLogo",
                        Url = "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg"
                    });
                }
            }
            return imageModels;
        }

        #endregion
    }
}
