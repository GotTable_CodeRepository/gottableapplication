﻿using GotTable.DAO.RestaurantAmenities;
using GotTable.Library.RestaurantAmenities;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// AmenityEdit
    /// </summary>
    [Route("api/amenityedit/")]
    public sealed class AmenityEditController : BaseAPIController
    {
        /// <summary>
        /// IAmenity
        /// </summary>
        private readonly IAmenity amenity;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amenity"></param>
        public AmenityEditController(IAmenity amenity)
        {
            this.amenity = amenity;
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="amenityId"></param>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task Save(int amenityId = default,  decimal restaurantId = default)
        {
            var amenityDAO = new AmenityDAO(amenityId: amenityId, branchId: restaurantId);
            await amenity.New(amenityDAO);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="amenityId"></param>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task Delete(decimal amenityId, decimal restaurantId)
        {
            await amenity.Delete(amenityId, restaurantId);
        }

    }
}
