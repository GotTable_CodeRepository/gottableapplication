﻿using GotTable.API.Model.Cuisines;
using GotTable.API.Model.Cuisines.V1;
using GotTable.Library.Cuisines;
using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/cuisines/")]
    public sealed class CuisinesController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cuisineList"></param>
        /// <param name="operationExceptionLog"></param>
        public CuisinesController(ICuisineList cuisineList, IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.cuisineList = cuisineList;
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="enableTreeView"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(bool enableTreeView = true, int currentPage = 0, int pageSize = 10)
        {
            var cuisineDAOs = await cuisineList.Get(currentPage, pageSize);
            if (!enableTreeView)
            {
                var model = new CuisinesListResponse<List<CuisineModel>>
                {
                    ErrorMessage = "",
                    CuisinesList = new List<CuisineModel>()
                };
                cuisineDAOs.ForEach(item => model.CuisinesList.Add(new CuisineModel()
                {
                    Id = item.Id.ToString("G29"),
                    Name = item.CuisineName
                }));
                return new OkObjectResult(model);
            }
            else
            {
                var model = new CuisinesListResponse<List<CategoryModel>>
                {
                    ErrorMessage = "",
                    CuisinesList = new List<CategoryModel>()
                };
                if (cuisineDAOs != null)
                {
                    var cuisine = cuisineDAOs.Where(x => x.CuisineName == "North Indian").FirstOrDefault();
                    if (cuisine != null)
                    {
                        model.CuisinesList.Add(new CategoryModel()
                        {
                            CuisineList = new List<CuisineModel>(),
                            Id = cuisine.Id.ToString("G29"),
                            Name = cuisine.CuisineName
                        });
                    }

                    cuisine = cuisineDAOs.Where(x => x.CuisineName == "South Indian").FirstOrDefault();
                    if (cuisine != null)
                    {
                        model.CuisinesList.Add(new CategoryModel()
                        {
                            CuisineList = new List<CuisineModel>(),
                            Id = cuisine.Id.ToString("G29"),
                            Name = cuisine.CuisineName
                        });
                    }

                    var tempCategory = new CategoryModel
                    {
                        Id = "0",
                        Name = "Regional"
                    };
                    foreach (var item in cuisineDAOs.Where(x => x.CategoryId == 1).ToList().OrderBy(x => x.CuisineName))
                    {
                        if (item.CuisineName != "South Indian" && item.CuisineName != "North Indian")
                        {
                            tempCategory.CuisineList.Add(new CuisineModel()
                            {
                                Id = item.Id.ToString("G29"),
                                Name = item.CuisineName
                            });
                        }
                    }

                    model.CuisinesList.Add(tempCategory);

                    tempCategory = new CategoryModel
                    {
                        Id = "0",
                        Name = "International"
                    };
                    foreach (var item in cuisineDAOs.Where(x => x.CategoryId == 2).ToList().OrderBy(x => x.CuisineName))
                    {
                        tempCategory.CuisineList.Add(new CuisineModel()
                        {
                            Id = item.Id.ToString("G29"),
                            Name = item.CuisineName
                        });
                    }

                    model.CuisinesList.Add(tempCategory);
                }
                return new OkObjectResult(model);
            }
        }
    }
}
