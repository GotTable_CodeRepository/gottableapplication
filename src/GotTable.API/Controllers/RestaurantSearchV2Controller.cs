﻿using GotTable.API.Model.Images;
using GotTable.API.Model.Restaurants;
using GotTable.API.Model.Resturants;
using GotTable.Common.Enumerations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantDocuments;
using GotTable.Library.Restaurants;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/restaurant/V2")]
    public class RestaurantSearchV2Controller : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        /// <summary>
        /// 
        /// </summary>
        private readonly IApplicationRestaurantList applicationRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="document"></param>
        /// <param name="applicationRestaurantList"></param>
        public RestaurantSearchV2Controller(IOperationExceptionLog operationExceptionLog, IDocument document, IApplicationRestaurantList applicationRestaurantList)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.document = document;
            this.applicationRestaurantList = applicationRestaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cuisineids"></param>
        /// <param name="offerCategory"></param>
        /// <param name="deviceid"></param>
        /// <param name="devicetype"></param>
        /// <param name="restaurantType"></param>
        /// <param name="restaurantCategoryId"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="todaysoffer"></param>
        /// <param name="restaurantName"></param>
        /// <param name="specialoffer"></param>
        /// <param name="areacoverage"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="localtyId"></param>
        /// <returns></returns>
        [Route("GetSearch/")]
        [HttpGet]
        public async Task<IActionResult> GetSearch(Enumeration.RestaurantTypes restaurantType, [FromRouteAttribute] List<decimal> cuisineids = null, [FromRouteAttribute] List<int> offerCategory = null, string deviceid = default, string devicetype = default, int? restaurantCategoryId = null, double latitude = default, double longitude = default, string todaysoffer = default, string restaurantName = default, string specialoffer = default, int areacoverage = 0, int currentPage = 0, int pageSize = 10, [FromRouteAttribute] List<decimal> localtyId = null)
        {
            var model = new ResturantListResponse<List<RestaurantModel>>
            {
                RestaurantList = new List<RestaurantModel>(),
                ErrorMessage = string.Empty
            };
            try
            {
                if (latitude == default)
                {
                    model.ErrorMessage = "Missing latitude";
                    return new BadRequestObjectResult(model);
                }
                if (longitude == default)
                {
                    model.ErrorMessage = "Missing longitude";
                    return new BadRequestObjectResult(model);
                }

                var offerType = specialoffer == default && todaysoffer == default ? null : todaysoffer == "yes" ? Enumeration.OfferType.Todays : specialoffer == "yes" ? Enumeration.OfferType.Special : (Enumeration.OfferType?)null;

                var applicationRestaurants = await applicationRestaurantList.Get(latitude, longitude, restaurantType, cuisineids, areacoverage, offerType, restaurantName, currentPage, pageSize, restaurantCategoryId, offerCategory, localtyId);
                applicationRestaurants.ForEach(async item => model.RestaurantList.Add(new RestaurantModel()
                {
                    Distance = item.Distance.ToString("G29"),
                    Id = item.Id.ToString("G29"),
                    Latitude = item.Latitude,
                    Longitude = item.Longitude,
                    AddressLine1 = item.AddressLine1,
                    AddressLine2 = item.AddressLine2,
                    City = item.City,
                    State = item.State,
                    Zip = item.ZipCode.ToString("G29"),
                    Name = item.Name,
                    Rating = item.AvgRating,
                    CuisineNames = item.CuisineNames,
                    OfferName = item.OfferName,
                    Tag = item.SelectedTag.ToString(),
                    IsOfferAvailable = item.IsOfferAvailable,
                    Images = await MapObjectToModel(item.Id, true),
                    RestaurantTag = item.RestaurantTag,
                    ActiveOffers = item.ActiveOffers,
                    AmbienceRating = item.AmbienceRating,
                    CostForTwo = item.CostForTwo,
                    FoodRating = item.FoodRating,
                    MusicRating = item.MusicRating,
                    PriceRating = item.PriceRating,
                    ServiceRating = item.ServiceRating,
                    Description = item.Description
                }));
                model.TotalRestaurants = applicationRestaurants.Count;
                return new OkObjectResult(model);
            }
            catch (Exception exceptionLog)
            {
                await operationExceptionLog.New(exceptionLog);
                model.ErrorMessage = exceptionLog.Message;
                return new BadRequestObjectResult(model);
            }
        }

        #region Helper method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="listType"></param>
        /// <returns></returns>
        private async Task<List<ImageModel>> MapObjectToModel(decimal restaurantId, bool listType)
        {
            var imageModels = new List<ImageModel>();
            var restaurantImages = await document.Get(restaurantId);
            if (listType)
            {
                if (restaurantImages.FrontImage != null && restaurantImages.FrontImage.FilePath != null)
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "FrontLogo",
                        Url = restaurantImages.FrontImage.FilePath
                    });
                }
                else
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "FrontLogo",
                        Url = "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg"
                    });
                }
            }
            else
            {
                if (restaurantImages.BackImage != null && restaurantImages.BackImage.FilePath != null)
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "InsideLogo",
                        Url = restaurantImages.BackImage.FilePath
                    });
                }
                else
                {
                    imageModels.Add(new ImageModel()
                    {
                        Name = "InsideLogo",
                        Url = "https://mk0tainsights9mcv7wv.kinstacdn.com/wp-content/uploads/2018/01/premiumforrestaurants_0.jpg"
                    });
                }
            }
            return imageModels;
        }

        #endregion
    }
}
