﻿using GotTable.API.Model;
using GotTable.API.Model.RestaurantBookings;
using GotTable.API.Model.RestaurantBookings.DineIn;
using GotTable.Common.Enumerations;
using GotTable.DAO.Bookings.DineIn;
using GotTable.DAO.Communications.Message;
using GotTable.DAO.Users;
using GotTable.Library.Communications.Message.Type2Factors;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/dineinconfirmation/")]
    public sealed class DineInConfirmationController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDineIn dineIn;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineIn"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="oTPCommand"></param>
        public DineInConfirmationController(IDineIn dineIn, IOperationExceptionLog operationExceptionLog, IOTPCommand oTPCommand)
        {
            this.dineIn = dineIn;
            this.operationExceptionLog = operationExceptionLog;
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(Request<DineInBookingModel> request)
        {
            var bookingModel = new BookingResponse();
            if (!string.IsNullOrEmpty(request.Detail.TransactionId) && string.IsNullOrEmpty(request.Detail.OtpNumber))
            {
                bookingModel.ErrorMessage = "Invalid request, Please check.!";
                return new BadRequestObjectResult(bookingModel);
            }
            if (string.IsNullOrEmpty(request.Detail.TransactionId) && !string.IsNullOrEmpty(request.Detail.OtpNumber))
            {
                bookingModel.ErrorMessage = "Invalid request, Please check.!";
                return new BadRequestObjectResult(bookingModel);
            }
            try
            {
                var otpDetail = new OTPDetailDAO();

                if (!string.IsNullOrEmpty(request.Detail.TransactionId))
                {
                    otpDetail = await oTPCommand.Get(Convert.ToDecimal(request.Detail.TransactionId));
                    if (otpDetail == null)
                    {
                        bookingModel.ErrorMessage = "Invalid OTP";
                        return new BadRequestObjectResult(bookingModel);
                    }
                    if (otpDetail.TransactionId == 0)
                    {
                        bookingModel.ErrorMessage = "Invalid OTP";
                        return new BadRequestObjectResult(bookingModel);
                    }
                    if (otpDetail.IsUsed)
                    {
                        bookingModel.ErrorMessage = "This OTP is already used";
                        return new BadRequestObjectResult(bookingModel);
                    }
                    if (otpDetail.OTP != request.Detail.OtpNumber)
                    {
                        bookingModel.ErrorMessage = "Invalid OTP";
                        return new BadRequestObjectResult(bookingModel);
                    }
                }

                var bookingDAO = new DineInBookingDAO()
                {
                    DeviceId = request.DeviceId,
                    DeviceType = request.DeviceType,
                    Id = String.IsNullOrEmpty(request.Detail.BookingId) ? 0 : decimal.Parse(request.Detail.BookingId),
                    BookingDate = DateTime.ParseExact(request.Detail.BookingDate, "MM-dd-yyyy", null),
                    BookingTime = request.Detail.BookingTime,
                    BranchId = String.IsNullOrEmpty(request.Detail.BranchId) ? 0 : decimal.Parse(request.Detail.BranchId),
                    Comment = request.Detail.Comment,
                    CreatedDate = DateTime.Now,
                    TableId = String.IsNullOrEmpty(request.Detail.TableId) ? 0 : decimal.Parse(request.Detail.TableId),
                    UserId = String.IsNullOrEmpty(request.Detail.UserId) ? 0 : decimal.Parse(request.Detail.UserId),
                    UserDetail = new UserDetailDAO()
                    {
                        FirstName = request.Detail.FirstName,
                        LastName = request.Detail.LastName,
                        EmailAddress = request.Detail.EmailAddress,
                        IsActive = true,
                        IsEmailOptedForCommunication = request.Detail.IsEmailOptedForCommunication,
                        IsEmailVerified = false
                    },
                    PhoneNumber = request.Detail.PhoneNumber,
                    OfferId = String.IsNullOrEmpty(request.Detail.OfferId) ? 0 : decimal.Parse(request.Detail.OfferId),
                    PromoCode = request.Detail.PromoCode
                };

                await dineIn.New(bookingDAO);

                bookingModel.ErrorMessage = bookingDAO.ErrorMessage;
                bookingModel.ConfirmationMessage = bookingDAO.ConfirmationMessage;
                bookingModel.BookingId = bookingDAO.Id.ToString("G29");
                bookingModel.BookingType = Enumeration.BookingType.DineIn.ToString();

                if (otpDetail != null && !string.IsNullOrEmpty(request.Detail.TransactionId))
                {
                    otpDetail.IsUsed = true;
                    await oTPCommand.Save(otpDetail);
                }
                return new OkObjectResult(bookingModel);
            }
            catch (Exception ee)
            {
                bookingModel.ErrorMessage = ee.Message.ToString();
                await operationExceptionLog.New(ee);
                return new OkObjectResult(bookingModel);
            }
        }
    }
}
