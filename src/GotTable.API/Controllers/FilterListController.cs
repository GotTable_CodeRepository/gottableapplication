﻿using GotTable.Library.Restaurants.Filters;
using GotTable.Library.OperationExceptionLogs;
using System;
using System.Threading.Tasks;
using GotTable.API.Model.Restaurants;
using System.Collections.Generic;
using GotTable.DAO.Restaurants;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/filterlist/")]
    public sealed class FilterListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IFilterList filterList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterList"></param>
        /// <param name="operationExceptionLog"></param>
        public FilterListController(IFilterList filterList, IOperationExceptionLog operationExceptionLog)
        {
            this.filterList = filterList;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("api/filter/V1/Get")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var model = await filterList.Get();
                return new OkObjectResult(MapObjectToModel(model));
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                return new BadRequestObjectResult(exception);
            }
        }

        #region Helpers method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dAOs"></param>
        /// <returns></returns>
        private List<FilterModel> MapObjectToModel(List<FilterDAO> dAOs)
        {
            return dAOs.Select(x => new FilterModel()
            {
                Name = x.Name,
                Index = x.Index,
                Key = x.Key,
                IconUrl = x.IconUrl,
                List = x.List.Select(z => new FilterItemModel<decimal>()
                {
                    Id = z.Id,
                    Name = z.Name
                }).ToList()
            }).ToList();
        }

        #endregion
    }
}
