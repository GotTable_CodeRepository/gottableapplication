﻿using GotTable.API.Model.UserAddresses;
using GotTable.API.Model.UserPhoneNumbers;
using GotTable.API.Model.Users;
using GotTable.DAO.Users;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Users;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/socialmedialogin/")]
    public sealed class SocialMediaLoginController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPhoneNumberList phoneNumberList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="user"></param>
        /// <param name="phoneNumberList"></param>
        public SocialMediaLoginController(IOperationExceptionLog operationExceptionLog, IUser user, IPhoneNumberList phoneNumberList)
        {
            this.user = user;
            this.operationExceptionLog = operationExceptionLog;
            this.phoneNumberList = phoneNumberList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(UserModel request)
        {
            var model = new UserResponse<UserDetailModel>();

            if (request.EmailAddress == "")
            {
                model.ErrorMessage = "Please provide email address.";
                return new BadRequestObjectResult(model);
            }
            if (request.FirstName == "")
            {
                model.ErrorMessage = "Please provide firstname.";
                return new BadRequestObjectResult(model);
            }
            if (request.LastName == "")
            {
                model.ErrorMessage = "Please provide lastname.";
                return new BadRequestObjectResult(model);
            }

            var userDAO = await user.Get((string)request.EmailAddress);

            if (userDAO != null && userDAO.UserId == 0)
            {
                userDAO = new UserDetailDAO()
                {
                    FirstName = request.FirstName,
                    UserType = "EndUser",
                    Password = Guid.NewGuid().ToString().Substring(1, 10),
                    LastName = request.LastName,
                    Gender = "NotDefined",
                    Prefix = "NotDefined",
                    IsActive = true,
                    UserId = decimal.Parse(request.UserId),
                    EmailAddress = request.EmailAddress
                };

                if (request.UserId == "0")
                {
                    await user.New(userDAO);
                }
            }

            model.ErrorMessage = string.Empty;
            model.UserDetail = new UserDetailModel()
            {
                FirstName = userDAO.FirstName,
                UserId = userDAO.UserId.ToString("G29"),
                Gender = userDAO.Gender,
                LastName = userDAO.LastName,
                EmailAddress = userDAO.EmailAddress,
                Password = userDAO.Password,
                Prefix = userDAO.Prefix,
                UserType = userDAO.UserType,
                RewardCount = userDAO.RewardCount ?? 0,
                AddressList = new System.Collections.Generic.List<AddressModel>(),
                PhoneNumberList = new System.Collections.Generic.List<PhoneNumberModel>()
            };

            foreach (var phoneItem in await phoneNumberList.Get(userDAO.UserId))
            {
                model.UserDetail.PhoneNumberList.Add(new PhoneNumberModel()
                {
                    NumberId = phoneItem.Id.ToString("G29"),
                    Type = phoneItem.Type,
                    Value = phoneItem.Value.ToString("G29"),
                    UserId = phoneItem.UserId.ToString("G29")
                });
            }
            return new OkObjectResult(model);
        }
    }
}
