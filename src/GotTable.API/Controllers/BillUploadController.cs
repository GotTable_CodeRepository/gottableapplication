﻿using GotTable.API.Model.RestaurantBookings.BillUploads;
using GotTable.DAO.RestaurantBookings.BillUploads;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/billupload/")]
    public sealed class BillUploadController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDineInBookingBillUpload dineInBookingBillUpload;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineInBookingBillUpload"></param>
        /// <param name="operationExceptionLog"></param>
        public BillUploadController(IDineInBookingBillUpload dineInBookingBillUpload, IOperationExceptionLog operationExceptionLog)
        {
            this.dineInBookingBillUpload = dineInBookingBillUpload;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] BillUploadModel model)
        {
            try
            {
                var dineInBookingBillUploadDAO = new DineInBookingBillUploadDAO()
                {
                    Amount = model.Amount,
                    BookingId = model.BookingId,
                    UserId = model.UserId,
                    ImagePath = model.BillPath
                };

                await dineInBookingBillUpload.New(dineInBookingBillUploadDAO);

                if (dineInBookingBillUploadDAO.Status)
                {
                    model.Status = true;
                    model.SuccessMessage = "Bill uploaded successfully.";
                    return new OkObjectResult(model);
                }
                else
                {
                    model.Status = false;
                    model.ErrorMessage = dineInBookingBillUploadDAO.ErrorMessage;
                    return new BadRequestObjectResult(model);
                }
            }
            catch (Exception exception)
            {
                await operationExceptionLog.New(exception);
                model.ErrorMessage = "There is some issue while processing your request, Please try after some time.";
                model.SuccessMessage = string.Empty;
                model.Status = false;
                return new BadRequestObjectResult(model);
            }
        }
    }
}
