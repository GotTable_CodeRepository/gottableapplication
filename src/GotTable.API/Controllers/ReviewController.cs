﻿using GotTable.API.Model;
using GotTable.API.Model.RestaurantReviews;
using GotTable.DAO.RestaurantRatings;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantRatings;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/review/")]
    public sealed class ReviewController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchRating branchRating;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchRating"></param>
        /// <param name="operationExceptionLog"></param>
        public ReviewController(IBranchRating branchRating, IOperationExceptionLog operationExceptionLog)
        {
            this.branchRating = branchRating;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(Request<ReviewModel> request)
        {
            var model = new ReviewResponse();
            var dao = new BranchRatingDAO()
            {
                BranchId = Convert.ToDecimal(request.Detail.BranchId),
                Comment = request.Detail.Comment,
                CreationDate = DateTime.Now,
                Id = Convert.ToDecimal(request.Detail.Id),
                IsActive = true,
                Rating = request.Detail.Rating,
                Title = request.Detail.Title,
                AmbienceRating = request.Detail.AmbienceRating,
                FoodRating = request.Detail.FoodRating,
                MusicRating = request.Detail.MusicRating,
                PriceRating = request.Detail.PriceRating,
                ServiceRating = request.Detail.ServiceRating,
                UserId = Convert.ToDecimal(request.Detail.UserId)
            };
            await branchRating.New(dao);
            if (dao.Status)
            {
                model.ReviewId = dao.StatusId.ToString();
                model.ErrorMessage = string.Empty;
                return new OkObjectResult(model);
            }
            else
            {
                model.ReviewId = string.Empty;
                model.ErrorMessage = dao.ErrorMessage;
                return new BadRequestObjectResult(model);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restuarantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Check(decimal restuarantId, decimal userId)
        {
            if (restuarantId == 0)
            {
                return new BadRequestObjectResult(new { errormessage = "Missing restaurantId" });
            }
            if (userId == 0)
            {
                return new BadRequestObjectResult(new { errormessage = "Missing userId" });
            }
            if (await branchRating.CheckAuthencity(userId, restuarantId))
            {
                return new OkObjectResult(new { message = "true" });
            }
            else
            {
                return new OkObjectResult(new { message = "false" });
            }
        }
    }
}
