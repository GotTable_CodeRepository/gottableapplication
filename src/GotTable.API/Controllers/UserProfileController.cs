﻿using GotTable.API.Model;
using GotTable.API.Model.UserAddresses;
using GotTable.API.Model.UserPhoneNumbers;
using GotTable.API.Model.Users;
using GotTable.DAO.Users;
using GotTable.Library.Communications.Message.Type2Factors;
using GotTable.Library.Users;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/userprofile/")]
    public sealed class UserProfileController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPhoneNumberList phoneNumberList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOTPCommand oTPCommand;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="phoneNumberList"></param>
        /// <param name="oTPCommand"></param>
        public UserProfileController(IUser user, IPhoneNumberList phoneNumberList, IOTPCommand oTPCommand)
        {
            this.user = user;
            this.phoneNumberList = phoneNumberList;
            this.oTPCommand = oTPCommand;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="deviceId"></param>
        /// <param name="devicetype"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(decimal userid, string deviceId = default, string devicetype = default)
        {
            var model = new UserResponse<UserDetailModel>
            {
                UserDetail = new UserDetailModel()
            };
            if (userid == 0)
            {
                model.ErrorMessage = "missing userId";
                model.UserDetail = default;
                return new BadRequestObjectResult(model);
            }
            var userDAO = await user.Get(userid);
            if (userDAO != null && userDAO.UserType != "EndUser")
            {
                model.ErrorMessage = "Invalid user type";
                return new BadRequestObjectResult(model);
            }
            if (!userDAO.IsActive)
            {
                model.ErrorMessage = "Inactive user";
                return new BadRequestObjectResult(model);
            }

            model.ErrorMessage = string.Empty;
            model.UserDetail.Password = userDAO.Password;
            model.UserDetail.UserId = userDAO.UserId.ToString("G29");
            model.UserDetail.UserType = userDAO.UserType;
            model.UserDetail.FirstName = userDAO.FirstName;
            model.UserDetail.Gender = userDAO.Gender;
            model.UserDetail.Prefix = userDAO.Prefix;
            model.UserDetail.LastName = userDAO.LastName;
            model.UserDetail.EmailAddress = userDAO.EmailAddress;
            model.UserDetail.RewardCount = userDAO.RewardCount ?? 0;
            model.UserDetail.PhoneNumberList = new System.Collections.Generic.List<PhoneNumberModel>();
            model.UserDetail.AddressList = new System.Collections.Generic.List<AddressModel>();
            foreach (var phoneItem in await phoneNumberList.Get(userDAO.UserId))
            {
                model.UserDetail.PhoneNumberList.Add(new PhoneNumberModel()
                {
                    UserId = phoneItem.UserId.ToString("G29"),
                    NumberId = phoneItem.Id.ToString("G29"),
                    Type = phoneItem.Type,
                    Value = phoneItem.Value.ToString("G29")
                });
            }
            return new OkObjectResult(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(Request<UserProfileModel> request)
        {
            var model = new UserResponse<UserDetailModel>();
            var requestModel = request.Detail;
            if (string.IsNullOrEmpty(requestModel.TransactionId))
            {
                model.ErrorMessage = "Invalid request";
                return new BadRequestObjectResult(model);
            }
            if (string.IsNullOrEmpty(requestModel.OtpNumber))
            {
                model.ErrorMessage = "Invalid request";
                return new BadRequestObjectResult(model);
            }
            if (string.IsNullOrEmpty(requestModel.EmailAddress))
            {
                model.ErrorMessage = "Please provide email address.";
                return new BadRequestObjectResult(model);
            }
            if (string.IsNullOrEmpty(requestModel.Password))
            {
                model.ErrorMessage = "Please provide password.";
                return new BadRequestObjectResult(model);
            }
            if (string.IsNullOrEmpty(requestModel.PhoneNumber))
            {
                model.ErrorMessage = "Please provide phone number.";
                return new BadRequestObjectResult(model);
            }
            if (string.IsNullOrEmpty(requestModel.FirstName))
            {
                model.ErrorMessage = "Please provide firstname.";
                return new BadRequestObjectResult(model);
            }
            if (string.IsNullOrEmpty(requestModel.LastName))
            {
                model.ErrorMessage = "Please provide lastname.";
                return new BadRequestObjectResult(model);
            }

            var userDAO = await user.Get(requestModel.EmailAddress);
            if (userDAO != null && userDAO.UserId != 0)
            {
                model.ErrorMessage = "This email is already exists";
                return new BadRequestObjectResult(model);
            }

            var otpDetail = await oTPCommand.Get(Convert.ToInt16(requestModel.TransactionId));
            if (otpDetail != null && otpDetail.TransactionId == 0)
            {
                model.ErrorMessage = "Invalid OTP";
                return new BadRequestObjectResult(model);
            }
            if (otpDetail.IsUsed)
            {
                model.ErrorMessage = "Invalid OTP";
                return new BadRequestObjectResult(model);
            }
            if (otpDetail.OTP != requestModel.OtpNumber)
            {
                model.ErrorMessage = "Invalid OTP";
                return new BadRequestObjectResult(model);
            }
            if (otpDetail.OTP == requestModel.OtpNumber)
            {
                otpDetail.IsUsed = true;
            }

            userDAO = new UserDetailDAO()
            {
                FirstName = requestModel.FirstName,
                UserType = "EndUser",
                Password = requestModel.Password,
                LastName = requestModel.LastName,
                Gender = "NotDefined",
                Prefix = "NotDefined",
                IsActive = true,
                UserId = decimal.Parse(requestModel.UserId),
                EmailAddress = requestModel.EmailAddress,
                PhoneNumberList = new System.Collections.Generic.List<PhoneNumberDAO>()
                { 
                    new PhoneNumberDAO()
                    {
                        Id = 0,
                        UserId = 0,
                        Value = decimal.Parse(requestModel.PhoneNumber)
                    }
                }
            };

            if (requestModel.UserId == "0")
            {
                await user.New(userDAO);
            }
            else
            {
                await user.Save(userDAO);
            }

            if (userDAO.UserId != 0)
            {
                model.ErrorMessage = string.Empty;
                model.UserDetail = new UserDetailModel()
                {
                    FirstName = userDAO.FirstName,
                    UserId = userDAO.UserId.ToString("G29"),
                    Gender = userDAO.Gender,
                    LastName = userDAO.LastName,
                    EmailAddress = userDAO.EmailAddress,
                    Password = userDAO.Password,
                    Prefix = userDAO.Prefix,
                    UserType = userDAO.UserType,
                    PhoneNumberList = new System.Collections.Generic.List<PhoneNumberModel>(),
                    AddressList = new System.Collections.Generic.List<AddressModel>()
                };

                foreach (var phoneItem in await phoneNumberList.Get(userDAO.UserId))
                {
                    model.UserDetail.PhoneNumberList.Add(new PhoneNumberModel()
                    {
                        NumberId = phoneItem.Id.ToString("G29"),
                        Type = phoneItem.Type,
                        Value = phoneItem.Value.ToString("G29"),
                        UserId = phoneItem.UserId.ToString("G29")
                    });
                }
                await oTPCommand.Save(otpDetail);
                return new OkObjectResult(model);
            }
            else
            {
                model.ErrorMessage = userDAO.ErrorMessage;
                return new BadRequestObjectResult(model);
            }
        }
    }
}
