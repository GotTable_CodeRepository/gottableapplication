﻿using GotTable.API.Model.Events;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace GotTable.API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/eventlist/")]
    public sealed class EventListController : BaseAPIController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            await Task.FromResult(1);
            var model = new EventModel();
                model = new EventModel
                {
                    Active = false,
                    ImageUrl = "https://gottableindia.com/Uploads/Events/1.jpg"
                };
            return new OkObjectResult(model);
        }
    }
}
