Alter Table HotelBranches
Add AdminId numeric(18,0)
GO

ALTER TABLE [dbo].[HotelBranches]  WITH CHECK ADD  CONSTRAINT [FK_HotelBranches_Administrator] FOREIGN KEY([AdminId])
REFERENCES [dbo].[Administrator] ([UserId])
GO

ALTER TABLE [dbo].[HotelBranches] CHECK CONSTRAINT [FK_HotelBranches_Administrator]
GO


UPDATE HB  
SET HB.AdminId = BS.UserId
FROM HotelBranches HB  
INNER JOIN BranchStaff BS ON HB.BranchId = BS.BranchId  
  