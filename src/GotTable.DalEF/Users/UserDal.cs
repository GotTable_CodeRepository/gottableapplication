﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Users;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Users
{
    [Serializable]
    public sealed class UserDal : BaseContext<UserDal>, IUserDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserDto> Fetch(decimal userId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Users
                        .Include(User => User.DineInBookingBillUploads)
                        .AsQueryable()
                        where c.UserId == userId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid userId");
            }

            return new UserDto()
            {
                EmailAddress = data.EmailAddress,
                FirstName = data.FirstName,
                GenderId = data.GenderId,
                GenderName = ((Enumeration.Gender)data.GenderId).ToString(),
                IsActive = data.IsActive,
                IsEmailOptedForCommunication = data.IsEmailOptedForCommunication,
                IsEmailVerified = data.IsEmailVerified,
                LastName = data.LastName,
                PrefixId = data.PrefixId,
                PrefixName = ((Enumeration.Prefix)data.GenderId).ToString(),
                TypeId = data.TypeId,
                TypeName = ((Enumeration.UserType)data.TypeId).ToString(),
                UserId = data.UserId,
                Password = data.Password,
                RewardCount = data.DineInBookingBillUploads.Where(x => x.Authorized == true && x.Redeem == false).Sum(item => item.AuthorizedRewardPoint),
                DoubleTheDealBookingId = data.DoubleTheDealBookingId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<UserDto> Fetch(string emailAddress, string password)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Users
                        .Include(user => user.DineInBookingBillUploads)
                        .AsQueryable()
                        where c.EmailAddress == emailAddress && c.Password == password
                        select c).SingleOrDefault();

            if (data == null)
            {
                return null;
            }

            return new UserDto()
            {
                EmailAddress = data.EmailAddress,
                FirstName = data.FirstName,
                GenderId = data.GenderId,
                GenderName = string.Empty,
                IsActive = data.IsActive,
                IsEmailOptedForCommunication = data.IsEmailOptedForCommunication,
                IsEmailVerified = data.IsEmailVerified,
                LastName = data.LastName,
                PrefixId = data.PrefixId,
                PrefixName = string.Empty,
                TypeId = data.TypeId,
                TypeName = ((Enumeration.UserType)data.TypeId).ToString(),
                UserId = data.UserId,
                Password = data.Password,
                RewardCount = data.DineInBookingBillUploads.Where(x => x.Authorized == true && x.Redeem == false).Sum(item => item.AuthorizedRewardPoint),
                DoubleTheDealBookingId = data.DoubleTheDealBookingId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public async Task<UserDto> Fetch(string emailAddress)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Users
                        .Include(user => user.DineInBookingBillUploads)
                        .AsQueryable()
                        where c.EmailAddress == emailAddress
                        select c).SingleOrDefault();

            if (data == null)
            {
                return null;
            }

            return new UserDto()
            {
                EmailAddress = data.EmailAddress,
                FirstName = data.FirstName,
                GenderId = data.GenderId,
                GenderName = data.GenderId != null ? ((Enumeration.Gender)data.GenderId).ToString() : string.Empty,
                IsActive = data.IsActive,
                IsEmailOptedForCommunication = data.IsEmailOptedForCommunication,
                IsEmailVerified = data.IsEmailVerified,
                LastName = data.LastName,
                PrefixId = data.PrefixId,
                PrefixName = string.Empty,
                TypeId = data.TypeId,
                TypeName = ((Enumeration.UserType)data.TypeId).ToString(),
                UserId = data.UserId,
                Password = data.Password,
                RewardCount = data.DineInBookingBillUploads.Where(x => x.Authorized == true && x.Redeem == false).Sum(item => item.AuthorizedRewardPoint),
                DoubleTheDealBookingId = data.DoubleTheDealBookingId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<PhoneNumberDto>> FetchPhoneNumbers(decimal userId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.PhoneNumbers.AsQueryable()
                        where c.UserId == userId
                        select new PhoneNumberDto()
                        {
                            Id = c.Id,
                            IsActive = c.IsActive ?? false,
                            IsVerified = c.IsVerified ?? false,
                            TypeId = c.TypeId,
                            UserId = c.UserId,
                            Value = c.Value,
                            VerificationDate = c.VerificationDate
                        }).ToList();
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(UserDto dto)
        {
            await Task.FromResult(1);
            var data = new User()
            {
                EmailAddress = dto.EmailAddress,
                Password = dto.Password ?? Guid.NewGuid().ToString().Substring(1, 10),
                GenderId = dto.GenderId,
                IsActive = dto.IsActive,
                IsEmailOptedForCommunication = dto.IsEmailOptedForCommunication,
                PrefixId = dto.PrefixId,
                IsEmailVerified = dto.IsEmailVerified,
                TypeId = dto.TypeId,
                LastName = dto.LastName,
                FirstName = dto.FirstName,
                CreatedDate = DateTime.Now,
                DoubleTheDealBookingId = null
            };
            Context.Users.Add(data);
            Context.SaveChanges();

            dto.UserId = data.UserId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(AddressDto dto)
        {
            await Task.FromResult(1);
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(PhoneNumberDto dto)
        {
            await Task.FromResult(1);
            var data = new PhoneNumber()
            {
                IsActive = dto.IsActive,
                IsVerified = dto.IsVerified,
                TypeId = dto.TypeId,
                UserId = dto.UserId,
                Value = dto.Value,
                VerificationDate = dto.VerificationDate
            };
            Context.PhoneNumbers.Add(data);
            Context.SaveChanges();
            dto.UserId = data.UserId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(UserDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Users.AsQueryable()
                        where c.UserId == dto.UserId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid userId");
            }

            data.EmailAddress = dto.EmailAddress ?? data.EmailAddress;
            data.Password = dto.Password ?? data.Password;
            data.LastName = dto.LastName ?? data.LastName;
            data.FirstName = dto.FirstName ?? data.FirstName;
            data.IsActive = dto.IsActive ?? data.IsActive;
            data.IsEmailVerified = dto.IsEmailVerified ?? data.IsEmailVerified;
            data.TypeId = dto.TypeId == 0 ? data.TypeId : dto.TypeId;

            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(AddressDto dto)
        {
            await Task.FromResult(1);
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(PhoneNumberDto dto)
        {
            await Task.FromResult(1);
            throw new NotImplementedException();
        }
    }
}
