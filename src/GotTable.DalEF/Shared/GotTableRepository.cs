﻿using GotTable.DalEF.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace GotTable.DalEF
{
    /// <summary>
    /// 
    /// </summary>
    public class GotTableRepository : GottableStaging1Context
    {
        /// <summary>
        /// 
        /// </summary>
        public GotTableRepository()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public static string ConnectionString { get; set; } 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public static void SetConnectionString(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("GotTableRepository");
        }
    }
}
