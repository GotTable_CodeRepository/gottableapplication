﻿using GotTable.Dal.MenuCategories;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.MenuCategories
{
    [Serializable]
    public sealed class MenuCategoryDal : BaseContext<MenuCategoryDto>, IMenuCategoryDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<IEnumerable<MenuCategoryDto>> FetchList(int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from c in Context.MenuCategories.AsQueryable()
                        orderby c.Name
                        select new MenuCategoryDto
                        {
                            Name = c.Name,
                            Id = c.Id,
                            IsActive = c.IsActive
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<MenuCategoryDto> Fetch(string name)
        {
            await Task.FromResult(1);
            var data = (from c in Context.MenuCategories.AsQueryable()
                        where c.Name == name
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid categoryname");
            }

            return new MenuCategoryDto()
            {
                Name = data.Name,
                Id = data.Id,
                IsActive = data.IsActive
            };
        }

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public async Task<MenuCategoryDto> Fetch(decimal categoryId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.MenuCategories.AsQueryable()
                        where c.Id == categoryId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid categoryId");
            }

            return new MenuCategoryDto()
            {
                Name = data.Name,
                Id = data.Id,
                IsActive = data.IsActive
            };
        }
    }
}
