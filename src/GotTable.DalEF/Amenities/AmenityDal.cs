﻿using GotTable.Dal.Amenities;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;

namespace GotTable.DalEF.Amenities
{
    /// <summary>
    /// AmenityDal
    /// </summary>
    public sealed class AmenityDal : BaseContext<AmenityDal>, IAmenityDal
    {
        /// <summary> 
        /// FetchList
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<AmenityDto>> FetchList(int currentPage = 0, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Amenities
                        where c.Active == true
                        select new AmenityDto()
                        {
                            Active = c.Active,
                            Id = c.Id,
                            Name = c.Name
                        }).ToList();
            return data;
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(AmenityDto dto)
        {
            await Task.FromResult(1);
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(AmenityDto dto)
        {
            await Task.FromResult(1);
            throw new System.NotImplementedException();
        }
    }
}
