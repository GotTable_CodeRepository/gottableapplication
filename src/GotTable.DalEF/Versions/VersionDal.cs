﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Versions;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Versions
{
    [Serializable]
    public sealed class VersionDal : BaseContext<VersionDal>, IVersionDal
    {
        /// <summary>
        /// Fetch List
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<VersionDto>> FetchList(int currentPage = 0, int pageSize = 10)
        {
            await Task.FromResult(1);
            var list = (from c in Context.Versions
                        orderby c.CreatedDate descending
                        select new VersionDto()
                        {
                            Active = c.Active,
                            Message = c.Message,
                            CreatedDate = c.CreatedDate,
                            TypeId = c.TypeId,
                            Number = c.Number,
                            VersionId = c.VersionId,
                            TypeName = ((Enumeration.Device)c.TypeId).ToString(),
                            AttachedDeviceCount = 0,
                            SyncName = c.SyncName,
                            SyncPassword = c.SyncPassword
                        }).ToList();

            if (list != null && currentPage > 0)
            {
                list = list.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return list;
        }

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="versionId"></param>
        /// <returns></returns>
        public async Task<VersionDto> Fetch(int versionId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Versions
                        where c.VersionId == versionId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception();
            }

            return new VersionDto()
            {
                Active = data.Active,
                Message = data.Message,
                CreatedDate = data.CreatedDate,
                TypeId = data.TypeId,
                Number = data.Number,
                VersionId = data.VersionId,
                SyncName = data.SyncName,
                SyncPassword = data.SyncPassword
            };
        }

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="number"></param>
        /// <param name="typeId"></param>
        /// <returns></returns>
        public async Task<VersionDto> Fetch(string number, int typeId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Versions
                        where c.Number == number && c.TypeId == typeId
                        select c).SingleOrDefault();

            if (data == null)
            {
                return null;
            }

            return new VersionDto()
            {
                Active = data.Active,
                Message = data.Message,
                CreatedDate = data.CreatedDate,
                TypeId = data.TypeId,
                Number = data.Number,
                VersionId = data.VersionId,
                SyncPassword = data.SyncPassword,
                SyncName = data.SyncName
            };
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(VersionDto dto)
        {
            await Task.FromResult(1);
            var data = new Models.Version()
            {
                Active = dto.Active,
                Message = dto.Message,
                CreatedDate = dto.CreatedDate,
                TypeId = dto.TypeId,
                Number = dto.Number,
                SyncName = dto.SyncName,
                SyncPassword = dto.SyncPassword
            };

            Context.Versions.Add(data);
            Context.SaveChanges();

            dto.VersionId = data.VersionId;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(VersionDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Versions
                        where c.VersionId == dto.VersionId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception();
            }

            data.Active = dto.Active;
            data.Message = dto.Message;
            data.CreatedDate = dto.CreatedDate;
            data.TypeId = dto.TypeId;
            data.Number = dto.Number;
            data.SyncName = dto.SyncName;
            data.SyncPassword = dto.SyncPassword;

            Context.SaveChanges();
        }
    }
}
