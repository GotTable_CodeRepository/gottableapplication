﻿using GotTable.Dal.RestaurantCuisines;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantCuisines
{
    [Serializable]
    public sealed class CuisineDal : BaseContext<CuisineDal>, ICuisineDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<CuisineDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchCusinies
                        .Include(cus => cus.Branch)
                        .Include(cus => cus.Cuisine)
                        .AsQueryable()
                        where c.BranchId == restaurantId
                        orderby c.Id descending
                        select new CuisineDto()
                        {
                            BranchId = restaurantId,
                            CreatedDate = c.CreatedDate,
                            CuisineId = c.CuisineId,
                            CuisineName = c.Cuisine.CuisineName,
                            Id = c.Id,
                            IsActive = c.IsActive
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantCuisineId"></param>
        /// <returns></returns>
        public async Task<CuisineDto> Fetch(decimal restaurantCuisineId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchCusinies
                        .Include(cus => cus.Branch)
                        .Include(cus => cus.Cuisine)
                        .AsQueryable()
                        where c.Id == restaurantCuisineId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid cuisineId");
            }

            return new CuisineDto()
            {
                BranchId = data.BranchId,
                CreatedDate = data.CreatedDate,
                CuisineId = data.CuisineId,
                CuisineName = data.Cuisine.CuisineName,
                Id = data.Id,
                IsActive = data.IsActive
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="masterCuisineId"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public async Task<CuisineDto> Fetch(decimal masterCuisineId, decimal branchId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchCusinies
                        .Include(cus => cus.Branch)
                        .Include(cus => cus.Cuisine)
                        .AsQueryable()
                        where c.CuisineId == masterCuisineId && c.BranchId == branchId
                        select c).SingleOrDefault();

            if (data == null)
            {
                return null;
            }

            return new CuisineDto()
            {
                BranchId = data.BranchId,
                CreatedDate = data.CreatedDate,
                CuisineId = data.CuisineId,
                CuisineName = data.Cuisine.CuisineName,
                Id = data.Id,
                IsActive = data.IsActive
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(CuisineDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchCusiny()
            {
                BranchId = dto.BranchId,
                CreatedDate = DateTime.Now,
                CuisineId = dto.CuisineId,
                IsActive = dto.IsActive
            };

            Context.BranchCusinies.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(CuisineDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchCusinies.AsQueryable()
                        where c.Id == dto.Id
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid cuisineId");
            }

            data.IsActive = dto.IsActive;

            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Delete(CuisineDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchCusinies.AsQueryable()
                        where c.Id == dto.Id
                        select c).SingleOrDefault();

            if (data != null)
            {
                Context.BranchCusinies.Remove(data);
            }
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<bool> IsExist(CuisineDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchCusinies
                        where c.BranchId == dto.BranchId && c.Cuisine.CuisineName == dto.CuisineName
                        select c).SingleOrDefault();

            if (data == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
