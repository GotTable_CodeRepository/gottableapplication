﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantMenus;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantMenus
{
    [Serializable]
    public sealed class MenuDal : BaseContext<MenuDal>, IMenuDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<MenuDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchMenus
                        .Include(menu => menu.Type)
                        .Include(menu => menu.Cuisine).ThenInclude(cusine => cusine.Cuisine)
                        .AsQueryable()
                        where c.BranchId == restaurantId
                        orderby c.Type.Id
                        select new MenuDto()
                        {
                            BranchId = c.BranchId,
                            CategoryId = c.Type.Id,
                            CategoryName = c.Type.Name,
                            Name = c.Name,
                            Id = c.Id,
                            Cost = c.Cost,
                            CuisineId = c.CuisineId,
                            CuisineName = c.Cuisine.Cuisine.CuisineName,
                            Description = c.Description,
                            IsActive = c.IsActive,
                            IsDeliveryAvailable = c.IsDeliveryAvailable,
                            IsDineAvailable = c.IsDineAvailable,
                            IsTakeAwayAvailable = c.IsTakeAwayAvailable,
                            MenuTypeId = c.MenuTypeId,
                            MenuTypeName = ((Enumeration.MenuType)c.MenuTypeId).ToString()
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<MenuDto>> FetchList(decimal restaurantId, Enumeration.RestaurantTypes? restaurantType, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var query = (from c in Context.BranchMenus
                         .Include(menu => menu.Type)
                         .Include(menu => menu.Cuisine).ThenInclude(cusine => cusine.Cuisine)
                         .AsQueryable()
                         where c.BranchId == restaurantId
                         select c);

            if (restaurantType == Enumeration.RestaurantTypes.DineIn)
                query = query.Where(x => x.IsDineAvailable == true);
            else if (restaurantType == Enumeration.RestaurantTypes.Delivery)
                query = query.Where(x => x.IsDeliveryAvailable == true);
            else if (restaurantType == Enumeration.RestaurantTypes.Takeaway)
                query = query.Where(x => x.IsTakeAwayAvailable == true);

            var data = (from c in query
                        select new MenuDto()
                        {
                            BranchId = c.BranchId,
                            CategoryId = c.Type.Id,
                            CategoryName = c.Type.Name,
                            Name = c.Name,
                            Id = c.Id,
                            Cost = c.Cost,
                            CuisineId = c.CuisineId,
                            CuisineName = c.Cuisine.Cuisine.CuisineName,
                            Description = c.Description,
                            IsActive = c.IsActive,
                            IsDeliveryAvailable = c.IsDeliveryAvailable,
                            IsDineAvailable = c.IsDineAvailable,
                            IsTakeAwayAvailable = c.IsTakeAwayAvailable,
                            MenuTypeId = c.MenuTypeId,
                            MenuTypeName = ((Enumeration.MenuType)c.MenuTypeId).ToString()
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="typeId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<MenuDto>> FetchList(decimal restaurantId, Enumeration.RestaurantTypes? restaurantType, decimal typeId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var query = (from c in Context.BranchMenus
                         .Include(menu => menu.Type)
                         .Include(menu => menu.Cuisine).ThenInclude(cusine => cusine.Cuisine)
                         .AsQueryable()
                         where c.BranchId == restaurantId && c.TypeId == typeId
                         select c);

            if (restaurantType == Enumeration.RestaurantTypes.DineIn)
                query = query.Where(x => x.IsDineAvailable == true);
            else if (restaurantType == Enumeration.RestaurantTypes.Delivery)
                query = query.Where(x => x.IsDeliveryAvailable == true);
            else if (restaurantType == Enumeration.RestaurantTypes.Takeaway)
                query = query.Where(x => x.IsTakeAwayAvailable == true);

            var data = (from c in query
                        select new MenuDto()
                        {
                            BranchId = c.BranchId,
                            CategoryId = c.Type.Id,
                            CategoryName = c.Type.Name,
                            Name = c.Name,
                            Id = c.Id,
                            Cost = c.Cost,
                            CuisineId = c.CuisineId,
                            CuisineName = c.Cuisine.Cuisine.CuisineName,
                            Description = c.Description,
                            IsActive = c.IsActive,
                            IsDeliveryAvailable = c.IsDeliveryAvailable,
                            IsDineAvailable = c.IsDineAvailable,
                            IsTakeAwayAvailable = c.IsTakeAwayAvailable,
                            MenuTypeId = c.MenuTypeId,
                            MenuTypeName = ((Enumeration.MenuType)c.MenuTypeId).ToString()
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        public async Task<MenuDto> Fetch(decimal menuId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchMenus
                        .Include(menu => menu.Type)
                        .Include(menu => menu.Cuisine).ThenInclude(cusine => cusine.Cuisine)
                        .AsQueryable()
                        where c.Id == menuId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid menuId");
            }

            return new MenuDto()
            {
                BranchId = data.BranchId,
                CategoryId = data.Type.Id,
                CategoryName = data.Type.Name,
                Name = data.Name,
                Id = data.Id,
                Cost = data.Cost,
                CuisineId = data.CuisineId,
                CuisineName = data.Cuisine.Cuisine.CuisineName,
                Description = data.Description,
                IsActive = data.IsActive,
                IsDeliveryAvailable = data.IsDeliveryAvailable,
                IsDineAvailable = data.IsDineAvailable,
                IsTakeAwayAvailable = data.IsTakeAwayAvailable,
                MenuTypeId = data.MenuTypeId,
                MenuTypeName = ((Enumeration.MenuType)data.MenuTypeId).ToString()
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(MenuDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchMenu()
            {
                IsActive = dto.IsActive,
                BranchId = dto.BranchId,
                Cost = dto.Cost,
                CuisineId = dto.CuisineId,
                Description = dto.Description,
                IsDeliveryAvailable = dto.IsDeliveryAvailable,
                IsDineAvailable = dto.IsDineAvailable,
                IsTakeAwayAvailable = dto.IsTakeAwayAvailable,
                MenuTypeId = dto.MenuTypeId,
                Name = dto.Name,
                TypeId = dto.CategoryId
            };

            Context.BranchMenus.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(MenuDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchMenus
                        where c.Id == dto.Id
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid menuId");
            }

            data.IsActive = dto.IsActive;
            data.IsDeliveryAvailable = dto.IsDeliveryAvailable;
            data.IsDineAvailable = dto.IsDineAvailable;
            data.IsTakeAwayAvailable = dto.IsTakeAwayAvailable;
            data.Cost = dto.Cost;
            data.Description = dto.Description;
            data.CuisineId = dto.CuisineId;
            data.TypeId = dto.CategoryId;
            data.MenuTypeId = dto.MenuTypeId;
            data.Description = dto.Description;

            Context.SaveChanges();
        }
    }
}
