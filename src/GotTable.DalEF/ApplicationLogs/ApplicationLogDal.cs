﻿using GotTable.Dal.ApplicationLogs;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Threading.Tasks;

namespace GotTable.DalEF.ApplicationLogs
{
    /// <summary>
    /// IApplicationLog
    /// </summary>
    [Serializable]
    public sealed class ApplicationLogDal : BaseContext<ApplicationLogDal>, IApplicationLogDal
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(ApplicationLogDto dto)
        {
            await Task.FromResult(1);
            var data = new ApplicationLog()
            {
                DeviceId = dto.DeviceId,
                DeviceTypeId = dto.DeviceTypeId,
                Error = dto.Error,
                Params = dto.Params,
                StatusCode = dto.StatusCode,
                Url = dto.Url,
                UserId = dto.UserId,
                ControllerName = dto.ControllerName
            };

            Context.ApplicationLogs.Add(data);
            Context.SaveChanges();
        }
    }
}
