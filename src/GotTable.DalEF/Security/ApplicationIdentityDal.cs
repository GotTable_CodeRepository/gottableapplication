﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Security;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Security
{
    [Serializable]
    public sealed class ApplicationIdentityDal : BaseContext<ApplicationIdentityDal>, IApplicationIdentityDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<ApplicationIdentityDto> Get(string username, string password)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Administrators
                        .Include(admin => admin.HotelBranchAdmins)
                        .ThenInclude(branch => branch.BranchInvoices)
                        .AsQueryable()
                        where c.EmailAddress == username && c.Password == password
                        select c).SingleOrDefault();

            if (data == null)
            {
                return null;
            }

            var applicationIdentityDto = new ApplicationIdentityDto()
            {
                EmailAddress = data.EmailAddress,
                EnableRestaurantLogin = data.EnableButtonForRestaurantLogin ?? false,
                FirstName = data.FirstName,
                Id = data.UserId,
                LastName = data.LastName,
                AdminType = (Enumeration.AdminType)data.TypeId
            };

            if(applicationIdentityDto.AdminType == Enumeration.AdminType.HotelAdmin)
            {
                if(data.HotelBranchAdmins != null && data.HotelBranchAdmins.Count > 0)
                {
                    applicationIdentityDto.RestaurantId = data.HotelBranchAdmins.FirstOrDefault().BranchId;
                    applicationIdentityDto.RestaurantName = data.HotelBranchAdmins.FirstOrDefault().BranchName;
                    applicationIdentityDto.RestaurantAddress = data.HotelBranchAdmins.FirstOrDefault().AddressLine1 + ", " + data.HotelBranchAdmins.FirstOrDefault().AddressLine2;
                    applicationIdentityDto.DineInActive = data.HotelBranchAdmins.FirstOrDefault().BranchInvoices.Any(x => x.DineIn == true && x.IsActive == true);
                    applicationIdentityDto.DeliveryActive = data.HotelBranchAdmins.FirstOrDefault().BranchInvoices.Any(x => x.Delivery == true && x.IsActive == true);
                    applicationIdentityDto.TakeawayActive = data.HotelBranchAdmins.FirstOrDefault().BranchInvoices.Any(x => x.Takeaway == true && x.IsActive == true);
                }
            }
            return applicationIdentityDto;
        }
    }
}
