﻿using GotTable.Dal.RestaurantConfigurations;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantConfigurations
{
    [Serializable]
    public sealed class ConfigurationDal : BaseContext<ConfigurationDal>, IConfigurationDal
    {
        /// <summary>
        /// 
        /// </summary>
        private const int DefaultGST = 5;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<ConfigurationDto> Fetch(decimal restaurantId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchConfigurations.AsQueryable()
                        where c.BranchId == restaurantId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid configuration");
            }

            return new ConfigurationDto()
            {
                BranchId = data.BranchId,
                CentralGST = data.CentralGst,
                DeliveryCharges = data.DeliveryCharges,
                DeliveryCoverage = data.DeliveryCoverage,
                DeliveryStandBy = data.DeliveryStandBy,
                DineInReward = data.DineInReward,
                ServiceTax = data.ServiceTax,
                StateGST = data.StateGst,
                TakeawayStandBy = data.TakeawayStandBy,
                DineInAutoCompleteActive = data.DineInAutoCompleteActive,
                DineInAutoCompleteHours = data.DineInAutoCompleteHours,
                DineInRewardActive = data.DineInRewardActive
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(ConfigurationDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchConfiguration()
            {
                BranchId = dto.BranchId,
                CentralGst = DefaultGST,
                DeliveryCharges = 0,
                DeliveryCoverage = "0",
                DeliveryStandBy = "0",
                DineInReward = 0,
                ServiceTax = 0,
                StateGst = DefaultGST,
                TakeawayStandBy = "0",
                DineInRewardActive = false,
                DineInAutoCompleteHours = 0,
                DineInAutoCompleteActive = false
            };
            Context.BranchConfigurations.Add(data);
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(ConfigurationDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchConfigurations.AsQueryable()
                        where c.BranchId == dto.BranchId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid configuration Id");
            }

            data.CentralGst = dto.CentralGST;
            data.DeliveryCharges = dto.DeliveryCharges;
            data.DeliveryCoverage = dto.DeliveryCoverage;
            data.DeliveryStandBy = dto.DeliveryStandBy;
            data.DineInReward = dto.DineInReward;
            data.ServiceTax = dto.ServiceTax;
            data.StateGst = dto.StateGST;
            data.TakeawayStandBy = dto.TakeawayStandBy;
            data.DineInAutoCompleteActive = dto.DineInAutoCompleteActive;
            data.DineInAutoCompleteHours = dto.DineInAutoCompleteHours;
            data.DineInRewardActive = dto.DineInRewardActive;

            Context.SaveChanges();
        }
    }
}
