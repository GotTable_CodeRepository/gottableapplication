﻿using GotTable.Dal.RestaurantAmenities;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantAmenities
{
    public sealed class AmenityDal : BaseContext<AmenityDal>, IAmenityDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<AmenityDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Amenities
                        select new AmenityDto()
                        {
                            AmenityId = c.Id,
                            AmenityName = c.Name,
                            BranchId = restaurantId,
                            Id = c.Id,
                            Checked = Context.BranchAmenities.Any(x => x.BranchId == restaurantId)
                        }).ToList();

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(AmenityDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchAmenity()
            {
                AmenityId = dto.AmenityId,
                BranchId = dto.BranchId
            };

            Context.BranchAmenities.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amenityId"></param>
        /// <param name="branchId"></param>
        public async Task Delete(decimal amenityId, decimal branchId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchAmenities
                        where c.AmenityId == amenityId && c.BranchId == branchId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Field is blank");
            }

            Context.BranchAmenities.Remove(data);
            Context.SaveChanges();
        }
    }
}
