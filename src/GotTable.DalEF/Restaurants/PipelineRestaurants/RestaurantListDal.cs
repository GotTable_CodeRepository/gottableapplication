﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants.PipelineRestaurants;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.PipelineRestaurants
{
    [Serializable]
    public sealed class RestaurantListDal : BaseContext<RestaurantListDal>, IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from HB in Context.HotelBranches
                        .Include(branch => branch.AccountAdminPerson)
                        .Include(branch => branch.SalesPerson)
                        .Include(branch => branch.BranchContacts)
                        .Include(branch => branch.BranchCusinies)
                        .Include(branch => branch.BranchMenus)
                        .Include(branch => branch.BranchTimings)
                        .Include(branch => branch.BranchTables).AsQueryable()
                        where !Context.BranchInvoices.Any(x => x.BranchId == HB.BranchId)
                        orderby HB.BranchName
                        select new RestaurantInfoDto()
                        {
                            AccountAdminEmailAddress = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.EmailAddress : "NA",
                            AccountAdminId = HB.AccountAdminPersonId ?? 0,
                            AccountAdminName = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.FirstName + " " + HB.AccountAdminPerson.LastName : "NA",
                            Line1 = HB.AddressLine1,
                            Line2 = HB.AddressLine2,
                            City = HB.City,
                            RestaurantId = HB.BranchId,
                            RestaurantName = HB.BranchName,
                            SalesAdminEmailAddress = HB.SalesPerson.EmailAddress,
                            SalesAdminPersonId = HB.SalesPerson.UserId,
                            SalesAdminName = HB.SalesPerson.FirstName ?? string.Empty + " " + HB.SalesPerson.LastName ?? string.Empty,
                            State = HB.State,
                            Zip = HB.Zipcode,
                            Category = (Enumeration.RestaurantCategories)HB.CategoryId,
                            ContactCount = HB.BranchContacts.Count(),
                            CuisineCount = HB.BranchCusinies.Count(),
                            MenuCount = HB.BranchMenus.Count(),
                            TablesCount = HB.BranchTables.Count(),
                            TimingCount = HB.BranchTimings.Count(),
                            OpenForAccountAdmin = HB.OpenforAccountAdmin
                        }).ToList();
            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesPersonId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(decimal salesPersonId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from HB in Context.HotelBranches
                        .Include(branch => branch.AccountAdminPerson)
                        .Include(branch => branch.SalesPerson)
                        .Include(branch => branch.BranchContacts)
                        .Include(branch => branch.BranchCusinies)
                        .Include(branch => branch.BranchMenus)
                        .Include(branch => branch.BranchTimings)
                        .Include(branch => branch.BranchTables).AsQueryable()
                        where HB.SalesPersonId == salesPersonId && !Context.BranchInvoices.Any(x => x.BranchId == HB.BranchId)
                        orderby HB.BranchName
                        select new RestaurantInfoDto()
                        {
                            AccountAdminEmailAddress = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.EmailAddress : "NA",
                            AccountAdminId = HB.AccountAdminPersonId ?? 0,
                            AccountAdminName = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.FirstName + " " + HB.AccountAdminPerson.LastName : "NA",
                            Line1 = HB.AddressLine1,
                            Line2 = HB.AddressLine2,
                            City = HB.City,
                            RestaurantId = HB.BranchId,
                            RestaurantName = HB.BranchName,
                            SalesAdminEmailAddress = HB.SalesPerson.EmailAddress,
                            SalesAdminPersonId = HB.SalesPerson.UserId,
                            SalesAdminName = HB.SalesPerson.FirstName ?? string.Empty + " " + HB.SalesPerson.LastName ?? string.Empty,
                            State = HB.State,
                            Zip = HB.Zipcode,
                            Category = (Enumeration.RestaurantCategories)HB.CategoryId,
                            ContactCount = HB.BranchContacts.Count(),
                            CuisineCount = HB.BranchCusinies.Count(),
                            MenuCount = HB.BranchMenus.Count(),
                            TablesCount = HB.BranchTables.Count(),
                            TimingCount = HB.BranchTimings.Count(),
                            OpenForAccountAdmin = HB.OpenforAccountAdmin
                        }).ToList();
            return data;
        }
    }
}
