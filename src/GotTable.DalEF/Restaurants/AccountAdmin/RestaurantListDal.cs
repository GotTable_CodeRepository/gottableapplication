﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants.AccountAdmin;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.AccountAdmin
{
    [Serializable]
    public sealed class RestaurantListDal : BaseContext<RestaurantListDal>, IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountAdminPersonId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Dal.Restaurants.AccountAdmin.RestaurantInfoDto>> FetchList(decimal accountAdminPersonId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from HB in Context.HotelBranches
                        .Include(branch => branch.SalesPerson).AsQueryable()
                        where HB.AccountAdminPersonId == accountAdminPersonId && HB.OpenforAccountAdmin == true && HB.Deleted == false
                        orderby HB.BranchName
                        select new Dal.Restaurants.AccountAdmin.RestaurantInfoDto()
                        {
                            City = HB.City,
                            Line1 = HB.AddressLine1,
                            Line2 = HB.AddressLine2,
                            RestaurantId = HB.BranchId,
                            RestaurantName = HB.BranchName,
                            SalesPersonId = HB.SalesPersonId,
                            SalesAdminEmailAddress = HB.SalesPerson.EmailAddress,
                            SalesAdminName = HB.SalesPerson.FirstName + " " + HB.SalesPerson.LastName,
                            State = HB.State,
                            Zip = HB.Zipcode,
                            IsActive = HB.IsActive ?? false,
                            OpenforAccountAdmin = HB.OpenforAccountAdmin ?? false,
                            Category = (Enumeration.RestaurantCategories)HB.CategoryId
                        }).ToList();
            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }
    }
}
