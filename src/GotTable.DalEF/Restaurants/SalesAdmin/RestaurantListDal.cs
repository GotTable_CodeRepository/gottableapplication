﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants.SalesAdmin;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.SalesAdmin
{
    [Serializable]
    public sealed class RestaurantListDal : BaseContext<RestaurantListDal>, IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="restaurantCategory"></param>
        /// <param name="restaurantType"></param>
        /// <param name="searchExpression"></param>
        /// <param name="restaurantOwnerEmailAddress"></param>
        /// <param name="expiredRestaurant"></param>
        /// <param name="inactiveRestaurant"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(decimal userId, Enumeration.RestaurantCategories? restaurantCategory = null, Enumeration.RestaurantTypes? restaurantType = null, string searchExpression = "", string restaurantOwnerEmailAddress = "", bool? expiredRestaurant = null, bool? inactiveRestaurant = null, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var query = (from HB in Context.HotelBranches
                         .Include(branch => branch.AccountAdminPerson)
                         .Include(branch => branch.BranchInvoices)
                         join BI in Context.BranchInvoices on HB.BranchId equals BI.BranchId
                         where HB.SalesPersonId == userId && BI.IsActive != false && HB.Deleted == false
                         orderby HB.BranchName
                         select new
                         {
                             SearchExpression = HB.BranchName + HB.AddressLine1 + HB.AddressLine2,
                             Line1 = HB.AddressLine1,
                             Line2 = HB.AddressLine2,
                             BI.Amount,
                             HB.City,
                             Delivery = BI.Delivery ?? false,
                             DineIn = BI.DineIn ?? false,
                             BI.EndDate,
                             BI.InvoiceId,
                             BI.IsActive,
                             BI.PrescriptionTypeId,
                             RestaurantId = HB.BranchId,
                             RestaurantName = HB.BranchName,
                             BI.StartDate,
                             HB.State,
                             TakeAway = BI.Takeaway,
                             Zip = HB.Zipcode,
                             Status = HB.IsActive ?? false,
                             AccountAdminId = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.UserId : 0,
                             AccountAdminEmailAddress = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.EmailAddress : string.Empty,
                             AccountAdminName = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.FirstName + " " + HB.AccountAdminPerson.LastName : string.Empty,
                             Category = (Enumeration.RestaurantCategories)HB.CategoryId,
                             ExpiredRestaurant = HB.BranchInvoices.Any(x => x.EndDate > DateTime.Now && x.IsActive == true)
                         });

            if (restaurantCategory != null)
            {
                query = query.Where(x => x.Category.ToString() == restaurantCategory.ToString());
            }

            if (!string.IsNullOrEmpty(searchExpression))
            {
                query = query.Where(x => x.SearchExpression.Contains(searchExpression));
            }

            if (expiredRestaurant != null)
            {
                query = query.Where(x => x.ExpiredRestaurant == expiredRestaurant);
            }

            if (inactiveRestaurant != null)
            {
                query = query.Where(x => x.Status == !inactiveRestaurant);
            }

            var data = (from c in query
                        select new RestaurantInfoDto()
                        {
                            Line1 = c.Line1,
                            Line2 = c.Line2,
                            Amount = c.Amount,
                            City = c.City,
                            Delivery = c.Delivery,
                            DineIn = c.DineIn,
                            EndDate = c.EndDate,
                            InvoiceId = c.InvoiceId,
                            RestaurantStatus = c.Status,
                            InvoiceStatus = c.IsActive,
                            PrescriptionTypeId = c.PrescriptionTypeId,
                            RestaurantId = c.RestaurantId,
                            RestaurantName = c.RestaurantName,
                            StartDate = c.StartDate,
                            State = c.State,
                            TakeAway = c.TakeAway,
                            Zip = c.Zip,
                            AccountAdminId = c.AccountAdminId,
                            AccountAdminEmailAddress = c.AccountAdminEmailAddress,
                            AccountAdminName = c.AccountAdminName,
                            Category = c.Category,
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }
    }
}
