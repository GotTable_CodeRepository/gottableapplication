﻿using GotTable.Dal.Restaurants.PreferredRestaurants;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.PreferredRestaurants
{
    [Serializable]
    public sealed class PreferredRestaurantDal : BaseContext<PreferredRestaurantDal>, IPreferredRestaurantDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(double latitude, double longitude, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var ratingSource = (from c in Context.BranchAvgRatings
                                select new
                                {
                                    c.AvgRating,
                                    c.BranchId
                                });

            var data = new List<RestaurantInfoDto>();

            //var data = (from c in ctx.FindPreferredRestaurant(latitude, longitude).AsQueryable()
            //            orderby c.RestaurantName
            //            select new RestaurantInfoDto()
            //            {
            //                RestaurantId = c.RestaurantId,
            //                Address = c.Address,
            //                City = c.City,
            //                Latitude = c.Latitude,
            //                Longitude = c.Longitude,
            //                Name = c.RestaurantName,
            //                State = c.State,
            //                Zipcode = c.ZipCode,
            //                Distance = c.Distance,
            //                SEODescription = c.SEODescription,
            //                SEOKeyword = c.SEOKeyword,
            //                SEOTitle = c.SEOTitle,
            //                TagLine = c.TagLine,
            //                Rating = ratingSource.Where(x => x.BranchId == c.RestaurantId).SingleOrDefault() != null ? ratingSource.Where(x => x.BranchId == c.RestaurantId).SingleOrDefault().AvgRating ?? 0 : 0
            //            }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantDto>> FetchList(decimal? cityId = null, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var query = (from c in Context.PreferredRestaurants
                         .Include(branch => branch.City)
                         .Include(branch => branch.Restaurant)
                         select c).AsQueryable();
            if (cityId != null)
            {
                query = query.Where(x => x.CityId == cityId);
            }
            var data = (from pR in query
                        orderby pR.Restaurant.BranchName
                        select new RestaurantDto()
                        {
                            RestaurantId = pR.RestaurantId,
                            Address = pR.Restaurant.AddressLine1 + " " + pR.Restaurant.AddressLine1,
                            CityId = pR.CityId,
                            CityName = pR.City.Name,
                            Name = pR.Restaurant.BranchName
                        }).ToList();
            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<RestaurantDto> Fetch(decimal restaurantId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.PreferredRestaurants
                        .Include(branch => branch.City)
                        .Include(branch => branch.Restaurant).AsQueryable()
                        where c.RestaurantId == restaurantId
                        select c).SingleOrDefault();

            if (data == null)
            {
                return null;
            }

            return new RestaurantDto()
            {
                Address = data.Restaurant.AddressLine1 + " " + data.Restaurant.AddressLine2,
                RestaurantId = data.RestaurantId,
                CityId = data.CityId,
                CityName = data.City.Name,
                IndexValue = data.IndexValue,
                Name = data.Restaurant.BranchName,
                Status = data.Status
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(RestaurantDto dto)
        {
            await Task.FromResult(1);
            var data = new PreferredRestaurant()
            {
                CityId = dto.CityId,
                Comment = string.Empty,
                RestaurantId = dto.RestaurantId,
                Status = dto.Status,
                IndexValue = dto.IndexValue
            };
            Context.PreferredRestaurants.Add(data);
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(RestaurantDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.PreferredRestaurants
                        where c.RestaurantId == dto.RestaurantId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid preferred restaurantId");
            }

            data.Status = dto.Status;
            data.IndexValue = dto.IndexValue;

            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurantId"></param>
        public async Task Delete(decimal preferredRestaurantId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.PreferredRestaurants
                        where c.RestaurantId == preferredRestaurantId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid preferred restaurantId");
            }
            Context.PreferredRestaurants.Remove(data);
            Context.SaveChanges();
        }

        public Task Update(List<decimal> restaurantIds)
        {
            throw new NotImplementedException();
        }
    }
}
