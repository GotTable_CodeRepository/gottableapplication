﻿using GotTable.Dal.Restaurants.DeletedRestaurants;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.DeletedRestaurants
{
    [Serializable]
    public sealed class RestaurantListDal : BaseContext<RestaurantListDal>, IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(int currentPage = default, int pageSize = default)
        {
            await Task.FromResult(1);
            var data = (from HB in Context.HotelBranches.AsQueryable()
                        where HB.Deleted == true
                        orderby HB.BranchName
                        select new RestaurantInfoDto()
                        {
                            Name = HB.BranchName,
                            BranchId = HB.BranchId,
                            Address = HB.AddressLine1 + HB.AddressLine2 + HB.City + HB.State
                        }).ToList();
            if (data != null && currentPage != default)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }
    }
}
