﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants
{
    [Serializable]
    public sealed class RestaurantDal : BaseContext<RestaurantDal>, IRestaurantDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(decimal? cityId = null)
        {
            await Task.FromResult(1);
            var data = (from HB in Context.HotelBranches
                        join BI in Context.BranchInvoices on HB.BranchId equals BI.BranchId
                        where !Context.PreferredRestaurants.Any(d => d.RestaurantId == HB.BranchId)
                        && HB.IsActive == true && BI.IsActive == true && BI.EndDate > DateTime.Now
                        select new RestaurantInfoDto()
                        {
                            BranchId = HB.BranchId,
                            AddressLine1 = HB.AddressLine1,
                            AddressLine2 = HB.AddressLine2,
                            City = HB.City,
                            Name = HB.BranchName
                        }).ToList();

            return data;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<RestaurantDto> Fetch(decimal restaurantId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.HotelBranches
                        .Include(branch => branch.Admin)
                        .Include(branch => branch.SalesPerson)
                        .Include(branch => branch.AccountAdminPerson)
                        .Include(branch => branch.BranchTables)
                        .Include(branch => branch.BranchTimings)
                        .Include(branch => branch.BranchContacts)
                        .Include(branch => branch.BranchCusinies)
                        .Include(branch => branch.LocalityId1Navigation)
                        .Include(branch => branch.LocalityId2Navigation)
                        .Include(branch => branch.LocalityId3Navigation)
                        .Include(branch => branch.LocalityId4Navigation).AsQueryable()
                        where c.BranchId == restaurantId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid restaurantId");
            }

            return new RestaurantDto()
            {
                AccountAdminPersonId = data.AccountAdminPersonId,
                AddressLine1 = data.AddressLine1,
                AddressLine2 = data.AddressLine2,
                BranchId = data.BranchId,
                City = data.City,
                IsActive = data.IsActive,
                Latitude = data.Latitude,
                Longitude = data.Longitude,
                Name = data.BranchName,
                SalesAdminPersonId = data.SalesPersonId,
                State = data.State,
                Zip = data.Zipcode,
                OpenforAccountAdmin = data.OpenforAccountAdmin,

                SalesAdminName = data.SalesPerson != null ? data.SalesPerson.FirstName + " " + data.SalesPerson.LastName : "",
                SalesAdminEmailAddress = data.SalesPerson != null ? data.SalesPerson.EmailAddress : "",

                AccountAdminEmailAddress = data.AccountAdminPerson != null ? data.AccountAdminPerson.EmailAddress : string.Empty,
                AccountAdminName = data.AccountAdminPerson != null ? data.AccountAdminPerson.FirstName + " " + data.AccountAdminPerson.LastName : string.Empty,

                AdminId = data.AdminId,
                AdminEmailAddress = data.Admin != null ? data.Admin.EmailAddress : string.Empty,
                AdminName = data.Admin != null ? data.Admin.FirstName + " " + data.Admin.LastName : string.Empty,

                ContactCount = data.BranchContacts != null ? data.BranchContacts.Count : 0,
                CuisineCount = data.BranchCusinies != null ? data.BranchCusinies.Count : 0,
                TableCount = data.BranchTables != null ? data.BranchTables.Count : 0,
                TimingCount = data.BranchTimings != null ? data.BranchTimings.Count : 0,

                TagLine = data.TagLine,
                SEOTitle = data.Seotitle ?? string.Empty,
                SEOKeyword = data.Seokeyword ?? string.Empty,
                SEODescription = data.Seodescription ?? string.Empty,
                Category = (Enumeration.RestaurantCategories)data.CategoryId,

                LocalityId1 = data.LocalityId1,
                LocalityName1 = data.LocalityId1Navigation != null ? data.LocalityId1Navigation.Name : "",

                LocalityId2 = data.LocalityId2,
                LocalityName2 = data.LocalityId2Navigation != null ? data.LocalityId2Navigation.Name : "",

                LocalityId3 = data.LocalityId3,
                LocalityName3 = data.LocalityId3Navigation != null ? data.LocalityId3Navigation.Name : "",

                LocalityId4 = data.LocalityId4,
                LocalityName4 = data.LocalityId4Navigation != null ? data.LocalityId4Navigation.Name : "",

                CostForTwo = data.CostForTwo,
                Description = data.Description
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(RestaurantDto dto)
        {
            await Task.FromResult(1);
            var data = new HotelBranch()
            {
                AccountAdminPersonId = null,
                AddressLine1 = dto.AddressLine1,
                AddressLine2 = dto.AddressLine2,
                City = dto.City,
                IsActive = dto.IsActive,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                SalesPersonId = dto.SalesAdminPersonId,
                State = dto.State,
                Zipcode = dto.Zip,
                OpenforAccountAdmin = false,
                BranchName = dto.Name,
                TagLine = dto.TagLine,
                Seodescription = dto.SEODescription ?? string.Empty,
                Seokeyword = dto.SEOKeyword ?? string.Empty,
                Seotitle = dto.SEOTitle ?? string.Empty,
                CategoryId = (int)dto.Category,
                LocalityId1 = dto.LocalityId1,
                LocalityId2 = dto.LocalityId2,
                LocalityId3 = dto.LocalityId3,
                LocalityId4 = dto.LocalityId4,
                CostForTwo = dto.CostForTwo,
                Description = dto.Description,
                Deleted = false
            };
            Context.HotelBranches.Add(data);
            Context.SaveChanges();

            dto.BranchId = data.BranchId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(RestaurantDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.HotelBranches.AsQueryable()
                        where c.BranchId == dto.BranchId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid Restaurant Id");
            }

            data.AccountAdminPersonId = dto.AccountAdminPersonId;
            data.AddressLine1 = dto.AddressLine1;
            data.AddressLine2 = dto.AddressLine2;
            data.City = dto.City;
            data.IsActive = dto.IsActive;
            data.Latitude = dto.Latitude;
            data.Longitude = dto.Longitude;
            data.SalesPersonId = dto.SalesAdminPersonId;
            data.State = dto.State;
            data.Zipcode = dto.Zip;
            data.BranchName = dto.Name;
            data.OpenforAccountAdmin = dto.OpenforAccountAdmin;
            data.TagLine = dto.TagLine ?? string.Empty;
            data.Seodescription = dto.SEODescription ?? string.Empty;
            data.Seokeyword = dto.SEOKeyword ?? string.Empty;
            data.Seotitle = dto.SEOTitle ?? string.Empty;
            data.CategoryId = (int)dto.Category;

            data.LocalityId1 = dto.LocalityId1;
            data.LocalityId2 = dto.LocalityId2;
            data.LocalityId3 = dto.LocalityId3;
            data.LocalityId4 = dto.LocalityId4;

            data.CostForTwo = dto.CostForTwo;
            data.Description = dto.Description;
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantName"></param>
        /// <param name="cityName"></param>
        /// <param name="addressLine1"></param>
        /// <returns></returns>
        public async Task<bool> IsExist(string restaurantName, string cityName, string addressLine1)
        {
            await Task.FromResult(1);
            var data = (from c in Context.HotelBranches
                        where c.BranchName == restaurantName
                        && c.City == cityName && c.AddressLine1 == addressLine1
                        select c).FirstOrDefault();

            if (data == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<RestaurantInfoDto> FetchInfo(decimal? restaurantId = null, decimal? adminId = null)
        {
            await Task.FromResult(1);
            var query = (from HB in Context.HotelBranches
                        .Include(branch => branch.Admin)
                        .Include(branch => branch.SalesPerson)
                        .Include(branch => branch.AccountAdminPerson)
                        .Include(branch => branch.BranchTables)
                        .Include(branch => branch.BranchTimings)
                        .Include(branch => branch.BranchContacts)
                        .Include(branch => branch.BranchCusinies)
                        .Include(branch => branch.LocalityId1Navigation)
                        .Include(branch => branch.LocalityId2Navigation)
                        .Include(branch => branch.LocalityId3Navigation)
                        .Include(branch => branch.LocalityId4Navigation).AsQueryable()
                         select HB).AsQueryable();

            if (restaurantId != null)
            {
                query = query.Where(x => x.BranchId == restaurantId);
            }
            if (adminId != null)
            {
                query = query.Where(x => x.AdminId == adminId);
            }

            var data = (from HB in query
                        select new RestaurantInfoDto()
                        {
                            AccountAdminPersonId = HB.AccountAdminPersonId,
                            AddressLine1 = HB.AddressLine1,
                            AddressLine2 = HB.AddressLine2,
                            BranchId = HB.BranchId,
                            City = HB.City,
                            IsActive = HB.IsActive,
                            Latitude = HB.Latitude,
                            Longitude = HB.Longitude,
                            Name = HB.BranchName,
                            SalesAdminPersonId = HB.SalesPersonId,
                            State = HB.State,
                            Zip = HB.Zipcode,
                            OpenforAccountAdmin = HB.OpenforAccountAdmin,

                            SalesAdminName = HB.SalesPerson.FirstName + " " + HB.SalesPerson.LastName,
                            SalesAdminEmailAddress = HB.SalesPerson.EmailAddress,

                            AccountAdminEmailAddress = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.EmailAddress : string.Empty,
                            AccountAdminName = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.FirstName + " " + HB.AccountAdminPerson.LastName : string.Empty,

                            RestaurantAdminPersonId = HB.AdminId,
                            RestaurantAdminEmailAddress = HB.Admin != null ? HB.Admin.EmailAddress : string.Empty,
                            RestaurantAdminName = HB.Admin != null ? HB.Admin.FirstName + " " + HB.Admin.LastName : string.Empty,

                            TagLine = HB.TagLine,
                            SEOTitle = HB.Seotitle ?? string.Empty,
                            SEOKeyword = HB.Seokeyword ?? string.Empty,
                            SEODescription = HB.Seodescription ?? string.Empty,
                            Category = (Enumeration.RestaurantCategories)HB.CategoryId,

                            LocalityId1 = HB.LocalityId1,
                            LocalityName1 = HB.LocalityId1Navigation != null ? HB.LocalityId1Navigation.Name : "",

                            LocalityId2 = HB.LocalityId2,
                            LocalityName2 = HB.LocalityId2Navigation != null ? HB.LocalityId2Navigation.Name : "",

                            LocalityId3 = HB.LocalityId3,
                            LocalityName3 = HB.LocalityId3Navigation != null ? HB.LocalityId3Navigation.Name : "",

                            LocalityId4 = HB.LocalityId4,
                            LocalityName4 = HB.LocalityId4Navigation != null ? HB.LocalityId4Navigation.Name : "",

                            CostForTwo = HB.CostForTwo,
                            Description = HB.Description,

                            DineInActive = HB.BranchInvoices.Where(x => x.IsActive == true && x.DineIn == true).FirstOrDefault() != null ?
                                           HB.BranchInvoices.Where(x => x.IsActive == true && x.DineIn == true).FirstOrDefault().DineIn ?? false : false,
                            DeliveryActive = HB.BranchInvoices.Where(x => x.IsActive == true && x.Delivery == true).FirstOrDefault() != null ?
                                             HB.BranchInvoices.Where(x => x.IsActive == true && x.Delivery == true).FirstOrDefault().Delivery ?? false : false,
                            TakeawayActive = HB.BranchInvoices.Where(x => x.IsActive == true && x.Takeaway == true).FirstOrDefault() != null ?
                                             HB.BranchInvoices.Where(x => x.IsActive == true && x.Takeaway == true).FirstOrDefault().Takeaway ?? false : false,
                        }).SingleOrDefault();

            return data;

        }
    }
}
