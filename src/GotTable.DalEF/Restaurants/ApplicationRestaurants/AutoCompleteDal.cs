﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants.ApplicationRestaurants;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.ApplicationRestaurants
{
    [Serializable]
    public sealed class AutoCompleteDal : BaseContext<AutoCompleteDal>, IAutoCompleteDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryExpression"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AutoCompleteDto>> FetchList(string queryExpression = "", Enumeration.RestaurantAutoComplete? type = null)
        {
            await Task.FromResult(1);
            if (type == Enumeration.RestaurantAutoComplete.RestaurantName)
            {
                return (from c in Context.HotelBranches
                        where c.BranchName.ToLower().Contains(queryExpression.ToLower())
                        select new AutoCompleteDto()
                        {
                            Id = c.BranchId,
                            Name = c.BranchName
                        }).Distinct().ToList();
            }
            else if (type == Enumeration.RestaurantAutoComplete.Cuisine)
            {
                return (from c in Context.Cuisines
                        where c.CuisineName.ToLower().Contains(queryExpression.ToLower())
                        select new AutoCompleteDto()
                        {
                            Id = c.Id,
                            Name = c.CuisineName
                        }).Distinct().ToList();
            }
            else if (type == Enumeration.RestaurantAutoComplete.Location)
            {
                var localityList = (from c in Context.Localities
                                    where c.Name.ToLower().Contains(queryExpression.ToLower())
                                    select new AutoCompleteDto()
                                    {
                                        Id = c.Id,
                                        Name = c.Name
                                    }).ToList();

                return localityList.Where(l => Context.HotelBranches.Any(x => x.Deleted != true && x.IsActive == true
                                          && (x.LocalityId1 == l.Id || x.LocalityId2 == l.Id
                                          || x.LocalityId3 == l.Id || x.LocalityId4 == l.Id))).ToList();
            }

            return default;
        }
    }
}
