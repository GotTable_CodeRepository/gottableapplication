﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants.ApplicationRestaurants;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.ApplicationRestaurants
{
    [Serializable]
    public sealed class RestaurantListDal : BaseContext<RestaurantListDal>, IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> FetchList(ListCriteria criteria)
        {
            await Task.FromResult(1);
            var source = (from c in Context.HotelBranches.Include(branch => branch.BranchOffers)
                          join d in Context.BranchInvoices on c.BranchId equals d.BranchId
                          where c.IsActive == true && d.IsActive == true && d.EndDate >= DateTime.Now && d.StartDate <= DateTime.Now
                          && c.Latitude != "" && c.Longitude != "" && c.Deleted == false
                          select new
                          {
                              c.AddressLine1,
                              c.AddressLine2,
                              c.BranchName,
                              SearchTerm = c.BranchName + c.Zipcode,
                              c.City,
                              c.State,
                              c.Zipcode,
                              c.Latitude,
                              c.Longitude,
                              c.BranchId,
                              Distance = 0,
                              DeliveryTodayOffer = c.BranchOffers.Where(x => x.IsActive == true && x.IsDelivery == true && x.EndDate > DateTime.Now && x.TypeId == (int)Enumeration.OfferType.Todays).Count() > 0,
                              DeliverySpecialOffer = c.BranchOffers.Where(x => x.IsActive == true && x.IsDelivery == true && x.EndDate > DateTime.Now && x.TypeId == (int)Enumeration.OfferType.Special).Count() > 0,
                              DineTodayOffer = c.BranchOffers.Where(x => x.IsActive == true && x.IsDineIn == true && x.EndDate > DateTime.Now && x.TypeId == (int)Enumeration.OfferType.Todays).Count() > 0,
                              DineSpecialOffer = c.BranchOffers.Where(x => x.IsActive == true && x.IsDineIn == true && x.EndDate > DateTime.Now && x.TypeId == (int)Enumeration.OfferType.Special).Count() > 0,
                              TakeawayTodayOffer = c.BranchOffers.Where(x => x.IsActive == true && x.IsTakeAway == true && x.EndDate > DateTime.Now && x.TypeId == (int)Enumeration.OfferType.Todays).Count() > 0,
                              TakeawaySpecialOffer = c.BranchOffers.Where(x => x.IsActive == true && x.IsTakeAway == true && x.EndDate > DateTime.Now && x.TypeId == (int)Enumeration.OfferType.Special).Count() > 0,
                              d.DineIn,
                              d.Delivery,
                              d.Takeaway,
                              c.TagLine,
                              c.CategoryId,
                              c.Seodescription,
                              c.Seokeyword,
                              c.Seotitle,
                              LocaltyIds = new List<decimal> { c.LocalityId1 ?? 0, c.LocalityId2 ?? 0, c.LocalityId3 ?? 0, c.LocalityId4 ?? 0 },
                              ActiveDineInOffersCount = c.BranchOffers.Where(x => x.IsActive == true && x.IsDineIn == true && x.EndDate > DateTime.Now).Count(),
                              ActiveDeliveryOffersCount = c.BranchOffers.Where(x => x.IsActive == true && x.IsDelivery == true && x.EndDate > DateTime.Now).Count(),
                              ActiveTakeawayOffersCount = c.BranchOffers.Where(x => x.IsActive == true && x.IsTakeAway == true && x.EndDate > DateTime.Now).Count(),
                              c.CostForTwo,
                              c.Description,
                              DineInOfferCategories = c.BranchOffers.Where(x => x.IsActive == true && x.IsDineIn == true && x.EndDate > DateTime.Now).Select(x => x.Category.Id).ToList()
                          });

            if (criteria.RestaurantType == Enumeration.RestaurantTypes.DineIn)
            {
                source = source.Where(x => x.DineIn == true);
            }

            if (criteria.RestaurantType == Enumeration.RestaurantTypes.Delivery)
            {
                source = source.Where(x => x.Delivery == true);
            }

            if (criteria.RestaurantType == Enumeration.RestaurantTypes.Takeaway)
            {
                source = source.Where(x => x.Takeaway == true);
            }

            if (criteria.RestaurantCategoryId != null)
            {
                source = source.Where(x => x.CategoryId == criteria.RestaurantCategoryId.Value);
            }

            if (criteria.OfferType == Enumeration.OfferType.Todays)
            {
                if (criteria.RestaurantType == Enumeration.RestaurantTypes.DineIn)
                {
                    source = source.Where(x => x.DineTodayOffer == true);
                }
                if (criteria.RestaurantType == Enumeration.RestaurantTypes.Delivery)
                {
                    source = source.Where(x => x.DeliveryTodayOffer == true);
                }
                if (criteria.RestaurantType == Enumeration.RestaurantTypes.Takeaway)
                {
                    source = source.Where(x => x.TakeawayTodayOffer == true);
                }
            }
            else if (criteria.OfferType == Enumeration.OfferType.Special)
            {
                if (criteria.RestaurantType == Enumeration.RestaurantTypes.DineIn)
                {
                    source = source.Where(x => x.DineSpecialOffer == true);
                }
                if (criteria.RestaurantType == Enumeration.RestaurantTypes.Delivery)
                {
                    source = source.Where(x => x.DeliverySpecialOffer == true);
                }
                if (criteria.RestaurantType == Enumeration.RestaurantTypes.Takeaway)
                {
                    source = source.Where(x => x.TakeawaySpecialOffer == true);
                }
            }

            if (criteria.OfferCategories != null && criteria.OfferCategories.Count > 0 && criteria.RestaurantType == Enumeration.RestaurantTypes.DineIn)
            {
                source = source.Where(s => criteria.OfferCategories.Any(item => s.DineInOfferCategories.Contains(item)));
            }

            if (!string.IsNullOrEmpty(criteria.QueryExpression))
            {
                source = from a in source
                         join b in Context.BranchCusinies.Include(branch => branch.Cuisine) on a.BranchId equals b.BranchId
                         where b.Cuisine.CuisineName.Contains(criteria.QueryExpression) || a.SearchTerm.Contains(criteria.QueryExpression)
                         select a;
            }

            if (criteria.CuisineIds != null && criteria.CuisineIds.Count() > 0)
            {
                source = from a in source
                         join b in Context.BranchCusinies.Include(branch => branch.Cuisine) on a.BranchId equals b.BranchId
                         where criteria.CuisineIds.Contains(b.Cuisine.Id)
                         select a;
            }

            if (criteria.LocaltyId != null && criteria.LocaltyId.Count() > 0)
            {
                source = source.Where(x => x.LocaltyIds.Any(l => criteria.LocaltyId.Any(c => l == c)));
            }

            var data = (from c in source.AsQueryable()
                        orderby c.Distance
                        select new RestaurantInfoDto()
                        {
                            City = c.City,
                            Distance = 0,
                            Latitude = c.Latitude,
                            Longitude = c.Longitude,
                            Line1 = c.AddressLine1,
                            Line2 = c.AddressLine2,
                            Name = c.BranchName,
                            OfferName = string.Empty,
                            RestaurantId = c.BranchId,
                            State = c.State,
                            Zip = c.Zipcode,
                            RestaurantTag = c.TagLine ?? string.Empty,
                            SEODescription = c.Seodescription,
                            SEOKeyword = c.Seokeyword,
                            SEOTitle = c.Seotitle,
                            Category = (Enumeration.RestaurantCategories)c.CategoryId,
                            ActiveOffers = criteria.RestaurantType == Enumeration.RestaurantTypes.DineIn ? c.ActiveDineInOffersCount : criteria.RestaurantType == Enumeration.RestaurantTypes.Takeaway ? c.ActiveTakeawayOffersCount : c.ActiveDeliveryOffersCount,
                            CostForTwo = c.CostForTwo ?? 0,
                            Description = c.Description
                        }).Distinct().ToList();

            data.ForEach(m => m.Distance = GetDistance.Execute(criteria.CurrentLatitude, criteria.CurrentLongitude, double.Parse(m.Latitude), double.Parse(m.Longitude)));

            if (criteria.AreaCoverage > 0)
            {
                data = data.Where(x => x.Distance < criteria.AreaCoverage).ToList();
            }
            if (data != null && criteria.CurrentPage > 0)
            {
                data = data.OrderBy(x => x.Distance).Skip((criteria.CurrentPage - 1) * criteria.PageSize).Take(criteria.PageSize).ToList();
            }
            else
            {
                data = data.OrderBy(x => x.Distance).ToList();
            }

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        public async Task<List<RestaurantInfoDto>> Fetch(decimal restaurantId, double latitude, double longitude, Enumeration.RestaurantTypes? restaurantType)
        {
            await Task.FromResult(1);
            if (restaurantType == Enumeration.RestaurantTypes.DineIn)
            {
                var data = (from c in Context.HotelBranches
                            where c.BranchId == restaurantId
                            select new RestaurantInfoDto()
                            {
                                Line1 = c.AddressLine1,
                                Line2 = c.AddressLine2,
                                City = c.City,
                                RestaurantId = c.BranchId,
                                Latitude = c.Latitude,
                                Longitude = c.Longitude,
                                Name = c.BranchName,
                                State = c.State,
                                Zip = c.Zipcode,
                                RestaurantTag = c.TagLine ?? string.Empty,
                                SEODescription = c.Seodescription ?? string.Empty,
                                SEOKeyword = c.Seokeyword ?? string.Empty,
                                SEOTitle = c.Seotitle ?? string.Empty,
                                Category = (Enumeration.RestaurantCategories)c.CategoryId,
                                CostForTwo = c.CostForTwo ?? 0,
                                Description = c.Description
                            }).ToList();

                data.ForEach(c => c.Distance = GetDistance.Execute(latitude, longitude, double.Parse(c.Latitude), double.Parse(c.Longitude)));

                return data;
            }
            else if (restaurantType == Enumeration.RestaurantTypes.Delivery)
            {
                var data = (from c in Context.HotelBranches
                            where c.BranchId == restaurantId
                            select new RestaurantInfoDto()
                            {
                                Line1 = c.AddressLine1,
                                Line2 = c.AddressLine2,
                                City = c.City,
                                RestaurantId = c.BranchId,
                                Latitude = c.Latitude,
                                Longitude = c.Longitude,
                                Name = c.BranchName,
                                State = c.State,
                                Zip = c.Zipcode,
                                RestaurantTag = c.TagLine ?? string.Empty,
                                SEODescription = c.Seodescription ?? string.Empty,
                                SEOKeyword = c.Seokeyword ?? string.Empty,
                                SEOTitle = c.Seotitle ?? string.Empty,
                                Category = (Enumeration.RestaurantCategories)c.CategoryId,
                                CostForTwo = c.CostForTwo ?? 0,
                                Description = c.Description
                            }).ToList();

                data.ForEach(c => c.Distance = GetDistance.Execute(latitude, longitude, double.Parse(c.Latitude), double.Parse(c.Longitude)));

                return data;
            }
            else if (restaurantType == Enumeration.RestaurantTypes.Takeaway)
            {
                var data = (from c in Context.HotelBranches
                            where c.BranchId == restaurantId
                            select new RestaurantInfoDto()
                            {
                                Line1 = c.AddressLine1,
                                Line2 = c.AddressLine2,
                                City = c.City,
                                RestaurantId = c.BranchId,
                                Latitude = c.Latitude,
                                Longitude = c.Longitude,
                                Name = c.BranchName,
                                State = c.State,
                                Zip = c.Zipcode,
                                RestaurantTag = c.TagLine ?? string.Empty,
                                SEODescription = c.Seodescription ?? string.Empty,
                                SEOKeyword = c.Seokeyword ?? string.Empty,
                                SEOTitle = c.Seotitle ?? string.Empty,
                                Category = (Enumeration.RestaurantCategories)c.CategoryId,
                                CostForTwo = c.CostForTwo ?? 0,
                                Description = c.Description
                            }).ToList();

                data.ForEach(c => c.Distance = GetDistance.Execute(latitude, longitude, double.Parse(c.Latitude), double.Parse(c.Longitude)));

                return data;
            }
            else
            {
                throw new System.Exception("Invalid restaurant category");
            }
        }
    }
}
