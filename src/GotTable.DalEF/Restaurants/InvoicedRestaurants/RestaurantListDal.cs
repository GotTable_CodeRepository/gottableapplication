﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Restaurants.InvoicedRestaurants;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Restaurants.InvoicedRestaurants
{
    [Serializable]
    public sealed class RestaurantListDal : BaseContext<RestaurantListDal>, IRestaurantListDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesPersonId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<Dal.Restaurants.InvoicedRestaurants.RestaurantInfoDto>> FetchList(decimal? salesPersonId = null, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from HB in Context.HotelBranches
                        .Include(branch => branch.SalesPerson)
                        .Include(branch => branch.AccountAdminPerson)
                        join BI in Context.BranchInvoices.AsQueryable()
                        on HB.BranchId equals BI.BranchId
                        where BI.IsActive == true && HB.Deleted == false
                        orderby HB.BranchName
                        select new RestaurantInfoDto()
                        {
                            AccountAdminEmailAddress = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.EmailAddress : string.Empty,
                            AccountAdminName = HB.AccountAdminPerson != null ? HB.AccountAdminPerson.FirstName + " " + HB.AccountAdminPerson.LastName : string.Empty,
                            AccountAdminPersonId = HB.AccountAdminPersonId ?? 0,
                            Amount = BI.Amount,
                            City = HB.City,
                            EndDate = BI.EndDate,
                            IsActive = HB.IsActive,
                            Line1 = HB.AddressLine1,
                            Line2 = HB.AddressLine2,
                            RestaurantId = HB.BranchId,
                            RestaurantName = HB.BranchName,
                            SalesAdminEmailAddress = HB.SalesPerson != null ? HB.SalesPerson.EmailAddress : string.Empty,
                            SalesAdminName = HB.SalesPerson != null ? HB.SalesPerson.FirstName + " " + HB.SalesPerson.LastName : string.Empty,
                            SalesPersonId = HB.SalesPersonId ?? 0,
                            StartDate = BI.StartDate,
                            State = HB.State,
                            SubscriptionTypeId = BI.PrescriptionTypeId,
                            Zip = HB.Zipcode,
                            Category = (Enumeration.RestaurantCategories)HB.CategoryId
                        }).ToList();

            if (salesPersonId != null)
            {
                data = data.Where(x => x.SalesPersonId == salesPersonId.Value).ToList();
            }
            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }
    }
}
