﻿using GotTable.Common.Enumerations;
using GotTable.Dal.OfferCategories;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.OfferCategories
{
    [Serializable]
    public sealed class OfferCategoryDal : BaseContext<OfferCategoryDal>, IOfferCategoryDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<OfferCategoryDto>> FetchList(int currentPage = 1, int pageSize = 10, Enumeration.RestaurantTypes? restaurantType = null)
        {
            await Task.FromResult(1);
            var data = (from c in Context.OfferCategories
                        .Include(offer => offer.BranchOffers).AsQueryable()
                        orderby c.Name
                        select new OfferCategoryDto
                        {
                            Name = c.Name,
                            Id = c.Id,
                            Active = c.Active,
                            Extension = c.Extension,
                            EngagedOffers = c.BranchOffers.Count()
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public async Task<OfferCategoryDto> Fetch(int categoryId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.OfferCategories
                        where c.Id == categoryId
                        select new
                        {
                            c.Id,
                            c.Active,
                            c.Name,
                            c.Extension
                        }).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid offer category Id");
            }

            return new OfferCategoryDto()
            {
                Active = data.Active,
                Extension = data.Extension,
                Id = data.Id,
                Name = data.Name
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(OfferCategoryDto dto)
        {
            await Task.FromResult(1);
            var data = new OfferCategory()
            {
                Active = dto.Active,
                Id = dto.Id,
                Name = dto.Name,
                Extension = dto.Extension
            };

            Context.OfferCategories.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(OfferCategoryDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.OfferCategories
                        where c.Id == dto.Id
                        select new OfferCategoryDto()
                        {
                            Id = c.Id,
                            Active = c.Active,
                            Name = c.Name,
                            Extension = c.Extension
                        }).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid offer category Id");
            }

            data.Active = dto.Active;
            Context.SaveChanges();
        }
    }
}
