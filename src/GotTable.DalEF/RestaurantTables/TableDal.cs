﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantTables;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantTables
{
    [Serializable]
    public sealed class TableDal : BaseContext<TableDal>, ITableDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<TableDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchTables.AsQueryable()
                        where c.BranchId == restaurantId
                        orderby c.Id
                        select new TableDto()
                        {
                            TableCount = c.Value,
                            Id = c.Id,
                            TableId = c.TableId,
                            TableName = ((Enumeration.Tables)c.TableId).ToString(),
                            RestaurantId = c.BranchId,
                            IsActive = c.IsActive ?? false
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantTableId"></param>
        /// <returns></returns>
        public async Task<TableDto> Fetch(decimal restaurantTableId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchTables.AsQueryable()
                        where c.Id == restaurantTableId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid TableId");
            }

            return new TableDto()
            {
                TableId = data.TableId,
                TableCount = data.Value,
                Id = data.Id,
                TableName = ((Enumeration.Tables)data.TableId).ToString(),
                RestaurantId = data.BranchId,
                IsActive = data.IsActive ?? false
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(TableDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchTable()
            {
                BranchId = dto.RestaurantId,
                IsActive = dto.IsActive,
                TableId = dto.TableId,
                Value = dto.TableCount
            };

            Context.BranchTables.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(TableDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchTables.AsQueryable()
                        where c.Id == dto.Id
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid tableId");
            }

            data.IsActive = true;
            data.Value = dto.TableCount;

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantTableId"></param>
        public async Task Delete(decimal restaurantTableId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchTables.AsQueryable()
                        where c.Id == restaurantTableId
                        select c).SingleOrDefault();

            if (data != null)
            {
                Context.BranchTables.Remove(data);
                Context.SaveChanges();
            }
        }
    }
}
