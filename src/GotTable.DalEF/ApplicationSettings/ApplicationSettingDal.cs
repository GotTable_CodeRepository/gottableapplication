﻿using GotTable.Dal.ApplicationSettings;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.ApplicationSettings
{
    /// <summary>
    /// ApplicationSettingDal
    /// </summary>
    [Serializable]
    public sealed class ApplicationSettingDal : BaseContext<ApplicationSettingDal>, IApplicationSettingDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <returns></returns>
        public async Task<ApplicationSettingDto> Fetch()
        {
            await Task.FromResult(1);
            var data = (from ar in Context.ApplicationSettings.AsQueryable()
                        select new
                        {
                            ar.ApnsCeritificatePassword,
                            ar.ApnsCertificateContent,
                            ar.GeoUrl,
                            ar.OtpauthKey,
                            ar.OtpauthUrl,
                            ar.TwoFactorApiauthKey,
                            ar.TwoFactorApiurl,
                            ar.FcmserverApikey,
                            ar.AdminApplicationBaseUrl,
                            ar.UserApplicationBaseUrl,
                            ar.ApplicationIdPackageName,
                            ar.FcmsenderId,
                            ar.Fcmurl,
                            ar.DineInActive,
                            ar.DeliveryActive,
                            ar.TakeawayActive,
                            ar.RestaurantDirectory,
                            ar.OfferDirectory,
                            ar.ServiceBusFeatureEnable,
                            ar.HangfireFeatureEnable,
                            ar.NotificationDirectory
                        }).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid application key");
            }

            return new ApplicationSettingDto()
            {
                ApnsCeritificatePassword = data.ApnsCeritificatePassword,
                ApnsCertificateContent = data.ApnsCertificateContent,
                GeoUrl = data.GeoUrl,
                OTPAuthKey = data.OtpauthKey,
                OTPAuthUrl = data.OtpauthUrl,
                TwoFactorAPIAuthKey = data.TwoFactorApiauthKey,
                TwoFactorAPIURl = data.TwoFactorApiurl,
                FCMServerKey = data.FcmserverApikey,
                AdminApplicationBaseUrl = data.AdminApplicationBaseUrl,
                UserApplicationBaseUrl = data.UserApplicationBaseUrl,
                ApplicationIdPackageName = data.ApplicationIdPackageName,
                FCMSenderId = data.FcmsenderId,
                FCMUrl = data.Fcmurl,
                DineInActive = data.DineInActive,
                TakeawayActive = data.TakeawayActive,
                DeliveryActive = data.DeliveryActive,
                RestaurantDirectory = data.RestaurantDirectory,
                OfferDirectory = data.OfferDirectory,
                HangfireFeatureEnable = data.HangfireFeatureEnable,
                ServiceBusFeatureEnable = data.ServiceBusFeatureEnable,
                NotificationDirectory = data.NotificationDirectory
            };
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(ApplicationSettingDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.ApplicationSettings
                        select c).SingleOrDefault();

            data.AdminApplicationBaseUrl = dto.AdminApplicationBaseUrl;
            data.ApnsCeritificatePassword = dto.ApnsCeritificatePassword;   
            data.ApplicationIdPackageName = dto.ApplicationIdPackageName;
            data.FcmsenderId = dto.FCMSenderId;
            data.FcmserverApikey = dto.FCMServerKey;
            data.Fcmurl = dto.FCMUrl;
            data.GeoUrl = dto.GeoUrl;
            data.OtpauthKey = dto.OTPAuthKey;
            data.OtpauthUrl = dto.OTPAuthUrl;
            data.TwoFactorApiauthKey = dto.TwoFactorAPIAuthKey;
            data.TwoFactorApiurl = dto.TwoFactorAPIURl;
            data.UserApplicationBaseUrl = dto.UserApplicationBaseUrl;
            data.DineInActive = dto.DineInActive;
            data.DeliveryActive = dto.DeliveryActive;
            data.TakeawayActive = dto.TakeawayActive;
            data.OfferDirectory = dto.OfferDirectory;
            data.RestaurantDirectory = dto.RestaurantDirectory;
            data.NotificationDirectory = dto.NotificationDirectory;
            data.HangfireFeatureEnable = dto.HangfireFeatureEnable;
            data.ServiceBusFeatureEnable = dto.ServiceBusFeatureEnable;
            data.ApnsCertificateContent = dto.ApnsCertificateContent ?? data.ApnsCertificateContent;

            Context.SaveChanges();
        }
    }
}
