﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Administrators;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Administrators
{
    [Serializable]
    public sealed class AdministratorDal : BaseContext<AdministratorDal>, IAdministratorDal
    {
        public AdministratorDal()
        {

        }

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<AdminDto> Fetch(decimal userId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Administrators
                        .Include(admin => admin.PreferredCity).AsQueryable()
                        where c.UserId == userId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid userId");
            }

            return new AdminDto()
            {
                EmailAddress = data.EmailAddress,
                FirstName = data.FirstName,
                GenderId = data.GenderId,
                GenderName = ((Enumeration.Gender)data.GenderId).ToString(),
                IsActive = data.IsActive,
                LastName = data.LastName,
                PrefixId = data.PrefixId,
                PrefixName = ((Enumeration.Prefix)data.GenderId).ToString(),
                TypeId = data.TypeId,
                TypeName = ((Enumeration.AdminType)data.TypeId).ToString(),
                UserId = data.UserId,
                Password = data.Password,
                CityId = data.PreferredCityId,
                EnableButtonForRestaurantLogin = data.EnableButtonForRestaurantLogin
            };
        }

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<AdminDto> Fetch(string emailAddress, string password)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Administrators
                        .Include(admin => admin.PreferredCity).AsQueryable()
                        where c.EmailAddress == emailAddress && c.Password == password
                        select c).SingleOrDefault();

            if (data == null)
            {
                return null;
            }

            return new AdminDto()
            {
                EmailAddress = data.EmailAddress,
                FirstName = data.FirstName,
                GenderId = data.GenderId,
                GenderName = string.Empty,
                IsActive = data.IsActive,
                LastName = data.LastName,
                PrefixId = data.PrefixId,
                PrefixName = string.Empty,
                TypeId = data.TypeId,
                TypeName = ((Enumeration.AdminType)data.TypeId).ToString(),
                UserId = data.UserId,
                CityId = data.PreferredCityId,
                Password = data.Password,
                EnableButtonForRestaurantLogin = data.EnableButtonForRestaurantLogin
            };
        }

        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public async Task<AdminDto> Fetch(string emailAddress)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Administrators
                        .Include(admin => admin.PreferredCity).AsQueryable()
                        where c.EmailAddress == emailAddress
                        select c).SingleOrDefault();

            if (data == null)
            {
                return null;
            }

            return new AdminDto()
            {
                EmailAddress = data.EmailAddress,
                FirstName = data.FirstName,
                GenderId = data.GenderId,
                GenderName = string.Empty,
                IsActive = data.IsActive,
                LastName = data.LastName,
                PrefixId = data.PrefixId,
                PrefixName = string.Empty,
                TypeId = data.TypeId,
                TypeName = ((Enumeration.AdminType)data.TypeId).ToString(),
                UserId = data.UserId,
                CityId = data.PreferredCityId,
                Password = data.Password,
                EnableButtonForRestaurantLogin = data.EnableButtonForRestaurantLogin
            };
        }

        /// <summary>
        /// Fetch admin list
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="adminType"></param>
        /// <param name="emailAddress"></param>
        /// <param name="adminName"></param>
        /// <param name="status"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<AdminDto>> FetchAdminList(Enumeration.AdminType? adminType = null, string emailAddress = null, string adminName = null,
            bool? status = null, int currentPage = 0, int pageSize = 10)
        {
            await Task.FromResult(1);
            var query = (from u in Context.Administrators
                         .Include(admin => admin.PreferredCity).AsQueryable()
                         where u.TypeId == (int)Enumeration.AdminType.AccountAdmin || u.TypeId == (int)Enumeration.AdminType.SalesAdmin
                         orderby u.FirstName, u.LastName
                         select new
                         {
                             adminName = u.FirstName + " " + u.LastName,
                             u.EmailAddress,
                             u.FirstName,
                             u.GenderId,
                             GenderName = Enumeration.Gender.NotDefined.ToString(),
                             Status = u.IsActive,
                             u.LastName,
                             u.Password,
                             u.PrefixId,
                             PrefixName = Enumeration.Prefix.NotDefined.ToString(),
                             u.TypeId,
                             TypeName = ((Enumeration.AdminType)u.TypeId).ToString(),
                             u.UserId,
                             CityId = u.PreferredCityId,
                             CityName = u.PreferredCity.Name,
                             u.EnableButtonForRestaurantLogin
                         });

            if (adminType != null)
            {
                query = query.Where(x => x.TypeId == (int)adminType);
            }
            if (emailAddress != null)
            {
                query = query.Where(x => x.EmailAddress.Contains(emailAddress));
            }
            if (adminName != null)
            {
                query = query.Where(x => x.adminName.Contains(adminName));
            }
            if (status != null)
            {
                query = query.Where(x => x.Status == status);
            }

            var data = (from c in query
                        select new AdminDto()
                        {
                            CityId = c.CityId,
                            CityName = c.CityName,
                            EmailAddress = c.EmailAddress,
                            FirstName = c.FirstName,
                            GenderId = c.GenderId,
                            GenderName = c.GenderName,
                            IsActive = c.Status,
                            LastName = c.LastName,
                            Password = c.Password,
                            PrefixId = c.PrefixId,
                            PrefixName = c.PrefixName,
                            TypeId = c.TypeId,
                            TypeName = c.TypeName,
                            UserId = c.UserId,
                            EnableButtonForRestaurantLogin = c.EnableButtonForRestaurantLogin
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }

            return data;
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(AdminDto dto)
        {
            await Task.FromResult(1);
            var data = new Administrator()
            {
                EmailAddress = dto.EmailAddress,
                Password = dto.Password ?? string.Empty,
                GenderId = dto.GenderId,
                IsActive = dto.IsActive ?? false,
                PrefixId = dto.PrefixId,
                TypeId = dto.TypeId,
                LastName = dto.LastName,
                FirstName = dto.FirstName,
                CreatedDate = DateTime.Now,
                PreferredCityId = dto.CityId,
                EnableButtonForRestaurantLogin = dto.EnableButtonForRestaurantLogin
            };
            Context.Administrators.Add(data);
            Context.SaveChanges();

            dto.UserId = data.UserId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(AdminDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Administrators.AsQueryable()
                        where c.UserId == dto.UserId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid userId");
            }

            data.EmailAddress = dto.EmailAddress ?? data.EmailAddress;
            data.Password = dto.Password ?? data.Password;
            data.LastName = dto.LastName ?? data.LastName;
            data.FirstName = dto.FirstName ?? data.FirstName;
            data.IsActive = dto.IsActive ?? data.IsActive;
            data.PreferredCityId = dto.CityId;
            data.EnableButtonForRestaurantLogin = dto.EnableButtonForRestaurantLogin;

            Context.SaveChanges();
        }
    }
}
