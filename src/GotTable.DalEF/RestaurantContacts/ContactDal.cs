﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantContacts;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantContacts
{
    [Serializable]
    public sealed class ContactDal : BaseContext<ContactDal>, IContactDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<ContactDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchContacts.AsQueryable()
                        where c.BranchId == restaurantId
                        orderby c.Id descending
                        select new ContactDto()
                        {
                            BranchId = restaurantId,
                            CreatedDate = c.CreatedDate,
                            EmailAddress = c.EmailAddress,
                            Id = c.Id,
                            IsActive = c.IsActive,
                            IsEmailAlert = c.IsEmailAlert,
                            IsPhoneAlert = c.IsPhoneAlert,
                            Name = c.Name,
                            PhoneNumber = c.PhoneNumber,
                            TypeId = c.TypeId,
                            TypeName = ((Enumeration.ContactType)c.TypeId).ToString(),
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public async Task<ContactDto> Fetch(decimal contactId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchContacts.AsQueryable()
                        where c.Id == contactId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid contact Id");
            }

            return new ContactDto()
            {
                BranchId = data.BranchId,
                Id = data.Id,
                CreatedDate = data.CreatedDate,
                EmailAddress = data.EmailAddress,
                IsActive = data.IsActive,
                IsEmailAlert = data.IsEmailAlert,
                IsPhoneAlert = data.IsPhoneAlert,
                Name = data.Name,
                PhoneNumber = data.PhoneNumber,
                TypeId = data.TypeId,
                TypeName = ((Enumeration.ContactType)data.TypeId).ToString()
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(ContactDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchContact()
            {
                BranchId = dto.BranchId,
                CreatedDate = dto.CreatedDate,
                EmailAddress = dto.EmailAddress,
                Id = dto.Id,
                IsActive = dto.IsActive,
                IsEmailAlert = dto.IsEmailAlert,
                IsPhoneAlert = dto.IsPhoneAlert,
                Name = dto.Name,
                PhoneNumber = dto.PhoneNumber,
                TypeId = dto.TypeId
            };

            Context.BranchContacts.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(ContactDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchContacts.AsQueryable()
                        where c.Id == dto.Id
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid contactId");
            }

            data.IsActive = dto.IsActive;
            data.IsEmailAlert = dto.IsEmailAlert;
            data.IsPhoneAlert = dto.IsPhoneAlert;
            data.Name = dto.Name;
            data.PhoneNumber = dto.PhoneNumber;
            data.EmailAddress = dto.EmailAddress;
            data.TypeId = dto.TypeId;

            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contactId"></param>
        /// <returns></returns>
        public async Task Delete(decimal contactId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchContacts.AsQueryable()
                        where c.Id == contactId
                        select c).SingleOrDefault();

            if (data != null)
            {
                Context.BranchContacts.Remove(data);
            }

            Context.SaveChanges();
        }
    }
}
