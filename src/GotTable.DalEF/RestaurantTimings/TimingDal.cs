﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantTimings;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantTimings
{
    [Serializable]
    public sealed class TimingDal : BaseContext<TimingDal>, ITimingDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<List<TimingDto>> FetchList(decimal restaurantId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchTimings.AsQueryable()
                        where c.BranchId == restaurantId
                        orderby c.BranchCategoryId, c.DayId
                        select new TimingDto()
                        {
                            BranchId = c.BranchId,
                            CategoryId = c.BranchCategoryId,
                            DayId = c.DayId,
                            DinnerEndTime = c.DinnerEndTime,
                            DinnerStartTime = c.DinnerStartTime,
                            Id = c.Id,
                            IsClosed = c.IsClosed,
                            LunchEndTime = c.LunchEndTime,
                            LunchStartTime = c.LunchStartTime,
                            CategoryName = ((Enumeration.RestaurantTypes)c.BranchCategoryId).ToString(),
                            DayName = ((Enumeration.WeekDays)c.DayId).ToString()
                        }).ToList();

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantCategoryId"></param>
        /// <returns></returns>
        public async Task<List<TimingDto>> Fetch(decimal restaurantId, int restaurantCategoryId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchTimings.AsQueryable()
                        where c.BranchId == restaurantId && c.BranchCategoryId == restaurantCategoryId
                        orderby c.BranchCategoryId, c.DayId
                        select new TimingDto()
                        {
                            BranchId = c.BranchId,
                            CategoryId = c.BranchCategoryId,
                            DayId = c.DayId,
                            DinnerEndTime = c.DinnerEndTime,
                            DinnerStartTime = c.DinnerStartTime,
                            Id = c.Id,
                            IsClosed = c.IsClosed,
                            LunchEndTime = c.LunchEndTime,
                            LunchStartTime = c.LunchStartTime,
                            CategoryName = ((Enumeration.RestaurantTypes)c.BranchCategoryId).ToString(),
                            DayName = ((Enumeration.WeekDays)c.DayId).ToString()
                        }).ToList();

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(TimingDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchTiming()
            {
                BranchCategoryId = dto.CategoryId,
                BranchId = dto.BranchId,
                DayId = dto.DayId,
                DinnerEndTime = "00:00",
                DinnerStartTime = "00:00",
                IsClosed = dto.IsClosed,
                LunchEndTime = "00:00",
                LunchStartTime = "00:00"
            };

            Context.BranchTimings.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(TimingDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchTimings.AsQueryable()
                        where c.Id == dto.Id
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid timingId");
            }

            data.DinnerEndTime = dto.DinnerEndTime;
            data.DinnerStartTime = dto.DinnerStartTime;
            data.LunchEndTime = dto.LunchEndTime;
            data.LunchStartTime = dto.LunchStartTime;
            data.IsClosed = dto.IsClosed;

            Context.SaveChanges();

            dto.Id = data.Id;
        }
    }
}
