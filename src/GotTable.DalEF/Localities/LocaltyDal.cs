﻿using GotTable.Dal.Localities;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Localities
{
    [Serializable]
    public sealed class LocaltyDal : BaseContext<LocaltyDal>, ILocaltyDal
    {
        /// <summary>
        /// FetchList
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="active"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<IEnumerable<LocaltyDto>> FetchList(bool? active = null ,int currentPage = default, int pageSize = 10)
        {
            await Task.FromResult(1);
            var query = (from c in Context.Localities.AsQueryable()
                         select c);

            if (active != null)
            {
                query = query.Where(x => x.Active == active);
            }

            var data = (from c in query
                        select new LocaltyDto()
                        {
                            Id = c.Id,
                            Active = c.Active,
                            Name = c.Name
                        }).ToList();

            if (data != null && currentPage != default)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }

            return data;
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(LocaltyDto dto)
        {
            await Task.FromResult(1);
            var data = new Locality()
            {
                Name = dto.Name,
                Active = true
            };
            Context.Localities.Add(data);
            Context.SaveChanges();
        }
    }
}
