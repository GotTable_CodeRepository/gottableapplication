﻿using GotTable.Common.Enumerations;
using GotTable.Dal.Reports.Bookings;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Reports.Bookings
{
    [Serializable]
    public sealed class BookingDal : BaseContext<BookingDal>, IBookingDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BookingDto>> FetchList(DateTime bookingDate, int currentPage = 0, int pageSize = 10)
        {
            await Task.FromResult(1);
            var dateForBooiking = bookingDate.Date;

            var data = (from d in Context.DineInBookings
                        .Include(booking => booking.Branch)
                        .Include(booking => booking.User)
                        .AsQueryable()
                        where d.BookingDate == dateForBooiking
                        select new BookingDto()
                        {
                            BranchId = d.BranchId,
                            PhoneNumber = d.PhoneNumber,
                            BranchName = d.Branch.BranchName,
                            BookingType = Enumeration.BookingType.DineIn.ToString(),
                            BranchAddress = d.Branch.AddressLine1 + " " + d.Branch.AddressLine2,
                            BranchCity = d.Branch.City,
                            BranchState = d.Branch.State,
                            BranchZipCode = d.Branch.Zipcode,
                            UserName = d.User.FirstName + " " + d.User.LastName,
                            BookingDate = d.BookingDate,
                            BookingTime = d.BookingTime,
                            CreatedDate = d.CreatedDate
                        }).ToList();

            var deliveryandTakeawayData = (from d in Context.DeliveryandTakeawayBookings
                                           .Include(booking => booking.Branch)
                                           .Include(booking => booking.User)
                                           .AsQueryable()
                                           where d.BookingDate == dateForBooiking
                                           select new BookingDto()
                                           {
                                               BranchId = d.BranchId,
                                               PhoneNumber = d.PhoneNumber,
                                               BranchName = d.Branch.BranchName,
                                               BookingType = ((Enumeration.BookingType)d.BookingTypeId).ToString(),
                                               BranchAddress = d.Branch.AddressLine1 + " " + d.Branch.AddressLine2,
                                               BranchCity = d.Branch.City,
                                               BranchState = d.Branch.State,
                                               BranchZipCode = d.Branch.Zipcode,
                                               UserName = d.User.FirstName + " " + d.User.LastName,
                                               BookingDate = d.BookingDate,
                                               BookingTime = d.BookingTime,
                                               CreatedDate = d.CreatedDate
                                           }).ToList();

            data.AddRange(deliveryandTakeawayData);
            data = data.OrderByDescending(x => x.BookingDate).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingStartDate"></param>
        /// <param name="bookingEndDate"></param>
        /// <param name="restaurantName"></param>
        /// <param name="restaurantCategory"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BookingDto>> FetchList(DateTime bookingStartDate, DateTime bookingEndDate, string restaurantName, string restaurantCategory, int currentPage = 0, int pageSize = 10)
        {
            await Task.FromResult(1);
            var dateForStartBooiking = bookingStartDate.Date;
            var dateForEndBooking = bookingEndDate.Date;

            var data = (from d in Context.DineInBookings
                        .Include(booking => booking.Branch)
                        .Include(booking => booking.User)
                        .AsQueryable()
                        where d.BookingDate >= dateForStartBooiking && d.BookingDate <= dateForEndBooking
                        select new BookingDto()
                        {
                            BranchId = d.BranchId,
                            PhoneNumber = d.PhoneNumber,
                            BranchName = d.Branch.BranchName,
                            BookingType = Enumeration.BookingType.DineIn.ToString(),
                            BranchAddress = d.Branch.AddressLine1 + " " + d.Branch.AddressLine2,
                            BranchCity = d.Branch.City,
                            BranchState = d.Branch.State,
                            BranchZipCode = d.Branch.Zipcode,
                            UserName = d.User.FirstName + " " + d.User.LastName,
                            BookingDate = d.BookingDate,
                            BookingTime = d.BookingTime,
                            CreatedDate = d.CreatedDate
                        }).ToList();

            var deliveryandTakeawayData = (from d in Context.DeliveryandTakeawayBookings
                                           .Include(booking => booking.Branch)
                                           .Include(booking => booking.User)
                                           .AsQueryable()
                                           where d.BookingDate >= dateForStartBooiking && d.BookingDate <= dateForEndBooking
                                           select new BookingDto()
                                           {
                                               BranchId = d.BranchId,
                                               PhoneNumber = d.PhoneNumber,
                                               BranchName = d.Branch.BranchName,
                                               BookingType = ((Enumeration.BookingType)d.BookingTypeId).ToString(),
                                               BranchAddress = d.Branch.AddressLine1 + " " + d.Branch.AddressLine2,
                                               BranchCity = d.Branch.City,
                                               BranchState = d.Branch.State,
                                               BranchZipCode = d.Branch.Zipcode,
                                               UserName = d.User.FirstName + " " + d.User.LastName,
                                               BookingDate = d.BookingDate,
                                               BookingTime = d.BookingTime,
                                               CreatedDate = d.CreatedDate
                                           }).ToList();

            data.AddRange(deliveryandTakeawayData);

            if (!string.IsNullOrEmpty(restaurantName))
            {
                data = data.Where(x => x.BranchName.ToLower().Contains(restaurantName.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(restaurantCategory))
            {
                data = data.Where(x => x.BookingType == restaurantCategory).ToList();
            }
            data = data.OrderByDescending(x => x.BookingDate).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }
    }
}
