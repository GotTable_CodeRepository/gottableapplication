﻿using GotTable.Dal.Reports.Restaurants;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Reports.Restaurants
{
    [Serializable]
    public sealed class RestaurantDal : BaseContext<RestaurantDal>, IRestaurantDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantDto>> FetchExpiredList(int currentPage = 0, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from HB in Context.HotelBranches
                        join BI in Context.BranchInvoices 
                        on HB.BranchId equals BI.BranchId
                        where BI.IsActive == true && BI.EndDate < DateTime.Now
                        select new RestaurantDto()
                        {
                            RestaurantId = BI.BranchId,
                            Address = HB.AddressLine1 + " " + HB.AddressLine2 + " " + HB.City + " " + HB.State + " " + HB.Zipcode,
                            ExpireDate = BI.EndDate,
                            Name = HB.BranchName
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }

            return data;
        }
    }
}
