﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantOffers;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantOffers
{
    [Serializable]
    public sealed class OfferDal : BaseContext<OfferDal>, IOfferDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="restaurantCategory"></param>
        /// <param name="offerType"></param>
        /// <param name="bookingDate"></param>
        /// <param name="expiredOffers"></param>
        /// <param name="validNow"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<OfferDto>> FetchList(decimal? restaurantId = null, Enumeration.RestaurantTypes? restaurantType = null, Enumeration.RestaurantCategories? restaurantCategory = null, Enumeration.OfferType? offerType = null, DateTime? bookingDate = null, bool? expiredOffers = null, bool? validNow = null, int currentPage = 0, int pageSize = 10)
        {
            await Task.FromResult(1);
            var query = (from c in Context.BranchOffers
                         select new
                         {
                             c.BranchId,
                             c.Description,
                             c.EndDate,
                             c.Id,
                             c.IsActive,
                             c.IsDelivery,
                             c.IsDineIn,
                             c.IsTakeAway,
                             c.Name,
                             c.StartDate,
                             c.Branch.CategoryId,
                             OfferTypeId = c.TypeId,
                             ExpiredOffers = c.EndDate <= DateTime.Now,
                             ValidNow = c.IsActive == true && c.StartDate < DateTime.Now && c.EndDate > DateTime.Now
                         });

            if (restaurantId != null)
            {
                query = query.Where(x => x.BranchId == restaurantId);
            }

            if (restaurantType != null)
            {
                if (restaurantType == Enumeration.RestaurantTypes.DineIn)
                {
                    query = query.Where(x => x.IsDineIn == true);
                }
                else if (restaurantType == Enumeration.RestaurantTypes.Delivery)
                {
                    query = query.Where(x => x.IsDelivery == true);
                }
                else if (restaurantType == Enumeration.RestaurantTypes.Takeaway)
                {
                    query = query.Where(x => x.IsTakeAway == true);
                }
            }

            if (restaurantCategory != null)
            {
                query = query.Where(x => x.CategoryId == (int)restaurantCategory);
            }

            if (offerType != null)
            {
                query = query.Where(x => x.OfferTypeId == (int)offerType);
            }

            if (bookingDate != null)
            {
                query = query.Where(x => x.EndDate > bookingDate);
            }

            if (expiredOffers != null)
            {
                query = query.Where(x => x.ExpiredOffers == expiredOffers);
            }

            if (validNow != null)
            {
                query = query.Where(x => x.ValidNow == validNow);
            }

            var data = (from c in query
                        orderby c.EndDate
                        select new OfferDto()
                        {
                            BranchId = c.BranchId,
                            EndDate = c.EndDate,
                            Description = c.Description,
                            Id = c.Id,
                            IsActive = c.IsActive,
                            IsDelivery = c.IsDelivery,
                            IsDineIn = c.IsDineIn,
                            IsTakeAway = c.IsTakeAway,
                            Name = c.Name,
                            OfferType = (Enumeration.OfferType)c.OfferTypeId,
                            StartDate = c.StartDate,
                            TypeId = c.OfferTypeId
                        }).ToList();

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerId"></param>
        /// <returns></returns>
        public async Task<OfferDto> Fetch(decimal offerId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchOffers
                        .Include(offer => offer.Category).AsQueryable()
                        where c.Id == offerId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid offerId");
            }

            return new OfferDto()
            {
                Id = data.Id,
                BranchId = data.BranchId,
                Description = data.Description,
                EndDate = data.EndDate,
                IsActive = data.IsActive,
                IsDelivery = data.IsDelivery,
                IsDineIn = data.IsDineIn,
                IsTakeAway = data.IsTakeAway,
                Name = data.Name,
                StartDate = data.StartDate,
                TypeId = data.TypeId,
                CategoryId = data.CategoryId,
                CategoryName = data.Category?.Name,
                OfferType = (Enumeration.OfferType)data.TypeId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(OfferDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchOffer()
            {
                BranchId = dto.BranchId,
                Description = dto.Description,
                EndDate = dto.EndDate,
                IsActive = dto.IsActive,
                IsDelivery = dto.IsDelivery,
                IsDineIn = dto.IsDineIn,
                IsTakeAway = dto.IsTakeAway,
                Name = dto.Name,
                StartDate = dto.StartDate,
                TypeId = dto.TypeId,
                CategoryId = dto.CategoryId
            };

            Context.BranchOffers.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(OfferDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchOffers.AsQueryable()
                        where c.Id == dto.Id
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid offerId");
            }

            data.IsActive = dto.IsActive;
            data.IsDelivery = dto.IsDelivery;
            data.IsDineIn = dto.IsDineIn;
            data.IsTakeAway = dto.IsTakeAway;
            data.Name = dto.Name;
            data.Description = dto.Description;
            data.EndDate = dto.EndDate;
            data.StartDate = dto.StartDate;
            data.TypeId = dto.TypeId;
            data.CategoryId = dto.CategoryId;

            Context.SaveChanges();
        }
    }
}
