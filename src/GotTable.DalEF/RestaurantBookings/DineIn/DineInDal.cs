﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantBookings.DineIn;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantBookings.DineIn
{
    [Serializable]
    public sealed class DineInDal : BaseContext<DineInDal>, IDineInDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<DineInDto> Fetch(decimal bookingId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.DineInBookings
                        .Include(b => b.Branch)
                        .Include(b => b.Table)
                        .Include(b => b.Offer)
                        .Include(b => b.DineInBookingBillUpload)
                        .Include(b => b.DineInStatuses).ThenInclude(d => d.Admin)
                        .Include(b => b.DineInStatuses).ThenInclude(d => d.User)
                        .AsQueryable()
                        where c.BookingId == bookingId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid Dine In booking Id");
            }

            return new DineInDto()
            {
                BookingDate = data.BookingDate,
                BookingTime = data.BookingTime,
                BranchId = data.BranchId,
                Comment = data.Comment,
                CreatedDate = data.CreatedDate,
                CurrentStatusId = data.CurrentStatusId,
                Id = data.BookingId,
                IsRead = data.IsRead,
                OfferId = data.OfferId,
                OfferTitle = data.Offer?.Name,
                OfferDescription = data.Offer?.Description,
                OfferTypeId = data.Offer?.TypeId,
                PhoneNumber = data.PhoneNumber,
                PromoCode = data.PromoCode,
                TableId = data.TableId,
                UserId = data.UserId,
                TableName = data.Table != null ? ((Enumeration.Tables)data.Table.TableId).ToString() : "",
                StatusList = data.DineInStatuses.OrderBy(x => x.CreatedDate).Select(item => new DineInStatusDto()
                {
                    BookingId = data.BookingId,
                    Comment = item.Comment,
                    UserId = item.UserId,
                    CreatedDate = item.CreatedDate,
                    Id = item.Id,
                    StatusName = ((Enumeration.DeliveryandTakeawayStatusType)item.StatusId).ToString(),
                    StatusId = item.StatusId,
                    UserName = item.User?.FirstName + " " + item.User?.LastName,
                    AdminName = item.Admin?.FirstName + " " + item.Admin?.LastName,
                    AdminId = item.AdminId
                }).ToList(),
                RewardApplied = data.RewardApplied ?? false,
                BillUploadStatus = data.BillUploadStatus,
                RewardPoint = data.DineInBookingBillUpload?.AuthorizedRewardPoint ?? 0,
                DoubleTheDealActive = data.DoubleTheDealActive
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(DineInDto dto)
        {
            await Task.FromResult(1);
            dto.RewardApplied = CheckForReward.Execute(dto.UserId, dto.BranchId);
            dto.DoubleTheDealActive = CheckForDoubleTheDeal.Execute(dto.UserId, dto.BranchId, dto.BookingDate, dto.OfferId);

            var data = new DineInBooking()
            {
                BookingDate = dto.BookingDate,
                BookingTime = dto.BookingTime,
                BranchId = dto.BranchId,
                Comment = dto.Comment,
                CreatedDate = dto.CreatedDate,
                CurrentStatusId = dto.CurrentStatusId,
                IsRead = dto.IsRead,
                OfferId = dto.OfferId,
                PhoneNumber = dto.PhoneNumber,
                PromoCode = dto.PromoCode,
                TableId = dto.TableId,
                UserId = dto.UserId,
                RewardApplied = false,
                BillUploadStatus = (int)Enumeration.BillUpload.Pending,
                DoubleTheDealActive = dto.DoubleTheDealActive
            };

            Context.DineInBookings.Add(data);
            Context.SaveChanges();

            dto.Id = data.BookingId;

            var currentStatus = new DineInStatus()
            {
                BookingId = data.BookingId,
                Comment = string.Empty,
                StatusId = (int)Enumeration.DineInStatusType.Confirm,
                UserId = dto.UserId,
                CreatedDate = dto.CreatedDate
            };

            Context.DineInStatuses.Add(currentStatus);
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(DineInStatusDto dto)
        {
            await Task.FromResult(1);
            var dataBooking = (from c in Context.DineInBookings
                               where c.BookingId == dto.BookingId
                               select c).SingleOrDefault();

            if (dataBooking == null)
            {
                throw new Exception("Invalid booking Id");
            }

            var dataStatus = new DineInStatus()
            {
                BookingId = dto.BookingId,
                Comment = dto.Comment,
                CreatedDate = DateTime.Now,
                StatusId = dto.StatusId,
                UserId = dto.UserId,
                AdminId = dto.AdminId
            };

            dataBooking.DineInStatuses.Add(dataStatus);
            dataBooking.CurrentStatusId = (int)dataStatus.StatusId;

            if (dataStatus.StatusId != (int)Enumeration.DineInStatusType.Complete)
                dataBooking.BillUploadStatus = (int)Enumeration.BillUpload.Done;

            Context.SaveChanges();

            dto.Id = dataStatus.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<bool> MarkAsRead(decimal bookingId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.DineInBookings.AsQueryable()
                        where c.BookingId == bookingId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid Dine In booking Id");
            }

            data.IsRead = true;

            Context.SaveChanges();

            return true;
        }
    }
}
