﻿using GotTable.DalEF.Shared;
using System;
using System.Linq;

namespace GotTable.DalEF.RestaurantBookings.DineIn
{
    [Serializable]
    internal sealed class CheckForDoubleTheDeal
    {
        private CheckForDoubleTheDeal()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentBookingRestaurantId"></param>
        /// <param name="currentBookingDateTime"></param>
        /// <param name="currentBookingOfferId"></param>
        /// <returns></returns>
        public static bool Execute(decimal userId, decimal currentBookingRestaurantId, DateTime currentBookingDateTime, decimal? currentBookingOfferId)
        {
            var doubleTheDeal = false;
            using (var ctx = new GotTableRepository())
            {
                var doubleTheDealBookingId = (from c in ctx.Users
                                              where c.UserId == userId
                                              select c).SingleOrDefault().DoubleTheDealBookingId;
                if (doubleTheDealBookingId != null)
                {
                    var pastBooking = (from c in ctx.DineInBookings
                                       where c.BookingId == doubleTheDealBookingId && c.UserId == userId
                                       select c).SingleOrDefault();

                    if (pastBooking.BookingDate >= currentBookingDateTime && pastBooking.BranchId == currentBookingRestaurantId && pastBooking.OfferId == currentBookingOfferId)
                    {
                        doubleTheDeal = true;
                    }
                }
            }
            return doubleTheDeal;
        }
    }
}
