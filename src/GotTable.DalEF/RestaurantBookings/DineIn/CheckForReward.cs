﻿using System;
using System.Linq;

namespace GotTable.DalEF.RestaurantBookings
{
    [Serializable]
    internal sealed class CheckForReward
    {
        private CheckForReward()
        {

        }

        public static bool Execute(decimal userId, decimal branchId)
        {
            bool reward = false;
            using (var ctx = new GotTableRepository())
            {
                var branchConfiguration = (from b in ctx.BranchConfigurations
                                           where b.BranchId == branchId
                                           select b).SingleOrDefault();

                if (branchConfiguration.DineInReward != null && branchConfiguration.DineInRewardActive == true)
                {
                    if (branchConfiguration.DineInReward > 0)
                    {
                        var userBooking = (from c in ctx.DineInBookings
                                           where c.UserId == userId && c.BranchId == branchId && c.CurrentStatusId != 1
                                           select new { }).Count() + 1;

                        if (userBooking >= branchConfiguration.DineInReward)
                        {
                            return userBooking % branchConfiguration.DineInReward == 0;
                        }
                    }
                }
            }

            return reward;
        }
    }
}
