﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantBookings.BillUploads;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantBookings.BillUploads
{
    [Serializable]
    public sealed class BillUploadDal : BaseContext<BillUploadDal>, IBillUploadDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadStatus"></param>
        /// <param name="emailAddress"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BillUploadListItemDto>> FetchList(Enumeration.BillUpload? uploadStatus = null, string emailAddress = null, string phoneNumber = null, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var query = (from c in Context.DineInBookingBillUploads
                         .Include(billUpload => billUpload.Booking).ThenInclude(booking => booking.Branch)
                         .Include(billUpload => billUpload.User).ThenInclude(user => user.PhoneNumbers)
                         orderby c.UploadedDate descending
                         select new
                         {
                             c.Amount,
                             BookingDate = c.Booking.BookingDate.ToString() + " " + c.Booking.BookingTime,
                             c.BookingId,
                             c.User.EmailAddress,
                             c.ImagePath,
                             PhoneNumber = c.User.PhoneNumbers.FirstOrDefault(x => x.UserId == c.UserId) != null ? c.User.PhoneNumbers.FirstOrDefault(x => x.UserId == c.UserId).Value : 0,
                             RestaurantAddress = c.Booking.Branch.AddressLine1 + " " + c.Booking.Branch.AddressLine2 + " " + c.Booking.Branch.City,
                             RestaurantName = c.Booking.Branch.BranchName,
                             c.UploadedDate,
                             c.UserId,
                             UserName = c.User.FirstName + " " + c.User.LastName,
                             UploadStatus = c.Booking.BillUploadStatus
                         });

            if (!string.IsNullOrEmpty(emailAddress))
            {
                query = query.Where(x => x.EmailAddress == emailAddress);
            }
            if (!string.IsNullOrEmpty(phoneNumber))
            {
                query = query.Where(x => x.PhoneNumber == decimal.Parse(phoneNumber));
            }
            if (uploadStatus != null)
            {
                query = query.Where(x => x.UploadStatus == (int)uploadStatus);
            }

            var data = (from c in query
                        select new BillUploadListItemDto
                        {
                            Amount = c.Amount,
                            BookingDate = c.BookingDate.ToString(),
                            BookingId = c.BookingId,
                            EmailAddress = c.EmailAddress,
                            ImagePath = c.ImagePath,
                            PhoneNumber = c.PhoneNumber,
                            RestaurantAddress = c.RestaurantAddress,
                            RestaurantName = c.RestaurantName,
                            UploadedDate = c.UploadedDate,
                            UserId = c.UserId,
                            UserName = c.UserName
                        }).ToList();
            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<BillUploadDto> Fetch(int bookingId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.DineInBookingBillUploads
                        .Include(billUpload => billUpload.Booking).ThenInclude(booking => booking.Branch)
                        .Include(billUpload => billUpload.User).ThenInclude(user => user.PhoneNumbers)
                        .AsQueryable()
                        where c.BookingId == bookingId
                        select c).SingleOrDefault();

            if (data == null)
            {
                return null;
            }

            return new BillUploadDto()
            {
                UploadedDate = data.UploadedDate,
                BookingId = data.BookingId,
                UserId = data.UserId,
                Amount = data.Amount,
                Authorized = data.Authorized,
                AuthorizedDate = data.AuthorizedDate,
                AuthorizedRewardPoint = data.AuthorizedRewardPoint,
                AuthorizedComment = data.AuthorizedComment,
                RedeemComment = data.RedeemComment,
                Redeem = data.Redeem,
                RedeemDate = data.RedeemDate,
                ImagePath = data.ImagePath,
                EmailAddress = data.User.EmailAddress,
                PhoneNumber = data.User.PhoneNumbers.Count > 0 ? data.User.PhoneNumbers.FirstOrDefault().Value.ToString() : "0",
                RestaurantName = data.Booking.Branch.BranchName,
                RestaurantAddress = data.Booking.Branch.AddressLine1 + " " + data.Booking.Branch.AddressLine2 + " " + data.Booking.Branch.City + " " + data.Booking.Branch.State + " " + data.Booking.Branch.Zipcode,
                UserName = data.User.FirstName + " " + data.User.LastName,
                OfferName = data.Booking?.Offer != null ? data.Booking.Offer?.Name : string.Empty,
                OfferType = data.Booking?.Offer != null ? ((Enumeration.OfferType)data.Booking?.Offer?.TypeId).ToString() : string.Empty,
                BookingDate = data.Booking.BookingDate.ToString("dd-MM-yyyy") + " " + data.Booking.BookingTime,
                AdminId = data.AdminId,
                AdminName = data.Admin?.FirstName + " " + data.Admin?.LastName
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(BillUploadDto dto)
        {
            await Task.FromResult(1);
            dto.UploadedDate = DateTime.Now;

            var data = (from c in Context.DineInBookings
                        .Include(booking => booking.DineInBookingBillUpload)
                        .AsQueryable()
                        where c.BookingId == dto.BookingId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid Dine In booking Id");
            }
            data.DineInBookingBillUpload = new DineInBookingBillUpload()
            {
                UploadedDate = dto.UploadedDate,
                BookingId = dto.BookingId,
                UserId = dto.UserId,
                Amount = dto.Amount,
                ImagePath = dto.ImagePath,
                Authorized = null,
                AuthorizedDate = null,
                AuthorizedRewardPoint = null,
                AuthorizedComment = null,
                RedeemComment = null,
                Redeem = false,
                RedeemDate = null,
                AdminId = null
            };
            data.BillUploadStatus = (int)Enumeration.BillUpload.InProcessing;
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(BillUploadDto dto)
        {
            await Task.FromResult(1);
            var data = (from d in Context.DineInBookings
                        .Include(booking => booking.DineInBookingBillUpload)
                        .Include(booking => booking.User)
                        where d.BookingId == dto.BookingId
                        select d).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid Dine In Booking Id");
            }

            if (data.OfferId != null && dto.Authorized.Value)
            {
                data.User.DoubleTheDealBookingId = data.BookingId;
            }

            data.DineInBookingBillUpload.Amount = dto.Amount;
            data.DineInBookingBillUpload.ImagePath = dto.ImagePath;
            data.DineInBookingBillUpload.Redeem = dto.Redeem;
            data.DineInBookingBillUpload.RedeemDate = dto.RedeemDate;
            data.DineInBookingBillUpload.RedeemComment = dto.RedeemComment;
            data.DineInBookingBillUpload.Authorized = dto.Authorized;
            data.DineInBookingBillUpload.AuthorizedComment = dto.AuthorizedComment;
            data.DineInBookingBillUpload.AuthorizedDate = dto.AuthorizedDate;
            data.DineInBookingBillUpload.AuthorizedRewardPoint = dto.AuthorizedRewardPoint;
            data.DineInBookingBillUpload.AdminId = dto.AdminId;
            data.BillUploadStatus = dto.Authorized == null ? data.BillUploadStatus : (int)Enumeration.BillUpload.Done;
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task Delete(int bookingId)
        {
            await Task.FromResult(1);
            var data = (from d in Context.DineInBookingBillUploads
                        where d.BookingId == bookingId
                        select d).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid Dine In Booking Id");
            }

            data.Booking.BillUploadStatus = 0;

            Context.DineInBookingBillUploads.Remove(data);
            Context.SaveChanges();
        }
    }
}
