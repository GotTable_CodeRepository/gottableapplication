﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantBookings.DeliveryandTakeaway;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantBookings.DeliveryandTakeaway
{
    [Serializable]
    public sealed class DeliveryandTakeawayDal : BaseContext<DeliveryandTakeawayDal>, IDeliveryandTakeawayDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<DeliveryandTakeawayDto> Fetch(decimal bookingId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.DeliveryandTakeawayBookings
                        .Include(booking => booking.DeliveryAddresses)
                        .Include(booking => booking.DeliveryandTakeawayStatuses).ThenInclude(status => status.User)
                        .Include(booking => booking.DeliveryandTakeawayStatuses).ThenInclude(status => status.Admin)
                        .Include(booking => booking.DeliveryandTakeawayCartItems).ThenInclude(item => item.Menu)
                        .AsQueryable()
                        where c.BookingId == bookingId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid exceptionId");
            }

            return new DeliveryandTakeawayDto()
            {
                BookingDate = data.BookingDate,
                BookingId = data.BookingId,
                BookingTime = data.BookingTime,
                BookingTypeId = data.BookingTypeId,
                BranchId = data.BranchId,
                CartAmount = data.CartAmount,
                CentralGST = data.CentralGst,
                Comment = data.Comment,
                CreatedDate = data.CreatedDate,
                CurrentStatusId = data.CurrentStatusId,
                DeliveryCharges = data.DeliveryCharges,
                IsRead = data.IsRead,
                OfferId = data.OfferId,
                OfferTitle = data.Offer != null ? data.Offer.Name : string.Empty,
                OfferDescription = data.Offer != null ? data.Offer.Description : string.Empty,
                PackingCharges = data.PackingCharges,
                PhoneNumber = data.PhoneNumber,
                PromoCode = data.PromoCode,
                ServiceCharges = data.ServiceCharges,
                StateGST = data.StateGst,
                UserId = data.UserId,
                DeliveryAddress = data.DeliveryAddresses.Select(item => new DeliveryAddressDto()
                {
                    BookingId = data.BookingId,
                    City = item.City,
                    Id = item.Id,
                    Landmark = item.Landmark,
                    Latitude = item.Latitude,
                    Line1 = item.AddressLine1,
                    Line2 = item.AddressLine2,
                    Longitude = item.Longitude,
                    State = item.State,
                    UserId = item.UserId,
                    Zip = item.Zip
                }).FirstOrDefault(),
                ItemList = data.DeliveryandTakeawayCartItems.OrderBy(x => x.Id).Select(item => new DeliveryandTakeawayCartIttemDto()
                {
                    Cost = item.Menu.Cost * item.Quantity,
                    ItemId = item.Id,
                    MenuDescription = item.Menu.Description,
                    MenuId = item.Menu.Id,
                    MenuName = item.Menu.Name,
                    Quantity = item.Quantity,
                    Remark = item.Remark
                }).ToList(),
                StatusList = data.DeliveryandTakeawayStatuses.OrderBy(x => x.CreatedDate).Select(item => new DeliveryandTakeawayStatusDto()
                {
                    BookingId = data.BookingId,
                    Comment = item.Comment,
                    UserId = item.UserId,
                    CreatedDate = item.CreatedDate,
                    Id = item.Id,
                    StatusName = ((Enumeration.DeliveryandTakeawayStatusType)item.StatusId).ToString(),
                    StatusId = item.StatusId,
                    UserName = item.User?.FirstName + " " + item.User?.LastName,
                    AdminName = item.Admin?.FirstName + " " + item.Admin?.LastName,
                    AdminId = item.AdminId
                }).ToList()
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(DeliveryandTakeawayDto dto)
        {
            var data = new DeliveryandTakeawayBooking()
            {
                BookingDate = dto.BookingDate,
                BookingTime = dto.BookingTime,
                BookingTypeId = dto.BookingTypeId,
                BranchId = dto.BranchId,
                CartAmount = dto.CartAmount,
                CentralGst = dto.CentralGST,
                Comment = dto.Comment,
                CreatedDate = dto.CreatedDate,
                CurrentStatusId = dto.CurrentStatusId,
                DeliveryCharges = dto.DeliveryCharges,
                IsRead = dto.IsRead,
                OfferId = dto.OfferId == 0 ? null : dto.OfferId,
                PackingCharges = dto.PackingCharges,
                PhoneNumber = dto.PhoneNumber,
                ServiceCharges = dto.ServiceCharges,
                PromoCode = dto.PromoCode,
                StateGst = dto.StateGST,
                UserId = dto.UserId,
            };
            Context.DeliveryandTakeawayBookings.Add(data);
            Context.SaveChanges();

            dto.BookingId = data.BookingId;
            var currentStatusDto = new DeliveryandTakeawayStatusDto()
            {
                BookingId = data.BookingId,
                Comment = dto.Comment,
                StatusId = (int)Enumeration.DeliveryandTakeawayStatusType.Confirm,
                CreatedDate = dto.CreatedDate
            };
            await Insert(currentStatusDto, dto.BookingId, dto.UserId, null);
            if (dto.DeliveryAddress != null)
            {
                await Insert(dto.DeliveryAddress, dto.BookingId, dto.UserId);
            }
            await Insert(dto.ItemList, dto.BookingId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="bookingId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task Insert(DeliveryAddressDto dto, decimal bookingId, decimal userId)
        {
            await Task.FromResult(1);
            var data = new DeliveryAddress()
            {
                AddressLine1 = dto.Line1,
                AddressLine2 = dto.Line2,
                BookingId = bookingId,
                City = dto.City,
                Landmark = dto.Landmark,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                State = dto.State,
                Zip = dto.Zip,
                UserId = userId
            };
            Context.DeliveryAddresses.Add(data);
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtos"></param>
        /// <param name="bookingId"></param>
        public async Task Insert(List<DeliveryandTakeawayCartIttemDto> dtos, decimal bookingId)
        {
            await Task.FromResult(1);
            foreach (var dto in dtos)
            {
                var data = new DeliveryandTakeawayCartItem()
                {
                    BookingId = bookingId,
                    MenuId = dto.MenuId,
                    Quantity = dto.Quantity,
                    Id = 0,
                    Remark = dto.Remark
                };
                Context.DeliveryandTakeawayCartItems.Add(data);
            }
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(DeliveryandTakeawayStatusDto dto)
        {
            await Task.FromResult(1);
            var dataBooking = (from c in Context.DeliveryandTakeawayBookings
                               where c.BookingId == dto.BookingId
                               select c).SingleOrDefault();

            if (dataBooking == null)
            {
                throw new Exception("Invalid booking Id");
            }

            var data = new DeliveryandTakeawayStatus()
            {
                BookingId = dto.BookingId,
                Comment = dto.Comment,
                CreatedDate = DateTime.Now,
                StatusId = dto.StatusId,
                UserId = dto.UserId,
                AdminId = dto.AdminId
            };

            dataBooking.CurrentStatusId = data.StatusId;

            Context.DeliveryandTakeawayStatuses.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<bool> MarkAsRead(decimal bookingId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.DeliveryandTakeawayBookings.AsQueryable()
                        where c.BookingId == bookingId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid booking Id");
            }

            data.IsRead = true;

            Context.SaveChanges();

            return true;
        }


        #region Helpers method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="bookingId"></param>
        /// <param name="userId"></param>
        /// <param name="adminId"></param>
        /// <returns></returns>
        private async Task Insert(DeliveryandTakeawayStatusDto dto, decimal bookingId, decimal? userId, decimal? adminId)
        {
            await Task.FromResult(1);
            var data = new DeliveryandTakeawayStatus()
            {
                BookingId = bookingId,
                Comment = dto.Comment,
                CreatedDate = dto.CreatedDate,
                StatusId = dto.StatusId,
                UserId = userId,
                AdminId = adminId
            };
            Context.DeliveryandTakeawayStatuses.Add(data);
            Context.SaveChanges();
        }

        #endregion
    }
}
