﻿using GotTable.Common.Enumerations;
using GotTable.Dal.RestaurantBookings.Rewards;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantBookings.Rewards
{
    [Serializable]
    public sealed class LoyaltyDal : BaseContext<LoyaltyDal>, ILoyaltyDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<LoyaltyDto>> FetchList(decimal restaurantId, decimal userId)
        {
            await Task.FromResult(1);
            var data = (from d in Context.DineInBookings.AsQueryable()
                        where d.BranchId == restaurantId && d.UserId == userId && d.CurrentStatusId == (int)Enumeration.DineInStatusType.Complete
                        select new LoyaltyDto()
                        {
                            BookingId = d.BookingId,
                            BranchId = restaurantId,
                            UserId = d.UserId
                        }).ToList();

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(LoyaltyDto dto)
        {
            await Task.FromResult(1);
            var data = new LoyaltyRewardTransactionLog()
            {
                BookingId = dto.BookingId,
                BranchId = dto.BranchId,
                CreatedDate = dto.CreatedDate,
                UserId = dto.UserId
            };

            Context.LoyaltyRewardTransactionLogs.Add(data);
            Context.SaveChanges();

            dto.RewardId = data.RewardId;
        }
    }
}
