﻿using GotTable.Dal.RestaurantDocuments;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantDocuments
{
    [Serializable]
    public sealed class DocumentDal : BaseContext<DocumentDal>, IDocumentDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="imageCategoryId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<DocumentDto>> FetchList(decimal restaurantId, int? imageCategoryId = default, int currentPage = default, int pageSize = default)
        {
            await Task.FromResult(1);
            var source = (from c in Context.BranchImages
                          where c.BranchId == restaurantId
                          select new
                          {
                              c.Active,
                              c.DocumentId,
                              c.CategoryId,
                              c.Name,
                              c.Extension
                          });

            if (imageCategoryId != null)
            {
                source = source.Where(m => m.CategoryId == imageCategoryId);
            }

            var data = (from c in source
                        select new DocumentDto()
                        {
                            Active = c.Active,
                            BranchId = restaurantId,
                            CategoryId = c.CategoryId,
                            DocumentId = c.DocumentId,
                            Extension = c.Extension,
                            Name = c.Name
                        }).ToList();

            if (data != null && currentPage != default)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantDocumentId"></param>
        /// <returns></returns>
        public async Task<DocumentDto> Fetch(int restaurantDocumentId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchImages.AsQueryable()
                        where c.DocumentId == restaurantDocumentId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid cuisineId");
            }

            return new DocumentDto()
            {
                BranchId = data.BranchId,
                Active = data.Active,
                CategoryId = data.CategoryId,
                DocumentId = restaurantDocumentId,
                Extension = data.Extension,
                Name = data.Name
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(DocumentDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchImage()
            {
                BranchId = dto.BranchId,
                Active = dto.Active,
                CategoryId = dto.CategoryId,
                Extension = dto.Extension,
                Name = dto.Name
            };

            Context.BranchImages.Add(data);
            Context.SaveChanges();

            dto.DocumentId = data.DocumentId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Update(DocumentDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchImages.AsQueryable()
                        where c.DocumentId == dto.DocumentId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid documentId");
            }

            data.Active = dto.Active;

            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public async Task Delete(int documentId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchImages.AsQueryable()
                        where c.DocumentId == documentId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid documentId");
            }

            Context.BranchImages.Remove(data);
            Context.SaveChanges();
        }
    }
}
