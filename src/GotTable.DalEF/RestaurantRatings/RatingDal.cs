﻿using GotTable.Dal.RestaurantRatings;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.RestaurantRatings
{
    [Serializable]
    public sealed class RatingDal : BaseContext<RatingDal>, IRatingDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RatingDto>> FetchList(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchRatings
                        .Include(rating => rating.User)
                        .AsQueryable()
                        where c.BranchId == restaurantId
                        orderby c.Id descending
                        select new RatingDto()
                        {
                            BranchId = c.BranchId,
                            Comment = c.Comment,
                            CreatedDate = c.CreatedDate,
                            Id = c.Id,
                            IsActive = c.IsActive,
                            Rating = c.Rating,
                            Title = c.Title,
                            TypeId = c.User.TypeId,
                            UserId = c.User.UserId,
                            UserName = c.User.FirstName + " " + c.User.LastName,
                            UserType = string.Empty,
                            AmbienceRating = c.AmbienceRating,
                            EmailAddress = c.User.EmailAddress,
                            FoodRating = c.FoodRating,
                            MusicRating = c.MusicRating,
                            PriceRating = c.PriceRating,
                            ServiceRating = c.ServiceRating
                        }).ToList();

            if (data != null)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<AvgRatingDto> FetchAvgRating(decimal restaurantId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchAvgRatings
                        where c.BranchId == restaurantId
                        select c).SingleOrDefault();

            if (data != null)
            {
                return new AvgRatingDto()
                {
                    AmbienceRating = data.AvgAmbienceRating ?? 1,
                    FoodRating = data.AvgFoodRating ?? 1,
                    MusicRating = data.AvgMusicRating ?? 1,
                    PriceRating = data.AvgPriceRating ?? 1,
                    Rating = data.AvgRating ?? 1,
                    ServiceRating = data.AvgServiceRating ?? 1
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reviewId"></param>
        /// <returns></returns>
        public async Task<RatingDto> Fetch(decimal reviewId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchRatings
                        .Include(rating => rating.User)
                        .AsQueryable()
                        where c.Id == reviewId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid review Id");
            }

            return new RatingDto()
            {
                BranchId = data.BranchId,
                Comment = data.Comment,
                CreatedDate = data.CreatedDate,
                Id = data.Id,
                IsActive = data.IsActive,
                Rating = data.Rating,
                Title = data.Title,
                TypeId = data.User.TypeId,
                UserId = data.User.UserId,
                UserName = data.User.FirstName + " " + data.User.LastName,
                UserType = string.Empty,
                AmbienceRating = data.AmbienceRating,
                EmailAddress = data.User.EmailAddress,
                MusicRating = data.MusicRating,
                FoodRating = data.FoodRating,
                PriceRating = data.PriceRating,
                ServiceRating = data.ServiceRating
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task Insert(RatingDto dto)
        {
            await Task.FromResult(1);
            var data = new BranchRating()
            {
                BranchId = dto.BranchId,
                Comment = dto.Comment,
                CreatedDate = dto.CreatedDate,
                IsActive = dto.IsActive,
                Rating = dto.Rating,
                Title = dto.Title,
                UserId = dto.UserId,
                AmbienceRating = dto.AmbienceRating,
                FoodRating = dto.FoodRating,
                MusicRating = dto.MusicRating,
                PriceRating = dto.PriceRating,
                ServiceRating = dto.ServiceRating
            };

            Context.BranchRatings.Add(data);
            Context.SaveChanges();

            dto.Id = data.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ratingId"></param>
        /// <returns></returns>
        public async Task Delete(decimal ratingId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.BranchRatings
                        where c.Id == ratingId
                        select c).SingleOrDefault();

            if (data != null)
            {
                Context.BranchRatings.Remove(data);
                Context.SaveChanges();
            }
        }
    }
}
