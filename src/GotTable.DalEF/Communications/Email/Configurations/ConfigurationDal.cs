﻿using GotTable.Dal.Communications.Email.Configurations;
using GotTable.DalEF.Shared;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Communications.Email.Configurations
{
    /// <summary>
    /// ConfigurationDal
    /// </summary>
    [Serializable]
    public sealed class ConfigurationDal : BaseContext<ConfigurationDal>, IConfigurationDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="configurationTypeId"></param>
        /// <returns></returns>
        public async Task<ConfigurationDto> Fetch(int configurationTypeId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.EmailConfigurations.AsQueryable()
                        where c.Id == configurationTypeId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid configurationTypeId");
            }

            return new ConfigurationDto()
            {
                HostName = data.HostName,
                Id = data.Id,
                IsActive = data.IsActive,
                IsSSL = data.IsSsl,
                IsTLS = data.IsTls,
                PortNumber = data.PortNumber
            };
        }
    }
}
