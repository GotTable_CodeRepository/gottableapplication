﻿using GotTable.Dal.Communications.Email.Content;
using GotTable.DalEF.Shared;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Communications.Email.Content
{
    /// <summary>
    /// ContentDal
    /// </summary>
    [Serializable]
    public sealed class ContentDal : BaseContext<ContentDal>, IContentDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public async Task<ContentDto> Fetch(decimal templateId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.EmailContents.AsQueryable()
                        where c.TemplateTypeId == templateId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid templateId");
            }

            return new ContentDto()
            {
                EmailBody = data.EmailBody,
                EmailCC = data.EmailCc,
                EmailSubject = data.EmailSubject,
                EmailTo = data.EmailTo,
                IsActive = data.IsActive,
                TemplateTypeId = data.TemplateTypeId
            };
        }
    }
}
