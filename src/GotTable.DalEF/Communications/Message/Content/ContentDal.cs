﻿using GotTable.Dal.Communications.Message.Content;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Communications.Message.Content
{
    /// <summary>
    /// ContentDal
    /// </summary>
    [Serializable]
    public sealed class ContentDal : BaseContext<ContentDal>, IContentDal
    { 
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public async Task<ContentDto> Fetch(int templateId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.TransmissionContents.AsQueryable()
                        where c.TypeId == templateId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new Exception("Invalid templateId");
            }

            return new ContentDto()
            {
                ContentTypeId = data.TypeId,
                IsActive = data.IsActive ?? false,
                Message = data.Message
            };
        }
    }
}
