﻿using GotTable.Dal.Communications.Message.TwoFactor;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.Communications.Message.TwoFactor
{
    /// <summary>
    /// TwoFactorDal
    /// </summary>
    [Serializable]
    public sealed class TwoFactorDal : BaseContext<TwoFactorDal>, ITwoFactorDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public async Task<TwoFactorDto> Fetch(int templateId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.TwoFactorMessageTemplates.AsQueryable()
                        where c.TemplateId == templateId
                        select new TwoFactorDto()
                        {
                            Name = c.Name,
                            QueryString = c.QueryString,
                            TemplateId = c.TemplateId,
                            SenderId = c.SenderId
                        }).SingleOrDefault();

            return data;
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(TwoFactorLogDto dto)
        {
            await Task.FromResult(1);
            var data = new TwoFactorMessageTransactionLog()
            {
                PhoneNumber = dto.PhoneNumber,
                TemplateId = dto.TemplateId,
                CreatedDate = DateTime.Now,
                ExternalId = dto.ExternalId,
                TargetUrlWithParameters = dto.TargetUrlWithParameters
            };

            Context.TwoFactorMessageTransactionLogs.Add(data);
            Context.SaveChanges();

            dto.LogId = data.LogId;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(TwoFactorLogDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.TwoFactorMessageTransactionLogs.AsQueryable()
                        where c.LogId == dto.LogId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid LogId");
            }

            data.TransactionId = dto.TransactionId;
            data.TransactionStatus = dto.TransactionStatus;

            Context.SaveChanges();
        }
    }
}
