﻿using GotTable.Common.Enumerations;
using System;
using System.Threading.Tasks;
using System.Linq;
using GotTable.Dal.Communications.Message.OTP;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;

namespace GotTable.DalEF.Communications.Message.OTP
{
    /// <summary>
    /// TransactionDal
    /// </summary>
    [Serializable]
    public sealed class TransactionDal : BaseContext<TransactionDal>, ITransactionDal
    {
        /// <summary>
        /// Fetch
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public async Task<TransactionDto> Fetch(decimal transactionId)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Otptransactions.AsQueryable()
                        where c.TransactionId == transactionId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid transactionId");
            }

            return new TransactionDto()
            {
                CreatedDate = data.CreatedDate,
                ExternalId = data.ExternalId,
                IsUsed = data.IsUsed,
                OTP = data.Otp,
                PhoneNumber = data.PhoneNumber,
                TransactionId = data.TransactionId,
                TypeId = data.TypeId,
                TypeName = ((Enumeration.OTPTransaction)data.TypeId).ToString()
            };
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(TransactionDto dto)
        {
            await Task.FromResult(1);
            var data = new Otptransaction()
            {
                CreatedDate = dto.CreatedDate,
                ExternalId = dto.ExternalId,
                Otp = dto.OTP,
                PhoneNumber = dto.PhoneNumber,
                TypeId = dto.TypeId,
                IsUsed = false
            };
            Context.Otptransactions.Add(data);
            Context.SaveChanges();

            dto.TransactionId = data.TransactionId;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(TransactionDto dto)
        {
            await Task.FromResult(1);
            var data = (from c in Context.Otptransactions.AsQueryable()
                        where c.TransactionId == dto.TransactionId
                        select c).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid exceptionId");
            }

            data.IsUsed = dto.IsUsed;
            Context.SaveChanges();
        }
    }
}
