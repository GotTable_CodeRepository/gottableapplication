﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class Cuisine
    {
        public Cuisine()
        {
            BranchCusinies = new HashSet<BranchCusiny>();
        }

        public decimal Id { get; set; }
        public string CuisineName { get; set; }
        public bool IsActive { get; set; }
        public int? CategoryId { get; set; }
        public int DisplayLevel { get; set; }

        public virtual ICollection<BranchCusiny> BranchCusinies { get; set; }
    }
}
