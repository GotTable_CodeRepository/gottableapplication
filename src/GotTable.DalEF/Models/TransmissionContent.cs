﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class TransmissionContent
    {
        public int TypeId { get; set; }
        public string Message { get; set; }
        public bool? IsActive { get; set; }
    }
}
