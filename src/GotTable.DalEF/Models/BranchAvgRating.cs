﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchAvgRating
    {
        public decimal BranchId { get; set; }
        public int? AvgRating { get; set; }
        public int? AvgAmbienceRating { get; set; }
        public int? AvgFoodRating { get; set; }
        public int? AvgMusicRating { get; set; }
        public int? AvgPriceRating { get; set; }
        public int? AvgServiceRating { get; set; }
    }
}
