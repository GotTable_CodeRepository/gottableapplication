﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class Device
    {
        public string Id { get; set; }
        public int TypeId { get; set; }
        public decimal? UserId { get; set; }
        public bool IsActive { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string City { get; set; }
        public DateTime? LastModifiedDate { get; set; }

        public virtual User User { get; set; }
    }
}
