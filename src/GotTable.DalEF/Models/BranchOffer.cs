﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchOffer
    {
        public BranchOffer()
        {
            DeliveryandTakeawayBookings = new HashSet<DeliveryandTakeawayBooking>();
            DineInBookings = new HashSet<DineInBooking>();
        }

        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelivery { get; set; }
        public bool? IsTakeAway { get; set; }
        public bool? IsDineIn { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public int? CategoryId { get; set; }

        public virtual HotelBranch Branch { get; set; }
        public virtual OfferCategory Category { get; set; }
        public virtual ICollection<DeliveryandTakeawayBooking> DeliveryandTakeawayBookings { get; set; }
        public virtual ICollection<DineInBooking> DineInBookings { get; set; }
    }
}
