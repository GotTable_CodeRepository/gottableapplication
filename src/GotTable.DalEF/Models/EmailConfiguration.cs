﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class EmailConfiguration
    {
        public int Id { get; set; }
        public string HostName { get; set; }
        public int PortNumber { get; set; }
        public bool IsSsl { get; set; }
        public bool IsTls { get; set; }
        public bool IsActive { get; set; }
    }
}
