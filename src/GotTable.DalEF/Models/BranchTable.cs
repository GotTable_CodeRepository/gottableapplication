﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchTable
    {
        public BranchTable()
        {
            DineInBookings = new HashSet<DineInBooking>();
        }

        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public decimal TableId { get; set; }
        public int Value { get; set; }
        public bool? IsActive { get; set; }

        public virtual HotelBranch Branch { get; set; }
        public virtual ICollection<DineInBooking> DineInBookings { get; set; }
    }
}
