﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchImage
    {
        public int DocumentId { get; set; }
        public decimal BranchId { get; set; }
        public int? CategoryId { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public bool Active { get; set; }

        public virtual HotelBranch Branch { get; set; }
    }
}
