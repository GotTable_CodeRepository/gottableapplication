﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class HotelBranch
    {
        public HotelBranch()
        {
            BranchAmenities = new HashSet<BranchAmenity>();
            BranchContacts = new HashSet<BranchContact>();
            BranchCusinies = new HashSet<BranchCusiny>();
            BranchImages = new HashSet<BranchImage>();
            BranchInvoices = new HashSet<BranchInvoice>();
            BranchMenus = new HashSet<BranchMenu>();
            BranchOffers = new HashSet<BranchOffer>();
            BranchRatings = new HashSet<BranchRating>();
            BranchTables = new HashSet<BranchTable>();
            BranchTimings = new HashSet<BranchTiming>();
            DeliveryandTakeawayBookings = new HashSet<DeliveryandTakeawayBooking>();
            DineInBookings = new HashSet<DineInBooking>();
        }

        public decimal BranchId { get; set; }
        public string BranchName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public decimal Zipcode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool? IsActive { get; set; }
        public decimal? SalesPersonId { get; set; }
        public decimal? AccountAdminPersonId { get; set; }
        public bool? OpenforAccountAdmin { get; set; }
        public bool? Deleted { get; set; }
        public string TagLine { get; set; }
        public string Seokeyword { get; set; }
        public string Seodescription { get; set; }
        public string Seotitle { get; set; }
        public int? CategoryId { get; set; }
        public int? CostForTwo { get; set; }
        public string Description { get; set; }
        public decimal? LocalityId1 { get; set; }
        public decimal? LocalityId2 { get; set; }
        public decimal? LocalityId3 { get; set; }
        public decimal? LocalityId4 { get; set; }
        public decimal? AdminId { get; set; }

        public virtual Administrator AccountAdminPerson { get; set; }
        public virtual Administrator Admin { get; set; }
        public virtual Locality LocalityId1Navigation { get; set; }
        public virtual Locality LocalityId2Navigation { get; set; }
        public virtual Locality LocalityId3Navigation { get; set; }
        public virtual Locality LocalityId4Navigation { get; set; }
        public virtual Administrator SalesPerson { get; set; }
        public virtual BranchConfiguration BranchConfiguration { get; set; }
        public virtual PreferredRestaurant PreferredRestaurant { get; set; }
        public virtual ICollection<BranchAmenity> BranchAmenities { get; set; }
        public virtual ICollection<BranchContact> BranchContacts { get; set; }
        public virtual ICollection<BranchCusiny> BranchCusinies { get; set; }
        public virtual ICollection<BranchImage> BranchImages { get; set; }
        public virtual ICollection<BranchInvoice> BranchInvoices { get; set; }
        public virtual ICollection<BranchMenu> BranchMenus { get; set; }
        public virtual ICollection<BranchOffer> BranchOffers { get; set; }
        public virtual ICollection<BranchRating> BranchRatings { get; set; }
        public virtual ICollection<BranchTable> BranchTables { get; set; }
        public virtual ICollection<BranchTiming> BranchTimings { get; set; }
        public virtual ICollection<DeliveryandTakeawayBooking> DeliveryandTakeawayBookings { get; set; }
        public virtual ICollection<DineInBooking> DineInBookings { get; set; }
    }
}
