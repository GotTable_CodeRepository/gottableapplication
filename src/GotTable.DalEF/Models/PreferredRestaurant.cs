﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class PreferredRestaurant
    {
        public int IndexValue { get; set; }
        public decimal RestaurantId { get; set; }
        public string Comment { get; set; }
        public bool? Status { get; set; }
        public decimal? CityId { get; set; }

        public virtual City City { get; set; }
        public virtual HotelBranch Restaurant { get; set; }
    }
}
