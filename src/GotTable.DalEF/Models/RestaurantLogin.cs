﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class RestaurantLogin
    {
        public decimal BranchId { get; set; }
        public string BranchName { get; set; }
        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public decimal UserId { get; set; }
    }
}
