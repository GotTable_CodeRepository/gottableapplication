﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class City
    {
        public City()
        {
            Administrators = new HashSet<Administrator>();
            Localities = new HashSet<Locality>();
            PreferredRestaurants = new HashSet<PreferredRestaurant>();
            PushNotifications = new HashSet<PushNotification>();
        }

        public decimal CityId { get; set; }
        public decimal? StateId { get; set; }
        public string Name { get; set; }
        public bool? Active { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public virtual ICollection<Administrator> Administrators { get; set; }
        public virtual ICollection<Locality> Localities { get; set; }
        public virtual ICollection<PreferredRestaurant> PreferredRestaurants { get; set; }
        public virtual ICollection<PushNotification> PushNotifications { get; set; }
    }
}
