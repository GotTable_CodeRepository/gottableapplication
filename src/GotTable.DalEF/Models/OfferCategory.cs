﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class OfferCategory
    {
        public OfferCategory()
        {
            BranchOffers = new HashSet<BranchOffer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Extension { get; set; }
        public bool? DineInActive { get; set; }
        public bool? DeliveryActive { get; set; }
        public bool? TakeawayActive { get; set; }

        public virtual ICollection<BranchOffer> BranchOffers { get; set; }
    }
}
