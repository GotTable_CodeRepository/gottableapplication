﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class User
    {
        public User()
        {
            Addresses = new HashSet<Address>();
            BranchRatings = new HashSet<BranchRating>();
            DeliveryAddresses = new HashSet<DeliveryAddress>();
            DeliveryandTakeawayBookings = new HashSet<DeliveryandTakeawayBooking>();
            DeliveryandTakeawayStatuses = new HashSet<DeliveryandTakeawayStatus>();
            Devices = new HashSet<Device>();
            DineInBookingBillUploads = new HashSet<DineInBookingBillUpload>();
            DineInBookings = new HashSet<DineInBooking>();
            DineInStatuses = new HashSet<DineInStatus>();
            PhoneNumbers = new HashSet<PhoneNumber>();
        }

        public decimal UserId { get; set; }
        public int TypeId { get; set; }
        public int? PrefixId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? GenderId { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsEmailVerified { get; set; }
        public bool? IsEmailOptedForCommunication { get; set; }
        public decimal? DoubleTheDealBookingId { get; set; }

        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<BranchRating> BranchRatings { get; set; }
        public virtual ICollection<DeliveryAddress> DeliveryAddresses { get; set; }
        public virtual ICollection<DeliveryandTakeawayBooking> DeliveryandTakeawayBookings { get; set; }
        public virtual ICollection<DeliveryandTakeawayStatus> DeliveryandTakeawayStatuses { get; set; }
        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<DineInBookingBillUpload> DineInBookingBillUploads { get; set; }
        public virtual ICollection<DineInBooking> DineInBookings { get; set; }
        public virtual ICollection<DineInStatus> DineInStatuses { get; set; }
        public virtual ICollection<PhoneNumber> PhoneNumbers { get; set; }
    }
}
