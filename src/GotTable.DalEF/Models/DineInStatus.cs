﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class DineInStatus
    {
        public decimal Id { get; set; }
        public decimal BookingId { get; set; }
        public decimal StatusId { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal? UserId { get; set; }
        public decimal? AdminId { get; set; }

        public virtual Administrator Admin { get; set; }
        public virtual DineInBooking Booking { get; set; }
        public virtual User User { get; set; }
    }
}
