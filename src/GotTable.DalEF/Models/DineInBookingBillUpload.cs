﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class DineInBookingBillUpload
    {
        public decimal BookingId { get; set; }
        public decimal UserId { get; set; }
        public double Amount { get; set; }
        public string ImagePath { get; set; }
        public DateTime UploadedDate { get; set; }
        public int? AuthorizedRewardPoint { get; set; }
        public bool? Authorized { get; set; }
        public DateTime? AuthorizedDate { get; set; }
        public string AuthorizedComment { get; set; }
        public bool? Redeem { get; set; }
        public DateTime? RedeemDate { get; set; }
        public string RedeemComment { get; set; }
        public decimal? AdminId { get; set; }

        public virtual Administrator Admin { get; set; }
        public virtual DineInBooking Booking { get; set; }
        public virtual User User { get; set; }
    }
}
