﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchMenu
    {
        public BranchMenu()
        {
            DeliveryandTakeawayCartItems = new HashSet<DeliveryandTakeawayCartItem>();
        }

        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public decimal CuisineId { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }
        public bool IsDineAvailable { get; set; }
        public bool IsTakeAwayAvailable { get; set; }
        public bool IsDeliveryAvailable { get; set; }
        public bool? IsActive { get; set; }
        public int? MenuTypeId { get; set; }
        public string Description { get; set; }

        public virtual HotelBranch Branch { get; set; }
        public virtual BranchCusiny Cuisine { get; set; }
        public virtual MenuCategory Type { get; set; }
        public virtual ICollection<DeliveryandTakeawayCartItem> DeliveryandTakeawayCartItems { get; set; }
    }
}
