﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class MenuCategory
    {
        public MenuCategory()
        {
            BranchMenus = new HashSet<BranchMenu>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<BranchMenu> BranchMenus { get; set; }
    }
}
