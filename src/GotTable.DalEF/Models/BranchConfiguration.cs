﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchConfiguration
    {
        public decimal BranchId { get; set; }
        public int? DineInReward { get; set; }
        public string TakeawayStandBy { get; set; }
        public string DeliveryStandBy { get; set; }
        public string DeliveryCoverage { get; set; }
        public decimal? ServiceTax { get; set; }
        public decimal? DeliveryCharges { get; set; }
        public decimal? CentralGst { get; set; }
        public decimal? StateGst { get; set; }
        public bool? DineInRewardActive { get; set; }
        public bool? DineInAutoCompleteActive { get; set; }
        public int? DineInAutoCompleteHours { get; set; }

        public virtual HotelBranch Branch { get; set; }
    }
}
