﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class ApplicationSetting
    {
        public int Id { get; set; }
        public string GeoUrl { get; set; }
        public string OtpauthUrl { get; set; }
        public string OtpauthKey { get; set; }
        public string TwoFactorApiurl { get; set; }
        public string TwoFactorApiauthKey { get; set; }
        public byte[] ApnsCertificateContent { get; set; }
        public string ApnsCeritificatePassword { get; set; }
        public string FcmserverApikey { get; set; }
        public string AdminApplicationBaseUrl { get; set; }
        public string UserApplicationBaseUrl { get; set; }
        public string FcmsenderId { get; set; }
        public string ApplicationIdPackageName { get; set; }
        public string Fcmurl { get; set; }
        public bool? DineInActive { get; set; }
        public bool? DeliveryActive { get; set; }
        public bool? TakeawayActive { get; set; }
        public string RestaurantDirectory { get; set; }
        public string OfferDirectory { get; set; }
        public bool? ServiceBusFeatureEnable { get; set; }
        public bool? HangfireFeatureEnable { get; set; }
        public string NotificationDirectory { get; set; }
        public bool? DineInEmailTransmission { get; set; }
        public bool? DeliveryEmailTransmission { get; set; }
        public bool? TakeawayEmailTransmission { get; set; }
        public bool? DineInMessageTransmission { get; set; }
        public bool? DeliveryMessageTransmission { get; set; }
        public bool? TakeawayMessageTransmission { get; set; }
    }
}
