﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class TwoFactorMessageTransactionLog
    {
        public decimal LogId { get; set; }
        public string TargetUrlWithParameters { get; set; }
        public int TemplateId { get; set; }
        public decimal ExternalId { get; set; }
        public decimal PhoneNumber { get; set; }
        public string TransactionId { get; set; }
        public string TransactionStatus { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
