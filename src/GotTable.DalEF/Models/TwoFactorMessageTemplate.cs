﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class TwoFactorMessageTemplate
    {
        public int TemplateId { get; set; }
        public string Name { get; set; }
        public string SenderId { get; set; }
        public string QueryString { get; set; }
        public bool IsActive { get; set; }
    }
}
