﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class PushNotification
    {
        public PushNotification()
        {
            NotificationExceptionLogs = new HashSet<NotificationExceptionLog>();
        }

        public int NotificationId { get; set; }
        public decimal UserId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public int NotificationTypeId { get; set; }
        public decimal? CityId { get; set; }
        public DateTime ScheduleTime { get; set; }
        public bool Active { get; set; }
        public bool Completed { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public int CategoryTypeId { get; set; }
        public int OfferTypeId { get; set; }
        public string Extension { get; set; }
        public int? SuccessCount { get; set; }
        public int? FailureCount { get; set; }
        public string IosPayLoad { get; set; }
        public string AndroidPayLoad { get; set; }
        public int? StatusId { get; set; }

        public virtual City City { get; set; }
        public virtual Administrator User { get; set; }
        public virtual ICollection<NotificationExceptionLog> NotificationExceptionLogs { get; set; }
    }
}
