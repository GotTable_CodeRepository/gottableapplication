﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class EmailCredential
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsUsedForTransactions { get; set; }
        public bool IsUsedForPromotions { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public bool IsArchived { get; set; }
        public string DisplayName { get; set; }
        public string EmailReplyTo { get; set; }
    }
}
