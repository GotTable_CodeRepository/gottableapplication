﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchAmenity
    {
        public int Id { get; set; }
        public decimal BranchId { get; set; }
        public int AmenityId { get; set; }

        public virtual HotelBranch Branch { get; set; }
    }
}
