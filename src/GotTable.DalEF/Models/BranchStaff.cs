﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchStaff
    {
        public int Id { get; set; }
        public decimal BranchId { get; set; }
        public decimal UserId { get; set; }
        public bool IsActive { get; set; }

        public virtual HotelBranch Branch { get; set; }
        public virtual Administrator User { get; set; }
    }
}
