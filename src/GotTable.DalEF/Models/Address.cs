﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class Address
    {
        public decimal Id { get; set; }
        public int TypeId { get; set; }
        public decimal UserId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int ZipCode { get; set; }
        public bool? IsActive { get; set; }
        public string Latitude { get; set; }
        public string Longitide { get; set; }

        public virtual User User { get; set; }
    }
}
