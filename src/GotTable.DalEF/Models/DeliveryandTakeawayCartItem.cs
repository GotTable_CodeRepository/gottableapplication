﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class DeliveryandTakeawayCartItem
    {
        public decimal Id { get; set; }
        public decimal BookingId { get; set; }
        public decimal MenuId { get; set; }
        public int Quantity { get; set; }
        public string Remark { get; set; }

        public virtual DeliveryandTakeawayBooking Booking { get; set; }
        public virtual BranchMenu Menu { get; set; }
    }
}
