﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class DeviceNotification
    {
        public decimal Id { get; set; }
        public string MessageContent { get; set; }
        public bool IsArchived { get; set; }
        public bool PushStatus { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? PushedScheduleDateTime { get; set; }
        public decimal UserId { get; set; }

        public virtual Administrator User { get; set; }
    }
}
