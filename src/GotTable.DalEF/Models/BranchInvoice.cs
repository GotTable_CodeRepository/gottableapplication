﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchInvoice
    {
        public Guid InvoiceId { get; set; }
        public decimal BranchId { get; set; }
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PrescriptionTypeId { get; set; }
        public int? PaymentMethodId { get; set; }
        public bool? IsPaymentDone { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool? DineIn { get; set; }
        public bool? Delivery { get; set; }
        public bool? Takeaway { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual HotelBranch Branch { get; set; }
    }
}
