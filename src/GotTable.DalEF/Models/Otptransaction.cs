﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class Otptransaction
    {
        public decimal TransactionId { get; set; }
        public decimal ExternalId { get; set; }
        public decimal PhoneNumber { get; set; }
        public int TypeId { get; set; }
        public string Otp { get; set; }
        public bool? IsUsed { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
