﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class GottableStaging1Context : DbContext
    {
        public GottableStaging1Context()
        {
        }

        public GottableStaging1Context(DbContextOptions<GottableStaging1Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<AdminLogin> AdminLogins { get; set; }
        public virtual DbSet<Administrator> Administrators { get; set; }
        public virtual DbSet<Amenity> Amenities { get; set; }
        public virtual DbSet<ApplicationLog> ApplicationLogs { get; set; }
        public virtual DbSet<ApplicationSetting> ApplicationSettings { get; set; }
        public virtual DbSet<BranchAmenity> BranchAmenities { get; set; }
        public virtual DbSet<BranchAvgRating> BranchAvgRatings { get; set; }
        public virtual DbSet<BranchConfiguration> BranchConfigurations { get; set; }
        public virtual DbSet<BranchContact> BranchContacts { get; set; }
        public virtual DbSet<BranchCusiny> BranchCusinies { get; set; }
        public virtual DbSet<BranchImage> BranchImages { get; set; }
        public virtual DbSet<BranchInvoice> BranchInvoices { get; set; }
        public virtual DbSet<BranchMenu> BranchMenus { get; set; }
        public virtual DbSet<BranchOffer> BranchOffers { get; set; }
        public virtual DbSet<BranchRating> BranchRatings { get; set; }
        public virtual DbSet<BranchTable> BranchTables { get; set; }
        public virtual DbSet<BranchTiming> BranchTimings { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Cuisine> Cuisines { get; set; }
        public virtual DbSet<DeliveryAddress> DeliveryAddresses { get; set; }
        public virtual DbSet<DeliveryandTakeawayBooking> DeliveryandTakeawayBookings { get; set; }
        public virtual DbSet<DeliveryandTakeawayCartItem> DeliveryandTakeawayCartItems { get; set; }
        public virtual DbSet<DeliveryandTakeawayStatus> DeliveryandTakeawayStatuses { get; set; }
        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<DeviceNotification> DeviceNotifications { get; set; }
        public virtual DbSet<DineInBooking> DineInBookings { get; set; }
        public virtual DbSet<DineInBookingBillUpload> DineInBookingBillUploads { get; set; }
        public virtual DbSet<DineInStatus> DineInStatuses { get; set; }
        public virtual DbSet<EmailConfiguration> EmailConfigurations { get; set; }
        public virtual DbSet<EmailContent> EmailContents { get; set; }
        public virtual DbSet<EmailCredential> EmailCredentials { get; set; }
        public virtual DbSet<HotelBranch> HotelBranches { get; set; }
        public virtual DbSet<Locality> Localities { get; set; }
        public virtual DbSet<LoyaltyRewardTransactionLog> LoyaltyRewardTransactionLogs { get; set; }
        public virtual DbSet<MenuCategory> MenuCategories { get; set; }
        public virtual DbSet<NotificationExceptionLog> NotificationExceptionLogs { get; set; }
        public virtual DbSet<OfferCategory> OfferCategories { get; set; }
        public virtual DbSet<OperationExceptionLog> OperationExceptionLogs { get; set; }
        public virtual DbSet<Otptransaction> Otptransactions { get; set; }
        public virtual DbSet<PhoneNumber> PhoneNumbers { get; set; }
        public virtual DbSet<PreferredRestaurant> PreferredRestaurants { get; set; }
        public virtual DbSet<PushNotification> PushNotifications { get; set; }
        public virtual DbSet<RestaurantLogin> RestaurantLogins { get; set; }
        public virtual DbSet<TransmissionContent> TransmissionContents { get; set; }
        public virtual DbSet<TwoFactorMessageTemplate> TwoFactorMessageTemplates { get; set; }
        public virtual DbSet<TwoFactorMessageTransactionLog> TwoFactorMessageTransactionLogs { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Version> Versions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(GotTableRepository.ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Address>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AddressLine1)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.AddressLine2).HasMaxLength(500);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitide).HasMaxLength(50);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Addresses)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Addresses__UserI__5629CD9C");
            });

            modelBuilder.Entity<AdminLogin>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("AdminLogins");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PreferredCityId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.UserId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Administrator>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK__Administ__1788CC4C4112B9F6");

                entity.ToTable("Administrator");

                entity.Property(e => e.UserId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PreferredCityId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.PreferredCity)
                    .WithMany(p => p.Administrators)
                    .HasForeignKey(d => d.PreferredCityId)
                    .HasConstraintName("FK__Administr__Prefe__1C873BEC");
            });

            modelBuilder.Entity<Amenity>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ApplicationLog>(entity =>
            {
                entity.Property(e => e.ControllerName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeviceId)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Error)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Params)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ApplicationSetting>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AdminApplicationBaseUrl)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ApnsCeritificatePassword)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ApplicationIdPackageName).HasMaxLength(500);

                entity.Property(e => e.FcmsenderId)
                    .HasMaxLength(50)
                    .HasColumnName("FCMSenderId");

                entity.Property(e => e.FcmserverApikey)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("FCMServerAPIKey");

                entity.Property(e => e.Fcmurl)
                    .HasMaxLength(2000)
                    .HasColumnName("FCMUrl");

                entity.Property(e => e.GeoUrl)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.NotificationDirectory)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfferDirectory)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtpauthKey)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("OTPAuthKey");

                entity.Property(e => e.OtpauthUrl)
                    .HasMaxLength(2000)
                    .IsUnicode(false)
                    .HasColumnName("OTPAuthUrl");

                entity.Property(e => e.RestaurantDirectory)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TwoFactorApiauthKey)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("TwoFactorAPIAuthKey");

                entity.Property(e => e.TwoFactorApiurl)
                    .HasMaxLength(2000)
                    .IsUnicode(false)
                    .HasColumnName("TwoFactorAPIURl");

                entity.Property(e => e.UserApplicationBaseUrl)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<BranchAmenity>(entity =>
            {
                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchAmenities)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BranchAmenities_HotelBranches");
            });

            modelBuilder.Entity<BranchAvgRating>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("BranchAvgRating");

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<BranchConfiguration>(entity =>
            {
                entity.HasKey(e => e.BranchId);

                entity.ToTable("BranchConfiguration");

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.CentralGst)
                    .HasColumnType("decimal(16, 2)")
                    .HasColumnName("CentralGST");

                entity.Property(e => e.DeliveryCharges).HasColumnType("decimal(16, 2)");

                entity.Property(e => e.DeliveryCoverage).HasMaxLength(100);

                entity.Property(e => e.DeliveryStandBy).HasMaxLength(100);

                entity.Property(e => e.ServiceTax).HasColumnType("decimal(16, 2)");

                entity.Property(e => e.StateGst)
                    .HasColumnType("decimal(16, 2)")
                    .HasColumnName("StateGST");

                entity.Property(e => e.TakeawayStandBy).HasMaxLength(100);

                entity.HasOne(d => d.Branch)
                    .WithOne(p => p.BranchConfiguration)
                    .HasForeignKey<BranchConfiguration>(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchContact__Contact");
            });

            modelBuilder.Entity<BranchContact>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchContacts)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchContact_Contact");
            });

            modelBuilder.Entity<BranchCusiny>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CuisineId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchCusinies)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchCusines__Cuisines");

                entity.HasOne(d => d.Cuisine)
                    .WithMany(p => p.BranchCusinies)
                    .HasForeignKey(d => d.CuisineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchCus__Cuisi__59FA5E80");
            });

            modelBuilder.Entity<BranchImage>(entity =>
            {
                entity.HasKey(e => e.DocumentId);

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Extension)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchImages)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BranchImages_HotelBranches");
            });

            modelBuilder.Entity<BranchInvoice>(entity =>
            {
                entity.HasKey(e => e.InvoiceId)
                    .HasName("PK_BranchInvoiceses");

                entity.Property(e => e.InvoiceId).ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.PaymentDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchInvoices)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchInv__Branc__5AEE82B9");
            });

            modelBuilder.Entity<BranchMenu>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Cost).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CuisineId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchMenus)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchMen__Branc__5BE2A6F2");

                entity.HasOne(d => d.Cuisine)
                    .WithMany(p => p.BranchMenus)
                    .HasForeignKey(d => d.CuisineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchMen__Cuisi__5CD6CB2B");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.BranchMenus)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchMen__TypeI__5DCAEF64");
            });

            modelBuilder.Entity<BranchOffer>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchOffers)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchOff__Branc__5EBF139D");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.BranchOffers)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_BranchOffers_OfferCategories");
            });

            modelBuilder.Entity<BranchRating>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchRatings)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchRat__Branc__0E6E26BF");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.BranchRatings)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchRat__UserI__0F624AF8");
            });

            modelBuilder.Entity<BranchTable>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.TableId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchTables)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BranchTab__Branc__6383C8BA");
            });

            modelBuilder.Entity<BranchTiming>(entity =>
            {
                entity.ToTable("BranchTiming");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.DinnerEndTime).HasMaxLength(20);

                entity.Property(e => e.DinnerStartTime).HasMaxLength(20);

                entity.Property(e => e.LunchEndTime).HasMaxLength(20);

                entity.Property(e => e.LunchStartTime).HasMaxLength(20);

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.BranchTimings)
                    .HasForeignKey(d => d.BranchId)
                    .HasConstraintName("FK__BranchTim__Branc__6477ECF3");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.Property(e => e.CityId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Latitude)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.StateId).HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<Cuisine>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CuisineName)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<DeliveryAddress>(entity =>
            {
                entity.ToTable("DeliveryAddress");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AddressLine1)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.AddressLine2).HasMaxLength(500);

                entity.Property(e => e.BookingId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Landmark)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Latitude)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Longitude)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Zip).HasColumnType("numeric(6, 0)");

                entity.HasOne(d => d.Booking)
                    .WithMany(p => p.DeliveryAddresses)
                    .HasForeignKey(d => d.BookingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeliveryandTakeawayBooking_BookingAddresses");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DeliveryAddresses)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DeliveryA__UserI__51300E55");
            });

            modelBuilder.Entity<DeliveryandTakeawayBooking>(entity =>
            {
                entity.HasKey(e => e.BookingId)
                    .HasName("PK__Delivery__73951AED9E7F5DDF");

                entity.ToTable("DeliveryandTakeawayBooking");

                entity.Property(e => e.BookingId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BookingDate).HasColumnType("date");

                entity.Property(e => e.BookingTime)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.CartAmount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CentralGst)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("CentralGST");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DeliveryCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.OfferId)
                    .HasColumnType("numeric(18, 0)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PackingCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.PhoneNumber).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.PromoCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ServiceCharges).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.StateGst)
                    .HasColumnType("decimal(10, 2)")
                    .HasColumnName("StateGST");

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.DeliveryandTakeawayBookings)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeliveryandTakeawayBooking_Branchs");

                entity.HasOne(d => d.Offer)
                    .WithMany(p => p.DeliveryandTakeawayBookings)
                    .HasForeignKey(d => d.OfferId)
                    .HasConstraintName("FK_DeliveryandTakeawayBooking_BranchOffers");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DeliveryandTakeawayBookings)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeliveryandTakeawayBooking_Users");
            });

            modelBuilder.Entity<DeliveryandTakeawayCartItem>(entity =>
            {
                entity.ToTable("DeliveryandTakeawayCartItem");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BookingId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.MenuId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Remark)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.Booking)
                    .WithMany(p => p.DeliveryandTakeawayCartItems)
                    .HasForeignKey(d => d.BookingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeliveryandTakeawayBooking_BookingOrders");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.DeliveryandTakeawayCartItems)
                    .HasForeignKey(d => d.MenuId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Deliverya__MenuI__55009F39");
            });

            modelBuilder.Entity<DeliveryandTakeawayStatus>(entity =>
            {
                entity.ToTable("DeliveryandTakeawayStatus");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AdminId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.BookingId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Comment)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.DeliveryandTakeawayStatuses)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK__Deliverya__Admin__7EF6D905");

                entity.HasOne(d => d.Booking)
                    .WithMany(p => p.DeliveryandTakeawayStatuses)
                    .HasForeignKey(d => d.BookingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeliveryandTakeawayBooking_DeliveryandTakeawayStatuses");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DeliveryandTakeawayStatuses)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__Deliverya__UserI__6EF57B66");
            });

            modelBuilder.Entity<Device>(entity =>
            {
                entity.Property(e => e.Id).HasMaxLength(500);

                entity.Property(e => e.City)
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Latitude)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Longitude)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Devices)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__Devices__UserId__10216507");
            });

            modelBuilder.Entity<DeviceNotification>(entity =>
            {
                entity.ToTable("DeviceNotification");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.MessageContent)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.PushedScheduleDateTime).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DeviceNotifications)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DeviceNot__UserI__7C1A6C5A");
            });

            modelBuilder.Entity<DineInBooking>(entity =>
            {
                entity.HasKey(e => e.BookingId)
                    .HasName("PK_ResturantBooking123");

                entity.ToTable("DineInBooking");

                entity.Property(e => e.BookingId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BookingDate).HasColumnType("date");

                entity.Property(e => e.BookingTime).HasMaxLength(10);

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.OfferId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.PhoneNumber).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.PromoCode).HasMaxLength(100);

                entity.Property(e => e.TableId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.DineInBookings)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DineInBooking_Branch");

                entity.HasOne(d => d.Offer)
                    .WithMany(p => p.DineInBookings)
                    .HasForeignKey(d => d.OfferId)
                    .HasConstraintName("FK_DineInBooking_BranchOffers");

                entity.HasOne(d => d.Table)
                    .WithMany(p => p.DineInBookings)
                    .HasForeignKey(d => d.TableId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DineInBooking_BranchTables");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DineInBookings)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DineInBooking_Users");
            });

            modelBuilder.Entity<DineInBookingBillUpload>(entity =>
            {
                entity.HasKey(e => e.BookingId);

                entity.ToTable("DineInBookingBillUpload");

                entity.Property(e => e.BookingId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.AdminId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.AuthorizedComment)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizedDate).HasColumnType("datetime");

                entity.Property(e => e.ImagePath)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.RedeemComment)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.RedeemDate).HasColumnType("datetime");

                entity.Property(e => e.UploadedDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.DineInBookingBillUploads)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK_DineInBookingBillUpload_Administrator");

                entity.HasOne(d => d.Booking)
                    .WithOne(p => p.DineInBookingBillUpload)
                    .HasForeignKey<DineInBookingBillUpload>(d => d.BookingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DineInBookingBillUpload_DineInBooking");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DineInBookingBillUploads)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DineInBookingBillUpload_Users");
            });

            modelBuilder.Entity<DineInStatus>(entity =>
            {
                entity.ToTable("DineInStatus");

                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AdminId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.BookingId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.StatusId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.DineInStatuses)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK__DineInStatus__Admin");

                entity.HasOne(d => d.Booking)
                    .WithMany(p => p.DineInStatuses)
                    .HasForeignKey(d => d.BookingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DineInBooking_DineInStatuses");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DineInStatuses)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__DineInStatus__UserId");
            });

            modelBuilder.Entity<EmailConfiguration>(entity =>
            {
                entity.ToTable("EmailConfiguration");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.HostName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.IsSsl).HasColumnName("IsSSL");

                entity.Property(e => e.IsTls).HasColumnName("IsTLS");
            });

            modelBuilder.Entity<EmailContent>(entity =>
            {
                entity.HasKey(e => e.TemplateTypeId)
                    .HasName("PK__EmailCon__E23AE6E09375C84D");

                entity.ToTable("EmailContent");

                entity.Property(e => e.TemplateTypeId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.EmailBody).HasColumnName("Email_Body");

                entity.Property(e => e.EmailCc)
                    .HasMaxLength(500)
                    .HasColumnName("Email_CC");

                entity.Property(e => e.EmailSubject)
                    .HasMaxLength(500)
                    .HasColumnName("Email_Subject");

                entity.Property(e => e.EmailTo)
                    .HasMaxLength(100)
                    .HasColumnName("Email_To");
            });

            modelBuilder.Entity<EmailCredential>(entity =>
            {
                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.EmailReplyTo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("EmailReply_To")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<HotelBranch>(entity =>
            {
                entity.HasKey(e => e.BranchId);

                entity.Property(e => e.BranchId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AccountAdminPersonId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.AddressLine1).HasMaxLength(200);

                entity.Property(e => e.AddressLine2).HasMaxLength(200);

                entity.Property(e => e.AdminId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.BranchName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.Description)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.LocalityId1).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.LocalityId2).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.LocalityId3).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.LocalityId4).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.SalesPersonId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Seodescription)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("SEODescription")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Seokeyword)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("SEOKeyword")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Seotitle)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("SEOTitle")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.TagLine)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Zipcode).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.AccountAdminPerson)
                    .WithMany(p => p.HotelBranchAccountAdminPeople)
                    .HasForeignKey(d => d.AccountAdminPersonId)
                    .HasConstraintName("FK__HotelBranches__AccountAdministrator");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.HotelBranchAdmins)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK_HotelBranches_Administrator");

                entity.HasOne(d => d.LocalityId1Navigation)
                    .WithMany(p => p.HotelBranchLocalityId1Navigations)
                    .HasForeignKey(d => d.LocalityId1)
                    .HasConstraintName("FK_HotelBranches_Localities1");

                entity.HasOne(d => d.LocalityId2Navigation)
                    .WithMany(p => p.HotelBranchLocalityId2Navigations)
                    .HasForeignKey(d => d.LocalityId2)
                    .HasConstraintName("FK_HotelBranches_Localities2");

                entity.HasOne(d => d.LocalityId3Navigation)
                    .WithMany(p => p.HotelBranchLocalityId3Navigations)
                    .HasForeignKey(d => d.LocalityId3)
                    .HasConstraintName("FK_HotelBranches_Localities3");

                entity.HasOne(d => d.LocalityId4Navigation)
                    .WithMany(p => p.HotelBranchLocalityId4Navigations)
                    .HasForeignKey(d => d.LocalityId4)
                    .HasConstraintName("FK_HotelBranches_Localities4");

                entity.HasOne(d => d.SalesPerson)
                    .WithMany(p => p.HotelBranchSalesPeople)
                    .HasForeignKey(d => d.SalesPersonId)
                    .HasConstraintName("FK__HotelBranches__SalesAdministrator");
            });

            modelBuilder.Entity<Locality>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CityId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Latitude)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Localities)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Localities_Cities");
            });

            modelBuilder.Entity<LoyaltyRewardTransactionLog>(entity =>
            {
                entity.HasKey(e => e.RewardId)
                    .HasName("PK__LoyaltyR__825015B9AE1A27AE");

                entity.Property(e => e.RewardId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.BookingId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<MenuCategory>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<NotificationExceptionLog>(entity =>
            {
                entity.HasKey(e => e.ExceptionId);

                entity.Property(e => e.ExceptionId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DeviceId)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.InnerException)
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Message)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Source)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.StackTrace)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.HasOne(d => d.Notification)
                    .WithMany(p => p.NotificationExceptionLogs)
                    .HasForeignKey(d => d.NotificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NotificationExceptionLogs_PushNotifications");
            });

            modelBuilder.Entity<OfferCategory>(entity =>
            {
                entity.Property(e => e.DeliveryActive).HasDefaultValueSql("((0))");

                entity.Property(e => e.DineInActive).HasDefaultValueSql("((0))");

                entity.Property(e => e.Extension)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.TakeawayActive).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<OperationExceptionLog>(entity =>
            {
                entity.HasKey(e => e.ExceptionLogId)
                    .HasName("PK__Operatio__7AE5C472EF658E7E");

                entity.Property(e => e.ExceptionLogId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ActionName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ControllerName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.DateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.InnerException).IsRequired();

                entity.Property(e => e.Message)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.StackTrace)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Otptransaction>(entity =>
            {
                entity.HasKey(e => e.TransactionId)
                    .HasName("PK_OTPTransactions1");

                entity.ToTable("OTPTransactions");

                entity.Property(e => e.TransactionId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ExternalId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Otp)
                    .IsRequired()
                    .HasMaxLength(6)
                    .HasColumnName("OTP");

                entity.Property(e => e.PhoneNumber).HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<PhoneNumber>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.TypeId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Value).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.VerificationDate).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PhoneNumbers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__PhoneNumb__UserI__76969D2E");
            });

            modelBuilder.Entity<PreferredRestaurant>(entity =>
            {
                entity.HasKey(e => e.RestaurantId)
                    .HasName("PK_PreferredTemp");

                entity.ToTable("PreferredRestaurant");

                entity.Property(e => e.RestaurantId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.CityId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.Comment).HasMaxLength(500);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.PreferredRestaurants)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK__Preferred__CityI__1D7B6025");

                entity.HasOne(d => d.Restaurant)
                    .WithOne(p => p.PreferredRestaurant)
                    .HasForeignKey<PreferredRestaurant>(d => d.RestaurantId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Preferred__Resta__19AACF41");
            });

            modelBuilder.Entity<PushNotification>(entity =>
            {
                entity.HasKey(e => e.NotificationId)
                    .HasName("PK_PushNotifications123");

                entity.Property(e => e.AndroidPayLoad)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CityId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.CreatedDatetime).HasColumnType("datetime");

                entity.Property(e => e.Extension)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IosPayLoad)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ScheduleTime).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.PushNotifications)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("FK_PushNotifications_Cities");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PushNotifications)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PushNotifications_Administrator");
            });

            modelBuilder.Entity<RestaurantLogin>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("RestaurantLogins");

                entity.Property(e => e.AddressLine1).HasMaxLength(200);

                entity.Property(e => e.BranchId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.BranchName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UserId).HasColumnType("numeric(18, 0)");
            });

            modelBuilder.Entity<TransmissionContent>(entity =>
            {
                entity.HasKey(e => e.TypeId)
                    .HasName("PK_OTPMessageContent");

                entity.ToTable("TransmissionContent");

                entity.Property(e => e.TypeId).ValueGeneratedNever();

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(2000);
            });

            modelBuilder.Entity<TwoFactorMessageTemplate>(entity =>
            {
                entity.HasKey(e => e.TemplateId)
                    .HasName("PK__TwoFacto__F87ADD27AFBE47A9");

                entity.ToTable("TwoFactorMessageTemplate");

                entity.Property(e => e.TemplateId).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.QueryString)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.SenderId).HasMaxLength(500);
            });

            modelBuilder.Entity<TwoFactorMessageTransactionLog>(entity =>
            {
                entity.HasKey(e => e.LogId)
                    .HasName("PK__TwoFacto__5E548648DA1F38D7");

                entity.Property(e => e.LogId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ExternalId).HasColumnType("numeric(18, 0)");

                entity.Property(e => e.PhoneNumber).HasColumnType("numeric(10, 0)");

                entity.Property(e => e.TargetUrlWithParameters)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.TransactionId).HasMaxLength(100);

                entity.Property(e => e.TransactionStatus).HasMaxLength(500);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.UserId)
                    .HasColumnType("numeric(18, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DoubleTheDealBookingId).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<Version>(entity =>
            {
                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.Number)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
