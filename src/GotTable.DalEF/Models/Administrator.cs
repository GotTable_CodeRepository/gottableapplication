﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class Administrator
    {
        public Administrator()
        {
            DeliveryandTakeawayStatuses = new HashSet<DeliveryandTakeawayStatus>();
            DeviceNotifications = new HashSet<DeviceNotification>();
            DineInBookingBillUploads = new HashSet<DineInBookingBillUpload>();
            DineInStatuses = new HashSet<DineInStatus>();
            HotelBranchAccountAdminPeople = new HashSet<HotelBranch>();
            HotelBranchAdmins = new HashSet<HotelBranch>();
            HotelBranchSalesPeople = new HashSet<HotelBranch>();
            PushNotifications = new HashSet<PushNotification>();
        }

        public decimal UserId { get; set; }
        public int TypeId { get; set; }
        public int? PrefixId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? GenderId { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public decimal? PreferredCityId { get; set; }
        public bool? EnableButtonForRestaurantLogin { get; set; }

        public virtual City PreferredCity { get; set; }
        public virtual ICollection<DeliveryandTakeawayStatus> DeliveryandTakeawayStatuses { get; set; }
        public virtual ICollection<DeviceNotification> DeviceNotifications { get; set; }
        public virtual ICollection<DineInBookingBillUpload> DineInBookingBillUploads { get; set; }
        public virtual ICollection<DineInStatus> DineInStatuses { get; set; }
        public virtual ICollection<HotelBranch> HotelBranchAccountAdminPeople { get; set; }
        public virtual ICollection<HotelBranch> HotelBranchAdmins { get; set; }
        public virtual ICollection<HotelBranch> HotelBranchSalesPeople { get; set; }
        public virtual ICollection<PushNotification> PushNotifications { get; set; }
    }
}
