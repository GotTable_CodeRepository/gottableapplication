﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class Version
    {
        public int VersionId { get; set; }
        public string Number { get; set; }
        public int TypeId { get; set; }
        public bool Active { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid SyncName { get; set; }
        public Guid SyncPassword { get; set; }
    }
}
