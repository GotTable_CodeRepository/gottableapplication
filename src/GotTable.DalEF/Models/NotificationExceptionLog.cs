﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class NotificationExceptionLog
    {
        public decimal ExceptionId { get; set; }
        public string DeviceId { get; set; }
        public int DeviceTypeId { get; set; }
        public int NotificationId { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string InnerException { get; set; }
        public string StackTrace { get; set; }

        public virtual PushNotification Notification { get; set; }
    }
}
