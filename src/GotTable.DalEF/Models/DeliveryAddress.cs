﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class DeliveryAddress
    {
        public decimal Id { get; set; }
        public decimal BookingId { get; set; }
        public decimal UserId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Landmark { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public decimal Zip { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public virtual DeliveryandTakeawayBooking Booking { get; set; }
        public virtual User User { get; set; }
    }
}
