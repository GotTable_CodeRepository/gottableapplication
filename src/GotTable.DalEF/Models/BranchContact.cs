﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchContact
    {
        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsActive { get; set; }
        public bool IsEmailAlert { get; set; }
        public bool IsPhoneAlert { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string EmailAddress { get; set; }
        public int? TypeId { get; set; }

        public virtual HotelBranch Branch { get; set; }
    }
}
