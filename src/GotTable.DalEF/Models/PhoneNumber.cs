﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class PhoneNumber
    {
        public decimal Id { get; set; }
        public decimal UserId { get; set; }
        public decimal TypeId { get; set; }
        public decimal Value { get; set; }
        public bool? IsVerified { get; set; }
        public DateTime? VerificationDate { get; set; }
        public bool? IsActive { get; set; }

        public virtual User User { get; set; }
    }
}
