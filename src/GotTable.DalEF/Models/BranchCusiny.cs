﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchCusiny
    {
        public BranchCusiny()
        {
            BranchMenus = new HashSet<BranchMenu>();
        }

        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public decimal CuisineId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual HotelBranch Branch { get; set; }
        public virtual Cuisine Cuisine { get; set; }
        public virtual ICollection<BranchMenu> BranchMenus { get; set; }
    }
}
