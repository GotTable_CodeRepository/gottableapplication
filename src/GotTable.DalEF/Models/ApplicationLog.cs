﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class ApplicationLog
    {
        public int Id { get; set; }
        public string DeviceId { get; set; }
        public int? DeviceTypeId { get; set; }
        public string Url { get; set; }
        public string ControllerName { get; set; }
        public string Params { get; set; }
        public int? UserId { get; set; }
        public int? StatusCode { get; set; }
        public string Error { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
