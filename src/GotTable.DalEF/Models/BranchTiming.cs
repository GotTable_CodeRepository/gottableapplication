﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchTiming
    {
        public decimal Id { get; set; }
        public decimal? BranchId { get; set; }
        public int? BranchCategoryId { get; set; }
        public int? DayId { get; set; }
        public string LunchStartTime { get; set; }
        public string LunchEndTime { get; set; }
        public string DinnerStartTime { get; set; }
        public string DinnerEndTime { get; set; }
        public bool? IsClosed { get; set; }

        public virtual HotelBranch Branch { get; set; }
    }
}
