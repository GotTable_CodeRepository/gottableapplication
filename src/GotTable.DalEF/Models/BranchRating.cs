﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class BranchRating
    {
        public decimal Id { get; set; }
        public decimal BranchId { get; set; }
        public decimal UserId { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public int? Rating { get; set; }
        public int? AmbienceRating { get; set; }
        public int? FoodRating { get; set; }
        public int? MusicRating { get; set; }
        public int? PriceRating { get; set; }
        public int? ServiceRating { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual HotelBranch Branch { get; set; }
        public virtual User User { get; set; }
    }
}
