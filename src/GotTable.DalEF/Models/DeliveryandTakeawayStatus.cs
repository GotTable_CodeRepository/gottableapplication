﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class DeliveryandTakeawayStatus
    {
        public decimal Id { get; set; }
        public decimal BookingId { get; set; }
        public decimal? UserId { get; set; }
        public int StatusId { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal? AdminId { get; set; }

        public virtual Administrator Admin { get; set; }
        public virtual DeliveryandTakeawayBooking Booking { get; set; }
        public virtual User User { get; set; }
    }
}
