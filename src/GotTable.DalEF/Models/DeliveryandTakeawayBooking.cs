﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class DeliveryandTakeawayBooking
    {
        public DeliveryandTakeawayBooking()
        {
            DeliveryAddresses = new HashSet<DeliveryAddress>();
            DeliveryandTakeawayCartItems = new HashSet<DeliveryandTakeawayCartItem>();
            DeliveryandTakeawayStatuses = new HashSet<DeliveryandTakeawayStatus>();
        }

        public decimal BookingId { get; set; }
        public decimal BranchId { get; set; }
        public decimal UserId { get; set; }
        public decimal? OfferId { get; set; }
        public int BookingTypeId { get; set; }
        public DateTime BookingDate { get; set; }
        public string BookingTime { get; set; }
        public decimal PhoneNumber { get; set; }
        public int CurrentStatusId { get; set; }
        public decimal CartAmount { get; set; }
        public decimal CentralGst { get; set; }
        public decimal StateGst { get; set; }
        public decimal ServiceCharges { get; set; }
        public decimal DeliveryCharges { get; set; }
        public decimal PackingCharges { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsRead { get; set; }
        public string PromoCode { get; set; }

        public virtual HotelBranch Branch { get; set; }
        public virtual BranchOffer Offer { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<DeliveryAddress> DeliveryAddresses { get; set; }
        public virtual ICollection<DeliveryandTakeawayCartItem> DeliveryandTakeawayCartItems { get; set; }
        public virtual ICollection<DeliveryandTakeawayStatus> DeliveryandTakeawayStatuses { get; set; }
    }
}
