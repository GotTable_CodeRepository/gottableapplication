﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class LoyaltyRewardTransactionLog
    {
        public decimal RewardId { get; set; }
        public decimal? UserId { get; set; }
        public decimal? BookingId { get; set; }
        public decimal? BranchId { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
