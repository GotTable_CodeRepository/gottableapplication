﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class Locality
    {
        public Locality()
        {
            HotelBranchLocalityId1Navigations = new HashSet<HotelBranch>();
            HotelBranchLocalityId2Navigations = new HashSet<HotelBranch>();
            HotelBranchLocalityId3Navigations = new HashSet<HotelBranch>();
            HotelBranchLocalityId4Navigations = new HashSet<HotelBranch>();
        }

        public decimal Id { get; set; }
        public string Name { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public decimal CityId { get; set; }
        public bool Active { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<HotelBranch> HotelBranchLocalityId1Navigations { get; set; }
        public virtual ICollection<HotelBranch> HotelBranchLocalityId2Navigations { get; set; }
        public virtual ICollection<HotelBranch> HotelBranchLocalityId3Navigations { get; set; }
        public virtual ICollection<HotelBranch> HotelBranchLocalityId4Navigations { get; set; }
    }
}
