﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GotTable.DalEF.Models
{
    public partial class DineInBooking
    {
        public DineInBooking()
        {
            DineInStatuses = new HashSet<DineInStatus>();
        }

        public decimal BookingId { get; set; }
        public decimal BranchId { get; set; }
        public decimal UserId { get; set; }
        public decimal TableId { get; set; }
        public decimal PhoneNumber { get; set; }
        public DateTime BookingDate { get; set; }
        public string BookingTime { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool? IsRead { get; set; }
        public decimal? OfferId { get; set; }
        public int? CurrentStatusId { get; set; }
        public string PromoCode { get; set; }
        public bool? RewardApplied { get; set; }
        public int BillUploadStatus { get; set; }
        public bool? DoubleTheDealActive { get; set; }

        public virtual HotelBranch Branch { get; set; }
        public virtual BranchOffer Offer { get; set; }
        public virtual BranchTable Table { get; set; }
        public virtual User User { get; set; }
        public virtual DineInBookingBillUpload DineInBookingBillUpload { get; set; }
        public virtual ICollection<DineInStatus> DineInStatuses { get; set; }
    }
}
