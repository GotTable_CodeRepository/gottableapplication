﻿using GotTable.Common.Enumerations;
using GotTable.Dal.PushNotifications;
using GotTable.DalEF.Models;
using GotTable.DalEF.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.DalEF.PushNotifications
{
    [Serializable]
    public sealed class PushNotificationDal : BaseContext<PushNotificationDal>, IPushNotificationDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="scheduleDate"></param>
        /// <param name="notificationCity"></param>
        /// <param name="notificationType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<PushNotificationInfoDto>> FetchList(decimal userId, DateTime? scheduleDate = null, string notificationCity = "", string notificationType = "", int currentPage = 1, int pageSize = 10)
        {
            await Task.FromResult(1);
            var query = (from dN in Context.PushNotifications
                         .Include(push => push.City)
                         .AsQueryable()
                         select new
                         {
                             dN.Active,
                             dN.City,
                             dN.CreatedDatetime,
                             dN.Message,
                             dN.NotificationId,
                             dN.NotificationTypeId,
                             dN.ScheduleTime,
                             dN.Title,
                             dN.UserId,
                             dN.CategoryTypeId,
                             dN.OfferTypeId,
                             dN.FailureCount,
                             dN.SuccessCount,
                             dN.AndroidPayLoad,
                             dN.IosPayLoad,
                             dN.CityId,
                             dN.StatusId
                         });

            if (userId > 0)
            {
                query = query.Where(x => x.UserId == userId);
            }

            if (!string.IsNullOrEmpty(notificationCity))
            {
                var city = Context.Cities.Where(x => x.Name == notificationCity).SingleOrDefault();
                if (city != null)
                {
                    query = query.Where(x => x.City.CityId == city.CityId);
                }
            }

            if (!string.IsNullOrEmpty(notificationType))
            {
                var notificationTypeId = (int)Enum.Parse(typeof(Enumeration.NotificationType), notificationType);
                query = query.Where(x => x.NotificationTypeId == notificationTypeId);
            }

            var data = (from dN in query
                        select new PushNotificationInfoDto
                        {
                            Active = dN.Active,
                            CityId = dN.CityId,
                            CreatedDatetime = dN.CreatedDatetime,
                            Message = dN.Message,
                            NotificationId = dN.NotificationId,
                            ScheduleTime = dN.ScheduleTime,
                            Title = dN.Title,
                            UserId = dN.UserId,
                            CityName = dN.City != null ? dN.City.Name : "All",
                            NotificationType = ((Enumeration.NotificationType)dN.NotificationTypeId).ToString(),
                            CategoryTypeName = ((Enumeration.RestaurantTypes)dN.CategoryTypeId).ToString(),
                            OfferTypeName = ((Enumeration.OfferType)dN.OfferTypeId).ToString(),
                            FailureCount = dN.FailureCount,
                            SuccessCount = dN.SuccessCount,
                            AndroidPayLoad = dN.AndroidPayLoad,
                            IosPayLoad = dN.IosPayLoad,
                            StatusName = ((Enumeration.NotificationStatusType)dN.StatusId).ToString(),
                        }).ToList();

            if (scheduleDate != null)
            {
                var scheduleTime = scheduleDate.Value.Date;
                data = data.Where(x => x.ScheduleTime.Date == scheduleTime).ToList();
            }

            if (data != null && currentPage > 0)
            {
                data = data.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public async Task<List<NotificationExceptionLogDto>> FetchList(decimal notificationId)
        {
            await Task.FromResult(1);
            var data = (from dN in Context.NotificationExceptionLogs
                        where dN.NotificationId == notificationId
                        orderby dN.ExceptionId
                        select new NotificationExceptionLogDto
                        {
                            Message = dN.Message,
                            NotificationId = dN.NotificationId,
                            DeviceId = dN.DeviceId,
                            DeviceTypeId = dN.DeviceTypeId,
                            InnerException = dN.InnerException,
                            Source = dN.Source,
                            StackTrace = dN.StackTrace
                        }).ToList();

            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public async Task<PushNotificationDto> Fetch(decimal notificationId)
        {
            await Task.FromResult(1);
            var data = (from dN in Context.PushNotifications
                        .Include(push => push.City).AsQueryable()
                        where (dN.NotificationId == notificationId)
                        select dN).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid notificationId");
            }

            return new PushNotificationDto
            {
                Active = data.Active,
                CityId = data.CityId,
                Completed = data.Completed,
                CreatedDatetime = data.CreatedDatetime,
                Message = data.Message,
                NotificationTypeId = data.NotificationTypeId,
                ScheduleTime = data.ScheduleTime,
                Title = data.Title,
                UserId = data.UserId,
                NotificationId = data.NotificationId,
                CityName = data.City != null ? data.City.Name : "All",
                NotificationType = ((Enumeration.NotificationType)data.NotificationTypeId).ToString(),
                CategoryTypeId = data.CategoryTypeId,
                CategoryTypeName = ((Enumeration.RestaurantTypes)data.CategoryTypeId).ToString(),
                OfferTypeId = data.OfferTypeId,
                OfferTypeName = ((Enumeration.OfferType)data.OfferTypeId).ToString(),
                Extension = data.Extension,
                AndroidPayLoad = data.AndroidPayLoad,
                FailureCount = data.FailureCount,
                IosPayLoad = data.IosPayLoad,
                SuccessCount = data.SuccessCount,
                StatusId = data.StatusId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceDtos"></param>
        public async Task Insert(List<NotificationExceptionLogDto> deviceDtos)
        {
            await Task.FromResult(1);
            foreach (var dto in deviceDtos)
            {
                var data = new NotificationExceptionLog()
                {
                    DeviceId = dto.DeviceId,
                    NotificationId = dto.NotificationId,
                    DeviceTypeId = dto.DeviceTypeId,
                    Source = dto.Source,
                    StackTrace = dto.StackTrace,
                    Message = dto.Message,
                    InnerException = dto.InnerException
                };
                Context.NotificationExceptionLogs.Add(data);
            }
            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Insert(PushNotificationDto dto)
        {
            await Task.FromResult(1);
            var data = new PushNotification()
            {
                Active = dto.Active,
                CityId = dto.CityId,
                Completed = dto.Completed,
                CreatedDatetime = dto.CreatedDatetime,
                Message = dto.Message,
                NotificationTypeId = dto.NotificationTypeId,
                ScheduleTime = dto.ScheduleTime,
                Title = dto.Title,
                UserId = dto.UserId,
                CategoryTypeId = dto.CategoryTypeId,
                OfferTypeId = dto.OfferTypeId,
                Extension = dto.Extension,
                StatusId = dto.StatusId
            };

            Context.PushNotifications.Add(data);
            Context.SaveChanges();

            dto.NotificationId = data.NotificationId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public async Task Update(PushNotificationDto dto)
        {
            await Task.FromResult(1);
            var data = (from dN in Context.PushNotifications.AsQueryable()
                        where dN.NotificationId == dto.NotificationId
                        select dN).SingleOrDefault();

            if (data == null)
            {
                throw new System.Exception("Invalid notificationId");
            }

            data.Completed = dto.Completed;
            data.SuccessCount = dto.SuccessCount;
            data.FailureCount = dto.FailureCount;
            data.IosPayLoad = dto.IosPayLoad;
            data.AndroidPayLoad = dto.AndroidPayLoad;
            data.Extension = dto.Extension;
            data.StatusId = dto.StatusId;

            Context.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        public async Task Delete(int notificationId)
        {
            await Task.FromResult(1);
            var data = (from dN in Context.PushNotifications.AsQueryable()
                        where dN.NotificationId == notificationId
                        select dN).SingleOrDefault();
            if (data != null)
            {
                Context.PushNotifications.Remove(data);
            }
            Context.SaveChanges();
        }
    }
}
