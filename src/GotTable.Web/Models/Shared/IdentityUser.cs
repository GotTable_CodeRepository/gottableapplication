﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Security.Claims;
using ClaimType = GotTable.Common.Enumerations.Enumeration.ClaimType;

namespace GotTable.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class IdentityUser
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        public IdentityUser(IHttpContextAccessor httpContextAccessor)
        {
            ClaimsPrincipal = httpContextAccessor.HttpContext.User;
        }

        /// <summary>
        /// 
        /// </summary>
        private readonly ClaimsPrincipal ClaimsPrincipal;

        /// <summary>
        /// 
        /// </summary>
        public string UserId => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.UserId.ToString()).SingleOrDefault().Value;

        /// <summary>
        /// 
        /// </summary>
        public string Name => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.Name.ToString()).SingleOrDefault().Value;

        /// <summary>
        /// 
        /// </summary>
        public string Role => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.Role.ToString()).SingleOrDefault().Value;

        /// <summary>
        /// 
        /// </summary>
        public string EmailAddress => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.Email.ToString()).SingleOrDefault().Value;

        /// <summary>
        /// 
        /// </summary>
        public bool EnableRestaurantLogin => Convert.ToBoolean(ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.EnableRestaurantLogin.ToString()).SingleOrDefault().Value);

        /// <summary>
        /// 
        /// </summary>
        public string CityId => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.CityId.ToString()).SingleOrDefault().Value;

        /// <summary>
        /// 
        /// </summary>
        public string RestaurantId => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.RestaurantId.ToString()).SingleOrDefault()?.Value;

        /// <summary>
        /// 
        /// </summary>
        public string RestaurantName => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.RestaurantName.ToString()).SingleOrDefault()?.Value;

        /// <summary>
        /// 
        /// </summary>
        public string RestaurantAddress => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.RestaurantAddress.ToString()).SingleOrDefault()?.Value;

        /// <summary>
        /// 
        /// </summary>
        public string DineInActive => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.DineInActive.ToString()).SingleOrDefault()?.Value;

        /// <summary>
        /// 
        /// </summary>
        public string DeliveryActive => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.DeliveryActive.ToString()).SingleOrDefault()?.Value;

        /// <summary>
        /// 
        /// </summary>
        public string TakeawayActive => ClaimsPrincipal?.Claims.DefaultIfEmpty().Where(x => x.Type == ClaimType.TakeawayActive.ToString()).SingleOrDefault()?.Value;
    }
}
