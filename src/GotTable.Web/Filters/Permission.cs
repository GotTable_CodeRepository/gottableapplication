﻿using GotTable.Common.Enumerations;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;

namespace GotTable.Web.Filters
{
    /// <summary>
    /// 
    /// </summary>
    public class Permission : Attribute, IAsyncAuthorizationFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="allowedRoles"></param>
        public Permission(string allowedRoles)
        {
            AllowedRoles = allowedRoles;
        }

        /// <summary>
        /// 
        /// </summary>
        public string AllowedRoles { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public async Task OnAuthorizationAsync(AuthorizationFilterContext filterContext)
        {
            await Task.FromResult(1);
            var role = filterContext.HttpContext.User.Claims.Where(x => x.Type == Enumeration.ClaimType.Role.ToString()).SingleOrDefault().Value;
            if (role != null)
            {
                if (!AllowedRoles.Contains(role))
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(new
                        {
                            action = "Index",
                            controller = "Error"
                        }));
                }
            }
            return;
        }
    }
}