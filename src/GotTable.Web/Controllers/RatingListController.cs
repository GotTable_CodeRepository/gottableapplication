﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantRatings;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class RatingListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IRatingList ratingList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ratingList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public RatingListController(IRatingList ratingList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.ratingList = ratingList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var ratingDAOs = await ratingList.Get(restaurantId, currentPage, pageSize);
            return View(ratingDAOs);
        }
    }
}
