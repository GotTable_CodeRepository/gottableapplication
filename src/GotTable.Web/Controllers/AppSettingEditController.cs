﻿using GotTable.DAO.ApplicationSettings;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class AppSettingEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IApplicationSetting applicationSetting;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationSetting"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public AppSettingEditController(IApplicationSetting applicationSetting, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.applicationSetting = applicationSetting;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var model = await applicationSetting.Get();
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(ApplicationSettingDAO model)
        {
            await applicationSetting.Save(model);
            return View(model);
        }
    }
}
