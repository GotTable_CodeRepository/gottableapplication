﻿using GotTable.DAO.Reports;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Reports;
using GotTable.Library.Reports.Criterias;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class DailyBookingListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IReportList<DailyBookingDAO, DailyBookingListCriteria> reportList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public DailyBookingListController(IReportList<DailyBookingDAO, DailyBookingListCriteria> reportList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.reportList = reportList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(string bookingDate = default, int currentPage = 0, int pageSize = 10)
        {
            DateTime dtBookingDate = String.IsNullOrEmpty(bookingDate) ? DateTime.Now : DateTime.ParseExact(bookingDate, "MM-dd-yyyy", null);
            var criteria = new DailyBookingListCriteria(dtBookingDate, currentPage, pageSize);
            var dailyBookings = await reportList.Get(criteria);
            ViewBag.SelectedDate = bookingDate;
            return View(dailyBookings);
        }
    }
}
