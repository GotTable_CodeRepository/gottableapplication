﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Bookings.DineIn;
using GotTable.DAO.Users;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Library.RestaurantOffers;
using GotTable.Library.Restaurants;
using GotTable.Library.RestaurantTables;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class DineInBookingController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDineIn dineIn;

        /// <summary>
        /// 
        /// </summary>
        private readonly IRestaurant restaurant;

        /// <summary>
        /// 
        /// </summary>
        private readonly ITableList tableList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOfferList offerList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineIn"></param>
        /// <param name="restaurant"></param>
        /// <param name="tableList"></param>
        /// <param name="offerList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public DineInBookingController(IDineIn dineIn, IRestaurant restaurant, ITableList tableList, IOfferList offerList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.dineIn = dineIn;
            this.restaurant = restaurant;
            this.tableList = tableList;
            this.offerList = offerList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal restaurantId)
        {
            var branchDetail = await restaurant.Get(restaurantId);
            var dineInBooking = new DineInBookingBySalesAdminDAO()
            {
                BookingDateTime = DateTime.Now.ToString("MM-dd-yyyy hh:mm tt"),
                BookingId = 0,
                Comment = string.Empty,
                ConfirmationMessage = string.Empty,
                EmailAddress = string.Empty,
                ErrorMessage = string.Empty,
                FirstName = string.Empty,
                LastName = string.Empty,
                PhoneNumber = 0,
                PromoCode = string.Empty,
                RestaurantId = restaurantId,
                RestaurantName = branchDetail.BranchName + " " + branchDetail.AddressLine1 + " " + branchDetail.AddressLine2 + " " + branchDetail.City + " " + branchDetail.State + " " + branchDetail.Zip,
                SelectedOfferId = 0,
                SelectedTableId = 0,
                StatusId = 1,
                UserId = 0
            };
            ViewBag.OfferList = await offerList.Get(restaurantId, Enumeration.RestaurantTypes.DineIn, null, null, null, null, true, 0, 10);
            ViewBag.TableList = await tableList.Get(restaurantId, 0, 10);
            return View(dineInBooking);
        }

        [HttpPost]
        public async Task<IActionResult> Index(DineInBookingBySalesAdminDAO dineInBookingBySalesAdmin)
        {
            var dineInBooking = new DineInBookingDAO()
            {
                BookingDate = DateTime.ParseExact(dineInBookingBySalesAdmin.BookingDate, "MM-dd-yyyy", null),
                BookingTime = dineInBookingBySalesAdmin.BookingTime,
                BranchId = dineInBookingBySalesAdmin.RestaurantId,
                Comment = dineInBookingBySalesAdmin.Comment ?? string.Empty,
                ConfirmationMessage = string.Empty,
                CreatedDate = DateTime.Now,
                ErrorMessage = string.Empty,
                Id = 0,
                OfferId = dineInBookingBySalesAdmin.SelectedOfferId,
                TableId = dineInBookingBySalesAdmin.SelectedTableId,
                PhoneNumber = dineInBookingBySalesAdmin.PhoneNumber,
                UserDetail = new UserDetailDAO()
                {
                    FirstName = dineInBookingBySalesAdmin.FirstName,
                    LastName = dineInBookingBySalesAdmin.LastName,
                    EmailAddress = dineInBookingBySalesAdmin.EmailAddress
                },
                UserId = 0,
                PromoCode = dineInBookingBySalesAdmin.PromoCode ?? string.Empty
            };

            await dineIn.New(dineInBooking);

            dineInBookingBySalesAdmin.ConfirmationMessage = dineInBooking.ConfirmationMessage;
            dineInBookingBySalesAdmin.ErrorMessage = dineInBooking.ErrorMessage;

            if (!dineInBooking.Status)
            {
                var branchDetail = await restaurant.Get(dineInBooking.BranchId);
                ViewBag.OfferList = await offerList.Get(dineInBooking.BranchId, Enumeration.RestaurantTypes.DineIn, null, null, null, null, true, 0, 10);
                ViewBag.TableList = await tableList.Get(dineInBooking.BranchId, 0, 10);
                dineInBookingBySalesAdmin.RestaurantId = dineInBooking.BranchId;
                dineInBookingBySalesAdmin.RestaurantName = branchDetail.BranchName + " " + branchDetail.AddressLine1 + " " + branchDetail.AddressLine2 + " " + branchDetail.City + " " + branchDetail.State + " " + branchDetail.Zip;
            }

            return View(dineInBookingBySalesAdmin);
        }
    }
}
