﻿using GotTable.DAO.OfferCategories;
using GotTable.Library.OfferCategories;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class OfferCategoriesListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ICategoryList categoryList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOfferCategory offerCategory;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryList"></param>
        /// <param name="offerCategory"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public OfferCategoriesListController(ICategoryList categoryList, IOfferCategory offerCategory, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.categoryList = categoryList;
            this.offerCategory = offerCategory;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<IActionResult> Index(int currentPage = default, int pageSize = default)
        {
            var categoryDAOs = await categoryList.Get(currentPage, pageSize);
            return View(categoryDAOs);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDAO"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save(OfferCategoryDAO categoryDAO)
        {
            await offerCategory.Save(categoryDAO);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerCategoryId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Update(int offerCategoryId)
        {
            await offerCategory.Update(offerCategoryId);
            return RedirectToAction("Index");
        }
    }
}
