﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Security.Claims;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class UserBaseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        public readonly IdentityUser identityUser;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public UserBaseController(IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor)
        {
            this.operationExceptionLog = operationExceptionLog;
            this.identityUser = new IdentityUser(httpContextAccessor);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        protected async virtual void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            filterContext.ExceptionHandled = true;
            var exceptionId = await operationExceptionLog.New(exception);
            filterContext.Result = RedirectToAction("Index", "Error", new { @Id = exceptionId });
        }
    }
}
