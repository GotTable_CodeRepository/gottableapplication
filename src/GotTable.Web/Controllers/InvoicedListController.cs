﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantInvoices;
using GotTable.Library.Restaurants;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class InvoicedListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IInvoiceList invoiceList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IInvoicedRestaurantList invoicedRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="invoicedRestaurantList"></param>
        /// <param name="httpContextAccessor"></param>
        public InvoicedListController(IInvoiceList invoiceList, IOperationExceptionLog operationExceptionLog, IInvoicedRestaurantList invoicedRestaurantList, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.invoiceList = invoiceList;
            this.invoicedRestaurantList = invoicedRestaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal? userId = null, int currentPage = 0, int pageSize = 10)
        {
            var restaurantList = await invoicedRestaurantList.Get(userId, currentPage, pageSize);
            ViewBag.Total = restaurantList.Count();
            ViewBag.Premium = restaurantList.Where(x => x.Category.ToString() == "Premium").Count();
            ViewBag.CasualDining = restaurantList.Where(x => x.Category.ToString() == "Casual_Dining").Count();
            ViewBag.Pub = restaurantList.Where(x => x.Category.ToString() == "Pub").Count();
            ViewBag.Microbrewery = restaurantList.Where(x => x.Category.ToString() == "Microbrewery").Count();
            return View(restaurantList);
        }
    }
}
