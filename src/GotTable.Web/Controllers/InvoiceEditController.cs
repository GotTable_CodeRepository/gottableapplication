﻿using GotTable.DAO.RestaurantInvoices;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantInvoices;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class InvoiceEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchInvoice branchInvoice;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchInvoice"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public InvoiceEditController(IBranchInvoice branchInvoice, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.branchInvoice = branchInvoice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(Guid invoiceId)
        {
            dynamic invoiceDAO;
            if (invoiceId == Guid.Empty)
            {
                invoiceDAO = await branchInvoice.Create(Convert.ToDecimal(identityUser.RestaurantId));
            }
            else
            {
                invoiceDAO = await branchInvoice.Get(invoiceId);
            }
            ViewBag.SusbscriptionList = Common.List.PrescriptionMethodList.Get();
            return View(invoiceDAO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceDAO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(BranchInvoiceDAO invoiceDAO)
        {
            await branchInvoice.New(invoiceDAO);
            if (invoiceDAO.Status)
            {
                return RedirectToAction("Index", "InvoiceList", new { restaurantId = invoiceDAO.BranchId });
            }
            ViewBag.SusbscriptionList = Common.List.PrescriptionMethodList.Get();
            return View(invoiceDAO);
        }
    }
}
