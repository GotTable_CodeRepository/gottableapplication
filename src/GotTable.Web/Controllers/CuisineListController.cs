﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantCuisines;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class CuisineListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cuisineList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public CuisineListController(ICuisineList cuisineList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.cuisineList = cuisineList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal restaurantId, int currentPage = 0, int pageSize = 10)
        {
            var cuisineDAOs = await cuisineList.Get(restaurantId, currentPage, pageSize);
            return View(cuisineDAOs);
        }
    }
}
