﻿using GotTable.DAO.Reports;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Reports;
using GotTable.Library.Reports.Criterias;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class DeletedRestaurantListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IReportList<RestaurantDAO, DeletedRestaurantListCriteria> reportList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="reportList"></param>
        /// <param name="httpContextAccessor"></param>
        public DeletedRestaurantListController(IOperationExceptionLog operationExceptionLog, IReportList<RestaurantDAO, DeletedRestaurantListCriteria> reportList, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.reportList = reportList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(int currentPage = default, int pageSize = default)
        {
            var criteria = new DeletedRestaurantListCriteria(currentPage, pageSize);
            var model = await reportList.Get(criteria);
            return View(model);
        }
    }
}