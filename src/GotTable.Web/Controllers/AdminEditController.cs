﻿using GotTable.DAO.Administrators;
using GotTable.Library.Administrators;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// AdminEdit
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class AdminEditController : UserBaseController
    {
        /// <summary>
        /// AllowedRoles
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// IAdministrator
        /// </summary>
        private readonly IAdministrator<AdminDAO> administrator;

        /// <summary>
        /// IAdministratorList
        /// </summary>
        private readonly IAdministratorList<AdminInfoDAO, ListCriteria> administratorList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="administrator"></param>
        /// <param name="administratorList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public AdminEditController(IAdministrator<AdminDAO> administrator, IAdministratorList<AdminInfoDAO, ListCriteria> administratorList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.administrator = administrator;
            this.administratorList = administratorList;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ViewResult> Index(decimal userId)
        {
            AdminDAO adminDao;
            if (userId == 0)
            {
                adminDao = await administrator.Create();
            }
            else
            {
                adminDao = await administrator.Get(userId);
            }
            ViewBag.TypeList = Common.List.UserTypeList.Get();
            return View(adminDao);
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="admin"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(AdminDAO admin)
        {
            if (admin.UserId == 0)
            {
                await administrator.New(admin);
                if (!admin.Status)
                {
                    ViewBag.TypeList = Common.List.UserTypeList.Get();
                    return View(admin);
                }
            }
            else
            {
                await administrator.Save(admin);
            }
            return RedirectToAction("Index", "AdminList", new { @addedBy = identityUser.UserId });
        }

        /// <summary>
        /// ToggleUser
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ToggleUser(decimal userId)
        {
            var admin = await administrator.Get(userId);
            admin.IsActive = !admin.IsActive;
            await administrator.Save(admin);
            return RedirectToAction("Index", "AdminList", new { addedBy = identityUser.UserId });
        }
    }
}
