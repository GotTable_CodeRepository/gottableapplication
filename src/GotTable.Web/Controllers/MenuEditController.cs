﻿using GotTable.DAO.RestaurantMenus;
using GotTable.Library.MenuCategories;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantCuisines;
using GotTable.Library.RestaurantMenus;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class MenuEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchMenu branchMenu;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICategoryList categoryList;

        public MenuEditController(ICuisineList cuisineList, IBranchMenu branchMenu, ICategoryList categoryList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.cuisineList = cuisineList;
            this.branchMenu = branchMenu;
            this.categoryList = categoryList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal menuId)
        {
            BranchMenuDAO branchMenuDAO;
            var restaurantId = identityUser.RestaurantId;
            if (menuId == 0)
            {
                branchMenuDAO = await branchMenu.Create(Convert.ToDecimal(restaurantId));
            }
            else
            {
                branchMenuDAO = await branchMenu.Get(menuId);
            }
            ViewBag.CuisineList = await cuisineList.Get(Convert.ToDecimal(restaurantId), 0, 10);
            ViewBag.CategoryList = await categoryList.Get(0, 10);
            return View(branchMenuDAO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(BranchMenuDAO model)
        {
            if (model.Id == 0)
            {
                await branchMenu.New(model);
            }
            else
            {
                await branchMenu.Save(model);
            }

            if (model.Status)
            {
                return RedirectToAction("Index", "MenuList", new { @restaurantId = model.BranchId });
            }
            else
            {
                var restaurantId = identityUser.RestaurantId;
                ViewBag.CuisineList = await cuisineList.Get(Convert.ToDecimal(restaurantId), 0, 10);
                ViewBag.CategoryList = await categoryList.Get(0, 10);
                return View(model);
            }
        }
    }
}
