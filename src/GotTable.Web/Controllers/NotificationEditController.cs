﻿using GotTable.Common.List;
using GotTable.DAO.PushNotifications;
using GotTable.Library.Cities;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.PushNotifications;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class NotificationEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, SalesAdmin, SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IPushNotification pushNotification;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICityList cityList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pushNotification"></param>
        /// <param name="cityList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public NotificationEditController(IPushNotification pushNotification, ICityList cityList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.pushNotification = pushNotification;
            this.cityList = cityList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(int notificationId)
        {
            if (notificationId > 0)
            {
                RedirectToAction("Index", "Error");
            }
            var pushNotificationDAO = await pushNotification.Create(Convert.ToDecimal(identityUser.UserId));
            ViewBag.CityList = await cityList.Get();
            ViewBag.NotificationTypeList = NotificationTypeList.Get(true);
            ViewBag.OfferTypeList = OfferTyeList.Get();
            ViewBag.CategoryTypeList = RestaurantTypeList.Get();
            return View(pushNotificationDAO);
        }

        [HttpPost]
        public async Task<IActionResult> Index(PushNotificationDAO pushNotificationDAO)
        {
            if (pushNotificationDAO.Id == 0)
            {
                await pushNotification.New(pushNotificationDAO);
            }
            else
            {
                await pushNotification.Save(pushNotificationDAO);
            }
            if (pushNotificationDAO.Status)
            {
                return RedirectToAction("Index", "NotificationList", new { @userId = identityUser.UserId });
            }
            ViewBag.CityList = await cityList.Get();
            ViewBag.NotificationTypeList = NotificationTypeList.Get(true);
            ViewBag.OfferTypeList = OfferTyeList.Get();
            ViewBag.CategoryTypeList = RestaurantTypeList.Get();
            return View(pushNotificationDAO);
        }
    }
}
