﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Administrators;
using GotTable.Library.Administrators;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class AdminListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IAdministratorList<AdminInfoDAO, ListCriteria> administratorList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="administratorList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public AdminListController(IAdministratorList<AdminInfoDAO, ListCriteria> administratorList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.administratorList = administratorList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adminType"></param>
        /// <param name="emailAddress"></param>
        /// <param name="adminName"></param>
        /// <param name="status"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(Enumeration.AdminType? adminType = null, string emailAddress = null, string adminName = "", bool? status = null, int currentPage = 0, int pageSize = 10)
        {
            ViewBag.AdminName = adminName;
            ViewBag.EmailAddress = emailAddress;
            ViewBag.Status = status;
            ViewBag.AdminType = adminType.ToString();
            var criteria = new ListCriteria(adminName, emailAddress, adminType, default, currentPage, pageSize);
            var adminlist = await administratorList.Get(criteria);
            return View(adminlist);
        }
    }
}
