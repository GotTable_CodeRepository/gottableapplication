﻿using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [AllowAnonymous]
    public sealed class ErrorController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        public ErrorController(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(string Id)
        {
            Guid exceptionLogId = new Guid(Id);
            var operationExceptionLogDAO = await operationExceptionLog.Get(exceptionLogId);
            return View(operationExceptionLogDAO);
        }
    }
}
