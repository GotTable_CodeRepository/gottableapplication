﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantInvoices;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class InvoiceListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IInvoiceList invoiceList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public InvoiceListController(IInvoiceList invoiceList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.invoiceList = invoiceList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal restaurantId, int currentPage = 0, int pageSize = 10)
        {
            var invoiceDAOs = await invoiceList.Get(restaurantId, currentPage, pageSize);
            return View(invoiceDAOs);
        }
    }
}
