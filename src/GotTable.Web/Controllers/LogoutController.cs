﻿using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class LogoutController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public LogoutController(IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}
