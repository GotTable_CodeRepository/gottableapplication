﻿using GotTable.Common.Enumerations;
using GotTable.Library.Security;
using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;
using GotTable.DAO.Administrators;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using ClaimType = GotTable.Common.Enumerations.Enumeration.ClaimType;
using System;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// HomeController
    /// </summary>
    [AllowAnonymous]
    public class HomeController : Controller
    {
        /// <summary>
        /// IAdministrator
        /// </summary>
        private readonly IApplicationPrincipal applicationPrincipal;
        
        /// <summary>
        /// IOperationExceptionLog
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationPrincipal"></param>
        /// <param name="branchStaff"></param>
        /// <param name="operationExceptionLog"></param>
        public HomeController(IApplicationPrincipal applicationPrincipal, IOperationExceptionLog operationExceptionLog)
        {
            this.applicationPrincipal = applicationPrincipal;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            await Task.FromResult(1);
            var user = new AdminDAO()
            {
                EmailAddress = string.Empty,
                Password = string.Empty,
                StatusId = 1,
                ErrorMessage = string.Empty
            };
            return View(user);
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(AdminDAO model)
        {
            var user = await applicationPrincipal.Get(model.EmailAddress, model.Password);
            if (user == null)
            {
                model.StatusId = 0;
                model.ErrorMessage = "Please enter valid credentials";
                return View(model);
            }
            else
            {
                var userId = user.Claims.Where(x => x.Type == ClaimType.UserId.ToString()).SingleOrDefault().Value;
                var role = user.Claims.Where(x => x.Type == ClaimType.Role.ToString()).SingleOrDefault().Value;
                await HttpContext.SignInAsync(user);
                if (role == Enumeration.AdminType.HotelAdmin.ToString())
                {
                    var restaurantId = user.Claims.Where(x => x.Type == ClaimType.Role.ToString()).SingleOrDefault().Value;
                    return RedirectToAction("Index", "Transaction", new { @restaurantId = restaurantId, @bookingDate = DateTime.Now, @bookingType = "Current" });
                }
                else if (role == Enumeration.AdminType.AccountAdmin.ToString())
                {
                    return RedirectToAction("Index", "RestaurantList", new { @userId = userId });
                }
                else if (role == Enumeration.AdminType.SalesAdmin.ToString())
                {
                    return RedirectToAction("Index", "RestaurantList", new { @userId = userId });
                }
                else if (role == Enumeration.AdminType.SuperAdmin.ToString())
                {
                    return RedirectToAction("Index", "InvoicedList");
                }
                else
                {
                    model.StatusId = 0;
                    model.ErrorMessage = "You are not authorized for login, Please contact support team";
                    return View(model);
                }
            }
        }

        protected async virtual void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            filterContext.ExceptionHandled = true;
            var exceptionId = await operationExceptionLog.New(exception);
            filterContext.Result = RedirectToAction("Index", "Error", new { @Id = exceptionId });
        }
    }
}
