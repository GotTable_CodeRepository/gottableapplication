﻿using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [AllowAnonymous]
    public sealed class TermsConditionsController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        public TermsConditionsController(IOperationExceptionLog operationExceptionLog)
        {
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("/termandcondition")]
        public async Task<IActionResult> Index()
        {
            await Task.FromResult(1);
            return View();
        }
    }
}
