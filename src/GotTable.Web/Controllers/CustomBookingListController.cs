﻿using GotTable.DAO.Reports;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Reports;
using GotTable.Library.Reports.Criterias;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class CustomBookingListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IReportList<CustomBookingDAO, CustomBookingListCriteria> reportList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public CustomBookingListController(IReportList<CustomBookingDAO, CustomBookingListCriteria> reportList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.reportList = reportList;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingStartDate"></param>
        /// <param name="bookingEndDate"></param>
        /// <param name="restaurantName"></param>
        /// <param name="restaurantCategory"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(string bookingStartDate = "", string bookingEndDate = "", string restaurantName = "", string restaurantCategory = "", int currentPage = 0, int pageSize = 10)
        {
            DateTime dtBookingStartDate = String.IsNullOrEmpty(bookingStartDate) ? DateTime.Now.AddDays(-15) : DateTime.ParseExact(bookingStartDate, "dd-MM-yyyy", null);
            DateTime dtBookingEndDate = String.IsNullOrEmpty(bookingEndDate) ? DateTime.Now.AddDays(15) : DateTime.ParseExact(bookingEndDate, "dd-MM-yyyy", null);
            var criteria = new CustomBookingListCriteria(dtBookingStartDate, dtBookingEndDate, restaurantName, restaurantCategory, currentPage, pageSize);
            var customBookingLists = await reportList.Get(criteria);
            ViewBag.SelectedBookingStartDate = bookingStartDate;
            ViewBag.SelectedBookingEndDate = bookingEndDate;
            ViewBag.RestaurantName = restaurantName;
            ViewBag.RestaurantCategory = restaurantCategory;
            ViewBag.TotalBookings = 0;
            return View(customBookingLists);
        }
    }
}
