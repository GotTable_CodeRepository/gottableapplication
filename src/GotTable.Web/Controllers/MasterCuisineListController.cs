﻿using GotTable.Library.Cuisines;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class MasterCuisineListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="cuisineList"></param>
        /// <param name="httpContextAccessor"></param>
        public MasterCuisineListController(IOperationExceptionLog operationExceptionLog, ICuisineList cuisineList, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.cuisineList = cuisineList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(int currentPage = 0, int pageSize = 10, bool status = true)
        {
            var masterCuisineList = await cuisineList.Get(currentPage, pageSize);
            return View(masterCuisineList);
        }
    }
}
