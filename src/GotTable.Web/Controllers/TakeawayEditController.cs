﻿using GotTable.Common.List;
using GotTable.DAO.Bookings.DeliveryandTakeaway;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class TakeawayEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDeliveryandTakeaway deliveryandTakeaway;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deliveryandTakeaway"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public TakeawayEditController(IDeliveryandTakeaway deliveryandTakeaway, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.deliveryandTakeaway = deliveryandTakeaway;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <param name="isRead"></param>
        /// <returns></returns>
        public async Task<IActionResult> Index(decimal bookingId, bool isRead)
        {
            if (isRead)
            {
                await deliveryandTakeaway.MarkAsRead(bookingId);
            }
            var takeawayModel = await deliveryandTakeaway.Get(bookingId);
            ViewBag.TakeawayStatusList = DeliveryandTakeawayStatusList.Get();
            return View(takeawayModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> NewStatus(DeliveryandTakeawayStatusDAO model)
        {
            await deliveryandTakeaway.New(model);
            return RedirectToAction("Index", new { bookingId = model.BookingId, isRead = false });
        }
    }
}
