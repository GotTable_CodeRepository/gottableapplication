﻿using GotTable.DAO.RestaurantConfigurations;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantConfigurations;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class ConfigurationEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchConfiguration branchConfiguration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchConfiguration"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public ConfigurationEditController(IBranchConfiguration branchConfiguration, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.branchConfiguration = branchConfiguration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal restaurantId)
        {
            var model = await branchConfiguration.Get(restaurantId);
            ViewBag.BooleanDropDown = new List<string>() { "true", "false" };
            ViewBag.IntegerDropDown = new List<string>() { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchConfigurationDAO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(BranchConfigurationDAO branchConfigurationDAO)
        {
            await branchConfiguration.Save(branchConfigurationDAO);
            ViewBag.BooleanDropDown = new List<string>() { "true", "false" };
            ViewBag.IntegerDropDown = new List<string>() { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            return View(branchConfigurationDAO);
        }
    }
}
