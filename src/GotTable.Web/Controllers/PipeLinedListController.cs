﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class PipeLinedListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IPipelineRestaurantList pipelineRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pipelineRestaurantList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public PipeLinedListController(IPipelineRestaurantList pipelineRestaurantList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.pipelineRestaurantList = pipelineRestaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal? userId = null, int currentPage = 0, int pageSize = 10)
        {
            dynamic restaurantList;
            if (userId == null)
            {
                restaurantList = await pipelineRestaurantList.Get(currentPage, pageSize);
            }
            else
            {
                restaurantList = await pipelineRestaurantList.Get((decimal)userId, currentPage, pageSize);
            }
            return View(restaurantList);
        }
    }
}
