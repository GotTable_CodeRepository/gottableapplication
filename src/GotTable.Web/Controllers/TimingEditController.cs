﻿using GotTable.Common.Enumerations;
using GotTable.Common.List;
using GotTable.DAO.RestaurantTimings;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantTimings;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class TimingEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTiming branchTiming;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="branchTiming"></param>
        /// <param name="httpContextAccessor"></param>
        public TimingEditController(IOperationExceptionLog operationExceptionLog, IBranchTiming branchTiming, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.branchTiming = branchTiming;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal restaurantId, Enumeration.RestaurantTypes type)
        {
            var branchTimingDAO = await branchTiming.Get(restaurantId, type);
            ViewBag.LunchSlot = LunchTimeSlotList.Get();
            ViewBag.DinnerSlot = DinnerTimeSlotList.Get();
            return View(branchTimingDAO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTimingDAO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(BranchTimingDAO branchTimingDAO)
        {
            await branchTiming.Save(branchTimingDAO);
            if (branchTimingDAO.Status)
            {
                return RedirectToAction("Index", "TimingList", new { @restaurantId = branchTimingDAO.BranchId });
            }
            ViewBag.LunchSlot = LunchTimeSlotList.Get();
            ViewBag.DinnerSlot = DinnerTimeSlotList.Get();
            return View(branchTimingDAO);
        }
    }
}
