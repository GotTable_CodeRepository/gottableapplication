﻿using GotTable.Library.Cuisines;
using GotTable.Library.OfferCategories;
using GotTable.Library.Localities;
using GotTable.Library.OperationExceptionLogs;

using System.Threading.Tasks;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class ApplicationRestaurantListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly ICategoryList categoryList;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        private readonly ILocaltyList localtyList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="categoryList"></param>
        /// <param name="cuisineList"></param>
        /// <param name="localtyList"></param>
        /// <param name="httpContextAccessor"></param>
        public ApplicationRestaurantListController(IOperationExceptionLog operationExceptionLog, ICategoryList categoryList, ICuisineList cuisineList, ILocaltyList localtyList, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.categoryList = categoryList;
            this.cuisineList = cuisineList;
            this.localtyList = localtyList;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewBag.CategoryList = await categoryList.Get();
            ViewBag.CuisineList = await cuisineList.Get(currentPage: 0, pageSize: 10);
            ViewBag.LocaltyList = await localtyList.Get(true);
            return View();
        }
    }
}