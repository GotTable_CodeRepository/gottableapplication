﻿using GotTable.DAO.RestaurantTables;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantTables;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class TableEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTable branchTable;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTable"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public TableEditController(IBranchTable branchTable, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.branchTable = branchTable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal tableId)
        {
            var branchTableDAO = await branchTable.Get(tableId);
            ViewBag.TableList = Common.List.TableList.Get();
            return View(branchTableDAO);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTableDAO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(BranchTableDAO branchTableDAO)
        {
            await branchTable.Save(branchTableDAO);
            if (branchTableDAO.Status)
            {
                return RedirectToAction("Index", "TableList", new { @restaurantId = branchTableDAO.BranchId });
            }
            ViewBag.TableList = Common.List.TableList.Get();
            return View(branchTableDAO);
        }
    }
}
