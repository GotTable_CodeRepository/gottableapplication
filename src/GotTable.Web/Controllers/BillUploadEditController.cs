﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantBookings.BillUploads;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantBookings;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class BillUploadEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDineInBookingBillUpload dineInBookingBillUpload;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineInBookingBillUpload"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public BillUploadEditController(IDineInBookingBillUpload dineInBookingBillUpload, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.dineInBookingBillUpload = dineInBookingBillUpload;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(int bookingId = 0)
        {
            var model = await dineInBookingBillUpload.Get(bookingId);
            model.AdminId = decimal.Parse(identityUser.UserId);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(DineInBookingBillUploadDAO model)
        {
            await dineInBookingBillUpload.Save(model);
            if (model.Status)
            {
                return RedirectToAction("Index", "BillUploadList", new { userId = identityUser.UserId, uploadStatus = Enumeration.BillUpload.Pending });
            }
            model.Authorized = null;
            return View(model);
        }
    }
}
