﻿using GotTable.DAO.Restaurants;
using GotTable.Library.Localities;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class RestaurantEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SalesAdmin, SuperAdmin, HotelAdmin, AccountAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IRestaurant restaurant;

        /// <summary>
        /// 
        /// </summary>
        private readonly ILocaltyList localtyList;

        public RestaurantEditController(IRestaurant restaurant, ILocaltyList localtyList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.restaurant = restaurant;
            this.localtyList = localtyList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal restaurantId)
        {
            dynamic branchDetail;
            if (restaurantId == 0)
            {
                branchDetail = await restaurant.Create(Convert.ToDecimal(identityUser.UserId));
            }
            else
            {
                
                branchDetail = await restaurant.Get(restaurantId);
            }
            ViewBag.LocaltyList = await localtyList.Get(true);
            return View(branchDetail);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchDetail"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(BranchDetailDAO branchDetail)
        {
            if (branchDetail.BranchId == 0)
            {
                await restaurant.New(branchDetail);
                if (branchDetail.Status)
                {
                    return RedirectToAction("Index", "PipeLinedList", new { @userId = identityUser.UserId });
                }
                else
                {
                    return View(branchDetail);
                }
            }
            else
            {
                await restaurant.Save(branchDetail);
            }
            ViewBag.LocaltyList = await localtyList.Get(true);
            return RedirectToAction("Index", "RestaurantEdit", new { @restaurantId = branchDetail.BranchId });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ToggleRestaurant(decimal restaurantId)
        {
            var restaurantDAO = await restaurant.Get(restaurantId);
            restaurantDAO.IsActive = !restaurantDAO.IsActive;
            await restaurant.Save(restaurantDAO);
            return RedirectToAction("Index", "InvoicedList");
        }
    }
}
