﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantOffers;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class OfferListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IOfferList offerList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public OfferListController(IOfferList offerList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.offerList = offerList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="displayExpiredOffers"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal restaurantId, bool? displayExpiredOffers = null, int currentPage = 0, int pageSize = 10)
        {
            ViewBag.ActiveSubMenu = displayExpiredOffers == true ? "History" : "Current";
            var offerDAOs = await offerList.Get(restaurantId, null, null, null, null, displayExpiredOffers, null, currentPage, pageSize);
            return View(offerDAOs);
        }
    }
}
