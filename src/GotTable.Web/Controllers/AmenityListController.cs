﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantAmenities;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// AmenityList
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class AmenityListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, AccountAdmin, HotelAdmin";

        /// <summary>
        /// IAmenityList
        /// </summary>
        private readonly IAmenityList amenityList;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="amenityList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public AmenityListController(IAmenityList amenityList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.amenityList = amenityList;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal restaurantId, int currentPage = 0, int pageSize = 10)
        {
            var amenityDAOS = await amenityList.Get(restaurantId, currentPage, pageSize);
            return View(amenityDAOS);
        }
    }
}