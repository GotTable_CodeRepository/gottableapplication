﻿using GotTable.DAO.Reports;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Reports;
using GotTable.Library.Reports.Criterias;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class ExpiredRestaurantListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IReportList<ExpiredRestaurantDAO, ExpiredRestaurantListCriteria> reportList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public ExpiredRestaurantListController(IReportList<ExpiredRestaurantDAO, ExpiredRestaurantListCriteria> reportList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.reportList = reportList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(int currentPage = 0, int pageSize = 10)
        {
            var criteria = new ExpiredRestaurantListCriteria(currentPage, pageSize);
            var restaurants = await reportList.Get(criteria);
            return View(restaurants);
        }
    }
}
