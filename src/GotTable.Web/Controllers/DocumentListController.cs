﻿using GotTable.Common.Enumerations;
using GotTable.DAO.RestaurantDocuments;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantDocuments;
using GotTable.Web.Filters;
using GotTable.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public class DocumentListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IDocumentList documentList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationExceptionLog"></param>
        /// <param name="documentList"></param>
        /// <param name="document"></param>
        /// <param name="httpContextAccessor"></param>
        public DocumentListController(IOperationExceptionLog operationExceptionLog, IDocumentList documentList, IDocument document, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.documentList = documentList;
            this.document = document;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="imageCategoryId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<IActionResult> Index(decimal restaurantId, Enumeration.ImageCategory? imageCategoryId = default, int currentPage = default, int pageSize = default)
        {
            var model = await documentList.Get(restaurantId, imageCategoryId, currentPage, pageSize);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentDAO"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(DocumentDAO documentDAO)
        {
            await document.Post(documentDAO);
            return RedirectToAction("index", new { restaurantId = identityUser.RestaurantId });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Delete(int documentId)
        {
            await document.Delete(documentId);
            return RedirectToAction("index", new { restaurantId = identityUser.RestaurantId });
        }
    }
}
