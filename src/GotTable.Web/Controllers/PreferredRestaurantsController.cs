﻿using GotTable.DAO.Restaurants.PreferredRestaurants;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.Restaurants;
using GotTable.Library.Restaurants.PreferredRestaurants;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class PreferredRestaurantsController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "SuperAdmin, SalesAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IPreferredRestaurantList preferredRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IPreferredRestaurant preferredRestaurant;

        /// <summary>
        /// 
        /// </summary>
        private readonly IInCityRestaurantList inCityRestaurantList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurantList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="preferredRestaurant"></param>
        /// <param name="inCityRestaurantList"></param>
        /// <param name="httpContextAccessor"></param>
        public PreferredRestaurantsController(IPreferredRestaurantList preferredRestaurantList, IOperationExceptionLog operationExceptionLog, IPreferredRestaurant preferredRestaurant, IInCityRestaurantList inCityRestaurantList, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.preferredRestaurantList = preferredRestaurantList;
            this.preferredRestaurant = preferredRestaurant;
            this.inCityRestaurantList = inCityRestaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal? cityId = null)
        {
            ViewBag.RestaurantList = await inCityRestaurantList.Get(cityId);
            var model = await preferredRestaurantList.Get(cityId, 0, 10);
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Add(PreferredRestaurantDAO model)
        {
            if (model.RestaurantId != 0)
            {
                await preferredRestaurant.New(model);
            }
            return RedirectToAction("Index", new { @cityId = identityUser.CityId });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preferredRestaurantId"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Remove(decimal preferredRestaurantId, decimal cityId)
        {
            if (preferredRestaurantId != 0)
            {
                await preferredRestaurant.Delete(preferredRestaurantId);
            }
            return RedirectToAction("Index", new { @cityId = cityId });
        }
    }
}
