﻿using GotTable.DAO.Communications.Email;
using GotTable.Library.Communications.Email;
using GotTable.Library.OperationExceptionLogs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ForgetPasswordController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IEmailTransmission<ForgetPassword, ForgetPasswordDAO> emailTransmission;

        /// <summary>
        /// 
        /// </summary>
        private readonly IOperationExceptionLog operationExceptionLog;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailTransmission"></param>
        /// <param name="operationExceptionLog"></param>
        public ForgetPasswordController(IEmailTransmission<ForgetPassword, ForgetPasswordDAO> emailTransmission, IOperationExceptionLog operationExceptionLog)
        {
            this.emailTransmission = emailTransmission;
            this.operationExceptionLog = operationExceptionLog;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            await Task.FromResult(1);
            ForgetPasswordDAO forgetPassword = new ForgetPasswordDAO();
            return View(forgetPassword);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="forgetPassword"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(ForgetPasswordDAO forgetPassword)
        {
            await emailTransmission.Execute(forgetPassword);
            return View(forgetPassword);
        }
    }
}
