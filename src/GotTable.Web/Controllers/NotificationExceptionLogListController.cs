﻿using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.PushNotifications;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class NotificationExceptionLogListController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, SalesAdmin, SuperAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly INotificationExceptionLogList notificationExceptionLogList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationExceptionLogList"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="httpContextAccessor"></param>
        public NotificationExceptionLogListController(INotificationExceptionLogList notificationExceptionLogList, IOperationExceptionLog operationExceptionLog, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.notificationExceptionLogList = notificationExceptionLogList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public async Task<IActionResult> Index(decimal notificationId)
        {
            var model = await notificationExceptionLogList.Get(notificationId);
            return View(model);
        }
    }
}
