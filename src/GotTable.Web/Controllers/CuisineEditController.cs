﻿using GotTable.DAO.RestaurantCuisines;
using GotTable.Library.OperationExceptionLogs;
using GotTable.Library.RestaurantCuisines;
using GotTable.Web.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace GotTable.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Permission(allowedRoles)]
    public sealed class CuisineEditController : UserBaseController
    {
        /// <summary>
        /// 
        /// </summary>
        private const string allowedRoles = "AccountAdmin, HotelAdmin";

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchCuisine branchCuisine;

        /// <summary>
        /// 
        /// </summary>
        private readonly Library.Cuisines.ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchCuisine"></param>
        /// <param name="operationExceptionLog"></param>
        /// <param name="cuisineList"></param>
        /// <param name="httpContextAccessor"></param>
        public CuisineEditController(IBranchCuisine branchCuisine, IOperationExceptionLog operationExceptionLog, Library.Cuisines.ICuisineList cuisineList, IHttpContextAccessor httpContextAccessor) : base(operationExceptionLog, httpContextAccessor)
        {
            this.branchCuisine = branchCuisine;
            this.cuisineList = cuisineList;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="cuisineId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index(decimal cuisineId)
        {
            BranchCuisineDAO branchCuisineDAO;
            if (cuisineId == 0)
            {
                var restaurantId = identityUser.RestaurantId;
                branchCuisineDAO = await branchCuisine.Create(Convert.ToDecimal(restaurantId));
            }
            else
            {
                branchCuisineDAO = await branchCuisine.Get(cuisineId);
            }
            ViewBag.CuisineList = await cuisineList.Get(0, 10);
            return View(branchCuisineDAO);
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(BranchCuisineDAO model)
        {
            if (model.Id == 0)
            {
                await branchCuisine.New(model);
            }
            else
            {
                await branchCuisine.Save(model);
            }
            if (model.Status)
            {
                return RedirectToAction("Index", "CuisineList", new { @restaurantId = model.BranchId });
            }
            else
            {
                ViewBag.CuisineList = await cuisineList.Get(0, 10);
                return View(model);
            }
        }
    }
}
