﻿
using Autofac;
using GotTable.DAO.Administrators;
using GotTable.DAO.Reports;
using GotTable.Library.Administrators;
using GotTable.Library.ApplicationLogs;
using GotTable.Library.Cities;
using GotTable.Library.Cuisines;
using GotTable.Library.Devices;
using GotTable.Library.MenuCategories;
using GotTable.Library.Reports;

namespace GotTable.Web.Shared
{
    /// <summary>
    /// 
    /// </summary>
    public class AutofacConfiguration
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public static void Init(ContainerBuilder builder)
        {
            #region Injecting dependecies

            builder.RegisterType<Library.Security.ApplicationPrincipal>().As<Library.Security.IApplicationPrincipal>();

            builder.RegisterType<Library.Versions.Version>().As<Library.Versions.IVersion>();
            builder.RegisterType<Library.Versions.VersionList>().As<Library.Versions.IVersionList>();

            builder.RegisterType<Library.Localities.Localty>().As<Library.Localities.ILocalty>();
            builder.RegisterType<Library.Localities.LocaltyList>().As<Library.Localities.ILocaltyList>();

            builder.RegisterType<Library.OperationExceptionLogs.OperationExceptionLog>().As<Library.OperationExceptionLogs.IOperationExceptionLog>();

            builder.RegisterType<Library.Amenities.AmenityList>().As<Library.Amenities.IAmenityList>();

            builder.RegisterType<Library.ApplicationSettings.ApplicationSetting>().As<Library.ApplicationSettings.IApplicationSetting>();

            builder.RegisterType<ApplicationLog>().As<IApplicationLog>();

            builder.RegisterType<CategoryList>().As<ICategoryList>();

            builder.RegisterType<CityList>().As<ICityList>();

            builder.RegisterType<Cuisine>().As<ICuisine>();
            builder.RegisterType<CuisineList>().As<ICuisineList>();

            builder.RegisterType<Library.OfferCategories.CategoryList>().As<Library.OfferCategories.ICategoryList>();
            builder.RegisterType<Library.OfferCategories.OfferCategory>().As<Library.OfferCategories.IOfferCategory>();

            builder.RegisterType<Administrator>().As<IAdministrator<AdminDAO>>();
            builder.RegisterType<AdministratorList>().As<IAdministratorList<AdminInfoDAO, ListCriteria>>();

            builder.RegisterType<Library.Users.User>().As<Library.Users.IUser>();
            builder.RegisterType<Library.Users.PhoneNumberList>().As<Library.Users.IPhoneNumberList>();

            builder.RegisterType<Device>().As<IDevice>();
            builder.RegisterType<TokenList>().As<ITokenList>();

            builder.RegisterType<Library.PushNotifications.PushNotification>().As<Library.PushNotifications.IPushNotification>();
            builder.RegisterType<Library.PushNotifications.NotificationList>().As<Library.PushNotifications.INotificationList>();
            builder.RegisterType<Library.PushNotifications.NotificationExceptionLogList>().As<Library.PushNotifications.INotificationExceptionLogList>();

            builder.RegisterType<CustomBookingList>().As<IReportList<CustomBookingDAO, Library.Reports.Criterias.CustomBookingListCriteria>>();
            builder.RegisterType<DailyBookingList>().As<IReportList<DailyBookingDAO, Library.Reports.Criterias.DailyBookingListCriteria>>();
            builder.RegisterType<ExpiredRestaurantList>().As<IReportList<ExpiredRestaurantDAO, Library.Reports.Criterias.ExpiredRestaurantListCriteria>>();
            builder.RegisterType<DeletedRestaurantList>().As<IReportList<RestaurantDAO, Library.Reports.Criterias.DeletedRestaurantListCriteria>>();

            builder.RegisterType<Library.Restaurants.PreferredRestaurantList>().As<Library.Restaurants.IPreferredRestaurantList>();
            builder.RegisterType<Library.Restaurants.AccountAdminRestaurantList>().As<Library.Restaurants.IAccountAdminRestaurantList>();
            builder.RegisterType<Library.Restaurants.PipelineRestaurantList>().As<Library.Restaurants.IPipelineRestaurantList>();
            builder.RegisterType<Library.Restaurants.SalesAdminRestaurantList>().As<Library.Restaurants.ISalesAdminRestaurantList>();
            builder.RegisterType<Library.Restaurants.ApplicationRestaurantList>().As<Library.Restaurants.IApplicationRestaurantList>();
            builder.RegisterType<Library.Restaurants.InCityRestaurantList>().As<Library.Restaurants.IInCityRestaurantList>();
            builder.RegisterType<Library.Restaurants.InvoicedRestaurantList>().As<Library.Restaurants.IInvoicedRestaurantList>();
            builder.RegisterType<Library.Restaurants.Restaurant>().As<Library.Restaurants.IRestaurant>();
            builder.RegisterType<Library.Restaurants.RestaurantSetting>().As<Library.Restaurants.IRestaurantSetting>();
            builder.RegisterType<Library.Restaurants.RestaurantTag>().As<Library.Restaurants.IRestaurantTag<Library.Restaurants.RestaurantTag>>();
            builder.RegisterType<Library.Restaurants.RestaurantListTag>().As<Library.Restaurants.IRestaurantTag<Library.Restaurants.RestaurantListTag>>();
            builder.RegisterType<Library.Restaurants.AutoCompleteList>().As<Library.Restaurants.IAutoCompleteList>();

            builder.RegisterType<Library.Restaurants.Filters.FilterList>().As<Library.Restaurants.Filters.IFilterList>();
            builder.RegisterType<Library.Restaurants.PreferredRestaurants.PreferredRestaurant>().As<Library.Restaurants.PreferredRestaurants.IPreferredRestaurant>();

            builder.RegisterType<Library.RestaurantTables.BranchTable>().As<Library.RestaurantTables.IBranchTable>();
            builder.RegisterType<Library.RestaurantTables.TableList>().As<Library.RestaurantTables.ITableList>();

            builder.RegisterType<Library.RestaurantTimings.TimingList>().As<Library.RestaurantTimings.ITimingList>();
            builder.RegisterType<Library.RestaurantTimings.BranchTiming>().As<Library.RestaurantTimings.IBranchTiming>();
            builder.RegisterType<Library.RestaurantTimings.TimingList>().As<Library.RestaurantTimings.ITimingList>();
            builder.RegisterType<Library.RestaurantTimings.BranchTiming>().As<Library.RestaurantTimings.IBranchTiming>();

            builder.RegisterType<Library.RestaurantRatings.BranchRating>().As<Library.RestaurantRatings.IBranchRating>();
            builder.RegisterType<Library.RestaurantRatings.RatingList>().As<Library.RestaurantRatings.IRatingList>();

            builder.RegisterType<Library.RestaurantBookings.UserBookingList>().As<Library.RestaurantBookings.IUserBookingList>();
            builder.RegisterType<Library.RestaurantBookings.RestaurantBookingList>().As<Library.RestaurantBookings.IRestaurantBookingList>();
            builder.RegisterType<Library.RestaurantBookings.DeliveryandTakeaway>().As<Library.RestaurantBookings.IDeliveryandTakeaway>();
            builder.RegisterType<Library.RestaurantBookings.DeliveryandTakeawayList>().As<Library.RestaurantBookings.IDeliveryandTakeawayList>();
            builder.RegisterType<Library.RestaurantBookings.DineIn>().As<Library.RestaurantBookings.IDineIn>();
            builder.RegisterType<Library.RestaurantBookings.DineInList>().As<Library.RestaurantBookings.IDineInList>();
            builder.RegisterType<Library.RestaurantBookings.DineInBookingBillUpload>().As<Library.RestaurantBookings.IDineInBookingBillUpload>();
            builder.RegisterType<Library.RestaurantBookings.DineInBookingBillUploadList>().As<Library.RestaurantBookings.IDineInBookingBillUploadList>();

            builder.RegisterType<Library.RestaurantConfigurations.BranchConfiguration>().As<Library.RestaurantConfigurations.IBranchConfiguration>();

            builder.RegisterType<Library.RestaurantContacts.BranchContact>().As<Library.RestaurantContacts.IBranchContact>();
            builder.RegisterType<Library.RestaurantContacts.ContactList>().As<Library.RestaurantContacts.IContactList>();

            builder.RegisterType<Library.RestaurantCuisines.BranchCuisine>().As<Library.RestaurantCuisines.IBranchCuisine>();
            builder.RegisterType<Library.RestaurantCuisines.CuisineList>().As<Library.RestaurantCuisines.ICuisineList>();

            builder.RegisterType<Library.RestaurantDocuments.Document>().As<Library.RestaurantDocuments.IDocument>();
            builder.RegisterType<Library.RestaurantDocuments.DocumentList>().As<Library.RestaurantDocuments.IDocumentList>();

            builder.RegisterType<Library.RestaurantInvoices.InvoiceList>().As<Library.RestaurantInvoices.IInvoiceList>();
            builder.RegisterType<Library.RestaurantInvoices.BranchInvoice>().As<Library.RestaurantInvoices.IBranchInvoice>();

            builder.RegisterType<Library.RestaurantMenus.BranchMenu>().As<Library.RestaurantMenus.IBranchMenu>();
            builder.RegisterType<Library.RestaurantMenus.MenuList>().As<Library.RestaurantMenus.IMenuList>();

            builder.RegisterType<Library.RestaurantOffers.BranchOffer>().As<Library.RestaurantOffers.IBranchOffer>();
            builder.RegisterType<Library.RestaurantOffers.OfferList>().As<Library.RestaurantOffers.IOfferList>();

            builder.RegisterType<Library.RestaurantAmenities.AmenityList>().As<Library.RestaurantAmenities.IAmenityList>();
            builder.RegisterType<Library.RestaurantAmenities.Amenity>().As<Library.RestaurantAmenities.IAmenity>();

            builder.RegisterType<Library.Security.ApplicationPrincipal>().As<Library.Security.IApplicationPrincipal>();


            //MESSAGE TRANSMISSION
            builder.RegisterType<Library.Communications.Message.DineInBookingConfirmation>().As<Library.Communications.Message.IMessageTransmission<Library.Communications.Message.DineInBookingConfirmation, string>>();
            builder.RegisterType<Library.Communications.Message.DeliveryandTakeawayBookingConfirmation>().As<Library.Communications.Message.IMessageTransmission<Library.Communications.Message.DeliveryandTakeawayBookingConfirmation, string>>();
            builder.RegisterType<Library.Communications.Message.Type2Factors.OTPCommand>().As<Library.Communications.Message.Type2Factors.IOTPCommand>();


            // EMAIL TRANSMISSION
            builder.RegisterType<Library.Communications.Email.ForgetPassword>().As<Library.Communications.Email.IEmailTransmission<Library.Communications.Email.ForgetPassword, DAO.Communications.Email.ForgetPasswordDAO>>();
            builder.RegisterType<Library.Communications.Email.RestaurantCredential>().As<Library.Communications.Email.IEmailTransmission<Library.Communications.Email.RestaurantCredential, decimal>>();


            // JOBS
            builder.RegisterType<Library.Communications.Jobs.Types.JobFactory>().As<Library.Communications.Jobs.Types.IJobFactory>();
            builder.RegisterType<Library.Communications.Jobs.AndroidPushNotificationJob>().As<Library.Communications.Jobs.Types.IJob<Library.Communications.Jobs.AndroidPushNotificationJob, DAO.PushNotifications.PushNotificationDAO>>();
            builder.RegisterType<Library.Communications.Jobs.IOSPushNotificationJob>().As<Library.Communications.Jobs.Types.IJob<Library.Communications.Jobs.IOSPushNotificationJob, DAO.PushNotifications.PushNotificationDAO>>();
            builder.RegisterType<Library.Communications.Jobs.DineInBookingAutoCompleteJob>().As<Library.Communications.Jobs.Types.IJob<Library.Communications.Jobs.DineInBookingAutoCompleteJob, DAO.Bookings.DineIn.DineInBookingDAO>>();
            builder.RegisterType<Library.Communications.Jobs.DeliveryandTakeawayBookingConfirmationJob>().As<Library.Communications.Jobs.Types.IJob<Library.Communications.Jobs.DeliveryandTakeawayBookingConfirmationJob, DAO.Bookings.DeliveryandTakeaway.DeliveryandTakeawayDAO>>();
            builder.RegisterType<Library.Communications.Jobs.DineInBookingConfirmationJob>().As<Library.Communications.Jobs.Types.IJob<Library.Communications.Jobs.DineInBookingConfirmationJob, DAO.Bookings.DineIn.DineInBookingDAO>>();
            builder.RegisterType<Library.Communications.Jobs.DeliveryandTakeawayBookingConfirmationJob>().As<Library.Communications.Jobs.Types.IJob<Library.Communications.Jobs.DeliveryandTakeawayBookingConfirmationJob, DAO.Bookings.DeliveryandTakeaway.DeliveryandTakeawayDAO>>();

            #endregion
        }
    }
}
