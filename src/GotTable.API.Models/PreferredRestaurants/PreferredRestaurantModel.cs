﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.PreferredRestaurants
{
    /// <summary>
    /// PreferredRestaurantModel
    /// </summary>
    public class PreferredRestaurantModel
    {
        public PreferredRestaurantModel(string restaurantId = default, string name = default, string longitude = default, string latitude = default, string state = default, string zipCode = default, string address = default, string distance = default, string imageUrl = default, string seoKeyword = default, string seoTitle = default, string seoDescription = default, string tagLine = default)
        {

        }

        /// <summary>
        /// RestaurantId
        /// </summary>
        [JsonProperty("RestaurantId")]
        public string RestaurantId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("Name")]
        public string Name { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        [JsonProperty("Address")]
        public string Address { get; set; }

        /// <summary>
        /// City
        /// </summary>
        [JsonProperty("City")]
        public string City { get; set; }

        /// <summary>
        /// State
        /// </summary>
        [JsonProperty("State")]
        public string State { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        [JsonProperty("Latitude")]
        public string Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        [JsonProperty("Longitude")]
        public string Longitude { get; set; }

        /// <summary>
        /// Zipcode
        /// </summary>
        [JsonProperty("Zipcode")]
        public string Zipcode { get; set; }

        /// <summary>
        /// Distance
        /// </summary>
        [JsonProperty("Distance")]
        public string Distance { get; set; }

        /// <summary>
        /// TagLine
        /// </summary>
        [JsonProperty("TagLine")]
        public string TagLine { get; set; }

        /// <summary>
        /// SEOTitle
        /// </summary>
        [JsonProperty("SEOTitle")]
        public string SEOTitle { get; set; }

        /// <summary>
        /// SEODescription
        /// </summary>
        [JsonProperty("SEODescription")]
        public string SEODescription { get; set; }

        /// <summary>
        /// SEOKeyword
        /// </summary>
        [JsonProperty("SEOKeyword")]
        public string SEOKeyword { get; set; }

        /// <summary>
        /// ImageUrl
        /// </summary>
        [JsonProperty("ImageUrl")]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Rating
        /// </summary>
        [JsonProperty("Rating")]
        public int Rating { get; set; }
    }
}
