﻿using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantTables
{
    /// <summary>
    /// Table model
    /// </summary>
    public sealed class TableModel
    {
        public TableModel(string id = default, string name = default, int total = default, bool vacant = default)
        {
            Id = id;
            Name = name;
            Total = total;
            Vacant = vacant;
        }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Total
        /// </summary>
        [JsonProperty("total")]
        public int Total { get; set; }

        /// <summary>
        /// Vacant
        /// </summary>
        [JsonProperty("vacant")]
        public bool Vacant { get; set; }
    }
}
