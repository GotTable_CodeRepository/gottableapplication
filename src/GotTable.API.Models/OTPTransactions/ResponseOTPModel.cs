﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.OTPTransactions
{
    /// <summary>
    /// ResponseOTPModel
    /// </summary>
    public class ResponseOTPModel
    {
        /// <summary>
        /// Message
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
