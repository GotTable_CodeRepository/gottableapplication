﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.RestaurantOffers
{
    /// <summary>
    /// RestaurantOfferModel
    /// </summary>
    public sealed class RestaurantOfferModel
    {
        public RestaurantOfferModel()
        {
            OfferList = new List<OfferModel>();
        }

        /// <summary>
        /// Error message
        /// </summary>
        [JsonProperty("error_message")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Offer list
        /// </summary>
        [JsonProperty("offer_list")]
        public List<OfferModel> OfferList { get; set; }
    }
}
