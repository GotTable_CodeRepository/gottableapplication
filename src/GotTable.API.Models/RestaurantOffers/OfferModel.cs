﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantOffers
{
    /// <summary>
    /// Offer model
    /// </summary>
    public sealed class OfferModel
    {
        /// <summary>
        /// OfferId
        /// </summary>
        [JsonProperty("offerid")]
        public string OfferId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// StartDate
        /// </summary>
        [JsonProperty("startdate")]
        public string StartDate { get; set; }

        /// <summary>
        /// EndDate
        /// </summary>
        [JsonProperty("enddate")]
        public string EndDate { get; set; }

        /// <summary>
        /// Tag
        /// </summary>
        [JsonProperty("tag")]
        public string Tag { get; set; }
    }
}
