﻿using Newtonsoft.Json;

namespace GotTable.API.Model.DeviceTokens
{
    /// <summary>
    /// DeviceResponse
    /// </summary>
    public sealed class DeviceResponse
    {
        /// <summary>
        /// ErrorMessage
        /// </summary>
        [JsonProperty("errormessage")]
        public string ErrorMessage { get; set; }
    }
}
