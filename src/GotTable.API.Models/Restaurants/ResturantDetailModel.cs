﻿using GotTable.API.Model.Cuisines.V1;
using GotTable.API.Model.Images;
using GotTable.API.Model.RestaurantAmenities;
using GotTable.API.Model.RestaurantContacts;
using GotTable.API.Model.RestaurantReviews;
using GotTable.API.Model.RestaurantTimings;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.Resturants
{
    public class ResturantDetailModel
    {
        public ResturantDetailModel()
        {
            this.Cuisines = new List<CuisineModel>();
            this.Contacts = new List<ContactModel>();
            this.Timings = new List<TimingModel>();
            this.Reviews = new List<ReviewModel>();
            this.Images = new List<ImageModel>();
            this.Amenities = new List<AmenityModel>();
            this.Images.Add(new ImageModel()
            {
                Name = "logo",
                Url = "https://www.gottable.in/images/food-restaurant-eat-snack.jpg"
            });
        }

        [JsonProperty("id")]
        public string Id { get; set; }


        [JsonProperty("name")]
        public string Name { get; set; }


        [JsonProperty("restaurantTag")]
        public string RestaurantTag { get; set; }


        [JsonProperty("distance")]
        public string Distance { get; set; }


        [JsonProperty("addressline1")]
        public string AddressLine1 { get; set; }


        [JsonProperty("addressline2")]
        public string AddressLine2 { get; set; }


        [JsonProperty("city")]
        public string City { get; set; }


        [JsonProperty("state")]
        public string State { get; set; }


        [JsonProperty("zip")]
        public string Zip { get; set; }


        [JsonProperty("latitude")]
        public string Latitude { get; set; }


        [JsonProperty("longitude")]
        public string Longitude { get; set; }


        [JsonProperty("rating")]
        public int Rating { get; set; }


        [JsonProperty("musicrating")]
        public int MusicRating { get; set; }


        [JsonProperty("ambiencerating")]
        public int AmbienceRating { get; set; }


        [JsonProperty("foodrating")]
        public int FoodRating { get; set; }


        [JsonProperty("pricerating")]
        public int PriceRating { get; set; }


        [JsonProperty("servicerating")]
        public int ServiceRating { get; set; }


        [JsonProperty("centralGST")]
        public string CentralGST { get; set; }


        [JsonProperty("stateGST")]
        public string StateGST { get; set; }


        [JsonProperty("deliverycharges")]
        public string DeliveryCharges { get; set; }


        [JsonProperty("standbytime")]
        public string StandByTime { get; set; }


        [JsonProperty("isofferavailable")]
        public bool IsOfferAvailable { get; set; }


        [JsonProperty("seokeyword")]
        public string SEOKeyword { get; set; }


        [JsonProperty("seotitle")]
        public string SEOTitle { get; set; }


        [JsonProperty("seodescription")]
        public string SEODescription { get; set; }


        [JsonProperty("description")]
        public string Description { get; set; }


        [JsonProperty("costfortwo")]
        public int CostForTwo { get; set; }


        [JsonProperty("tag")]
        public string Tag { get; set; }


        [JsonProperty("cuisines")]
        public List<CuisineModel> Cuisines { get; set; }


        [JsonProperty("reviews")]
        public List<ReviewModel> Reviews { get; set; }


        [JsonProperty("timings")]
        public List<TimingModel> Timings { get; set; }


        [JsonProperty("images")]
        public List<ImageModel> Images { get; set; }


        [JsonProperty("contacts")]
        public List<ContactModel> Contacts { get; set; }


        [JsonProperty("amenities")]
        public List<AmenityModel> Amenities { get; set; }
    }
}
