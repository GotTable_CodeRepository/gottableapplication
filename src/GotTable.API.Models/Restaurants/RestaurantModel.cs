﻿using GotTable.API.Model.Images;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.Resturants
{
    public sealed class RestaurantModel
    {
        public RestaurantModel()
        {
            Images = new List<ImageModel>();
        }

        [JsonProperty("id")]
        public string Id { get; set; }


        [JsonProperty("name")]
        public string Name { get; set; }


        [JsonProperty("restaurantTag")]
        public string RestaurantTag { get; set; }


        [JsonProperty("cuisinenames")]
        public string CuisineNames { get; set; }


        [JsonProperty("cuisineshortname")]
        public string CuisineShortName 
        {
            get
            {
                var cuisineShortName = string.Empty;
                if(!string.IsNullOrEmpty(CuisineNames))
                {
                    if (CuisineNames.Length > 8)
                    {
                        cuisineShortName = CuisineNames.Substring(0, 8) + "...";
                    }
                    else
                    {
                        cuisineShortName = CuisineNames;
                    }
                }
                return cuisineShortName;
            }
        }


        [JsonProperty("latitude")]
        public string Latitude { get; set; }


        [JsonProperty("longitude")]
        public string Longitude { get; set; }


        [JsonProperty("addressline1")]
        public string AddressLine1 { get; set; }


        [JsonProperty("addressline2")]
        public string AddressLine2 { get; set; }


        [JsonProperty("city")]
        public string City { get; set; }


        [JsonProperty("state")]
        public string State { get; set; }


        [JsonProperty("zip")]
        public string Zip { get; set; }


        [JsonProperty("distance")]
        public string Distance { get; set; }


        [JsonProperty("rating")]
        public int Rating { get; set; }

        [JsonProperty("musicrating")]
        public int MusicRating { get; set; }


        [JsonProperty("ambiencerating")]
        public int AmbienceRating { get; set; }


        [JsonProperty("foodrating")]
        public int FoodRating { get; set; }


        [JsonProperty("pricerating")]
        public int PriceRating { get; set; }


        [JsonProperty("servicerating")]
        public int ServiceRating { get; set; }


        [JsonProperty("tag")]
        public string Tag { get; set; }


        [JsonProperty("offername")]
        public string OfferName { get; set; }


        [JsonProperty("isofferavailable")]
        public bool IsOfferAvailable { get; set; }


        [JsonProperty("images")]
        public List<ImageModel> Images { get; set; }


        [JsonProperty("activeoffers")]
        public int ActiveOffers { get; set; }


        [JsonProperty("costfortwo")]
        public int CostForTwo { get; set; }


        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
