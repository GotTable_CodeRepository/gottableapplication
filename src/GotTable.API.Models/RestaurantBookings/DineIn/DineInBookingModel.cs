﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantBookings.DineIn
{
    /// <summary>
    /// Model uses for dine in booking.
    /// </summary>
    public sealed class DineInBookingModel
    {
        [JsonProperty("transactionid")]
        public string TransactionId { get; set; }

        [JsonProperty("otpnumber")]
        public string OtpNumber { get; set; }

        [JsonProperty("userid")]
        public string UserId { get; set; }

        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [JsonProperty("emailaddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("isemailoptedforcommunication")]
        public bool IsEmailOptedForCommunication { get; set; }

        [JsonProperty("bookingid")]
        public string BookingId { get; set; }

        [JsonProperty("phonenumber")]
        public long PhoneNumber { get; set; }

        [JsonProperty("branchid")]
        public string BranchId { get; set; }

        [JsonProperty("offerid")]
        public string OfferId { get; set; }

        [JsonProperty("promocode")]
        public string PromoCode { get; set; }

        [JsonProperty("tableid")]
        public string TableId { get; set; }

        [JsonProperty("bookingdate")]
        public string BookingDate { get; set; }

        [JsonProperty("bookingtime")]
        public string BookingTime { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("rewardapplied")]
        public bool RewardApplied { get; set; }

        [JsonProperty("billuploadstatus")]
        public string BillUploadStatus { get; set; }
    }
}
