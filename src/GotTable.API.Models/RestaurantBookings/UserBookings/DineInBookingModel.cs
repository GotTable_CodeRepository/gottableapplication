﻿using System.Collections.Generic;

namespace GotTable.API.Model.UserBookings
{
    public sealed class DineInBookingModel
    {
        public DineInBookingModel()
        {
            RestaurantDetail = new RestaurantModel();
            ContactList = new List<RestaurantContactModel>();
        }

        public string BookingId { get; set; }

        public string SelectedOffer { get; set; }

        public string DateForBooking { get; set; }

        public string TimeForBooking { get; set; }

        public string SelectedTable { get; set; }

        public string Status { get; set; }

        public string BillUploadStatus { get; set; }

        public RestaurantModel RestaurantDetail { get; set; }

        public List<RestaurantContactModel> ContactList { get; set; }
    }
}
