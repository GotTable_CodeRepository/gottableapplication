﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.RestaurantBookings.DeliveryandTakeaway
{
    public class DeliveryandTakeawayBookingModel
    {
        public DeliveryandTakeawayBookingModel()
        {
            this.CartItem = new List<CartModel>();
            this.AddressDetail = new DeliveryModel();
        }

        [JsonProperty("transactionid")]
        public string TransactionId { get; set; }


        [JsonProperty("otpnumber")]
        public string OtpNumber { get; set; }


        [JsonProperty("restaurantid")]
        public string RestaurantId { get; set; }


        [JsonProperty("bookingid")]
        public string BookingId { get; set; }


        [JsonProperty("bookingtype")]
        public string BookingType { get; set; }


        [JsonProperty("bookingdate")]
        public string BookingDate { get; set; }


        [JsonProperty("bookingtime")]
        public string BookingTime { get; set; }


        [JsonProperty("phonenumber")]
        public long PhoneNumber { get; set; }


        [JsonProperty("centralgst")]
        public string CentralGst { get; set; }


        [JsonProperty("stategst")]
        public string StateGst { get; set; }


        [JsonProperty("servicetax")]
        public string ServiceTax { get; set; }


        [JsonProperty("decimalcharges")]
        public string DecimalCharges { get; set; }


        [JsonProperty("totalamount")]
        public string TotalAmount { get; set; }


        [JsonProperty("offerid")]
        public string OfferId { get; set; }


        [JsonProperty("currentstatus")]
        public string CurrentStatus { get; set; }


        [JsonProperty("comment")]
        public string Comment { get; set; }


        [JsonProperty("userid")]
        public string UserId { get; set; }


        [JsonProperty("firstname")]
        public string FirstName { get; set; }


        [JsonProperty("lastname")]
        public string LastName { get; set; }


        [JsonProperty("emailaddress")]
        public string EmailAddress { get; set; }


        [JsonProperty("promocode")]
        public string PromoCode { get; set; }


        [JsonProperty("addressdetail")]
        public DeliveryModel AddressDetail { get; set; }


        [JsonProperty("cartitem")]
        public List<CartModel> CartItem { get; set; }
    }
}
