﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantBookings.DeliveryandTakeaway
{
    /// <summary>
    /// Model uses for cart information.
    /// </summary>
    public sealed class CartModel
    {
        /// <summary>
        /// Unique id of model.
        /// </summary>
        [JsonProperty("cartid")]
        public string CartId { get; set; }

        /// <summary>
        /// Menu id for the menu item.
        /// </summary>
        [JsonProperty("menuid")]
        public string MenuId { get; set; }

        /// <summary>
        /// Provides the quantity.
        /// </summary>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        /// <summary>
        /// Provides the amount.
        /// </summary>
        [JsonProperty("amount")]
        public string Amount { get; set; }
    }
}
