﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.RestaurantContacts
{
    /// <summary>
    /// ContactModel
    /// </summary>
    public sealed class ContactModel
    {
        public ContactModel(string id = default, string name = default, string emailaddress = default, long phoneNumber = default)
        {
            Id = id;
            Name = name;
            EmailAddress = emailaddress;
            PhoneNumber = phoneNumber;
        }

        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// EmailAddress
        /// </summary>
        [JsonProperty("emailaddress")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// PhoneNumber
        /// </summary>
        [JsonProperty("phonenumber")]
        public long PhoneNumber { get; set; }
    }
}
