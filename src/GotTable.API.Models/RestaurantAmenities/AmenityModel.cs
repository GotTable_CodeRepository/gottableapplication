﻿using System;

namespace GotTable.API.Model.RestaurantAmenities
{
    /// <summary>
    /// AmenityModel
    /// </summary>
    public sealed class AmenityModel
    {
        /// <summary>
        /// AmenityId
        /// </summary>
        public int AmenityId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
