﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Categories
{
    public sealed class CategoryModel
    {
        /// <summary>
        /// id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// list
        /// </summary>
        [JsonProperty("list")]
        public dynamic List { get; set; }
    }
}
