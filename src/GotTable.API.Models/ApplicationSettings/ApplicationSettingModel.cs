﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.ApplicationSettings
{
    /// <summary>
    /// Model uses to get the application settings
    /// </summary>
    public sealed class ApplicationSettingModel
    {
        /// <summary>
        /// Contain admin application base url.
        /// </summary>
        [JsonProperty("adminapplicationbaseurl")]
        public string AdminApplicationBaseUrl { get; set; }

        /// <summary>
        /// Contain user application base url.
        /// </summary>
        [JsonProperty("userapplicationbaseurl")]
        public string UserApplicationBaseUrl { get; set; }

        /// <summary>
        /// Provide "DineIn" status wheather true or false
        /// </summary>
        [JsonProperty("dineinactive")]
        public bool DineInActive { get; set; }

        /// <summary>
        /// Provide "Delivery" status wheather true or false
        /// </summary>
        [JsonProperty("deliveryactive")]
        public bool DeliveryActive { get; set; }

        /// <summary>
        /// Provide "Takeaway" status wheather true or false
        /// </summary>
        [JsonProperty("takeawayactive")]
        public bool TakeawayActive { get; set; }
    }
}
