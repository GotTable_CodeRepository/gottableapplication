﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.UserPasswords
{
    public sealed class UserPasswordModel
    {
        public UserPasswordModel()
        {
            this.UserId = string.Empty;
            this.OldPassword = string.Empty;
            this.NewPassword = string.Empty;
        }

        [JsonProperty("userid")]
        public string UserId { get; set; }

        [JsonProperty("oldpassword")]
        public string OldPassword { get; set; }

        [JsonProperty("newpassword")]
        public string NewPassword { get; set; }
    }
}
