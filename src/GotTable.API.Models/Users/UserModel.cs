﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Users
{
    public sealed class UserModel
    {
        [JsonProperty("userid")]
        public string UserId { get; set; }

        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [JsonProperty("emailaddress")]
        public string EmailAddress { get; set; }
    }
}
