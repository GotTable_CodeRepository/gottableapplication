﻿using GotTable.API.Model.UserAddresses;
using GotTable.API.Model.UserPhoneNumbers;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace GotTable.API.Model.Users
{
    public sealed class UserDetailModel
    {
        public UserDetailModel()
        {

        }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("emailaddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("usertype")]
        public string UserType { get; set; }

        [JsonProperty("prefix")]
        public string Prefix { get; set; }

        [JsonProperty("rewardcount")]
        public int RewardCount { get; set; }

        [JsonProperty("addresslist")]
        public List<AddressModel> AddressList { get; set; }

        [JsonProperty("phonenumberlist")]
        public List<PhoneNumberModel> PhoneNumberList { get; set; }
    }

}
