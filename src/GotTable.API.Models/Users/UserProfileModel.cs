﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.Users
{
    public sealed class UserProfileModel
    {
        public UserProfileModel()
        {

        }

        [JsonProperty("transactionid")]
        public string TransactionId { get; set; }

        [JsonProperty("otpnumber")]
        public string OtpNumber { get; set; }

        [JsonProperty("userid")]
        public string UserId { get; set; }

        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        public string LastName { get; set; }

        [JsonProperty("emailaddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("phonenumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("prefix")]
        public string Prefix { get; set; }
    }
}
