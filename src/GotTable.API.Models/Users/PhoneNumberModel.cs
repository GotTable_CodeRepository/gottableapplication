﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.UserPhoneNumbers
{
    public sealed class PhoneNumberModel
    {
        public PhoneNumberModel()
        {

        }

        [JsonProperty("numberid")]
        public string NumberId { get; set; }

        [JsonProperty("userid")]
        public string UserId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
