﻿
using Newtonsoft.Json;

namespace GotTable.API.Model.UserEmailAddresses
{
    public sealed class PostEmailAddress
    {
        [JsonProperty("emailaddress")]
        public string EmailAddress { get; set; }
    }
}
