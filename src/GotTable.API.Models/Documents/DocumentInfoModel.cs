﻿using Newtonsoft.Json;

namespace GotTable.API.Model.Documents
{
    /// <summary>
    /// DocumentInfoModel
    /// </summary>
    public sealed class DocumentInfoModel
    {
        /// <summary>
        /// DocumentId
        /// </summary>
        [JsonProperty("documentid")]
        public int DocumentId { get; set; }


        /// <summary>
        /// Path
        /// </summary>
        [JsonProperty("path")]
        public string Path { get; set; }
    }
}
