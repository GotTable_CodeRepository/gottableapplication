﻿using GotTable.DAO.RestaurantContacts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantContacts
{
    public interface IContactList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<BranchContactDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10);
    }
}