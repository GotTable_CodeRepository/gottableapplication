﻿using GotTable.Dal;
using GotTable.Dal.Amenities;
using GotTable.DAO.Amenities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Amenities
{
    /// <summary>
    /// AmenityList
    /// </summary>
    [Serializable]
    public sealed class AmenityList : IAmenityList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<AmenityDAO>> Get(int currentPage = 0, int pageSize = 10)
        {
            var amenityDaos = new List<AmenityDAO>();
            var dal = DalFactory.Create<IAmenityDal>();
            var list = await dal.FetchList(currentPage, pageSize);
            list.ForEach(item => amenityDaos.Add(new AmenityDAO() 
            { 
                Active = item.Active,
                Id = item.Id,
                Name = item.Name
            }));
            return amenityDaos;
        }
    }
}
