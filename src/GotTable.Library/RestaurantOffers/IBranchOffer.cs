﻿using GotTable.DAO.RestaurantOffers;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantOffers
{
    public interface IBranchOffer
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<BranchOfferDAO> Create(decimal restaurantId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerDAO"></param>
        Task New(BranchOfferDAO offerDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerDAO"></param>
        Task Save(BranchOfferDAO offerDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerId"></param>
        /// <returns></returns>
        Task<BranchOfferDAO> Get(decimal offerId);
    }
}