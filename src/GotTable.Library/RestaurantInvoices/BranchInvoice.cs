﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Restaurants;
using GotTable.Dal.RestaurantInvoices;
using GotTable.DAO.Administrators;
using GotTable.DAO.RestaurantInvoices;
using GotTable.Library.Administrators;
using GotTable.Library.Communications.Email;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantInvoices
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class BranchInvoice : IBranchInvoice
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IInvoiceList invoiceList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IAdministrator<AdminDAO> administrator;

        /// <summary>
        /// 
        /// </summary>
        private readonly IEmailTransmission<RestaurantCredential, decimal> emailTransmission;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceList"></param>
        /// <param name="emailTransmission"></param>
        /// <param name="administrator"></param>
        public BranchInvoice(IInvoiceList invoiceList, IEmailTransmission<RestaurantCredential, decimal> emailTransmission, IAdministrator<AdminDAO> administrator)
        {
            this.invoiceList = invoiceList;
            this.emailTransmission = emailTransmission;
            this.administrator = administrator;
        }

        #region Factory methods

        public async Task<BranchInvoiceDAO> Create(decimal restaurantId)
        {
            var invoiceDAOs = await invoiceList.Get(restaurantId, 0, 1);
            var branchInvoice = new BranchInvoiceDAO()
            {
                BranchId = restaurantId,
                FirstInvoice = !invoiceDAOs.Any(),
                UserId = invoiceDAOs.Any() ? invoiceDAOs.First().UserId : 0,
                FirstName = invoiceDAOs.Any() ? invoiceDAOs.First().FirstName : string.Empty,
                LastName = invoiceDAOs.Any() ? invoiceDAOs.First().LastName : string.Empty,
                EmailAddress = invoiceDAOs.Any() ? invoiceDAOs.First().EmailAddress : string.Empty,
                StatusId = 1,
                IsActive = true,
                PaymentMethodId = 0,
                ErrorMessage = string.Empty
            };
            return branchInvoice;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchInvoiceDAO"></param>
        /// <returns></returns>
        public async Task New(BranchInvoiceDAO branchInvoiceDAO)
        {
            if (String.IsNullOrEmpty(branchInvoiceDAO.SelectedPrescription))
            {
                branchInvoiceDAO.StatusId = 0;
                branchInvoiceDAO.ErrorMessage = "Invalid susbscription, Please check.!";
                return;
            }
            if (!branchInvoiceDAO.DineIn && !branchInvoiceDAO.Delivery && !branchInvoiceDAO.Takeaway)
            {
                branchInvoiceDAO.StatusId = 0;
                branchInvoiceDAO.ErrorMessage = "Invalid selection for categories, Please check.!";
                return;
            }
            branchInvoiceDAO.StartDate = DateTime.Now;
            branchInvoiceDAO.EndDate = SelectPackage(int.Parse(branchInvoiceDAO.SelectedPrescription));
            if (branchInvoiceDAO.FirstInvoice)
            {
                var user = new AdminDAO()
                {
                    FirstName = branchInvoiceDAO.FirstName,
                    LastName = branchInvoiceDAO.LastName,
                    EmailAddress = branchInvoiceDAO.EmailAddress,
                    Gender = Enumeration.Gender.NotDefined.ToString(),
                    Prefix = Enumeration.Prefix.NotDefined.ToString(),
                    IsActive = true,
                    UserType = Enumeration.AdminType.HotelAdmin.ToString(),
                    Password = DateTime.Now.ToString("dd-MM-yyyy")
                };

                await administrator.New(user);
                if (!user.Status)
                {
                    branchInvoiceDAO.StatusId = user.StatusId;
                    branchInvoiceDAO.ErrorMessage = user.ErrorMessage;
                    return;
                }

                if (branchInvoiceDAO.UserId == 0)
                {
                    var restaurantDal = DalFactory.Create<IRestaurantDal>();
                    var restarauntDto = await restaurantDal.Fetch(branchInvoiceDAO.BranchId);
                    restarauntDto.AdminId = user.UserId;
                    await restaurantDal.Update(restarauntDto);
                    branchInvoiceDAO.UserId = user.UserId;
                    branchInvoiceDAO.IsActive = true;
                }
            }

            var dto = new InvoiceDto()
            {
                InvoiceId = Guid.NewGuid(),
                BranchId = branchInvoiceDAO.BranchId,
                StartDate = branchInvoiceDAO.StartDate,
                Amount = branchInvoiceDAO.Amount,
                EndDate = branchInvoiceDAO.EndDate,
                IsActive = true,
                PrescriptionTypeId = (int)Enum.Parse(typeof(Enumeration.PrescriptionTypes), branchInvoiceDAO.SelectedPrescription),
                DineIn = branchInvoiceDAO.DineIn,
                Delivery = branchInvoiceDAO.Delivery,
                Takeaway = branchInvoiceDAO.Takeaway,
                IsPaymentDone = false,
                CreatedDate = DateTime.Now,
                PaymentMethodId = branchInvoiceDAO.PaymentMethodId ?? 0
            };
            var dal = DalFactory.Create<IInvoiceDal>();
            await dal.Insert(dto);

            if(branchInvoiceDAO.FirstInvoice)
            {
                await emailTransmission.Execute(branchInvoiceDAO.BranchId);
            }

            branchInvoiceDAO.InvoiceId = dto.InvoiceId;
            branchInvoiceDAO.StatusId = 1;
            branchInvoiceDAO.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceId"></param>
        /// <returns></returns>
        public async Task<BranchInvoiceDAO> Get(Guid invoiceId)
        {
            var dal = DalFactory.Create<IInvoiceDal>();
            var dto = await dal.Fetch(invoiceId);
            return new BranchInvoiceDAO()
            {
                Amount = dto.Amount,
                BranchId = dto.BranchId,
                CreatedDate = dto.CreatedDate ?? DateTime.Now,
                Delivery = dto.Delivery ?? false,
                DineIn = dto.DineIn ?? false,
                EndDate = dto.EndDate,
                FirstInvoice = false,
                InvoiceId = dto.InvoiceId,
                IsActive = dto.IsActive,
                IsPaymentDone = dto.IsPaymentDone ?? false,
                StartDate = dto.StartDate,
                SelectedPrescription = dto.PrescriptionTypeName,
                Takeaway = dto.Takeaway ?? false,
                PaymentDate = dto.PaymentDate,
                PaymentMethodId = dto.PaymentMethodId,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                EmailAddress = dto.EmailAddress,
                UserId = dto.UserId
            };
        }

        #endregion

        #region Helpers method

        private DateTime SelectPackage(int selectedSubscription)
        {

            if (selectedSubscription == (int)Enumeration.PrescriptionTypes.PackageOne_ForSixMonths)
            {
                return DateTime.Now.AddMonths(6);
            }
            else if (selectedSubscription == (int)Enumeration.PrescriptionTypes.PackageThree_For24Months)
            {
                return DateTime.Now.AddMonths(24);
            }
            else if (selectedSubscription == (int)Enumeration.PrescriptionTypes.PackageTwo_ForTwelveMonths)
            {
                return DateTime.Now.AddMonths(12);
            }
            else
            {
                return DateTime.Now.AddMonths(1);
            }
        }

        #endregion
    }
}
