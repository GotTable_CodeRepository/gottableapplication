﻿using GotTable.Dal;
using GotTable.Dal.OfferCategories;
using GotTable.DAO.OfferCategories;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.OfferCategories
{
    [Serializable]
    public sealed class OfferCategory : IOfferCategory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public async Task<OfferCategoryDAO> Get(int categoryId)
        {
            var dal = DalFactory.Create<IOfferCategoryDal>();
            var dto = await dal.Fetch(categoryId);
            return new OfferCategoryDAO()
            {
                Active = dto.Active,
                ErrorMessage = string.Empty,
                StatusId = 0,
                Id = dto.Id,
                Name = dto.Name
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryDAO"></param>
        public async Task Save(OfferCategoryDAO categoryDAO)
        {
            //if (categoryDAO.Content == null)
            //{
            //    categoryDAO.StatusId = 0;
            //    categoryDAO.ErrorMessage = "Invalid request, please check.";
            //    return;
            //}
            //if (string.IsNullOrEmpty(categoryDAO.Name))
            //{
            //    categoryDAO.StatusId = 0;
            //    categoryDAO.ErrorMessage = "Invalid request, please check.";
            //    return;
            //}
            //var baseDirectory = HttpContext.Current.Server.MapPath("~/Uploads/OfferCategories/");
            //var fileType = categoryDAO.Content.ContentType;
            //var validImageTypes = new string[] { "image/gif", "image/jpeg", "image/pjpeg", "image/png" };
            //if (!validImageTypes.Contains(fileType))
            //{
            //    categoryDAO.StatusId = 0;
            //    categoryDAO.ErrorMessage = "Invalid file type, Please check.";
            //    return;
            //}
            //var imageExtension = ImageExtension.GetExtension(categoryDAO.Content.ContentType);

            //var dto = new OfferCategoryDto()
            //{
            //    Active = true,
            //    Name = categoryDAO.Name,
            //    Extension = imageExtension
            //};

            //var dal = DalFactory.Create<IOfferCategoryDal>();
            //await dal.Insert(dto);
            //if (dto.Id > 0)
            //{
            //    categoryDAO.Content.SaveAs(baseDirectory + "/" + dto.Id.ToString() + imageExtension);
            //    categoryDAO.StatusId = 1;
            //}
            //else
            //{
            //    categoryDAO.StatusId = 0;
            //    categoryDAO.ErrorMessage = "There is some issue while working on the request.";
            //}
            throw new Exception();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerCategoryId"></param>
        /// <returns></returns>
        public async Task Update(int offerCategoryId)
        {
            if (offerCategoryId == 0) return;
            var dal = DalFactory.Create<IOfferCategoryDal>();
            var dto = await dal.Fetch(offerCategoryId);
            if (dto != null)
            {
                dto.Active = !dto.Active;
            }
            await dal.Update(dto);
        }
    }
}
