﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Users;
using GotTable.DAO.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Users
{
    [Serializable]
    public sealed class PhoneNumberList : IPhoneNumberList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<PhoneNumberDAO>> Get(decimal userId)
        {
            var phoneNumbers = new List<PhoneNumberDAO>();
            var dal = DalFactory.Create<IUserDal>();
            foreach (var item in await dal.FetchPhoneNumbers(userId))
            {
                phoneNumbers.Add(new PhoneNumberDAO()
                {
                    Id = item.Id,
                    isActive = item.IsActive,
                    isVerified = item.IsVerified,
                    Value = item.Value,
                    Type = ((Enumeration.PhoneNumberType)item.TypeId).ToString(),
                    UserId = item.UserId
                });
            }
            return phoneNumbers;
        }
    }
}
