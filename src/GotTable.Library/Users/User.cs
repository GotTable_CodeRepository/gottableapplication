﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Users;
using GotTable.DAO.Users;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.Users
{
    [Serializable]
    public sealed class User : IUser
    {

        #region Factory method
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserDetailDAO> Create(decimal userId)
        {
            await Task.FromResult(1);
            return new UserDetailDAO()
            {
                StatusId = 1,
                ErrorMessage = string.Empty,
                UserId = userId
            };
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<UserDetailDAO> Get(decimal userId)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(userId);
            return new UserDetailDAO()
            {
                StatusId = 1,
                ErrorMessage = string.Empty,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                EmailAddress = dto.EmailAddress,
                Gender = dto.GenderName,
                Prefix = dto.PrefixName,
                UserId = dto.UserId,
                Password = dto.Password ?? string.Empty,
                UserType = dto.TypeName,
                IsActive = dto.IsActive ?? false,
                IsEmailVerified = dto.IsEmailVerified ?? false,
                IsEmailOptedForCommunication = dto.IsEmailOptedForCommunication ?? false,
                RewardCount = dto.RewardCount,
                DoubleTheDealBookingId = dto.DoubleTheDealBookingId
            };
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<UserDetailDAO> Get(string emailAddress, string password)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(emailAddress, password);
            if (dto == null)
            {
                return new UserDetailDAO()
                {
                    StatusId = 0,
                    ErrorMessage = "Invalid emailAddress and password"
                };
            }
            else
            {
                return new UserDetailDAO()
                {
                    StatusId = 1,
                    ErrorMessage = string.Empty,
                    FirstName = dto.FirstName,
                    LastName = dto.LastName,
                    EmailAddress = dto.EmailAddress,
                    Gender = dto.GenderName,
                    Prefix = dto.PrefixName,
                    UserId = dto.UserId,
                    Password = dto.Password,
                    UserType = dto.TypeName,
                    IsActive = dto.IsActive ?? false,
                    IsEmailVerified = dto.IsEmailVerified ?? false,
                    IsEmailOptedForCommunication = dto.IsEmailOptedForCommunication ?? false,
                    RewardCount = dto.RewardCount,
                    DoubleTheDealBookingId = dto.DoubleTheDealBookingId
                };
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public async Task<UserDetailDAO> Get(string emailAddress)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(emailAddress);
            if (dto == null)
            {
                return new UserDetailDAO()
                {
                    UserId = 0,
                    StatusId = 0,
                    ErrorMessage = "Invalid emailAddress"
                };
            }
            else
            {
                return new UserDetailDAO()
                {
                    StatusId = 1,
                    ErrorMessage = string.Empty,
                    FirstName = dto.FirstName,
                    LastName = dto.LastName,
                    EmailAddress = dto.EmailAddress,
                    Gender = dto.GenderName,
                    Prefix = dto.PrefixName,
                    UserId = dto.UserId,
                    Password = dto.Password ?? string.Empty,
                    UserType = dto.TypeName,
                    IsActive = dto.IsActive ?? false,
                    IsEmailVerified = dto.IsEmailVerified ?? false,
                    IsEmailOptedForCommunication = dto.IsEmailOptedForCommunication ?? false,
                    RewardCount = dto.RewardCount,
                    DoubleTheDealBookingId = dto.DoubleTheDealBookingId
                };
            }
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="userDetailDAO"></param>
        /// <returns></returns>
        public async Task Save(UserDetailDAO userDetailDAO)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(userDetailDAO.UserId);
            if (dto != null)
            {
                dto.FirstName = userDetailDAO.FirstName;
                dto.LastName = userDetailDAO.LastName;
                dto.Password = userDetailDAO.Password;
                dto.EmailAddress = userDetailDAO.EmailAddress;
                dto.IsActive = userDetailDAO.IsActive;

                await dal.Update(dto);
                if (dto.UserId > 0)
                {
                    userDetailDAO.StatusId = 1;
                    userDetailDAO.ErrorMessage = string.Empty;
                }
                else
                {
                    userDetailDAO.StatusId = 0;
                    userDetailDAO.ErrorMessage = "There is some issue while processing your record. Please contact Got Table support.";
                }
            }
            else
            {
                userDetailDAO.StatusId = 0;
                userDetailDAO.ErrorMessage = "There is some issue while processing your record. Please contact Got Table support.";
            }
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="userDetailDAO"></param>
        public async Task New(UserDetailDAO userDetailDAO)
        {
            if (string.IsNullOrEmpty(userDetailDAO.FirstName))
            {
                userDetailDAO.StatusId = 0;
                userDetailDAO.ErrorMessage = "Invalid firstname, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(userDetailDAO.LastName))
            {
                userDetailDAO.StatusId = 0;
                userDetailDAO.ErrorMessage = "Invalid lastname, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(userDetailDAO.EmailAddress))
            {
                userDetailDAO.StatusId = 0;
                userDetailDAO.ErrorMessage = "Invalid emailaddress, Please check.!";
                return;
            }
            if (userDetailDAO.Password != null && userDetailDAO.Password.Length < 8)
            {
                userDetailDAO.StatusId = 0;
                userDetailDAO.ErrorMessage = "Invalid password length, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(userDetailDAO.UserType) && !Enum.IsDefined(typeof(Enumeration.UserType), userDetailDAO.UserType))
            {
                userDetailDAO.StatusId = 0;
                userDetailDAO.ErrorMessage = "Invalid user type, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(userDetailDAO.UserType) && !Enum.IsDefined(typeof(Enumeration.UserType), userDetailDAO.UserType))
            {
                userDetailDAO.StatusId = 0;
                userDetailDAO.ErrorMessage = "Invalid user type, Please check.!";
                return;
            }
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(userDetailDAO.EmailAddress);
            if (dto == null)
            {
                if (string.IsNullOrEmpty(userDetailDAO.Gender))
                {
                    userDetailDAO.Gender = Enumeration.Gender.NotDefined.ToString();
                }
                if (string.IsNullOrWhiteSpace(userDetailDAO.Prefix))
                {
                    userDetailDAO.Prefix = Enumeration.Prefix.NotDefined.ToString();
                }
                dto = new UserDto()
                {
                    FirstName = userDetailDAO.FirstName,
                    LastName = userDetailDAO.LastName,
                    EmailAddress = userDetailDAO.EmailAddress,
                    Password = userDetailDAO.Password,
                    IsActive = userDetailDAO.IsActive,
                    TypeId = (int)Enum.Parse(typeof(Enumeration.UserType), userDetailDAO.UserType),
                    GenderId = (int)Enum.Parse(typeof(Enumeration.Gender), userDetailDAO.Gender),
                    PrefixId = (int)Enum.Parse(typeof(Enumeration.Prefix), userDetailDAO.Prefix),
                    IsEmailVerified = userDetailDAO.IsEmailVerified,
                    IsEmailOptedForCommunication = userDetailDAO.IsEmailOptedForCommunication
                };
                await dal.Insert(dto);
                if (dto.UserId > 0)
                {
                    userDetailDAO.StatusId = 1;
                    userDetailDAO.UserId = dto.UserId;
                    userDetailDAO.ErrorMessage = string.Empty;

                    if (userDetailDAO.PhoneNumberList.Any())
                    {
                        await New(new PhoneNumberDAO()
                        {
                            UserId = dto.UserId,
                            isActive = true,
                            isVerified = true,
                            VerificationDate = DateTime.Now.ToString("MM-dd-yyyy"),
                            Value = userDetailDAO.PhoneNumberList.First().Value
                        });
                    }

                    //if (dto.TypeId == (int)Enumeration.UserType.EndUser || dto.TypeId == (int)Enumeration.UserType.Guest)
                    //{
                    //    Guid transactionId = Guid.Empty;
                    //    List<KeyValuePair<string, string>> emailAttributes = new List<KeyValuePair<string, string>>();
                    //    emailAttributes.Add(new KeyValuePair<string, string>("xxxUserNamexxx", dto.FirstName + " " + dto.LastName));
                    //    emailAttributes.Add(new KeyValuePair<string, string>("xxxEmailAddressxxx", dto.EmailAddress));
                    //    emailAttributes.Add(new KeyValuePair<string, string>("xxxPasswordxxx", dto.Password));
                    //    emailAttributes.Add(new KeyValuePair<string, string>("xxxURLxxx", string.Empty));
                    //    EmailTransmission.Invoke(Enumeration.EmailAction.UserRegistration, Enumeration.CredentialType.NoReply, emailAttributes, out transactionId, dto.EmailAddress);
                    //}
                }
                else
                {
                    userDetailDAO.UserId = 0;
                    userDetailDAO.StatusId = 0;
                    userDetailDAO.ErrorMessage = "There is some issue while registering the user, Please contact support team!";
                }
            }
            else
            {
                userDetailDAO.UserId = 0;
                userDetailDAO.StatusId = 0;
                userDetailDAO.ErrorMessage = "This email is already exists";
            }
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="phoneNumberDAO"></param>
        public async Task New(PhoneNumberDAO phoneNumberDAO)
        {
            if (phoneNumberDAO.UserId == 0)
            {
                phoneNumberDAO.StatusId = 0;
                phoneNumberDAO.ErrorMessage = "Invalid request, Please check.!";
                return;
            }
            if (phoneNumberDAO.Value == 0)
            {
                phoneNumberDAO.StatusId = 0;
                phoneNumberDAO.ErrorMessage = "Invalid phone number, Please check.!";
                return;
            }
            if (phoneNumberDAO.Value.ToString().Length != 10)
            {
                phoneNumberDAO.StatusId = 0;
                phoneNumberDAO.ErrorMessage = "Phone number length should be of 10 digits, Please check.!";
                return;
            }
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(phoneNumberDAO.UserId);
            if (dto == null)
            {
                phoneNumberDAO.StatusId = 0;
                phoneNumberDAO.ErrorMessage = "Invalid user, Please check.!";
                return;
            }
            else
            {
                var phoneNumberDto = new PhoneNumberDto()
                {
                    IsActive = true,
                    IsVerified = false,
                    TypeId = (int)Enumeration.PhoneNumberType.Cell,
                    UserId = phoneNumberDAO.UserId,
                    Value = phoneNumberDAO.Value,
                    VerificationDate = DateTime.Now
                };
                await dal.Insert(phoneNumberDto);
            }
        }

        #endregion
    }
}
