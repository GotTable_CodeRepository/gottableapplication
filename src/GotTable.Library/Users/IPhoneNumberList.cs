﻿using GotTable.DAO.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Users
{
    public interface IPhoneNumberList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<PhoneNumberDAO>> Get(decimal userId);
    }
}