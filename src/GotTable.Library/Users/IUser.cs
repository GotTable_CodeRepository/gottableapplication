﻿using GotTable.DAO.Users;
using System.Threading.Tasks;

namespace GotTable.Library.Users
{
    /// <summary>
    /// IUser
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<UserDetailDAO> Get(decimal userId);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<UserDetailDAO> Get(string emailAddress, string password);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        Task<UserDetailDAO> Get(string emailAddress);

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="userDetailDAO"></param>
        Task Save(UserDetailDAO userDetailDAO);

        /// <summary>
        /// New
        /// </summary>
        /// <param name="userDetailDAO"></param>
        Task New(UserDetailDAO userDetailDAO);

        /// <summary>
        /// New
        /// </summary>
        /// <param name="phoneNumberDAO"></param>
        Task New(PhoneNumberDAO phoneNumberDAO);

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<UserDetailDAO> Create(decimal userId);
    }
}
