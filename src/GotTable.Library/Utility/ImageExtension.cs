﻿using Microsoft.Win32;
using System.Web;

namespace GotTable.Library.Utility
{
    internal sealed class ImageExtension
    {
        private ImageExtension()
        {

        }

        internal static string GetExtension(string mimeType)
        {
            RegistryKey key = Registry.ClassesRoot.OpenSubKey(@"MIME\Database\Content Type\" + mimeType, false);
            return key?.GetValue("Extension", null)?.ToString() ?? string.Empty;
        }

        internal static string GetBaseUrl
        {
            get
            {
                return string.Empty; //HttpContext.Current.Request.Url.GetLeftPart(System.UriPartial.Authority);
            }
        }
    }
}
