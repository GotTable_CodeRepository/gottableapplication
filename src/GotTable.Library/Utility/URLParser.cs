﻿using System.Web;

namespace GotTable.Library.Utility
{
    public static class URLParser
    {
        public static string Encode(string urlValue)
        {
            return HttpUtility.UrlEncode(urlValue);
        }

        public static string Decode(string urlValue)
        {
            return HttpUtility.UrlEncode(urlValue);
        }
    }
}
