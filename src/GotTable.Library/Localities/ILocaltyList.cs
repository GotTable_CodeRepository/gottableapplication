﻿using GotTable.DAO.Localities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Localities
{
    //
    public interface ILocaltyList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        Task<IEnumerable<LocaltyDAO>> Get(bool? active = null);
    }
}