﻿using GotTable.Dal;
using GotTable.Dal.Localities;
using GotTable.DAO.Localities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Localities
{
    [Serializable]
    public sealed class LocaltyList : ILocaltyList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="totalCount"></param>
        /// <param name="active"></param>
        /// <returns></returns>
        public async Task<IEnumerable<LocaltyDAO>> Get(bool? active = null)
        {
            var localtyList = new List<LocaltyDAO>();
            var dal = DalFactory.Create<ILocaltyDal>();
            var dtos = await dal.FetchList(active);
            foreach(var item in dtos)
            {
                localtyList.Add(new LocaltyDAO()
                {
                    Active = item.Active,
                    Id = item.Id,
                    Name = item.Name
                });
            }
            return localtyList;
        }
    }
}
