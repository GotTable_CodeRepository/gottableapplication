﻿using GotTable.DAO.Cities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Cities
{
    /// <summary>
    /// ICityList
    /// </summary>
    public interface ICityList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        Task<List<CityInfoDAO>> Get();
    }
}
