﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantBookings;
using GotTable.Dal.RestaurantRatings;
using GotTable.DAO.RestaurantRatings;
using GotTable.Library.Users;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantRatings
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class BranchRating : IBranchRating
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        public BranchRating(IUser user)
        {
            this.user = user;
        }

        #region Factory methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ratingDAO"></param>
        public async Task New(BranchRatingDAO ratingDAO)
        {
            if (ratingDAO.BranchId == 0)
            {
                ratingDAO.StatusId = 0;
                ratingDAO.ErrorMessage = "Invalid request, Please check";
                return;
            }
            if (ratingDAO.UserId == 0)
            {
                ratingDAO.StatusId = 0;
                ratingDAO.ErrorMessage = "Invalid userId, Please check";
                return;
            }
            if (ratingDAO.Rating < 1)
            {
                ratingDAO.StatusId = 0;
                ratingDAO.ErrorMessage = "Rating can not be zero, Please check";
                return;
            }
            if (String.IsNullOrEmpty(ratingDAO.Title))
            {
                ratingDAO.StatusId = 0;
                ratingDAO.ErrorMessage = "Title can not be blank, Please check";
                return;
            }
            if (String.IsNullOrEmpty(ratingDAO.Comment))
            {
                ratingDAO.StatusId = 0;
                ratingDAO.ErrorMessage = "Comment can not be blank, Please check";
                return;
            }

            var userDetailDAO = await user.Get(ratingDAO.UserId);
            if (userDetailDAO.UserType != Enumeration.UserType.EndUser.ToString())
            {
                ratingDAO.StatusId = 0;
                ratingDAO.ErrorMessage = "Invalid request, Please check";
                return;
            }
            if (! await CheckAuthencity(userDetailDAO.UserId, ratingDAO.BranchId))
            {
                ratingDAO.StatusId = 0;
                ratingDAO.ErrorMessage = "You can post review for this restaurant only after one successfull Booking/Order.";
                return;
            }
            var dto = new RatingDto()
            {
                BranchId = ratingDAO.BranchId,
                Comment = ratingDAO.Comment,
                CreatedDate = ratingDAO.CreationDate,
                Id = ratingDAO.Id,
                IsActive = ratingDAO.IsActive,
                Rating = ratingDAO.Rating,
                UserId = ratingDAO.UserId,
                Title = ratingDAO.Title,
                FoodRating = ratingDAO.FoodRating,
                AmbienceRating = ratingDAO.AmbienceRating,
                EmailAddress = ratingDAO.EmailAddress,
                MusicRating = ratingDAO.MusicRating,
                ServiceRating = ratingDAO.ServiceRating,
                PriceRating = ratingDAO.PriceRating
            };
            var dal = DalFactory.Create<IRatingDal>();
            await dal.Insert(dto);
            if (dto.Id > 0)
            {
                ratingDAO.Id = dto.Id;
                ratingDAO.StatusId = 1;
                ratingDAO.ErrorMessage = string.Empty;
            }
            else
            {
                ratingDAO.StatusId = 0;
                ratingDAO.ErrorMessage = "There is some issue while processing the request, Please try after some time.";
            }
            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<AvgRatingDAO> GetAvgRating(decimal restaurantId)
        {
            var dal = DalFactory.Create<IRatingDal>();
            var dto = await dal.FetchAvgRating(restaurantId);
            return new AvgRatingDAO()
            {
                AmbienceRating = dto.AmbienceRating,
                FoodRating = dto.FoodRating,
                MusicRating = dto.MusicRating,
                PriceRating = dto.PriceRating,
                Rating = dto.Rating,
                ServiceRating = dto.ServiceRating
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="restuarantId"></param>
        /// <returns></returns>
        public async Task<bool> CheckAuthencity(decimal userId, decimal restuarantId)
        {
            bool retStatus = false;
            var dal = DalFactory.Create<IBookingListDal>();
            var dtos = await dal.FetchUserBookingList(userId, restuarantId);
            if  (dtos.Any())
            {
                retStatus = true;
            }
            return retStatus;
        }

        #endregion
    }
}
