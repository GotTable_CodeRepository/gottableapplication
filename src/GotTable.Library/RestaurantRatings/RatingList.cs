﻿using GotTable.Dal;
using GotTable.Dal.RestaurantRatings;
using GotTable.DAO.RestaurantRatings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantRatings
{
    [Serializable]
    public sealed class RatingList : IRatingList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BranchRatingDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var ratingList = new List<BranchRatingDAO>();
            var dal = DalFactory.Create<IRatingDal>();
            foreach (var item in await dal.FetchList(restaurantId, currentPage, pageSize))
            {
                ratingList.Add(new BranchRatingDAO()
                {
                    BranchId = item.BranchId,
                    Comment = item.Comment,
                    CreationDate = item.CreatedDate,
                    Id = item.Id,
                    IsActive = item.IsActive,
                    Rating = item.Rating,
                    Title = item.Title,
                    UserId = item.UserId,
                    EmailAddress = item.EmailAddress,
                    UserName = item.UserName,
                    AmbienceRating = item.AmbienceRating,
                    FoodRating = item.FoodRating,
                    MusicRating = item.MusicRating,
                    PriceRating = item.PriceRating,
                    ServiceRating = item.ServiceRating
                });
            }
            return ratingList;
        }
    }
}
