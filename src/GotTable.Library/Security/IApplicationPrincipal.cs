﻿using System.Security.Claims;
using System.Threading.Tasks;

namespace GotTable.Library.Security
{
    public interface IApplicationPrincipal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<ClaimsPrincipal> Get(string userName, string password);
    }
}