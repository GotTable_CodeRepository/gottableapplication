﻿using GotTable.Dal;
using GotTable.Dal.Restaurants;
using GotTable.Dal.Security;
using Microsoft.AspNetCore.Authentication.Cookies;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ClaimType = GotTable.Common.Enumerations.Enumeration.ClaimType;

namespace GotTable.Library.Security
{
    [Serializable]
    public class ApplicationPrincipal : IApplicationPrincipal
    {

        #region Factory methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<ClaimsPrincipal> Get(string userName, string password)
        {
            var dal = DalFactory.Create<IApplicationIdentityDal>();
            var applicationIdentityDto = await dal.Get(userName, password);
            if (applicationIdentityDto != null)
            {
                var identity = new ClaimsIdentity(new[]
                {
                   new Claim(ClaimType.Name.ToString(), applicationIdentityDto.FirstName + " " + applicationIdentityDto.LastName),
                   new Claim(ClaimType.Role.ToString(), applicationIdentityDto.AdminType.ToString()),
                   new Claim(ClaimType.Email.ToString(), applicationIdentityDto.EmailAddress),
                   new Claim(ClaimType.UserId.ToString(), applicationIdentityDto.Id.ToString()),
                   new Claim(ClaimType.EnableRestaurantLogin.ToString(), applicationIdentityDto.EnableRestaurantLogin.ToString())
            }, CookieAuthenticationDefaults.AuthenticationScheme);

                if (applicationIdentityDto.RestaurantId > 0)
                {
                    identity.AddClaim(new Claim(ClaimType.RestaurantId.ToString(), applicationIdentityDto.RestaurantId.ToString()));
                }
                if (!string.IsNullOrEmpty(applicationIdentityDto.RestaurantName))
                {
                    identity.AddClaim(new Claim(ClaimType.RestaurantName.ToString(), applicationIdentityDto.RestaurantName));
                }
                if (!string.IsNullOrEmpty(applicationIdentityDto.RestaurantAddress))
                {
                    identity.AddClaim(new Claim(ClaimType.RestaurantAddress.ToString(), applicationIdentityDto.RestaurantAddress));
                }
                identity.AddClaim(new Claim(ClaimType.DineInActive.ToString(), applicationIdentityDto.DineInActive.ToString()));
                identity.AddClaim(new Claim(ClaimType.DeliveryActive.ToString(), applicationIdentityDto.DeliveryActive.ToString()));
                identity.AddClaim(new Claim(ClaimType.TakeawayActive.ToString(), applicationIdentityDto.TakeawayActive.ToString()));
                var principal = new ClaimsPrincipal(identity);
                return principal;
            }
            return null;
        }

        #endregion
    }
}
