﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Versions;
using GotTable.DAO.Versions;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Versions
{
    [Serializable]
    public sealed class Version : IVersion
    {
        #region Factory method

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        public async Task<VersionDAO> Create()
        {
            await Task.FromResult(1);
            var dao = new VersionDAO()
            {
                StatusId = 1,
                ErrorMessage = string.Empty,
                SyncName = Guid.NewGuid(),
                SyncPassword = Guid.NewGuid()
            };
            return dao;
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="versionDAO"></param>
        /// <returns></returns>
        public async Task New(VersionDAO versionDAO)
        {
            var dal = DalFactory.Create<IVersionDal>();
            var dto = await dal.Fetch(versionDAO.Number, versionDAO.TypeId);
            if (dto == null)
            {
                if (String.IsNullOrEmpty(versionDAO.Number))
                {
                    versionDAO.StatusId = 0;
                    versionDAO.ErrorMessage = "Invalid version, Please check.!";
                    return;
                }
                if (versionDAO.TypeId == 0)
                {
                    versionDAO.StatusId = 0;
                    versionDAO.ErrorMessage = "Invalid type, Please check.!";
                    return;
                }
                if (String.IsNullOrEmpty(versionDAO.Message))
                {
                    versionDAO.StatusId = 0;
                    versionDAO.ErrorMessage = "Please insert the message details.!";
                    return;
                }
                dto = new VersionDto()
                {
                    Active = versionDAO.Active,
                    Message = versionDAO.Message,
                    Number = versionDAO.Number,
                    CreatedDate = DateTime.Now,
                    TypeId = versionDAO.TypeId,
                    SyncName = versionDAO.SyncName,
                    SyncPassword = versionDAO.SyncPassword
                };
                await dal.Insert(dto);
            }
            else
            {
                versionDAO.StatusId = 0;
                versionDAO.ErrorMessage = "";
            }
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="versionInfo"></param>
        /// <returns></returns>
        public async Task Save(VersionDAO versionInfo)
        {
            var dal = DalFactory.Create<IVersionDal>();
            var dto = await dal.Fetch(versionInfo.VersionId);
            if (dto != null)
            {
                if (String.IsNullOrEmpty(versionInfo.Number))
                {
                    versionInfo.StatusId = 0;
                    versionInfo.ErrorMessage = "Invalid version, Please check.!";
                    return;
                }
                if (versionInfo.TypeId == 0)
                {
                    versionInfo.StatusId = 0;
                    versionInfo.ErrorMessage = "Invalid type, Please check.!";
                    return;
                }
                if (String.IsNullOrEmpty(versionInfo.Message))
                {
                    versionInfo.StatusId = 0;
                    versionInfo.ErrorMessage = "Please insert the message details.!";
                    return;
                }

                dto.Number = versionInfo.Number;
                dto.Message = versionInfo.Message;
                dto.TypeId = versionInfo.TypeId;
                dto.Active = versionInfo.Active;
                dto.SyncName = versionInfo.SyncName;
                dto.SyncPassword = versionInfo.SyncPassword;

                await dal.Update(dto);
                if (dto.VersionId > 0)
                {
                    versionInfo.StatusId = 1;
                    versionInfo.ErrorMessage = string.Empty;
                }
                else
                {
                    versionInfo.StatusId = 0;
                    versionInfo.ErrorMessage = "There is some issue while processing your record. Please contact Got Table support.";
                }
            }
            else
            {
                versionInfo.StatusId = 0;
                versionInfo.ErrorMessage = "There is some issue while processing your record. Please contact Got Table support.";
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="versionId"></param>
        /// <returns></returns>
        public async Task<VersionDAO> Get(int versionId)
        {
            var dal = DalFactory.Create<IVersionDal>();
            var dto = await dal.Fetch(versionId);
            return new VersionDAO()
            {
                Active = dto.Active,
                Message = dto.Message,
                CreatedDate = dto.CreatedDate,
                TypeId = dto.TypeId,
                Number = dto.Number,
                VersionId = dto.VersionId,
                SyncName = dto.SyncName,
                SyncPassword = dto.SyncPassword
            };
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="version"></param>
        /// <param name="typeId"></param>
        /// <returns></returns>
        public async Task<VersionDAO> Get(string version, int typeId)
        {
            var dal = DalFactory.Create<IVersionDal>();
            var dto = await dal.Fetch(version, typeId);
            if (dto == null)
            {
                return new VersionDAO()
                {
                    StatusId = 0,
                    ErrorMessage = "Invalid version number."
                };
            }
            else
            {
                return new VersionDAO()
                {
                    StatusId = 1,
                    ErrorMessage = string.Empty,
                    Active = dto.Active,
                    Message = dto.Message,
                    CreatedDate = dto.CreatedDate,
                    TypeId = dto.TypeId,
                    Number = dto.Number,
                    TypeName = ((Enumeration.Device)dto.TypeId).ToString(),
                    VersionId = dto.VersionId,
                    SyncName = dto.SyncName,
                    SyncPassword = dto.SyncPassword
                };
            }
        }

        #endregion
    }
}
