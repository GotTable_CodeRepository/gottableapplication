﻿using GotTable.DAO.Versions;
using System.Threading.Tasks;

namespace GotTable.Library.Versions
{
    /// <summary>
    /// IVersion
    /// </summary>
    public interface IVersion
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="versionId"></param>
        /// <returns></returns>
        Task<VersionDAO> Get(int versionId);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="version"></param>
        /// <param name="typeId"></param>
        /// <returns></returns>
        Task<VersionDAO> Get(string version, int typeId);

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="versionInfo"></param>
        Task Save(VersionDAO versionInfo);

        /// <summary>
        /// New
        /// </summary>
        /// <param name="versionDAO"></param>
        /// <returns></returns>
        Task New(VersionDAO versionDAO);

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        Task<VersionDAO> Create();
    }
}