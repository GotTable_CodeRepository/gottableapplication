﻿using GotTable.Dal;
using GotTable.Dal.Versions;
using GotTable.DAO.Versions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Versions
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class VersionList : IVersionList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<VersionDAO>> Get(int currentPage = 0, int pageSize = 10)
        {
            var versionList = new List<VersionDAO>();
            var dal = DalFactory.Create<IVersionDal>();
            var dtos = await dal.FetchList(currentPage, pageSize);

            foreach (var item in dtos)
            {
                versionList.Add(new VersionDAO()
                {
                    Active = item.Active,
                    Message = item.Message,
                    CreatedDate = item.CreatedDate,
                    ErrorMessage = string.Empty,
                    StatusId = 0,
                    TypeId = item.TypeId,
                    TypeName = ((Common.Enumerations.Enumeration.Device)item.TypeId).ToString(),
                    Number = item.Number,
                    VersionId = item.VersionId,
                    AttachedDeviceCount = item.AttachedDeviceCount,
                    SyncName = item.SyncName,
                    SyncPassword = item.SyncPassword
                });
            }

            return versionList;
        }
    }
}
