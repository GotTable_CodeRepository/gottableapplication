﻿using GotTable.DAO.Versions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Versions
{
    /// <summary>
    /// IVersionList
    /// </summary>
    public interface IVersionList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<VersionDAO>> Get(int currentPage = 0, int pageSize = 10);
    }
}