﻿using GotTable.Dal;
using GotTable.Dal.Cuisines;
using GotTable.DAO.Cuisines;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Cuisines
{
    [Serializable]
    public sealed class CuisineList : ICuisineList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="cusineStatus"></param>
        /// <returns></returns>
        public async Task<List<CuisineDAO>> Get(int currentPage = 1, int pageSize = 10, bool cusineStatus = true)
        {
            var cuisineList = new List<CuisineDAO>();
            var dal = DalFactory.Create<ICuisineDal>();
            var dtos = await dal.FetchList(currentPage, pageSize, cusineStatus);
            foreach (var item in dtos)
            {
                cuisineList.Add(new CuisineDAO()
                {
                    CuisineName = item.Name,
                    Id = item.Id,
                    CategoryId = item.CategoryId,
                    CategoryName = item.CategoryName,
                    DisplayLevel = item.DisplayLevel,
                    EngagedRestaurant = item.EngagedRestaurant
                });
            }
            return cuisineList;
        }
    }
}
