﻿using GotTable.DAO.Cuisines;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Cuisines
{
    public interface ICuisineList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="cusineStatus"></param>
        /// <returns></returns>
        Task<List<CuisineDAO>> Get(int currentPage = 1, int pageSize = 10, bool cusineStatus = true);
    }
}
