﻿using GotTable.Common;
using GotTable.Dal;
using GotTable.Dal.RestaurantTables;
using GotTable.DAO.RestaurantTables;
using GotTable.Library.RestaurantBookings;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantTables
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class BranchTable : IBranchTable
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDineInList dineInList;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineInList"></param>
        public BranchTable(IDineInList dineInList)
        {
            this.dineInList = dineInList;
        }

        #region Factory methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task Default(decimal restaurantId)
        {
            var dal = DalFactory.Create<ITableDal>();
            foreach (var item in Common.List.TableList.Get())
            {
                var dto = new TableDto()
                {
                    IsActive = true,
                    RestaurantId = restaurantId,
                    TableCount = 0,
                    TableId = item.Id
                };
                await dal.Insert(dto);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTableDAO"></param>
        /// <returns></returns>
        public async Task Save(BranchTableDAO branchTableDAO)
        {
            if (branchTableDAO.Value < 0)
            {
                branchTableDAO.StatusId = 0;
                branchTableDAO.ErrorMessage = "Table value can not be blank, Please check.!";
                return;
            }

            var dto = new TableDto()
            {
                Id = branchTableDAO.Id,
                TableCount = branchTableDAO.Value
                
            };

            var dal = DalFactory.Create<ITableDal>();
            await dal.Update(dto);
            if (dto.Id > 0)
            {
                branchTableDAO.StatusId = 1;
                branchTableDAO.ErrorMessage = string.Empty;
            }
            else
            {
                branchTableDAO.StatusId = 0;
                branchTableDAO.ErrorMessage = "There is some issue while processing your request, Please try after some time.!";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableId"></param>
        /// <returns></returns>
        public async Task<BranchTableDAO> Get(decimal tableId)
        {
            var dal = DalFactory.Create<ITableDal>();
            var dto = await dal.Fetch(tableId);
            return new BranchTableDAO()
            {
                BranchId = dto.RestaurantId,
                Id = dto.Id,
                SelectedTable = dto.TableName,
                Value = dto.TableCount,
                Active = dto.IsActive
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchTableDAO"></param>
        /// <param name="bookingDate"></param>
        /// <param name="bookingTime"></param>
        /// <returns></returns>
        public async Task<bool> CheckAvailablity(BranchTableDAO branchTableDAO, DateTime bookingDate, string bookingTime)
        {
            int totalCount = 0;
            int bookingTimeInt16 = int.Parse(bookingTime.ToString().Replace(":", ""));
            if(!branchTableDAO.Active || branchTableDAO.Value == 0)
            {
                return false;
            }
            if (bookingTimeInt16 > 0)
            {
                var bookingDateTime = DateTime.ParseExact(bookingDate.ToString(GotTableCommanConstant.BookingDateFormat) + " " + bookingTime, GotTableCommanConstant.BookingDateTimeFormat, null).ToDateTimeInteger();
                var dineinBookingList = await dineInList.Get(branchTableDAO.Id, bookingDate);
                if (totalCount > 0)
                {
                    int bookingCount = dineinBookingList.Where(x => x.CurrentStatusId == (int)Common.Enumerations.Enumeration.DineInStatusType.Confirm 
                    && x.BookingDateTime != null
                    && ((x.BookingTime.ToInteger() > bookingTime.ToInteger() && x.LockInDateTime < bookingDateTime) 
                       || (x.BookingTime.ToInteger() < bookingTime.ToInteger() && x.LockOutDateTime > bookingDateTime)
                       || (x.BookingTime == bookingTime))).Count();
                    return branchTableDAO.Value > bookingCount;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        #endregion
    }
}
