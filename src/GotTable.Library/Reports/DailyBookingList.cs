﻿using GotTable.Dal;
using GotTable.Dal.Reports.Bookings;
using GotTable.DAO.Reports;
using GotTable.Library.Reports.Criterias;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Reports
{
    [Serializable]
    public sealed class DailyBookingList : IReportList<DailyBookingDAO, DailyBookingListCriteria>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<List<DailyBookingDAO>> Get(DailyBookingListCriteria criteria)
        {
            var dailyBookings = new List<DailyBookingDAO>();
            var dal = DalFactory.Create<IBookingDal>();
            foreach (var booking in await dal.FetchList(criteria.BookingDate, criteria.CurrentPage, criteria.PageSize))
            {
                dailyBookings.Add(new DailyBookingDAO()
                {
                    // Booking detail
                    BookingType = booking.BookingType,
                    PhoneNumber = booking.PhoneNumber,
                    BookingDate = booking.BookingDate.ToString("MM-dd-yyyy"),
                    BookingTime = booking.BookingTime,

                    // Branch detail
                    BranchAddress = booking.BranchAddress,
                    BranchCity = booking.BranchCity,
                    BranchId = booking.BranchId,
                    BranchName = booking.BranchName,
                    BranchState = booking.BranchState,
                    BranchZipCode = booking.BranchZipCode,
                    UserName = booking.UserName,
                    CreatedDate = booking.CreatedDate
                });
            }
            return dailyBookings;
        }
    }
}
