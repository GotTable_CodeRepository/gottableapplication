﻿using GotTable.Dal;
using GotTable.Dal.Reports.Bookings;
using GotTable.DAO.Reports;
using GotTable.Library.Reports.Criterias;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Reports
{
    [Serializable]
    public sealed class CustomBookingList : IReportList<CustomBookingDAO, CustomBookingListCriteria>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<List<CustomBookingDAO>> Get(CustomBookingListCriteria criteria)
        {
            var dal = DalFactory.Create<IBookingDal>();
            var bookings = new List<CustomBookingDAO>();
            foreach (var booking in await dal.FetchList(criteria.BookingStartDate, criteria.BookingEndDate, criteria.RestaurantName, criteria.RestaurantCategory, criteria.CurrentPage, criteria.PageSize))
            {
                bookings.Add(new CustomBookingDAO()
                {
                    // Booking detail
                    BookingType = booking.BookingType,
                    PhoneNumber = booking.PhoneNumber,
                    BookingDate = booking.BookingDate.ToString("MM-dd-yyyy"),
                    BookingTime = booking.BookingTime,

                    // Branch detail
                    BranchAddress = booking.BranchAddress,
                    BranchCity = booking.BranchCity,
                    BranchId = booking.BranchId,
                    BranchName = booking.BranchName,
                    BranchState = booking.BranchState,
                    BranchZipCode = booking.BranchZipCode,
                    UserName = booking.UserName,
                    CreatedDate = booking.CreatedDate
                });
            }
            return bookings;
        }
    }
}
