﻿using GotTable.Dal;
using GotTable.Dal.Restaurants.DeletedRestaurants;
using GotTable.DAO.Reports;
using GotTable.Library.Reports.Criterias;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Reports
{
    [Serializable]
    public sealed class DeletedRestaurantList : IReportList<RestaurantDAO, DeletedRestaurantListCriteria>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<List<RestaurantDAO>> Get(DeletedRestaurantListCriteria criteria)
        {
            var deletedRestaurantList = new List<RestaurantDAO>();
            var dal = DalFactory.Create<IRestaurantListDal>();
            foreach (var restaurant in await dal.FetchList(criteria.CurrentPage, criteria.PageSize))
            {
                deletedRestaurantList.Add(new RestaurantDAO()
                {
                    Address = restaurant.Address,
                    Name = restaurant.Name,
                    BranchId = restaurant.BranchId
                });
            }
            return deletedRestaurantList;
        }
    }
}
