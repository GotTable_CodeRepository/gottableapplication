﻿using GotTable.Dal;
using GotTable.Dal.ApplicationLogs;
using GotTable.DAO.ApplicationLogs;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.ApplicationLogs
{
    /// <summary>
    /// ApplicationLog
    /// </summary>
    [Serializable]
    public sealed class ApplicationLog : IApplicationLog
    {
        /// <summary>
        /// 
        /// </summary>
        public ApplicationLog()
        {

        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="applicationLogDAO"></param>
        public async Task New(ApplicationLogDAO applicationLogDAO)
        {
            var dto = new ApplicationLogDto()
            {
                DeviceId = applicationLogDAO.DeviceId,
                DeviceTypeId = applicationLogDAO.DeviceTypeId,
                Error = applicationLogDAO.Error,
                Params = applicationLogDAO.Params,
                StatusCode = applicationLogDAO.StatusCode,
                TypeId = applicationLogDAO.TypeId,
                Url = applicationLogDAO.Url,
                UserId = applicationLogDAO.UserId
            };

            var dal = DalFactory.Create<IApplicationLogDal>();
            await dal.Insert(dto);
        }
    }
}
