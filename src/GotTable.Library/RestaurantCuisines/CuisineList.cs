﻿using GotTable.Dal;
using GotTable.Dal.RestaurantCuisines;
using GotTable.DAO.RestaurantCuisines;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantCuisines
{
    [Serializable]
    public sealed class CuisineList : ICuisineList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BranchCuisineDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var branchCuisines = new List<BranchCuisineDAO>();
            var dal = DalFactory.Create<ICuisineDal>();
            foreach (var dto in await dal.FetchList(restaurantId, currentPage, pageSize))
            {
                branchCuisines.Add(new BranchCuisineDAO()
                {
                    BranchId = dto.BranchId,
                    Id = dto.Id,
                    IsActive = dto.IsActive,
                    SelectedCuisine = dto.CuisineName
                });
            }
            return branchCuisines;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="activeCuisines"></param>
        /// <returns></returns>
        public async Task<List<BranchCuisineDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10, bool activeCuisines = true)
        {
            var branchCuisines = new List<BranchCuisineDAO>();
            var dal = DalFactory.Create<ICuisineDal>();
            foreach (var dto in await dal.FetchList(restaurantId, currentPage, pageSize))
            {
                if (dto.IsActive)
                {
                    branchCuisines.Add(new BranchCuisineDAO()
                    {
                        BranchId = dto.BranchId,
                        Id = dto.Id,
                        IsActive = dto.IsActive,
                        SelectedCuisine = dto.CuisineName
                    });
                }
            }
            return branchCuisines;
        }
    }
}
