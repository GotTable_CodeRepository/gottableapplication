﻿using GotTable.Dal;
using GotTable.Dal.RestaurantAmenities;
using GotTable.DAO.RestaurantAmenities;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantAmenities
{
    [Serializable]
    public sealed class Amenity : IAmenity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="amenityDAO"></param>
        /// <returns></returns>
        public async Task New(AmenityDAO amenityDAO)
        {
            if(amenityDAO.BranchId == 0)
            {
                amenityDAO.StatusId = 0;
                amenityDAO.ErrorMessage = "Invalid request, please check.!";
            }
            if (amenityDAO.AmenityId == 0)
            {
                amenityDAO.StatusId = 0;
                amenityDAO.ErrorMessage = "Invalid request, please check.!";
            }

            var dto = new AmenityDto()
            {
                AmenityId = amenityDAO.AmenityId,
                BranchId = amenityDAO.BranchId
            };

            var dal = DalFactory.Create<IAmenityDal>();
            await dal.Insert(dto);

            amenityDAO.Id = dto.Id;
            amenityDAO.StatusId = 1;
            amenityDAO.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amenityId"></param>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task Delete(decimal amenityId, decimal restaurantId)
        {
            var dal = DalFactory.Create<IAmenityDal>();
            await dal.Delete(amenityId, restaurantId);
        }
    }
}
