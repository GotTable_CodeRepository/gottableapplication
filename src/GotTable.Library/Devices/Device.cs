﻿using GotTable.Dal;
using GotTable.Dal.Devices;
using GotTable.DAO.Devices;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Devices
{
    [Serializable]
    public sealed class Device : IDevice
    {
        /// <summary>
        /// 
        /// </summary>
        public Device()
        {

        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="deviceDAO"></param>
        /// <returns></returns>
        public async Task Save(DeviceDAO deviceDAO)
        {
            //if(string.IsNullOrEmpty(deviceDAO.DeviceId))
            //{
            //    deviceDAO.StatusId = 1;
            //    deviceDAO.ErrorMessage = "Invalid deviceId";
            //    return;
            //}
            //var dal = DalFactory.Create<IDeviceDal>();
            //var dto = await dal.Fetch(deviceDAO.DeviceId);
            //if (dto == null)
            //{
            //    dto = new DeviceDto()
            //    {
            //        Id = deviceDAO.DeviceId,
            //        TypeId = (int)deviceDAO.SelectedDevice,
            //        IsActive = deviceDAO.IsActive,
            //        UserId = deviceDAO.UserId,
            //        City = deviceDAO.City,
            //        Latitude = deviceDAO.Latitude,
            //        Longitude = deviceDAO.Longitude,
            //    };
            //    await dal.Insert(dto);
            //}
            //else
            //{
            //    dto = new DeviceDto()
            //    {
            //        Id = deviceDAO.DeviceId,
            //        TypeId = (int)deviceDAO.SelectedDevice,
            //        IsActive = deviceDAO.IsActive,
            //        UserId = deviceDAO.UserId,
            //        City = deviceDAO.City,
            //        Latitude = deviceDAO.Latitude,
            //        Longitude = deviceDAO.Longitude,
            //    };
            //    await dal.Update(dto);
            //}
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public async Task<DeviceDAO> Get(string deviceId)
        {
            var dal = DalFactory.Create<IDeviceDal>();
            var dto = await dal.Fetch(deviceId);
            return new DeviceDAO()
            {
                City = dto.City,
                DeviceId = dto.Id,
                IsActive = dto.IsActive,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                SelectedDevice = (Common.Enumerations.Enumeration.Device)dto.TypeId,
                UserId = dto.UserId
            };
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="deviceId"></param>
        public async Task Delete(string deviceId)
        {
            var dal = DalFactory.Create<IDeviceDal>();
            await dal.Delete(deviceId);
        }
    }
}
