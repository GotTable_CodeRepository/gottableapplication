﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Devices;
using GotTable.DAO.Devices;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Devices
{
    [Serializable]
    public sealed class TokenList : ITokenList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        public async Task<List<DeviceDAO>> Get(Enumeration.Device deviceType)
        {
            var dal = DalFactory.Create<IDeviceDal>();
            var dtos = await dal.FetchList(deviceType);
            List<DeviceDAO> deviceList = new List<DeviceDAO>();
            foreach (var item in dtos)
            {
                deviceList.Add(new DeviceDAO()
                {
                    City = item.City,
                    DeviceId = item.Id,
                    IsActive = item.IsActive,
                    Latitude = item.Latitude,
                    Longitude = item.Longitude,
                    SelectedDevice = (Enumeration.Device)item.TypeId,
                    UserId = item.UserId
                });
            }
            return deviceList;
        }
    }
}
