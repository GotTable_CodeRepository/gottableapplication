﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Devices;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Devices
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITokenList
    {
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        Task<List<DeviceDAO>> Get(Enumeration.Device deviceType);
    }
}
