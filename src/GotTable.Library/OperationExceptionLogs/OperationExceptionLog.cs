﻿using GotTable.Dal;
using GotTable.Dal.OperationExceptionLogs;
using GotTable.DAO.OperationExceptions;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.OperationExceptionLogs
{
    [Serializable]
    public sealed class OperationExceptionLog : IOperationExceptionLog
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionLog"></param>
        /// <returns></returns>
        public async Task<Guid> New(Exception exceptionLog)
        {
            var innerException = exceptionLog.InnerException != null ? exceptionLog.InnerException.ToString() : "";
            var message = exceptionLog.Message != null ? exceptionLog.Message.ToString() : "";
            var stackTrace = exceptionLog.StackTrace != null ? exceptionLog.StackTrace.ToString() : "";
            var actionName = string.Empty;
            var controllerName = string.Empty;
            var dto = new OperationExceptionLogDto()
            {
                ControllerName = controllerName.Replace("'", "''"),
                Message = message.Replace("'", "''"),
                StackTrace = stackTrace.Replace("'", "''"),
                ActionName = actionName.Replace("'", "''"),
                InnerException = innerException.Replace("'", "''")
            };
            var dal = DalFactory.Create<IOperationExceptionLogDal>();
            await dal.Insert(dto);
            return dto.ExceptionLogId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionLogId"></param>
        /// <returns></returns>
        public async Task<OperationExceptionLogDAO> Get(Guid exceptionLogId)
        {
            var dal = DalFactory.Create<IOperationExceptionLogDal>();
            var dto = await dal.Fetch(exceptionLogId);
            return new OperationExceptionLogDAO()
            {
                ActionName = dto.ActionName,
                ControllerName = dto.ControllerName,
                CreatedDate = dto.DateTime,
                ExceptionId = dto.ExceptionLogId,
                InnerException = dto.InnerException,
                Message = dto.Message,
                StackTrace = dto.StackTrace
            };
        }
    }
}
