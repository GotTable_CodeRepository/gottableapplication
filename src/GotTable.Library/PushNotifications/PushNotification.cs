﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.PushNotifications;
using GotTable.DAO.PushNotifications;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Communications.Jobs.Types;
using GotTable.Library.Communications.Jobs;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.PushNotifications
{
    [Serializable]
    public sealed class PushNotification : IPushNotification
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IJobFactory jobFactory;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJob<AndroidPushNotificationJob, PushNotificationDAO> androidNotificationJob;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJob<IOSPushNotificationJob, PushNotificationDAO> iOSNotificationJob;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobFactory"></param>
        /// <param name="androidNotificationJob"></param>
        /// <param name="iOSNotificationJob"></param>
        public PushNotification(IJobFactory jobFactory, IJob<AndroidPushNotificationJob, PushNotificationDAO> androidNotificationJob, IJob<IOSPushNotificationJob, PushNotificationDAO> iOSNotificationJob)
        {
            this.jobFactory = jobFactory;
            this.androidNotificationJob = androidNotificationJob;
            this.iOSNotificationJob = iOSNotificationJob;
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<PushNotificationDAO> Create(decimal userId)
        {
            await Task.FromResult(1);
            return new PushNotificationDAO()
            {
                Id = 0,
                UserId = userId,
                StatusId = 1,
                ErrorMessage = string.Empty,
                Active = true,
                ScheduleTime = DateTime.Now.ToString("MM-dd-yyyy hh:mm tt"),
                CreatedDatetime = DateTime.Now,
                CurrentStatusId = (int)Enumeration.NotificationStatusType.Queued
            };
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="pushNotificationDAO"></param>
        /// <returns></returns>
        public async Task New(PushNotificationDAO pushNotificationDAO)
        {
            //if (String.IsNullOrEmpty(pushNotificationDAO.Message))
            //{
            //    pushNotificationDAO.StatusId = 0;
            //    pushNotificationDAO.ErrorMessage = "Invalid message, Please check.!";
            //    return;
            //}
            //if (String.IsNullOrEmpty(pushNotificationDAO.Title))
            //{
            //    pushNotificationDAO.StatusId = 0;
            //    pushNotificationDAO.ErrorMessage = "Invalid title, Please check.!";
            //    return;
            //}
            //if (pushNotificationDAO.CityId == 0)
            //{
            //    pushNotificationDAO.StatusId = 0;
            //    pushNotificationDAO.ErrorMessage = "Invalid city, Please check.!";
            //    return;
            //}
            //if (pushNotificationDAO.NotificationTypeId == 0)
            //{
            //    pushNotificationDAO.StatusId = 0;
            //    pushNotificationDAO.ErrorMessage = "Invalid notification type, Please check.!";
            //    return;
            //}
            //if (string.IsNullOrEmpty(pushNotificationDAO.ScheduleTime) && Convert.ToDateTime(pushNotificationDAO.ScheduleTime) == DateTime.MinValue)
            //{
            //    pushNotificationDAO.StatusId = 0;
            //    pushNotificationDAO.ErrorMessage = "Invalid schedule date time, Please check.!";
            //    return;
            //}
            //if (pushNotificationDAO.OfferTypeId == 0)
            //{
            //    pushNotificationDAO.StatusId = 0;
            //    pushNotificationDAO.ErrorMessage = "Invalid offer type, Please check.!";
            //    return;
            //}
            //if (pushNotificationDAO.CategoryTypeId == 0)
            //{
            //    pushNotificationDAO.StatusId = 0;
            //    pushNotificationDAO.ErrorMessage = "Invalid category type, Please check.!";
            //    return;
            //}
            //if (pushNotificationDAO.Content != null)
            //{
            //    var fileType = pushNotificationDAO.Content.ContentType;
            //    pushNotificationDAO.Extension = ImageExtension.GetExtension(pushNotificationDAO.Content.ContentType);
            //    var validImageTypes = new string[] { "image/gif", "image/jpeg", "image/pjpeg", "image/png" };
            //    if (!validImageTypes.Contains(fileType))
            //    {
            //        pushNotificationDAO.StatusId = 0;
            //        pushNotificationDAO.ErrorMessage = "Invalid file type, Please check.";
            //        return;
            //    }
            //}

            //if (pushNotificationDAO.NotificationTypeId == (int)Enumeration.NotificationType.Android)
            //{

            //}

            //var dto = new PushNotificationDto()
            //{
            //    Message = pushNotificationDAO.Message,
            //    Title = pushNotificationDAO.Title,
            //    Active = pushNotificationDAO.Active,
            //    CityId = pushNotificationDAO.CityId,
            //    Completed = false,
            //    CreatedDatetime = pushNotificationDAO.CreatedDatetime,
            //    NotificationTypeId = pushNotificationDAO.NotificationTypeId,
            //    ScheduleTime = DateTime.ParseExact(pushNotificationDAO.ScheduleTime, "MM-dd-yyyy hh:mm tt", null),
            //    UserId = pushNotificationDAO.UserId,
            //    CategoryTypeId = pushNotificationDAO.CategoryTypeId,
            //    OfferTypeId = pushNotificationDAO.OfferTypeId,
            //    Extension = pushNotificationDAO.Extension,
            //    StatusId = (int)pushNotificationDAO.StatusId
            //};

            //var dal = DalFactory.Create<IPushNotificationDal>();
            //await dal.Insert(dto);

            //if (dto.NotificationId > 0)
            //{
            //    pushNotificationDAO.Id = dto.NotificationId;
            //    if (pushNotificationDAO.Content != null)
            //    {
            //        var baseDirectory = HttpContext.Current.Server.MapPath("~/" + AppSettingKeys.NotificationDirectory);
            //        pushNotificationDAO.Content.SaveAs(baseDirectory + "/" + dto.NotificationId.ToString() + pushNotificationDAO.Extension);
            //    }

            //    if (AppSettingKeys.HangfireFeatureEnable)
            //    {
            //        if (pushNotificationDAO.NotificationTypeId == (int)Enumeration.NotificationType.Android)
            //        {
            //            jobFactory.CreateJob(androidNotificationJob, pushNotificationDAO, dto.ScheduleTime);
            //        }
            //        else if (pushNotificationDAO.NotificationTypeId == (int)Enumeration.NotificationType.IOS)
            //        {
            //            jobFactory.CreateJob(iOSNotificationJob, pushNotificationDAO, dto.ScheduleTime);
            //        }
            //    }

            //    pushNotificationDAO.StatusId = 1;
            //    pushNotificationDAO.ErrorMessage = string.Empty;
            //}
            //else
            //{
            //    pushNotificationDAO.StatusId = 0;
            //    pushNotificationDAO.ErrorMessage = "There is some issue while processing your request, Please try after some time";
            //}

            throw new Exception();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pushNotificationDAO"></param>
        /// <returns></returns>
        public async Task Save(PushNotificationDAO pushNotificationDAO)
        {
            if (String.IsNullOrEmpty(pushNotificationDAO.Message))
            {
                pushNotificationDAO.StatusId = 0;
                pushNotificationDAO.ErrorMessage = "Invalid message content, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(pushNotificationDAO.Title))
            {
                pushNotificationDAO.StatusId = 0;
                pushNotificationDAO.ErrorMessage = "Invalid message content, Please check.!";
                return;
            }
            if (pushNotificationDAO.UserId == 0)
            {
                pushNotificationDAO.StatusId = 0;
                pushNotificationDAO.ErrorMessage = "Invalid message content, Please check.!";
                return;
            }
            if (pushNotificationDAO.CityId == 0)
            {
                pushNotificationDAO.StatusId = 0;
                pushNotificationDAO.ErrorMessage = "Invalid message content, Please check.!";
                return;
            }
            if (pushNotificationDAO.NotificationTypeId == 0)
            {
                pushNotificationDAO.StatusId = 0;
                pushNotificationDAO.ErrorMessage = "Invalid message content, Please check.!";
                return;
            }

            var dto = new PushNotificationDto()
            {
                NotificationId = pushNotificationDAO.Id,
                Completed = true,
                AndroidPayLoad = pushNotificationDAO.AndroidPayLoad,
                IosPayLoad = pushNotificationDAO.IosPayLoad,
                SuccessCount = pushNotificationDAO.SuccessCount,
                FailureCount = pushNotificationDAO.FailureCount,
                StatusId = pushNotificationDAO.CurrentStatusId
            };

            var dal = DalFactory.Create<IPushNotificationDal>();
            await dal.Update(dto);

            if (dto.NotificationId > 0)
            {
                pushNotificationDAO.StatusId = 1;
                pushNotificationDAO.ErrorMessage = string.Empty;
            }
            return;
        }

        public async Task<PushNotificationDAO> Get(int notificationId)
        {
            var dal = DalFactory.Create<IPushNotificationDal>();
            var dto = await dal.Fetch(notificationId);
            return new PushNotificationDAO()
            {
                Message = dto.Message,
                CreatedDatetime = dto.CreatedDatetime,
                Id = dto.NotificationId,
                UserId = dto.UserId,
                Active = dto.Active,
                CityId = dto.CityId,
                NotificationTypeId = dto.NotificationTypeId,
                ScheduleTime = dto.ScheduleTime.ToString("MM-dd-yyyy hh:mm tt"),
                Title = dto.Title,
                NotificationType = ((Enumeration.NotificationType)dto.NotificationTypeId).ToString(),
                CategoryTypeId = dto.CategoryTypeId,
                CityName = dto.CityName,
                OfferTypeId = dto.OfferTypeId,
                Extension = dto.Extension,
                SuccessCount = dto.SuccessCount ?? 0,
                FailureCount = dto.FailureCount ?? 0,
                IosPayLoad = dto.IosPayLoad,
                AndroidPayLoad = dto.AndroidPayLoad,
                OfferTypeName = ((Enumeration.OfferType)dto.OfferTypeId).ToString(),
                CategoryTypeName = dto.CategoryTypeName,
                CurrentStatusId = dto.StatusId,
                ImagePath = string.Format("{0}{1}{2}{3}", AppSettingKeys.AdminApplicationBaseUrl + "/", AppSettingKeys.NotificationDirectory.Replace("\\", "/"), dto.NotificationId, dto.Extension)
            };
        }
    }
}
