﻿using GotTable.Dal;
using GotTable.Dal.PushNotifications;
using GotTable.DAO.PushNotifications;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.PushNotifications
{
    [Serializable]
    public sealed class NotificationList : INotificationList
    {
        public async Task<List<PushNotificationInfoDAO>> Get(decimal userId, DateTime? scheduleDate = null, string notificationCity = "", string notificationType = "", int currentPage = 1, int pageSize = 10)
        {
            var notificationList = new List<PushNotificationInfoDAO>();
            var dal = DalFactory.Create<IPushNotificationDal>();
            var dtos = await dal.FetchList(userId, scheduleDate, notificationCity, notificationType, currentPage, pageSize);
            dtos.ForEach(item =>
            {
                notificationList.Add(new PushNotificationInfoDAO()
                {
                    CreatedDatetime = item.CreatedDatetime,
                    Id = item.NotificationId,
                    UserId = item.UserId,
                    Active = item.Active,
                    CityId = item.CityId,
                    Message = item.Message,
                    ScheduleTime = item.ScheduleTime.ToString("MM-dd-yyyy hh:mm tt"),
                    Title = item.Title,
                    NotificationType = item.NotificationType,
                    CategoryTypeName = item.CategoryTypeName,
                    OfferTypeName = item.OfferTypeName,
                    AndroidPayLoad = item.AndroidPayLoad,
                    IosPayLoad = item.IosPayLoad,
                    SuccessCount = item.SuccessCount ?? 0,
                    FailureCount = item.FailureCount ?? 0,
                    CityName = item.CityName,
                    StatusName = item.StatusName
                });
            });
            return notificationList;
        }
    }
}
