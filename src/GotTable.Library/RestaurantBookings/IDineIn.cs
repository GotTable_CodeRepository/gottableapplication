﻿using GotTable.DAO.Bookings.DineIn;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    /// <summary>
    /// DineIn
    /// </summary>
    public interface IDineIn
    {
        /// <summary>
        /// New
        /// </summary>
        /// <param name="dineInBookingDAO"></param>
        Task New(DineInBookingDAO dineInBookingDAO);

        /// <summary>
        /// New
        /// </summary>
        /// <param name="dineInStatusDAO"></param>
        Task New(DineInStatusDAO dineInStatusDAO);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        Task<DineInBookingDAO> Get(decimal bookingId);

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="bookingId"></param>
        Task MarkAsRead(decimal bookingId);
    }
}