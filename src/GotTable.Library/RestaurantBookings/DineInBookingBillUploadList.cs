﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantBookings.BillUploads;
using GotTable.DAO.RestaurantBookings.BillUploads;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    [Serializable]
    public sealed class DineInBookingBillUploadList : IDineInBookingBillUploadList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uploadStatus"></param>
        /// <param name="emailAddress"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BillUploadListItemInfoDAO>> Get(Enumeration.BillUpload? uploadStatus = null, string emailAddress = "", string phoneNumber = "", int currentPage = 1, int pageSize = 10)
        {
            var billUploadList = new List<BillUploadListItemInfoDAO>();
            var dal = DalFactory.Create<IBillUploadDal>();
            foreach (var dto in await dal.FetchList(uploadStatus, emailAddress, phoneNumber, currentPage, pageSize))
            {
                billUploadList.Add(new BillUploadListItemInfoDAO()
                {
                    Amount = dto.Amount,
                    BookingDate = dto.BookingDate,
                    BookingId = dto.BookingId,
                    EmailAddress = dto.EmailAddress,
                    ImagePath = dto.ImagePath,
                    PhoneNumber = dto.PhoneNumber,
                    RestaurantAddress = dto.RestaurantAddress,
                    RestaurantName = dto.RestaurantName,
                    UploadedDate = dto.UploadedDate,
                    UserId = dto.UserId,
                    UserName = dto.UserName
                });
            }
            return billUploadList;
        }
    }
}
