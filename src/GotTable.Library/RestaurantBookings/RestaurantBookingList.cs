﻿using GotTable.Dal;
using GotTable.Dal.RestaurantBookings;
using GotTable.DAO.Bookings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    [Serializable]
    public sealed class RestaurantBookingList : IRestaurantBookingList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="bookingDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BookingDAO>> Get(decimal restaurantId, DateTime bookingDate, int currentPage = 1, int pageSize = 10)
        {
            var dal = DalFactory.Create<IBookingListDal>();
            var dtos = await dal.FetchRestaurantBookingList(restaurantId, bookingDate, currentPage, pageSize);
            return await MapModelToObject(dtos);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="bookingStartDate"></param>
        /// <param name="bookingEndDate"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BookingDAO>> Get(decimal restaurantId, DateTime bookingStartDate, DateTime bookingEndDate, int currentPage = 1, int pageSize = 10)
        {
            var dal = DalFactory.Create<IBookingListDal>();
            var dtos = await dal.FetchRestaurantBookingList(restaurantId, bookingStartDate, bookingEndDate, currentPage, pageSize);
            return await MapModelToObject(dtos);
        }

        #region Helper methods

        internal async Task<List<BookingDAO>> MapModelToObject(List<BookingInfoDto> dtos)
        {
            await Task.FromResult(1);
            var bookingList = new List<BookingDAO>();
            foreach (var item in dtos)
            {
                bookingList.Add(new BookingDAO()
                {
                    BookingDateTime = DateTime.ParseExact(item.BookingDate.ToString("MM-dd-yyyy") + " " + item.BookingTime, "MM-dd-yyyy HH:mm", null),
                    BookingId = item.BookingId,
                    BookingType = item.BookingTypeName,
                    CurrentStatus = item.CurrentStatusName,
                    EmailAddress = item.UserEmail,
                    IsRead = item.IsRead ?? false,
                    Name = item.UserName,
                    PhoneNumber = item.PhoneNumber,
                    TableName = item.TableName,
                    UserId = item.UserId,
                    DateTimeForOrder = item.CreatedDate.ToString("dd-MMM-yyyy hh:mm tt")
                });
            }
            return bookingList;
        }
        #endregion
    }
}
