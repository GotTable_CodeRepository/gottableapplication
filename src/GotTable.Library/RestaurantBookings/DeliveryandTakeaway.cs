﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantBookings.DeliveryandTakeaway;
using GotTable.DAO.Bookings.DeliveryandTakeaway;
using GotTable.DAO.Users;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Communications.Jobs;
using GotTable.Library.Communications.Jobs.Types;
using GotTable.Library.Communications.Message;
using GotTable.Library.Users;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    [Serializable]
    public sealed class DeliveryandTakeaway : IDeliveryandTakeaway
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IJobFactory jobFactory;

        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMessageTransmission<DeliveryandTakeawayBookingConfirmation, string> messageTransmission;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJob<DeliveryandTakeawayBookingConfirmationJob, DeliveryandTakeawayDAO> emailJob;

        /// <summary>
        /// 
        /// </summary>
        private const string confirmationMessage = "Your order has been confirmed. Thank you for using Got Table!";

        public DeliveryandTakeaway(IJobFactory jobFactory, IUser user, IMessageTransmission<DeliveryandTakeawayBookingConfirmation, string> messageTransmission, IJob<DeliveryandTakeawayBookingConfirmationJob, DeliveryandTakeawayDAO> emailJob)
        {
            this.jobFactory = jobFactory;
            this.user = user;
            this.messageTransmission = messageTransmission;
            this.emailJob = emailJob;
        }

        #region Factory methods

        /// <summary>
        /// New
        /// </summary>
        /// <param name="deliveryandTakeawayDAO"></param>
        /// <returns></returns>
        public async Task New(DeliveryandTakeawayDAO deliveryandTakeawayDAO)
        {
            deliveryandTakeawayDAO.ConfirmationMessage = string.Empty;
            deliveryandTakeawayDAO.ErrorMessage = string.Empty;
            deliveryandTakeawayDAO.StatusId = 0;

            if (deliveryandTakeawayDAO.BranchId == 0)
            {
                deliveryandTakeawayDAO.ErrorMessage = "Invalid request, Please check.!";
                return;
            }
            if (deliveryandTakeawayDAO.UserId == 0 && deliveryandTakeawayDAO.UserDetail.FirstName == string.Empty)
            {
                deliveryandTakeawayDAO.ErrorMessage = "First name can not be blank, Please check.!";
                return;
            }
            if (deliveryandTakeawayDAO.UserId == 0 && deliveryandTakeawayDAO.UserDetail.LastName == string.Empty)
            {
                deliveryandTakeawayDAO.ErrorMessage = "Last name can not be blank, Please check.!";
                return;
            }
            if (deliveryandTakeawayDAO.UserId == 0 && deliveryandTakeawayDAO.UserDetail.EmailAddress == string.Empty)
            {
                deliveryandTakeawayDAO.ErrorMessage = "Email address can not be blank, Please check.!";
                return;
            }
            if (deliveryandTakeawayDAO.PhoneNumber == 0)
            {
                deliveryandTakeawayDAO.ErrorMessage = "Phone number can not be blank, Please check.!";
                return;
            }
            if (deliveryandTakeawayDAO.BookingDate == null)
            {
                deliveryandTakeawayDAO.ErrorMessage = "Invalid booking date, Please check.!";
                return;
            }
            if (deliveryandTakeawayDAO.BookingTime == string.Empty)
            {
                deliveryandTakeawayDAO.ErrorMessage = "Invalid booking time, Please check.!";
                return;
            }
            if (deliveryandTakeawayDAO.Cartamount == 0)
            {
                deliveryandTakeawayDAO.ErrorMessage = "Invalid cartamount, Please check.!";
                return;
            }
            if (deliveryandTakeawayDAO.CentralGST == 0)
            {
                deliveryandTakeawayDAO.ErrorMessage = "Invalid central gst, Please check.!";
                return;
            }
            if (deliveryandTakeawayDAO.StateGST == 0)
            {
                deliveryandTakeawayDAO.ErrorMessage = "Invalid state gst, Please check.!";
                return;
            }

            UserDetailDAO userDao = null;

            if (deliveryandTakeawayDAO.UserId != 0)
            {
                userDao = await user.Get(deliveryandTakeawayDAO.UserId);
                if (userDao.UserType != Enumeration.UserType.EndUser.ToString() && userDao.UserType != Enumeration.UserType.Guest.ToString())
                {
                    deliveryandTakeawayDAO.ErrorMessage = "Invalid request, You can not use this emailaddress for booking.";
                    return;
                }
            }
            else if (deliveryandTakeawayDAO.UserId == 0)
            {
                userDao = await user.Get(deliveryandTakeawayDAO.UserDetail.EmailAddress);
                if (userDao.UserId == 0)
                {
                    userDao = new UserDetailDAO()
                    {
                        UserId = deliveryandTakeawayDAO.UserDetail.UserId,
                        EmailAddress = deliveryandTakeawayDAO.UserDetail.EmailAddress,
                        FirstName = deliveryandTakeawayDAO.UserDetail.FirstName,
                        LastName = deliveryandTakeawayDAO.UserDetail.LastName,
                        UserType = Enumeration.UserType.Guest.ToString(),
                        Gender = Enumeration.Gender.NotDefined.ToString(),
                        Prefix = Enumeration.Prefix.NotDefined.ToString(),
                        IsActive = deliveryandTakeawayDAO.UserDetail.IsActive,
                        IsEmailVerified = deliveryandTakeawayDAO.UserDetail.IsEmailVerified,
                        IsEmailOptedForCommunication = deliveryandTakeawayDAO.UserDetail.IsEmailOptedForCommunication,
                        Password = Guid.NewGuid().ToString().Substring(1, 10)
                    };
                    await user.New(userDao);
                }
            }

            var dto = new DeliveryandTakeawayDto()
            {
                BookingDate = deliveryandTakeawayDAO.BookingDate,
                BookingTime = deliveryandTakeawayDAO.BookingTime,
                BookingId = deliveryandTakeawayDAO.BookingId,
                BookingTypeId = (int)deliveryandTakeawayDAO.BookingType,
                BranchId = deliveryandTakeawayDAO.BranchId,
                CartAmount = deliveryandTakeawayDAO.CartItem.Sum(x => x.Price * x.Quantity),
                Comment = string.Empty,
                CreatedDate = DateTime.Now,
                OfferId = deliveryandTakeawayDAO.OfferId,
                PhoneNumber = deliveryandTakeawayDAO.PhoneNumber,
                CentralGST = deliveryandTakeawayDAO.CentralGST,
                StateGST = deliveryandTakeawayDAO.StateGST,
                UserId = userDao.UserId,
                CurrentStatusId = (int)Enumeration.DeliveryandTakeawayStatusType.Confirm,
                PromoCode = deliveryandTakeawayDAO.PromoCode,
                DeliveryCharges = deliveryandTakeawayDAO.DeliveryCharges,
                DeliveryAddress = deliveryandTakeawayDAO.BookingType == Enumeration.BookingType.Takeaway ? null : new DeliveryAddressDto()
                {
                    City = deliveryandTakeawayDAO.AddressDetail.City,
                    Id = deliveryandTakeawayDAO.AddressDetail.Id,
                    Landmark = deliveryandTakeawayDAO.AddressDetail.Landmark,
                    Latitude = deliveryandTakeawayDAO.AddressDetail.Latitude,
                    Line1 = deliveryandTakeawayDAO.AddressDetail.AddressLine1,
                    Line2 = deliveryandTakeawayDAO.AddressDetail.AddressLine2,
                    Longitude = deliveryandTakeawayDAO.AddressDetail.Longitude,
                    State = deliveryandTakeawayDAO.AddressDetail.State,
                    Zip = deliveryandTakeawayDAO.AddressDetail.Zip
                },
                ItemList = deliveryandTakeawayDAO.CartItem.Select(m => new DeliveryandTakeawayCartIttemDto()
                {
                    MenuId = m.MenuId,
                    Quantity = m.Quantity,
                    Cost = m.Price,
                    Remark = m.Remark
                }).ToList()
            };

            var dal = DalFactory.Create<IDeliveryandTakeawayDal>();
            await dal.Insert(dto);

            if (dto.BookingId != 0)
            {
                var deliveryAddress = string.Empty;
                if (deliveryandTakeawayDAO.BookingType == Enumeration.BookingType.Delivery)
                    deliveryAddress = deliveryandTakeawayDAO.AddressDetail.AddressLine1 + " " + deliveryandTakeawayDAO.AddressDetail.AddressLine2 + " " + deliveryandTakeawayDAO.AddressDetail.Landmark + ", " + deliveryandTakeawayDAO.AddressDetail.City + " " + deliveryandTakeawayDAO.AddressDetail.State;

                var dateTimeForDeliveryandTakeaway = DateTime.ParseExact(deliveryandTakeawayDAO.BookingDate.ToString("MM-dd-yyyy") + " " + deliveryandTakeawayDAO.BookingTime, "MM-dd-yyyy HH:mm", null);

                deliveryandTakeawayDAO.StatusId = 1;

                await CreateJob(deliveryandTakeawayDAO);

                var bookingConfirmationMessage = confirmationMessage;
                if(deliveryandTakeawayDAO.BookingType == Enumeration.BookingType.Delivery && AppSettingKeys.DeliveryMessageTransmission)
                {
                    bookingConfirmationMessage = await messageTransmission.Execute(deliveryandTakeawayDAO.BookingType, dateTimeForDeliveryandTakeaway, deliveryandTakeawayDAO.PhoneNumber, deliveryandTakeawayDAO.BranchId, null, deliveryAddress, deliveryandTakeawayDAO.UserId, deliveryandTakeawayDAO.BookingId);
                }
                if (deliveryandTakeawayDAO.BookingType == Enumeration.BookingType.Takeaway && AppSettingKeys.TakeawayMessageTransmission)
                {
                    bookingConfirmationMessage = await messageTransmission.Execute(deliveryandTakeawayDAO.BookingType, dateTimeForDeliveryandTakeaway, deliveryandTakeawayDAO.PhoneNumber, deliveryandTakeawayDAO.BranchId, null, deliveryAddress, deliveryandTakeawayDAO.UserId, deliveryandTakeawayDAO.BookingId);
                }
                deliveryandTakeawayDAO.ConfirmationMessage = bookingConfirmationMessage;
            }
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<DeliveryandTakeawayDAO> Get(decimal bookingId)
        {
            var dal = DalFactory.Create<IDeliveryandTakeawayDal>();
            var dto = await dal.Fetch(bookingId);
            return new DeliveryandTakeawayDAO()
            {
                BookingDate = DateTime.ParseExact(dto.BookingDate.ToString("MM-dd-yyyy"), "MM-dd-yyyy", null),
                BookingTime = dto.BookingTime,
                BookingId = dto.BookingId,
                BookingTypeId = dto.BookingTypeId,
                BranchId = dto.BranchId,
                OfferId = dto.OfferId ?? 0,
                PhoneNumber = dto.PhoneNumber,
                StateGST = dto.StateGST,
                OfferTitle = dto.OfferTitle,
                OfferDescription = dto.OfferDescription,
                CartItem = dto.ItemList.Select(item => new DeliveryandTakeawayCartItemDAO()
                {
                    ItemId = item.ItemId,
                    MenuId = item.MenuId,
                    MenuName = item.MenuName,
                    Price = item.Cost,
                    Quantity = item.Quantity,
                    Remark = item.Remark
                }).ToList(),
                StatusList = dto.StatusList.Select(item => new DeliveryandTakeawayStatusDAO()
                {
                    Comment = item.Comment,
                    CreatedDate = item.CreatedDate,
                    StatusId = item.StatusId,
                    StatusName = item.StatusName,
                    UserId = item.UserId,
                    UserName = item.UserName,
                    AdminId = item.AdminId,
                    AdminName = item.AdminName
                }).ToList(),
                CurrentStatusId = dto.CurrentStatusId,
                CurrentStatus = ((Enumeration.DeliveryandTakeawayStatusType)dto.CurrentStatusId).ToString(),
                AddressDetail = dto.DeliveryAddress == null ? null : MapModelToObject(dto.DeliveryAddress),
                UserId = dto.UserId,
                UserDetail = await user.Get(dto.UserId),
                NewStatus = new DeliveryandTakeawayStatusDAO()
                {
                    BookingId = dto.BookingId,
                    Comment = string.Empty,
                },
                CentralGST = dto.CentralGST,
                PromoCode = dto.PromoCode ?? "No promo code",
                DeliveryCharges = dto.DeliveryCharges
            };
        }

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="bookingId"></param>
        public async Task MarkAsRead(decimal bookingId)
        {
            var dal = DalFactory.Create<IDeliveryandTakeawayDal>();
            await dal.MarkAsRead(bookingId);
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="deliveryandTakeawayStatusDAO"></param>
        public async Task New(DeliveryandTakeawayStatusDAO deliveryandTakeawayStatusDAO)
        {
            var dto = new DeliveryandTakeawayStatusDto()
            {
                BookingId = deliveryandTakeawayStatusDAO.BookingId,
                Comment = deliveryandTakeawayStatusDAO.Comment == null ? string.Empty : deliveryandTakeawayStatusDAO.Comment,
                StatusId = (int)Enum.Parse(typeof(Enumeration.DineInStatusType), deliveryandTakeawayStatusDAO.StatusName),
                UserId = deliveryandTakeawayStatusDAO.UserId,
                AdminId = deliveryandTakeawayStatusDAO.AdminId
            };
            var dal = DalFactory.Create<IDeliveryandTakeawayDal>();
            await dal.Insert(dto);
        }

        #endregion

        #region Helper Method

        private DeliveryAddressDAO MapModelToObject(DeliveryAddressDto dto)
        {
            return new DeliveryAddressDAO()
            {
                AddressLine1 = dto.Line1,
                AddressLine2 = dto.Line2,
                City = dto.City,
                Id = dto.Id ?? 0,
                Landmark = dto.Landmark,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                State = dto.State,
                Zip = dto.Zip
            };
        }

        /// <summary>
        /// CreateJob
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        private async Task CreateJob(DeliveryandTakeawayDAO dao)
        {
            if (dao.BookingType == Enumeration.BookingType.Delivery && AppSettingKeys.DeliveryEmailTransmission)
            {
                var scheduleDateTime = DateTime.Now.AddMinutes(2);
                jobFactory.CreateJob(emailJob, dao, scheduleDateTime);
            }

            if (dao.BookingType == Enumeration.BookingType.Takeaway && AppSettingKeys.TakeawayEmailTransmission)
            {
                var scheduleDateTime = DateTime.Now.AddMinutes(2);
                jobFactory.CreateJob(emailJob, dao, scheduleDateTime);
            }
        }

        #endregion  
    }
}
