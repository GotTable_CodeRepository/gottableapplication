﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantBookings.DineIn;
using GotTable.DAO.Bookings.DineIn;
using GotTable.DAO.Users;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Communications.Jobs;
using GotTable.Library.Communications.Jobs.Types;
using GotTable.Library.Communications.Message;
using GotTable.Library.Devices;
using GotTable.Library.RestaurantConfigurations;
using GotTable.Library.RestaurantTables;
using GotTable.Library.Users;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantBookings
{
    /// <summary>
    /// DineIn
    /// </summary>
    [Serializable]
    public sealed class DineIn : IDineIn
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDevice device;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJobFactory jobFactory;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTable branchTable;

        /// <summary>
        /// 
        /// </summary>
        private readonly IMessageTransmission<DineInBookingConfirmation, string> messageTransmission;

        /// <summary>
        /// 
        /// </summary>
        private readonly IUser user;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchConfiguration branchConfiguration;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJob<DineInBookingAutoCompleteJob, DineInBookingDAO> autoCompleteDineInJob;

        /// <summary>
        /// 
        /// </summary>
        private readonly IJob<DineInBookingConfirmationJob, DineInBookingDAO> emailJob;

        /// <summary>
        /// 
        /// </summary>
        private const string confirmationMessage = "Your {0} reservation for {1} has been confirmed. Thank you for using Got Table!";

        public DineIn(IBranchTable branchTable = null, IMessageTransmission<DineInBookingConfirmation, string> messageTransmission = null, IUser user = null, IBranchConfiguration branchConfiguration = null, IJobFactory jobFactory = null, IJob<DineInBookingAutoCompleteJob, DineInBookingDAO> autoCompleteDineInJob = null, IJob<DineInBookingConfirmationJob, DineInBookingDAO> emailJob = null, IDevice device = null)
        {
            this.jobFactory = jobFactory;
            this.branchTable = branchTable;
            this.messageTransmission = messageTransmission;
            this.user = user;
            this.branchConfiguration = branchConfiguration;
            this.autoCompleteDineInJob = autoCompleteDineInJob;
            this.emailJob = emailJob;
            this.device = device;
        }


        #region Factory methods

        /// <summary>
        /// New
        /// </summary>
        /// <param name="dineInBookingDAO"></param>
        /// <returns></returns>
        public async Task New(DineInBookingDAO dineInBookingDAO)
        {
            if (dineInBookingDAO.UserId == 0 && String.IsNullOrEmpty(dineInBookingDAO.UserDetail.FirstName))
            {
                dineInBookingDAO.StatusId = 0;
                dineInBookingDAO.ErrorMessage = "First name should not be blank, Please check.!";
                return;
            }
            if (dineInBookingDAO.UserId == 0 && String.IsNullOrEmpty(dineInBookingDAO.UserDetail.LastName))
            {
                dineInBookingDAO.StatusId = 0;
                dineInBookingDAO.ErrorMessage = "Last name should not be blank, Please check.!";
                return;
            }
            if (dineInBookingDAO.UserId == 0 && String.IsNullOrEmpty(dineInBookingDAO.UserDetail.EmailAddress))
            {
                dineInBookingDAO.StatusId = 0;
                dineInBookingDAO.ErrorMessage = "Email address should not be blank, Please check.!";
                return;
            }
            if (dineInBookingDAO.PhoneNumber == 0)
            {
                dineInBookingDAO.StatusId = 0;
                dineInBookingDAO.ErrorMessage = "Phonenumber can not be blank, Please check.!";
                return;
            }
            if (dineInBookingDAO.TableId == 0)
            {
                dineInBookingDAO.StatusId = 0;
                dineInBookingDAO.ErrorMessage = "Please select one table.!";
                return;
            }
            var bookingDateTimeForDineIn = DateTime.ParseExact(dineInBookingDAO.BookingDate.ToString("MM-dd-yyyy") + " " + dineInBookingDAO.BookingTime, "MM-dd-yyyy HH:mm", null);
            if (DateTime.Now.AddMinutes(15) > bookingDateTimeForDineIn)
            {
                dineInBookingDAO.StatusId = 0;
                dineInBookingDAO.ErrorMessage = "Booking time should be 15 minutes more than current date and time.!";
                return;
            }
            var tableDetail = await branchTable.Get(dineInBookingDAO.TableId);
            if (tableDetail == null)
            {
                dineInBookingDAO.StatusId = 0;
                dineInBookingDAO.ErrorMessage = "Invalid table, Please check.!";
                return;
            }
            if (await branchTable.CheckAvailablity(tableDetail, dineInBookingDAO.BookingDate, dineInBookingDAO.BookingTime) == false)
            {
                dineInBookingDAO.StatusId = 0;
                dineInBookingDAO.ErrorMessage = "Table you are looking for is not available, Please try with different table";
                return;
            }
            dynamic userDetail = null;
            if (dineInBookingDAO.UserId != 0)
            {
                userDetail = await user.Get(dineInBookingDAO.UserId);
                if (userDetail.UserType != Enumeration.UserType.Guest.ToString() && userDetail.UserType != Enumeration.UserType.EndUser.ToString())
                {
                    dineInBookingDAO.StatusId = 0;
                    dineInBookingDAO.ErrorMessage = "Invalid email address, Please check.!";
                    return;
                }
            }

            if (dineInBookingDAO.UserId == 0)
            {
                userDetail = await user.Get(dineInBookingDAO.UserDetail.EmailAddress);
                if (userDetail.UserId == 0)
                {
                    userDetail = new UserDetailDAO()
                    {
                        UserId = dineInBookingDAO.UserId,
                        EmailAddress = dineInBookingDAO.UserDetail.EmailAddress,
                        FirstName = dineInBookingDAO.UserDetail.FirstName,
                        LastName = dineInBookingDAO.UserDetail.LastName,
                        UserType = Enumeration.UserType.Guest.ToString(),
                        Gender = Enumeration.Gender.NotDefined.ToString(),
                        Prefix = Enumeration.Prefix.NotDefined.ToString(),
                        IsEmailOptedForCommunication = dineInBookingDAO.UserDetail.IsEmailOptedForCommunication,
                        Password = Guid.NewGuid().ToString().Substring(1, 10),
                        IsActive = dineInBookingDAO.UserDetail.IsActive
                    };
                    await user.New(userDetail);
                }
            }
            dineInBookingDAO.UserDetail = userDetail;

            var dto = new DineInDto()
            {
                BookingDate = dineInBookingDAO.BookingDate,
                BookingTime = dineInBookingDAO.BookingTime,
                BranchId = dineInBookingDAO.BranchId,
                Comment = dineInBookingDAO.Comment,
                CreatedDate = dineInBookingDAO.CreatedDate,
                Id = dineInBookingDAO.Id,
                TableId = dineInBookingDAO.TableId,
                UserId = userDetail.UserId,
                PhoneNumber = dineInBookingDAO.PhoneNumber,
                OfferId = dineInBookingDAO.OfferId == 0 ? null : dineInBookingDAO.OfferId,
                CurrentStatusId = (int)Enumeration.DineInStatusType.Confirm,
                PromoCode = dineInBookingDAO.PromoCode,
                BillUploadStatus = (int)Enumeration.BillUpload.Pending,
                DoubleTheDealActive = false
            };

            var dal = DalFactory.Create<IDineInDal>();
            await dal.Insert(dto);

            dineInBookingDAO.Id = dto.Id;
            dineInBookingDAO.OfferTitle = dto.OfferTitle;
            dineInBookingDAO.SelectedTable = dto.TableName;

            if (Enum.TryParse<Enumeration.Device>(dineInBookingDAO.DeviceType, false, out var selectedDevice))
            {
                await device.Save(new DAO.Devices.DeviceDAO()
                {
                    DeviceId = dineInBookingDAO.DeviceId,
                    SelectedDevice = selectedDevice,
                    UserId = dineInBookingDAO.UserDetail.UserId,
                    IsActive = true
                });
            }

            await CreateJob(dineInBookingDAO);

            string bookingConfirmationMessage;
            if (AppSettingKeys.DineInMessageTransmission)
            {
                bookingConfirmationMessage = await messageTransmission.Execute(Enumeration.BookingType.DineIn, bookingDateTimeForDineIn, dineInBookingDAO.PhoneNumber, dineInBookingDAO.BranchId, tableDetail.SelectedTable, null, dineInBookingDAO.UserId, dineInBookingDAO.Id, dto.RewardApplied);
            }
            else
            {
                bookingConfirmationMessage = string.Format(confirmationMessage, dineInBookingDAO.BookingDate, dineInBookingDAO.SelectedTable.ToString());
            }

            dineInBookingDAO.StatusId = 1;
            dineInBookingDAO.ConfirmationMessage = bookingConfirmationMessage;
            dineInBookingDAO.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// New
        /// </summary>
        /// <param name="dineInStatusDAO"></param>
        public async Task New(DineInStatusDAO dineInStatusDAO)
        {
            var dto = new DineInStatusDto()
            {
                BookingId = dineInStatusDAO.BookingId,
                Comment = dineInStatusDAO.Comment ?? string.Empty,
                StatusId = (int)Enum.Parse(typeof(Enumeration.DineInStatusType), dineInStatusDAO.StatusName),
                UserId = null,
                AdminId = dineInStatusDAO.UserId
            };
            var dal = DalFactory.Create<IDineInDal>();
            await dal.Insert(dto);
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task<DineInBookingDAO> Get(decimal bookingId)
        {
            var dal = DalFactory.Create<IDineInDal>();
            var dto = await dal.Fetch(bookingId);
            return new DineInBookingDAO()
            {
                BookingDate = DateTime.ParseExact(dto.BookingDate.ToString("MM-dd-yyyy"), "MM-dd-yyyy", null),
                BookingTime = dto.BookingTime,
                BranchId = dto.BranchId,
                Comment = dto.Comment,
                CreatedDate = dto.CreatedDate,
                Id = dto.Id,
                TableId = dto.TableId,
                SelectedTable = dto.TableName,
                UserId = dto.UserId,
                PhoneNumber = dto.PhoneNumber,
                UserDetail = await user.Get(dto.UserId),
                StatusList = dto.StatusList.Select(item => new DineInStatusDAO()
                {
                    BookingId = item.BookingId,
                    Comment = item.Comment,
                    CreatedDate = item.CreatedDate,
                    Id = item.Id,
                    StatusName = ((Enumeration.DineInStatusType)item.StatusId).ToString(),
                    UserId = item.UserId,
                    UserName = item.UserName,
                    AdminId = item.AdminId,
                    AdminName = item.AdminName
                }).ToList(),
                OfferId = dto.OfferId,
                OfferTitle = dto.OfferTitle,
                OfferDescription = dto.OfferDescription,
                OfferType = dto.OfferTypeId != null ? ((Enumeration.OfferType)dto.OfferTypeId).ToString() : default,
                PromoCode = dto.PromoCode ?? "No promo code",
                RewardApplied = dto.RewardApplied,
                BillUploadStatus = ((Enumeration.BillUpload)dto.BillUploadStatus).ToString(),
                DoubleTheDeal = dto.DoubleTheDealActive ?? false
            };
        }

        /// <summary>
        /// MarkAsRead
        /// </summary>
        /// <param name="bookingId"></param>
        /// <returns></returns>
        public async Task MarkAsRead(decimal bookingId)
        {
            var dal = DalFactory.Create<IDineInDal>();
            await dal.MarkAsRead(bookingId);
        }

        #endregion

        #region Helpers

        /// <summary>
        /// CreateJob
        /// </summary>
        /// <param name="dineInBookingDAO"></param>
        /// <returns></returns>
        private async Task CreateJob(DineInBookingDAO dineInBookingDAO)
        {
            var branchConfigurationDao = await branchConfiguration.Get(dineInBookingDAO.BranchId);

            if (branchConfigurationDao.DineInAutoCompleteActive && branchConfigurationDao.DineInAutoCompleteHours != null)
            {
                var scheduleDateTime = DateTime.ParseExact(dineInBookingDAO.BookingDate.Date.ToString("MM-dd-yyyy") + " " + dineInBookingDAO.BookingTime, "MM-dd-yyyy HH:mm", null).AddHours(Convert.ToDouble(branchConfigurationDao.DineInAutoCompleteHours));
                jobFactory.CreateJob(autoCompleteDineInJob, dineInBookingDAO, scheduleDateTime);
            }

            if (AppSettingKeys.DineInEmailTransmission)
            {
                var scheduleDateTime = DateTime.Now.AddMinutes(2);
                jobFactory.CreateJob(emailJob, dineInBookingDAO, scheduleDateTime);
            }
        }

        #endregion
    }
}
