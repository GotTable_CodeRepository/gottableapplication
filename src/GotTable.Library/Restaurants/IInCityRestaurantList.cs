﻿using GotTable.DAO.Restaurants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    public interface IInCityRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        Task<List<InCityRestaurantDAO>> Get(decimal? cityId);
    }
}