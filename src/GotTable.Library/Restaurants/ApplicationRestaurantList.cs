﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Restaurants.ApplicationRestaurants;
using GotTable.DAO.Restaurants;
using GotTable.Library.RestaurantCuisines;
using GotTable.Library.RestaurantRatings;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    [Serializable]
    public sealed class ApplicationRestaurantList : IApplicationRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchRating branchRating;

        /// <summary>
        /// 
        /// </summary>
        private readonly ICuisineList cuisineList;

        /// <summary>
        /// 
        /// </summary>
        private readonly IRestaurantTag<RestaurantListTag> restaurantListTag;

        /// <summary>
        /// 
        /// </summary>
        private readonly IRestaurantTag<RestaurantTag> restaurantTag;


        public ApplicationRestaurantList(IBranchRating branchRating, ICuisineList cuisineList, IRestaurantTag<RestaurantListTag> restaurantListTag, IRestaurantTag<RestaurantTag> restaurantTag)
        {
            this.branchRating = branchRating;
            this.cuisineList = cuisineList;
            this.restaurantTag = restaurantTag;
            this.restaurantListTag = restaurantListTag;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <returns></returns>
        public async Task<List<ResuturantModelDAO>> Get(double latitude, double longitude, decimal restaurantId, Enumeration.RestaurantTypes restaurantType)
        {
            var restaurantList = new List<ResuturantModelDAO>();
            var dal = DalFactory.Create<IRestaurantListDal>();
            var dtos = await dal.Fetch(restaurantId, latitude, longitude, restaurantType);
            foreach (var item in dtos)
            {
                var ratingDAO = await branchRating.GetAvgRating(item.RestaurantId);
                var restaurant = new ResuturantModelDAO()
                {
                    AddressLine1 = item.Line1,
                    AddressLine2 = item.Line2,
                    AvgRating = ratingDAO.Rating > 3 ? ratingDAO.Rating : 0,
                    City = item.City,
                    Distance = item.Distance,
                    Id = item.RestaurantId,
                    Latitude = item.Latitude,
                    Longitude = item.Longitude,
                    Name = item.Name,
                    OfferName = string.Empty,
                    State = item.State,
                    ZipCode = item.Zip,
                    RestaurantTag = item.RestaurantTag,
                    SEODescription = item.SEODescription,
                    SEOKeyword = item.SEOKeyword,
                    SEOTitle = item.SEOTitle,
                    Category = item.Category,
                    ActiveOffers = 0,
                    CuisineNames = string.Empty,
                    ImageList = new List<DAO.Images.ImageDAO>(),
                    IsOfferAvailable = false,
                    SelectedTag = Enumeration.RestaurantTags.NoTag,
                    CostForTwo = item.CostForTwo,
                    Description = item.Description,
                    FoodRating = ratingDAO.FoodRating,
                    AmbienceRating = ratingDAO.AmbienceRating,
                    MusicRating = ratingDAO.MusicRating,
                    PriceRating = ratingDAO.PriceRating,
                    ServiceRating = ratingDAO.ServiceRating
                };
                var cuisineArray = new ArrayList();
                var cuisines = await cuisineList.Get(item.RestaurantId, 1, 3, true);
                cuisines.ForEach(a => cuisineArray.Add(a.SelectedCuisine));
                restaurant.CuisineNames = string.Join(", ", cuisineArray);
                var restaurantTagDao = await restaurantTag.Get(restaurantType, item.RestaurantId, DateTime.Now, DateTime.Now.ToString("HH:mm"), null);
                restaurant.SelectedTag = restaurantTagDao.Item1;
                restaurant.IsOfferAvailable = restaurant.SelectedTag == Enumeration.RestaurantTags.Yellow;
                restaurant.OfferName = string.IsNullOrEmpty(restaurantTagDao.Item2) ? "No offer available" : restaurantTagDao.Item2;
                restaurantList.Add(restaurant);
            }
            return restaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="cuisineIds"></param>
        /// <param name="areaCoverage"></param>
        /// <param name="offerType"></param>
        /// <param name="queryExpression"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <param name="restaurantType"></param>
        /// <param name="restaurantCategoryId"></param>
        /// <param name="OfferCategories"></param>
        /// <param name="localtyId"></param>
        /// <returns></returns>
        public async Task<List<ResuturantModelDAO>> Get(double latitude, double longitude, Enumeration.RestaurantTypes restaurantType, List<decimal> cuisineIds = default, int areaCoverage = 0, Enumeration.OfferType? offerType = null, string queryExpression = "", int currentPage = 0, int pageSize = 10, int? restaurantCategoryId = null, List<int> OfferCategories = default, List<decimal> localtyId = default)
        {
            var restaurantList = new List<ResuturantModelDAO>();
            var dal = DalFactory.Create<IRestaurantListDal>();
            var criteria = new ListCriteria(latitude, longitude, currentPage, pageSize, restaurantType, cuisineIds, queryExpression, offerType, areaCoverage, restaurantCategoryId, OfferCategories, localtyId);
            var dtos = await dal.FetchList(criteria);
            foreach (var item in dtos)
            {
                var ratingDAO = await branchRating.GetAvgRating(item.RestaurantId);
                var restaurant = new ResuturantModelDAO()
                {
                    AddressLine1 = item.Line1,
                    AddressLine2 = item.Line2,
                    AvgRating = ratingDAO.Rating > 3 ? ratingDAO.Rating : 0,
                    City = item.City,
                    Distance = item.Distance,
                    Id = item.RestaurantId,
                    Latitude = item.Latitude,
                    Longitude = item.Longitude,
                    Name = item.Name,
                    OfferName = item.OfferName,
                    State = item.State,
                    ZipCode = item.Zip,
                    RestaurantTag = item.RestaurantTag,
                    SEODescription = item.SEODescription,
                    SEOKeyword = item.SEOKeyword,
                    SEOTitle = item.SEOTitle,
                    Category = item.Category,
                    ActiveOffers = item.ActiveOffers,
                    CuisineNames = string.Empty,
                    ImageList = new List<DAO.Images.ImageDAO>(),
                    IsOfferAvailable = item.IsOfferAvailable,
                    SelectedTag = Enumeration.RestaurantTags.NoTag,
                    AmbienceRating = ratingDAO.AmbienceRating,
                    CostForTwo = item.CostForTwo,
                    Description = item.Description,
                    FoodRating = ratingDAO.FoodRating,
                    MusicRating = ratingDAO.MusicRating,
                    PriceRating = ratingDAO.PriceRating,
                    ServiceRating = ratingDAO.ServiceRating
                };
                var cuisineArray = new List<string>();
                var cuisines = await cuisineList.Get(item.RestaurantId, 0, 3, true);
                cuisines.ForEach(a => cuisineArray.Add(a.SelectedCuisine));
                restaurant.CuisineNames = string.Join(", ", cuisineArray);
                var restaurantTagDao = await restaurantListTag.Get(restaurantType, item.RestaurantId, DateTime.Now, DateTime.Now.ToString("HH:mm"), offerType);
                restaurant.SelectedTag = restaurantTagDao.Item1;
                restaurant.OfferName = string.IsNullOrEmpty(restaurantTagDao.Item2) ? "No offer available" : restaurantTagDao.Item2;
                restaurantList.Add(restaurant);
            }
            return restaurantList;
        }
    }
}
