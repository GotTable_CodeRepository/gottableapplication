﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Restaurants.InvoicedRestaurants;
using GotTable.DAO.Restaurants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    [Serializable]
    public sealed class InvoicedRestaurantList : IInvoicedRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesPersonAdminId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<InvoicedRestaurantDAO>> Get(decimal? salesPersonAdminId = null, int currentPage = 1, int pageSize = 10)
        {
            var restaurantList = new List<InvoicedRestaurantDAO>();
            var dal = DalFactory.Create<IRestaurantListDal>();
            var dtos = await dal.FetchList(salesPersonAdminId, currentPage, pageSize);
            foreach (var item in dtos)
            {
                restaurantList.Add(new InvoicedRestaurantDAO()
                {
                    AddressLine1 = item.Line1,
                    AddressLine2 = item.Line2,
                    City = item.City,
                    RestaurantId = item.RestaurantId,
                    RestaurantName = item.RestaurantName,
                    SalesPersonId = item.SalesPersonId,
                    State = item.State,
                    Zip = item.Zip,
                    Amount = item.Amount,
                    EndDate = item.EndDate,
                    IsActive = item.IsActive,
                    SalesPerson = item.SalesAdminName,
                    StartDate = item.StartDate,
                    Subscription = ((Enumeration.PrescriptionTypes)item.SubscriptionTypeId).ToString(),
                    Category = item.Category
                });
            }

            return restaurantList;
        }
    }
}
