﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Restaurants;
using GotTable.DAO.Restaurants;
using GotTable.Library.RestaurantConfigurations;
using GotTable.Library.RestaurantTables;
using GotTable.Library.RestaurantTimings;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class Restaurant : IRestaurant
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchConfiguration branchConfiguration;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTable branchTable;

        /// <summary>
        /// 
        /// </summary>
        private readonly IBranchTiming branchTiming;

        public Restaurant(IBranchConfiguration branchConfiguration, IBranchTiming branchTiming, IBranchTable branchTable)
        {
            this.branchConfiguration = branchConfiguration;
            this.branchTable = branchTable;
            this.branchTiming = branchTiming;
        }

        #region Factory methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<BranchDetailDAO> Create(decimal userId)
        {
            await Task.FromResult(1);
            return new BranchDetailDAO()
            {
                SalesPersonId = userId,
                BranchId = 0,
                ErrorMessage = string.Empty,
                StatusId = 1
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchDetailDAO"></param>
        /// <returns></returns>
        public async Task New(BranchDetailDAO branchDetailDAO)
        {
            if (branchDetailDAO.SalesPersonId == 0)
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid request, Please try after some time.!";
                return;
            }
            if (String.IsNullOrEmpty(branchDetailDAO.BranchName))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid restaurant name, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchDetailDAO.AddressLine1))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid address line 1, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchDetailDAO.City))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid city name, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchDetailDAO.State))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid state name, Please check.!";
                return;
            }
            if (branchDetailDAO.Zip == 0)
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid zip code, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(branchDetailDAO.Category))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid category, Please check.!";
                return;
            }
            if (await IsExist(branchDetailDAO.BranchName, branchDetailDAO.AddressLine1, branchDetailDAO.City))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "One restaurant with mentioned name already exists for the mentioned city, Please check.!";
                return;
            }

            var dto = new RestaurantDto()
            {
                CreatedDate = DateTime.Now,
                Name = branchDetailDAO.BranchName,
                IsActive = false,
                SalesAdminPersonId = branchDetailDAO.SalesPersonId,
                Latitude = branchDetailDAO.Latitude,
                Longitude = branchDetailDAO.Longitude,
                AddressLine1 = branchDetailDAO.AddressLine1,
                AddressLine2 = branchDetailDAO.AddressLine2,
                BranchId = branchDetailDAO.BranchId,
                City = branchDetailDAO.City,
                State = branchDetailDAO.State,
                AccountAdminPersonId = branchDetailDAO.AccountAdminPersonId,
                OpenforAccountAdmin = branchDetailDAO.Open4AccountAdmin,
                Zip = branchDetailDAO.Zip,
                TagLine = branchDetailDAO.TagLine,
                SEOTitle = branchDetailDAO.SEOTitle,
                SEODescription = branchDetailDAO.SEODescription,
                SEOKeyword = branchDetailDAO.SEOKeyword,
                Category = (Enumeration.RestaurantCategories)Enum.Parse(typeof(Enumeration.RestaurantCategories), branchDetailDAO.Category),
                LocalityId1 = branchDetailDAO.LocalityId1,
                LocalityId2 = branchDetailDAO.LocalityId2,
                LocalityId3 = branchDetailDAO.LocalityId3,
                LocalityId4 = branchDetailDAO.LocalityId4,
                CostForTwo = branchDetailDAO.CostForTwo,
                Description = branchDetailDAO.Description
            };

            var dal = DalFactory.Create<IRestaurantDal>();
            await dal.Insert(dto);

            if (dto.BranchId == 0)
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "There is some issue while processing your request, Please try after some time.!";
                return;
            }

            // Set default value for branch configuration.
            await branchConfiguration.Default(dto.BranchId);

            // Set default value for branch tables.
            await branchTable.Default(dto.BranchId);

            // Set default value for branch timings
            await branchTiming.Default(dto.BranchId);

            branchDetailDAO.BranchId = dto.BranchId;
            branchDetailDAO.StatusId = 1;
            branchDetailDAO.ErrorMessage = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchDetailDAO"></param>
        /// <returns></returns>
        public async Task Save(BranchDetailDAO branchDetailDAO)
        {
            if (branchDetailDAO.SalesPersonId == 0)
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid request, Please try after some time.!";
                return;
            }
            if (String.IsNullOrEmpty(branchDetailDAO.BranchName))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid restaurant name, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchDetailDAO.AddressLine1))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid address line 1, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchDetailDAO.City))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid city name, Please check.!";
                return;
            }
            if (String.IsNullOrEmpty(branchDetailDAO.State))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid state name, Please check.!";
                return;
            }
            if (branchDetailDAO.Zip == 0)
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid zip code, Please check.!";
                return;
            }
            if (branchDetailDAO.AccountAdminPersonId == 0)
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid account admin id, Please check.!";
                return;
            }
            if (string.IsNullOrEmpty(branchDetailDAO.Category))
            {
                branchDetailDAO.StatusId = 0;
                branchDetailDAO.ErrorMessage = "Invalid category, Please check.!";
                return;
            }

            var dto = new RestaurantDto()
            {
                BranchId = branchDetailDAO.BranchId,
                AddressLine1 = branchDetailDAO.AddressLine1,
                AddressLine2 = branchDetailDAO.AddressLine2,
                City = branchDetailDAO.City,
                IsActive = branchDetailDAO.IsActive,
                Latitude = branchDetailDAO.Latitude,
                Longitude = branchDetailDAO.Longitude,
                Name = branchDetailDAO.BranchName,
                State = branchDetailDAO.State,
                Zip = branchDetailDAO.Zip,
                SalesAdminPersonId = branchDetailDAO.SalesPersonId,
                AccountAdminPersonId = branchDetailDAO.AccountAdminPersonId,
                OpenforAccountAdmin = branchDetailDAO.Open4AccountAdmin,
                TagLine = branchDetailDAO.TagLine,
                SEOKeyword = branchDetailDAO.SEOKeyword,
                SEODescription = branchDetailDAO.SEODescription,
                SEOTitle = branchDetailDAO.SEOTitle,
                Category = (Enumeration.RestaurantCategories)Enum.Parse(typeof(Enumeration.RestaurantCategories), branchDetailDAO.Category),
                LocalityId1 = branchDetailDAO.LocalityId1,
                LocalityId2 = branchDetailDAO.LocalityId2,
                LocalityId3 = branchDetailDAO.LocalityId3,
                LocalityId4 = branchDetailDAO.LocalityId4,
                CostForTwo = branchDetailDAO.CostForTwo,
                Description = branchDetailDAO.Description
            };

            var dal = DalFactory.Create<IRestaurantDal>();
            await dal.Update(dto);

            branchDetailDAO.StatusId = 1;
            branchDetailDAO.ErrorMessage = string.Empty;
            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        public async Task<BranchDetailDAO> Get(decimal restaurantId)
        {
            var dal = DalFactory.Create<IRestaurantDal>();
            var dto = await dal.Fetch(restaurantId);
            var branchDetail = new BranchDetailDAO()
            {
                AddressLine1 = dto.AddressLine1,
                AddressLine2 = dto.AddressLine2,
                City = dto.City,
                BranchId = dto.BranchId,
                BranchName = dto.Name,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                State = dto.State,
                Zip = dto.Zip,
                IsActive = dto.IsActive ?? false,
                SalesPersonId = dto.SalesAdminPersonId ?? 0,
                AccountAdminPersonId = dto.AccountAdminPersonId ?? 0,
                Open4AccountAdmin = dto.OpenforAccountAdmin ?? false,
                TagLine = dto.TagLine,
                SEOKeyword = dto.SEOKeyword,
                SEODescription = dto.SEODescription,
                SEOTitle = dto.SEOTitle,
                Category = dto.Category.ToString(),
                LocalityId1 = dto.LocalityId1,
                LocalityId2 = dto.LocalityId2,
                LocalityId3 = dto.LocalityId3,
                LocalityId4 = dto.LocalityId4,
                CostForTwo = dto.CostForTwo,
                Description = dto.Description,
                LocalityName1 = dto.LocalityName1,
                LocalityName2 = dto.LocalityName2,
                LocalityName3 = dto.LocalityName3,
                LocalityName4 = dto.LocalityName4
            };
            return branchDetail;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchName"></param>
        /// <param name="addressLine1"></param>
        /// <param name="cityName"></param>
        /// <returns></returns>
        private async Task<bool> IsExist(string branchName, string addressLine1, string cityName)
        {
            var dal = DalFactory.Create<IRestaurantDal>();
            return await dal.IsExist(branchName, cityName, addressLine1);
        }

        #endregion
    }
}
