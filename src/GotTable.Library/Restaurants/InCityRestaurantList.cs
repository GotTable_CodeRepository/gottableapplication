﻿using GotTable.Dal;
using GotTable.Dal.Restaurants;
using GotTable.DAO.Restaurants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class InCityRestaurantList : IInCityRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public async Task<List<InCityRestaurantDAO>> Get(decimal? cityId)
        {
            var restaurantList = new List<InCityRestaurantDAO>();
            var dal = DalFactory.Create<IRestaurantDal>();
            var dtos = await dal.FetchList(cityId);
            foreach (var item in dtos)
            {
                var restaurant = new InCityRestaurantDAO()
                {
                    City = item.City,
                    Address = item.AddressLine1 + " " + item.AddressLine2,
                    BranchId = item.BranchId,
                    Name = item.Name
                };
                restaurantList.Add(restaurant);
            }
            return restaurantList;
        }
    }
}
