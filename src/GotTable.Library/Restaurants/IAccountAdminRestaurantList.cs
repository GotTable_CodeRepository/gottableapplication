﻿using GotTable.DAO.Restaurants;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    public interface IAccountAdminRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountAdminPersonId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<List<RestaurantByAccountAdminDAO>> Get(decimal accountAdminPersonId, int currentPage = 1, int pageSize = 10);
    }
}