﻿using GotTable.Dal;
using GotTable.Dal.Restaurants;
using GotTable.DAO.RestaurantSettings;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    [Serializable]
    public sealed class RestaurantSetting : IRestaurantSetting
    {
        public async Task<RestaurantSettingDAO> Get(decimal restaurantId)
        {
            var dal = DalFactory.Create<IRestaurantDal>();
            var dto = await dal.Fetch(restaurantId);

            return new RestaurantSettingDAO()
            {
                AccountAdminPersonId = dto.AccountAdminPersonId ?? 0,
                Contacts = dto.ContactCount,
                Cuisines = dto.CuisineCount,
                BranchId = restaurantId,
                Tables = dto.TableCount,
                Timings = dto.TimingCount,
                ApplicationStatus = dto.IsActive ?? false,
                OpenForAccountAdmin = dto.OpenforAccountAdmin ?? false
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantSettingDAO"></param>
        /// <returns></returns>
        public async Task Save(RestaurantSettingDAO restaurantSettingDAO)
        {
            var dal = DalFactory.Create<IRestaurantDal>();
            var dto = await dal.Fetch(restaurantSettingDAO.BranchId);

            dto.AccountAdminPersonId = restaurantSettingDAO.AccountAdminPersonId;
            dto.OpenforAccountAdmin = restaurantSettingDAO.OpenForAccountAdmin;
            dto.IsActive = restaurantSettingDAO.ApplicationStatus;

            await dal.Update(dto);
        }
    }
}
