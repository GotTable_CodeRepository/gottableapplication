﻿using GotTable.DAO.Restaurants;
using System.Collections.Generic;
using GotTable.Dal;
using GotTable.Dal.Restaurants.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants.Filters
{
    public sealed class FilterList : IFilterList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<FilterDAO>> Get()
        {
            var dal = DalFactory.Create<IFilterDal>();
            var dtos = await dal.FetchList();
            return await MapObjectToModel(dtos);
        }

        #region Helpers method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        private async Task<List<FilterDAO>> MapObjectToModel(List<FilterDto> dtos)
        {
            await Task.FromResult(1);
            return dtos.Select(x => new FilterDAO()
            {
                IconUrl = x.IconUrl,
                Name = x.Name,
                Index = x.Index,
                Key = x.Key,
                List = x.List.Select(z => new Common.EnumDtoV2<decimal>()
                {
                    Id = z.Id,
                    Name = z.Name
                }).ToList()
            }).ToList();
        }

        #endregion
    }
}
