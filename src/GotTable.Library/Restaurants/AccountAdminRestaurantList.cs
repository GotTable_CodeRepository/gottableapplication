﻿using GotTable.Dal;
using GotTable.Dal.Restaurants.AccountAdmin;
using GotTable.DAO.Restaurants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    [Serializable]
    public sealed class AccountAdminRestaurantList : IAccountAdminRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountAdminPersonId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<RestaurantByAccountAdminDAO>> Get(decimal accountAdminPersonId, int currentPage = 1, int pageSize = 10)
        {
            var restaurantList = new List<RestaurantByAccountAdminDAO>();
            var dal = DalFactory.Create<IRestaurantListDal>();
            var dtos = await dal.FetchList(accountAdminPersonId, currentPage, pageSize);

            dtos.ForEach(item =>
            {
                restaurantList.Add(new RestaurantByAccountAdminDAO()
                {
                    AddressLine1 = item.Line1,
                    AddressLine2 = item.Line2,
                    City = item.City,
                    IsActive = item.IsActive,
                    Open4AccountAdmin = item.OpenforAccountAdmin,
                    RestaurantId = item.RestaurantId,
                    RestaurantName = item.RestaurantName,
                    SalesPersonId = item.SalesPersonId ?? 0,
                    State = item.State,
                    Zip = item.Zip,
                    Category = item.Category,
                    SalesAdminEmailAddress = item.SalesAdminEmailAddress,
                    SalesAdminName = item.SalesAdminName
                });
            });

            return restaurantList;
        }
    }
}
