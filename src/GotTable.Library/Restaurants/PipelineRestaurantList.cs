﻿using GotTable.Dal;
using GotTable.Dal.Restaurants.PipelineRestaurants;
using GotTable.DAO.Restaurants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    [Serializable]
    public sealed class PipelineRestaurantList : IPipelineRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="salesPersonAdminId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<PipelineRestaurantDAO>> Get(decimal salesPersonAdminId, int currentPage = 1, int pageSize = 10)
        {
            var restaurantList = new List<PipelineRestaurantDAO>();
            var dal = DalFactory.Create<IRestaurantListDal>();
            var dtos = await dal.FetchList(salesPersonAdminId, currentPage, pageSize);

            foreach (var item in dtos)
            {
                restaurantList.Add(new PipelineRestaurantDAO()
                {
                    AddressLine1 = item.Line1,
                    AddressLine2 = item.Line2,
                    City = item.City,
                    RestaurantId = item.RestaurantId,
                    RestaurantName = item.RestaurantName,
                    SalesPersonId = item.SalesAdminPersonId,
                    State = item.State,
                    Zip = item.Zip,
                    AccountAdminEmailAddress = item.AccountAdminEmailAddress,
                    AccountAdminId = item.AccountAdminId,
                    AccountAdminName = item.AccountAdminName,
                    OpenForAccountAdmin = item.OpenForAccountAdmin ?? false,
                    SalesPerson = item.SalesAdminName,
                    SalesPersonEmailAddress = item.SalesAdminEmailAddress,
                    Category = item.Category,
                    ContactCount = item.ContactCount,
                    CuisineCount = item.CuisineCount,
                    MenuCount = item.MenuCount,
                    TablesCount = item.TablesCount,
                    TimingCount = item.TimingCount
                });
            }
            return restaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<PipelineRestaurantDAO>> Get(int currentPage = 1, int pageSize = 10)
        {
            var restaurantList = new List<PipelineRestaurantDAO>();
            var dal = DalFactory.Create<IRestaurantListDal>();
            var dtos = await dal.FetchList(currentPage, pageSize);

            foreach (var item in dtos)
            {
                restaurantList.Add(new PipelineRestaurantDAO()
                {
                    AddressLine1 = item.Line1,
                    AddressLine2 = item.Line2,
                    City = item.City,
                    RestaurantId = item.RestaurantId,
                    RestaurantName = item.RestaurantName,
                    SalesPersonId = item.SalesAdminPersonId,
                    SalesPerson = item.SalesAdminName,
                    SalesPersonEmailAddress = item.SalesAdminEmailAddress,
                    State = item.State,
                    Zip = item.Zip,
                    AccountAdminEmailAddress = item.AccountAdminEmailAddress,
                    AccountAdminId = item.AccountAdminId,
                    AccountAdminName = item.AccountAdminName,
                    OpenForAccountAdmin = item.OpenForAccountAdmin ?? false,
                    Category = item.Category,
                    ContactCount = item.ContactCount,
                    CuisineCount = item.CuisineCount,
                    MenuCount = item.MenuCount,
                    TimingCount = item.TimingCount,
                    TablesCount = item.TablesCount
                });
            }

            return restaurantList;
        }
    }
}
