﻿using GotTable.Dal;
using GotTable.Dal.Restaurants.PreferredRestaurants;
using GotTable.DAO.Restaurants.PreferredRestaurants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    [Serializable]
    public sealed class PreferredRestaurantList : IPreferredRestaurantList
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="PreferredRestaurantInfoDAO"></typeparam>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <param name="totalCount"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<PreferredRestaurantInfoDAO>> Get(double latitude, double longitude, int currentPage = 1, int pageSize = 10)
        {
            var restaurantList = new List<PreferredRestaurantInfoDAO>();
            var dal = DalFactory.Create<IPreferredRestaurantDal>();
            var dtos = await dal.FetchList(latitude, longitude, currentPage, pageSize);

            foreach (var item in dtos)
            {
                restaurantList.Add(new PreferredRestaurantInfoDAO()
                {
                    Address = item.Address,
                    City = item.City,
                    RestaurantId = item.RestaurantId,
                    Name = item.Name,
                    State = item.State,
                    Distance = item.Distance,
                    Latitude = item.Latitude,
                    Longitude = item.Longitude,
                    Zipcode = item.Zipcode,
                    SEODescription = item.SEODescription,
                    SEOKeyword = item.SEOKeyword,
                    SEOTitle = item.SEOTitle,
                    TagLine = item.TagLine,
                    Rating = item.Rating
                });
            }

            return restaurantList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<PreferredRestaurantDAO>> Get(decimal? cityId, int currentPage = 1, int pageSize = 12)
        {
            var restaurantList = new List<PreferredRestaurantDAO>();
            var dal = DalFactory.Create<IPreferredRestaurantDal>();
            var dtos = await dal.FetchList(cityId, currentPage, pageSize);

            dtos.ForEach(item =>
            {
                restaurantList.Add(new PreferredRestaurantDAO()
                {
                    Address = item.Address,
                    CityName = item.CityName,
                    RestaurantId = item.RestaurantId,
                    Name = item.Name,
                    CityId = item.CityId
                });
            });

            return restaurantList;
        }
    }
}
