﻿using GotTable.DAO.Restaurants;
using System.Threading.Tasks;

namespace GotTable.Library.Restaurants
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRestaurant
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<BranchDetailDAO> Create(decimal userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchDetailDAO"></param>
        Task New(BranchDetailDAO branchDetailDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branchDetailDAO"></param>
        Task Save(BranchDetailDAO branchDetailDAO);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        Task<BranchDetailDAO> Get(decimal restaurantId);
    }
}