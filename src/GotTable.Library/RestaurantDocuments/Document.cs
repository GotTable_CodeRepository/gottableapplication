﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantDocuments;
using GotTable.DAO.RestaurantDocuments;
using GotTable.Library.Utility;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace GotTable.Library.RestaurantDocuments
{
    [Serializable]
    public sealed class Document : IDocument
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentDAO"></param>
        public async Task Post(DocumentDAO documentDAO)
        {
            //if (documentDAO.BranchId == 0)
            //{
            //    documentDAO.StatusId = 0;
            //    documentDAO.ErrorMessage = "Invalid request, please check.";
            //}
            //if (documentDAO.Content == null)
            //{
            //    documentDAO.StatusId = 0;
            //    documentDAO.ErrorMessage = "Invalid request, please check.";
            //}
            //if (documentDAO.CategoryId == 0)
            //{
            //    documentDAO.StatusId = 0;
            //    documentDAO.ErrorMessage = "Invalid request, please check.";
            //}

            //var baseDirectory = HttpContext.Current.Server.MapPath("~/Uploads/Restaurants/");
            //var restaurantDirectory = baseDirectory + documentDAO.BranchId;
            //bool directoryStatus = Directory.Exists(restaurantDirectory);
            //if (!directoryStatus)
            //{
            //    Directory.CreateDirectory(restaurantDirectory);
            //}
            //var fileType = documentDAO.Content.ContentType;
            //var validImageTypes = new string[] { "image/gif", "image/jpeg", "image/pjpeg", "image/png" };
            //if (!validImageTypes.Contains(fileType))
            //{
            //    documentDAO.StatusId = 0;
            //    documentDAO.ErrorMessage = "Invalid file type, Please check.";
            //    return;
            //}
            //var imageExtension = ImageExtension.GetExtension(documentDAO.Content.ContentType);

            //var dto = new DocumentDto()
            //{
            //    Active = true,
            //    BranchId = documentDAO.BranchId,
            //    CategoryId = documentDAO.CategoryId,
            //    DocumentId = 0,
            //    Name = documentDAO.Content.FileName,
            //    Extension = imageExtension
            //};

            //var dal = DalFactory.Create<IDocumentDal>();
            //await dal.Insert(dto);
            //if (dto.DocumentId != 0)
            //{
            //    documentDAO.Content.SaveAs(restaurantDirectory + "/" + dto.DocumentId.ToString() + imageExtension);
            //    documentDAO.StatusId = 1;
            //}
            //else
            //{
            //    documentDAO.StatusId = 0;
            //    documentDAO.ErrorMessage = "There is some issue while working on the request.";
            //}
            throw new Exception();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileModelDAO"></param>
        /// <returns></returns>
        public async Task Save(FileModelDAO fileModelDAO)
        {
            //await Task.FromResult(1);
            //if (fileModelDAO.RestaurantId != 0 && fileModelDAO.LogoName != null && fileModelDAO.Content != null)
            //{
            //    var baseDirectory = HttpContext.Current.Server.MapPath("~/Uploads/Restaurants/");
            //    var restaurantDirectory = baseDirectory + fileModelDAO.RestaurantId;
            //    bool directoryStatus = Directory.Exists(restaurantDirectory);
            //    if (!directoryStatus)
            //    {
            //        Directory.CreateDirectory(restaurantDirectory);
            //    }
            //    var fileType = fileModelDAO.Content.ContentType;
            //    var validImageTypes = new string[] { "image/gif", "image/jpeg", "image/pjpeg", "image/png" };
            //    if (!validImageTypes.Contains(fileType))
            //    {
            //        fileModelDAO.StatusId = 0;
            //        fileModelDAO.ErrorMessage = "Invalid file type, Please check.";
            //        return;
            //    }

            //    var imageExtension = ImageExtension.GetExtension(fileModelDAO.Content.ContentType);
            //    if (fileModelDAO.LogoName.Contains(Enumeration.ImageType.Front.ToString()))
            //    {
            //        var directoryInfo = new DirectoryInfo(restaurantDirectory);
            //        foreach (var fileInfo in directoryInfo.GetFiles().Where(x => x.Name.Contains(Enumeration.ImageType.Front.ToString())))
            //        {
            //            fileInfo.Delete();
            //        }
            //        fileModelDAO.Content.SaveAs(restaurantDirectory + "/" + string.Format(DocumentResources.FrontImageName, Guid.NewGuid().ToString().Substring(1, 10), imageExtension));
            //    }
            //    if (fileModelDAO.LogoName.Contains(Enumeration.ImageType.Inside.ToString()))
            //    {
            //        var directoryInfo = new DirectoryInfo(restaurantDirectory);
            //        foreach (var fileInfo in directoryInfo.GetFiles().Where(x => x.Name.Contains(Enumeration.ImageType.Inside.ToString())))
            //        {
            //            fileInfo.Delete();
            //        }
            //        fileModelDAO.Content.SaveAs(restaurantDirectory + "/" + string.Format(DocumentResources.InsideImageName, Guid.NewGuid().ToString().Substring(1, 10), imageExtension));
            //    }
            //    if (fileModelDAO.LogoName.Contains(Enumeration.ImageType.Logo.ToString()))
            //    {
            //        var directoryInfo = new DirectoryInfo(restaurantDirectory);
            //        foreach (var fileInfo in directoryInfo.GetFiles().Where(x => x.Name.Contains(Enumeration.ImageType.Logo.ToString())))
            //        {
            //            fileInfo.Delete();
            //        }
            //        fileModelDAO.Content.SaveAs(restaurantDirectory + "/" + string.Format(DocumentResources.LogoImageName, Guid.NewGuid().ToString().Substring(1, 10), imageExtension));
            //    }
            //}
            throw new Exception();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="imageType"></param>
        /// <returns></returns>
        public async Task<RestaurantImageDAO> Get(decimal restaurantId, Enumeration.ImageType? imageType = null)
        {
            //await Task.FromResult(1);
            //var restaurantImages = new RestaurantImageDAO(restaurantId);
            //var baseDirectory = HttpContext.Current.Server.MapPath("~/Uploads/Restaurants/");
            //var restaurantDirectory = baseDirectory + restaurantId;
            //if (Directory.Exists(restaurantDirectory))
            //{
            //    var baseUrl = ImageExtension.GetBaseUrl;
            //    var directoryInfo = new DirectoryInfo(restaurantDirectory);
            //    foreach (var fileInfo in directoryInfo.GetFiles())
            //    {
            //        if (fileInfo.Name.Contains(Enumeration.ImageType.Front.ToString()))
            //        {
            //            restaurantImages.FrontImage = new FileModelDAO(restaurantId, Enumeration.ImageType.Front.ToString());
            //            restaurantImages.FrontImage.FilePath = baseUrl + "/Uploads/Restaurants/" + restaurantId + "/" + fileInfo.Name;
            //        }
            //        if (fileInfo.Name.Contains(Enumeration.ImageType.Inside.ToString()))
            //        {
            //            restaurantImages.BackImage = new FileModelDAO(restaurantId, Enumeration.ImageType.Inside.ToString());
            //            restaurantImages.BackImage.FilePath = baseUrl + "/Uploads/Restaurants/" + restaurantId + "/" + fileInfo.Name;
            //        }
            //        if (fileInfo.Name.Contains(Enumeration.ImageType.Logo.ToString()))
            //        {
            //            restaurantImages.LogoImage = new FileModelDAO(restaurantId, Enumeration.ImageType.Logo.ToString());
            //            restaurantImages.LogoImage.FilePath = baseUrl + "/Uploads/Restaurants/" + restaurantId + "/" + fileInfo.Name;
            //        }
            //    }
            //}
            //return restaurantImages;
            throw new Exception();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        public async Task Delete(int documentId)
        {
            if (documentId == 0) return;
            var dal = DalFactory.Create<IDocumentDal>();
            await dal.Delete(documentId);
        }
    }
}
