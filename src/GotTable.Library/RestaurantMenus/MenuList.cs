﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantMenus;
using GotTable.DAO.Images;
using GotTable.DAO.RestaurantMenus;
using GotTable.Library.RestaurantDocuments;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace GotTable.Library.RestaurantMenus
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class MenuList : IMenuList
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IDocument document;

        public MenuList(IDocument document)
        {
            this.document = document;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BranchMenuDAO>> Get(decimal restaurantId, int currentPage = 1, int pageSize = 10)
        {
            var menuList = new List<BranchMenuDAO>();
            var imageDAO = await document.Get(restaurantId, null);
            var dal = DalFactory.Create<IMenuDal>();
            foreach (var item in await dal.FetchList(restaurantId, currentPage, pageSize))
            {
                var menuItem = new BranchMenuDAO()
                {
                    BranchId = item.BranchId,
                    Cost = item.Cost,
                    SelectedCuisine = item.CuisineName,
                    Id = item.Id,
                    IsActive = (bool)item.IsActive,
                    IsDeliveryAvailable = item.IsDeliveryAvailable,
                    IsDineAvailable = item.IsDineAvailable,
                    SelectedCategory = item.CategoryName,
                    IsTakeAwayAvailable = item.IsTakeAwayAvailable,
                    Name = item.Name,
                    SelectedMenuCategory = item.MenuTypeName,
                    Description = item.Description,
                    MenuLogo = new ImageDAO()
                    {
                        Name = imageDAO.LogoImage.LogoName,
                        Type = Path.GetExtension(imageDAO.LogoImage.FilePath),
                        Url = imageDAO.LogoImage.FilePath
                    }
                };
                menuList.Add(menuItem);
            }
            return menuList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="restaurantType"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BranchMenuDAO>> Get(decimal restaurantId, Enumeration.RestaurantTypes? restaurantType, int currentPage = 1, int pageSize = 10)
        {
            var menuList = new List<BranchMenuDAO>();
            var dal = DalFactory.Create<IMenuDal>();
            var imageDAO = await document.Get(restaurantId, null);
            foreach (var item in await dal.FetchList(restaurantId, restaurantType, currentPage, pageSize))
            {
                var menuItem = new BranchMenuDAO()
                {
                    BranchId = item.BranchId,
                    Cost = item.Cost,
                    SelectedCuisine = item.CuisineName,
                    Id = item.Id,
                    IsActive = (bool)item.IsActive,
                    IsDeliveryAvailable = item.IsDeliveryAvailable,
                    IsDineAvailable = item.IsDineAvailable,
                    SelectedCategory = item.CategoryName,
                    IsTakeAwayAvailable = item.IsTakeAwayAvailable,
                    Name = item.Name,
                    SelectedMenuCategory = item.MenuTypeName,
                    Description = item.Description,
                    MenuLogo = new ImageDAO()
                    {
                        Name = imageDAO.LogoImage.LogoName,
                        Type = Path.GetExtension(imageDAO.LogoImage.FilePath),
                        Url = imageDAO.LogoImage.FilePath
                    }
                };
                menuList.Add(menuItem);
            }
            return menuList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <param name="typeId"></param>
        /// <param name="type"></param>
        /// <param name="currentPage"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<List<BranchMenuDAO>> Get(decimal restaurantId, decimal typeId, Enumeration.RestaurantTypes? type, int currentPage = 1, int pageSize = 10)
        {
            var menuList = new List<BranchMenuDAO>();
            var imageDAO = await document.Get(restaurantId, null);
            var dal = DalFactory.Create<IMenuDal>();
            foreach (var item in await dal.FetchList(restaurantId, type, typeId, currentPage, pageSize))
            {
                var menuItem = new BranchMenuDAO()
                {
                    BranchId = item.BranchId,
                    Cost = item.Cost,
                    SelectedCuisine = item.CuisineName,
                    Id = item.Id,
                    IsActive = (bool)item.IsActive,
                    IsDeliveryAvailable = item.IsDeliveryAvailable,
                    IsDineAvailable = item.IsDineAvailable,
                    SelectedCategory = item.CategoryName,
                    IsTakeAwayAvailable = item.IsTakeAwayAvailable,
                    Name = item.Name,
                    SelectedMenuCategory = item.MenuTypeName,
                    Description = item.Description,
                    MenuLogo = new ImageDAO()
                    {
                        Name = imageDAO.LogoImage.LogoName,
                        Type = Path.GetExtension(imageDAO.LogoImage.FilePath),
                        Url = imageDAO.LogoImage.FilePath
                    }
                };
                menuList.Add(menuItem);
            }
            return menuList;
        }
    }
}
