﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Restaurants;
using GotTable.Library.ApplicationSettings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Email
{
    [Serializable]
    public sealed class RestaurantCredential : IEmailTransmission<RestaurantCredential, decimal>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Execute(decimal restaurantId)
        {
            var dal = DalFactory.Create<IRestaurantDal>();
            var dto = await dal.FetchInfo(restaurantId: restaurantId, null);
            if (dto != null)
            {
                var emailAttributes = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("xxxUserNamexxx", dto.Name),
                    new KeyValuePair<string, string>("xxxEmailAddressxxx", dto.RestaurantAdminEmailAddress),
                    new KeyValuePair<string, string>("xxxPasswordxxx", dto.RestaurantAdminPassword),
                    new KeyValuePair<string, string>("xxxURLxxx", AppSettingKeys.AdminApplicationBaseUrl)
                };
                await EmailTypes.Email.Invoke(Enumeration.EmailAction.RestaurantCredential, Enumeration.CredentialType.Info, emailAttributes, new List<string>() { dto.RestaurantAdminEmailAddress });
            }
        }
    }
}
