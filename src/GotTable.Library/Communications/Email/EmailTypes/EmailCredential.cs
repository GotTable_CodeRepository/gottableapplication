﻿using GotTable.Dal;
using GotTable.Dal.Communications.Email.Credentials;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Email.EmailTypes
{
    /// <summary>
    /// EmailCredential
    /// </summary>
    [Serializable]
    internal sealed class EmailCredential
    {
        private EmailCredential()
        {

        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="credentialTypeId"></param>
        /// <returns></returns>
        public async static Task<CredentialDto> Get(int credentialTypeId)
        {
            var dal = DalFactory.Create<ICredentialDal>();
            return await dal.Fetch(credentialTypeId);
        }
    }
}
