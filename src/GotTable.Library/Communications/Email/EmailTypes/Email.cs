﻿using GotTable.Common.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Email.EmailTypes
{
    [Serializable]
    internal sealed class Email
    {
        /// <summary>
        /// Invoke
        /// </summary>
        /// <param name="emailAction"></param>
        /// <param name="credentialType"></param>
        /// <param name="emailAttributes"></param>
        /// <param name="emailAddresses"></param>
        /// <returns></returns>
        public async static Task Invoke(Enumeration.EmailAction emailAction, Enumeration.CredentialType credentialType, List<KeyValuePair<string, string>> emailAttributes, List<string> emailAddresses)
        {
            var contentDto = await EmailContent.Get((int)emailAction);
            var credentialDto = await EmailCredential.Get((int)credentialType);
            var configurationDto = await SMTPConfiguration.Get(1);
            if (contentDto != null && credentialDto != null && configurationDto != null && emailAttributes.Any())
            {
                var emailDto = new EmailDto()
                {
                    ToAddress = emailAddresses.First(),
                    CCAddresses = emailAddresses.Skip(1).ToList(),
                    Subject = GetAttributes(emailAttributes, contentDto.EmailSubject),
                    FromAddress = credentialDto.Username,
                    HostName = configurationDto.HostName,
                    IsSSL = configurationDto.IsSSL,
                    PortNumber = configurationDto.PortNumber,
                    ReplyAddress = credentialDto.ReplyAddress,
                    Body = GetAttributes(emailAttributes, contentDto.EmailBody),
                    DisplayName = credentialDto.DisplayName,
                    Password = credentialDto.Password
                };
                Send(emailDto);
            }
        }

        #region Helper method
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailDto"></param>
        private static void Send(EmailDto emailDto)
        {
            var ccAddresses = new MailAddressCollection();
            emailDto.CCAddresses.ForEach(item => ccAddresses.Add(item));

            using (var smtpMail = new SmtpClient(emailDto.HostName, emailDto.PortNumber))
            {
                var message = new MailMessage(emailDto.FromAddress, emailDto.ToAddress, emailDto.Subject, emailDto.Body)
                {
                    From = new MailAddress(emailDto.FromAddress, emailDto.DisplayName)
                };
                emailDto.CCAddresses.ForEach(item => message.CC.Add(item));
                if (emailDto.ReplyAddress != "")
                {
                    message.ReplyToList.Add(new MailAddress(emailDto.ReplyAddress));
                }
                message.Priority = MailPriority.Normal;
                message.IsBodyHtml = true;
                smtpMail.UseDefaultCredentials = false;
                smtpMail.EnableSsl = emailDto.IsSSL;
                smtpMail.Credentials = new NetworkCredential(emailDto.FromAddress, emailDto.Password);
                smtpMail.Send(message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAttributes"></param>
        /// <param name="emailLabelDto"></param>
        /// <returns></returns>
        private static string GetAttributes(List<KeyValuePair<string, string>> emailAttributes, string emailLabelDto)
        {
            foreach (KeyValuePair<string, string> item in emailAttributes)
            {
                emailLabelDto = emailLabelDto.Replace(item.Key, item.Value);
            }
            return emailLabelDto;
        }

        #endregion
    }
}
