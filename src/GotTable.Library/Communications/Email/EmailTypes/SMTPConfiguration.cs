﻿using GotTable.Dal;
using GotTable.Dal.Communications.Email.Configurations;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Email.EmailTypes
{
    /// <summary>
    /// SMTPConfiguration
    /// </summary>
    [Serializable]
    internal sealed class SMTPConfiguration
    {
        private SMTPConfiguration()
        {

        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="configurationId"></param>
        /// <returns></returns>
        public async static Task<ConfigurationDto> Get(int configurationId)
        {
            var dal = DalFactory.Create<IConfigurationDal>();
            return await dal.Fetch(configurationId);
        }
    }
}
