﻿using GotTable.Dal;
using GotTable.Dal.Communications.Email.Content;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Email.EmailTypes
{
    [Serializable]
    internal class EmailContent
    {
        private EmailContent()
        {

        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="contentTypeId"></param>
        /// <returns></returns>
        public async static Task<ContentDto> Get(int contentTypeId)
        {
            var dal = DalFactory.Create<IContentDal>();
            return await dal.Fetch(contentTypeId);
        }
    }
}
