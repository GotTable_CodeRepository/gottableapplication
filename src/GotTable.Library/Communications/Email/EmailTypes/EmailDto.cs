﻿using System;
using System.Collections.Generic;

namespace GotTable.Library.Communications.Email.EmailTypes
{
    [Serializable]
    internal class EmailDto
    {
        public string HostName { get; set; }

        public int PortNumber { get; set; }

        public bool IsSSL { get; set; }

        public string ToAddress { get; set; }

        public List<string> CCAddresses { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string ReplyAddress { get; set; }

        public string FromAddress { get; set; }

        public string DisplayName { get; set; }

        public string Password { get; set; }
    }
}
