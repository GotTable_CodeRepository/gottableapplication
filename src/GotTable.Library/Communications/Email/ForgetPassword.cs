﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Users;
using GotTable.DAO.Communications.Email;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Email
{
    [Serializable]
    public sealed class ForgetPassword : IEmailTransmission<ForgetPassword, ForgetPasswordDAO>
    {
        /// <summary>
        /// ForgetPassword
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Execute(ForgetPasswordDAO dao)
        {
            var dal = DalFactory.Create<IUserDal>();
            var dto = await dal.Fetch(dao.EmailAddress);
            if (dto != null)
            {
                dao.ErrorMessage = string.Empty;
                if (dto.TypeId == (int)Enumeration.UserType.EndUser || dto.TypeId == (int)Enumeration.UserType.Guest)
                {
                    var emailAttributes = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("xxxUserNamexxx", dto.FirstName + " " + dto.LastName),
                        new KeyValuePair<string, string>("xxxEmailAddressxxx", dto.EmailAddress),
                        new KeyValuePair<string, string>("xxxPasswordxxx", dto.Password),
                        new KeyValuePair<string, string>("xxxURLxxx", string.Empty)
                    };
                    await EmailTypes.Email.Invoke(Enumeration.EmailAction.ResendPassword, Enumeration.CredentialType.Info, emailAttributes, new List<string>() { dao.EmailAddress });
                    dao.ConfirmationMessage = "One mail with credentials detail has been sent to provided email address, Please check.!";
                    dao.StatusId = 1;
                    dao.UserId = dto.UserId;
                }
                else
                {
                    dao.StatusId = 0;
                    dao.UserId = 0;
                    dao.ErrorMessage = "There is some issue while processing your request, Please try again later.!";
                }
            }
            else
            {
                dao.StatusId = 0;
                dao.UserId = 0;
                dao.ErrorMessage = "This email address does not exists in our application, Please check.!";
            }
        }
    }
}
