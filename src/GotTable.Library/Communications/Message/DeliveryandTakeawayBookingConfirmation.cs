﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Communications.Message.Content;
using GotTable.Dal.Communications.Message.TwoFactor;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Communications.Message.Type2Factors;
using GotTable.Library.RestaurantContacts;
using GotTable.Library.Restaurants;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Message
{
    [Serializable]
    public sealed class DeliveryandTakeawayBookingConfirmation : IMessageTransmission<DeliveryandTakeawayBookingConfirmation, string>
    {
        /// <summary>
        /// IRestaurant
        /// </summary>
        private readonly IRestaurant restaurant;

        /// <summary>
        /// IContactList
        /// </summary>
        private readonly IContactList contactList;

        public DeliveryandTakeawayBookingConfirmation(IRestaurant restaurant, IContactList contactList)
        {
            this.contactList = contactList;
            this.restaurant = restaurant;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingType"></param>
        /// <param name="bookingDateTime"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="restaurantId"></param>
        /// <param name="selectedTable"></param>
        /// <param name="deliveryAddress"></param>
        /// <param name="userId"></param>
        /// <param name="bookingId"></param>
        /// <param name="rewardStatus"></param>
        /// <returns></returns>
        public async Task<string> Execute(Enumeration.BookingType bookingType, DateTime bookingDateTime, decimal phoneNumber, decimal restaurantId, string selectedTable, string deliveryAddress, decimal userId, decimal bookingId, bool rewardStatus = false)
        {
            string returnMessage = string.Empty;
            string apiURL = AppSettingKeys.TwoFactorAPIURl;
            string authKey = AppSettingKeys.Two2FactorApiAuthKey;
            _2FactorRequestDto factorRequestDto;
            TwoFactorDto messageTemplate;
            TwoFactorLogDto twoFactorTransactionLogDto;
            var restaurantDetail = await restaurant.Get(restaurantId);
            var restaurantName = restaurantDetail.BranchName + ", " + restaurantDetail.City;
            if (bookingType == Enumeration.BookingType.Delivery)
            {
                var dal = DalFactory.Create<ITwoFactorDal>();
                var messageContent = string.Empty;
                var contactNumber = string.Empty;
                var contactListDao = await contactList.Get(restaurantId, 0, 10);
                if (contactListDao != null && contactListDao.Any())
                {
                    messageTemplate = await dal.Fetch((int)Enumeration.TransmissionContent.NewDeliveryConfirmation2Restaurant);
                    foreach (var contactItem in contactListDao)
                    {
                        if (contactNumber == string.Empty && Enumeration.ContactType.FloorManager == contactItem.SelectedType && contactItem.IsActive)
                        {
                            contactNumber = contactItem.PhoneNumber.ToString();
                        }
                        if (contactNumber == string.Empty && Enumeration.ContactType.RestaurantStaff == contactItem.SelectedType && contactItem.IsActive)
                        {
                            contactNumber = contactItem.PhoneNumber.ToString();
                        }
                        if (contactNumber == string.Empty && Enumeration.ContactType.GotTableRepersentative == contactItem.SelectedType && contactItem.IsActive)
                        {
                            contactNumber = contactItem.PhoneNumber.ToString();
                        }
                        if (contactItem.IsPhoneAlert && contactItem.IsActive)
                        {
                            var staffName = contactItem.Name;
                            if (contactItem.PhoneNumber.ToString().Length == 10)
                            {
                                factorRequestDto = new _2FactorRequestDto()
                                {
                                    BaseUrl = apiURL,
                                    ServiceUrl = string.Format(messageTemplate.QueryString, authKey, contactItem.PhoneNumber, messageTemplate.SenderId, messageTemplate.Name, staffName),
                                    TemplateName = messageTemplate.Name
                                };
                                twoFactorTransactionLogDto = new TwoFactorLogDto()
                                {
                                    ExternalId = contactItem.Id,
                                    LogId = 0,
                                    PhoneNumber = contactItem.PhoneNumber,
                                    TargetUrlWithParameters = factorRequestDto.ServiceUrl,
                                    TemplateId = messageTemplate.TemplateId,
                                    CreatedDate = DateTime.Now
                                };
                                await dal.Insert(twoFactorTransactionLogDto);
                                factorRequestDto.LogId = twoFactorTransactionLogDto.LogId;
                                _2Factor.Send(factorRequestDto);
                            }
                        }
                    }
                }
                messageTemplate = await dal.Fetch((int)Enumeration.TransmissionContent.NewDeliveryConfirmation2User);
                var formatedBookingDateTime = bookingDateTime.ToString("dd-MMM-yyyy hh:mm tt");
                factorRequestDto = new _2FactorRequestDto()
                {
                    BaseUrl = apiURL,
                    ServiceUrl = string.Format(messageTemplate.QueryString, authKey, phoneNumber, messageTemplate.SenderId, messageTemplate.Name, formatedBookingDateTime, restaurantName, deliveryAddress, contactNumber),
                    TemplateName = messageTemplate.Name,
                };
                twoFactorTransactionLogDto = new TwoFactorLogDto()
                {
                    ExternalId = 0,
                    LogId = 0,
                    PhoneNumber = phoneNumber,
                    TargetUrlWithParameters = factorRequestDto.ServiceUrl,
                    TemplateId = messageTemplate.TemplateId,
                    CreatedDate = DateTime.Now
                };
                await dal.Insert(twoFactorTransactionLogDto);
                factorRequestDto.LogId = twoFactorTransactionLogDto.LogId;
                _2Factor.Send(factorRequestDto);
                var contentDal = DalFactory.Create<IContentDal>();
                var content = await contentDal.Fetch((int)Enumeration.TransmissionContent.NewDeliveryConfirmation2User);
                returnMessage = string.Format(content.Message, restaurantName, formatedBookingDateTime, deliveryAddress, contactNumber);
            }
            else if (bookingType == Enumeration.BookingType.Takeaway)
            {
                var dal = DalFactory.Create<ITwoFactorDal>();
                var messageContent = string.Empty;
                var contactNumber = string.Empty;
                var contactDAOs = await contactList.Get(restaurantId, 0, 10);
                if (contactDAOs != null)
                {
                    messageTemplate = await dal.Fetch((int)Enumeration.TransmissionContent.NewTakeawayConfirmation2Restaurant);
                    foreach (var contactItem in contactDAOs)
                    {
                        if (contactNumber == string.Empty && Enumeration.ContactType.FloorManager == contactItem.SelectedType && contactItem.IsActive)
                        {
                            contactNumber = contactItem.PhoneNumber.ToString();
                        }
                        if (contactNumber == string.Empty && Enumeration.ContactType.RestaurantStaff == contactItem.SelectedType && contactItem.IsActive)
                        {
                            contactNumber = contactItem.PhoneNumber.ToString();
                        }
                        if (contactNumber == string.Empty && Enumeration.ContactType.GotTableRepersentative == contactItem.SelectedType && contactItem.IsActive)
                        {
                            contactNumber = contactItem.PhoneNumber.ToString();
                        }
                        if (contactItem.IsPhoneAlert && contactItem.IsActive)
                        {
                            string staffName = contactItem.Name;
                            if (contactItem.PhoneNumber.ToString().Length == 10)
                            {
                                factorRequestDto = new _2FactorRequestDto()
                                {
                                    BaseUrl = apiURL,
                                    ServiceUrl = string.Format(messageTemplate.QueryString, authKey, contactItem.PhoneNumber, messageTemplate.SenderId, messageTemplate.Name, staffName),
                                    TemplateName = messageTemplate.Name
                                };
                                twoFactorTransactionLogDto = new TwoFactorLogDto()
                                {
                                    TargetUrlWithParameters = factorRequestDto.ServiceUrl,
                                    TemplateId = messageTemplate.TemplateId,
                                    ExternalId = 0,
                                    PhoneNumber = contactItem.PhoneNumber
                                };
                                await dal.Insert(twoFactorTransactionLogDto);
                                factorRequestDto.LogId = twoFactorTransactionLogDto.LogId;
                                _2Factor.Send(factorRequestDto);
                            }
                        }
                    }
                }
                messageTemplate = await dal.Fetch((int)Enumeration.TransmissionContent.NewTakeawayConfirmation2User);
                var formatedBookingDateTime = bookingDateTime.ToString("dd-MMM-yyyy hh:mm tt");
                factorRequestDto = new _2FactorRequestDto()
                {
                    BaseUrl = apiURL,
                    ServiceUrl = string.Format(messageTemplate.QueryString, authKey, phoneNumber, messageTemplate.SenderId, messageTemplate.Name, restaurantName, formatedBookingDateTime, contactNumber),
                    TemplateName = messageTemplate.Name,
                };
                twoFactorTransactionLogDto = new TwoFactorLogDto()
                {
                    TargetUrlWithParameters = factorRequestDto.ServiceUrl,
                    TemplateId = messageTemplate.TemplateId,
                    ExternalId = 0,
                    PhoneNumber = phoneNumber
                };
                await dal.Insert(twoFactorTransactionLogDto);
                factorRequestDto.LogId = twoFactorTransactionLogDto.LogId;
                _2Factor.Send(factorRequestDto);
                var contentDal = DalFactory.Create<IContentDal>();
                var content = await contentDal.Fetch((int)Enumeration.TransmissionContent.NewTakeawayConfirmation2User);
                returnMessage = string.Format(content.Message, restaurantName, formatedBookingDateTime, contactNumber);
            }
            return returnMessage;
        }
    }
}
