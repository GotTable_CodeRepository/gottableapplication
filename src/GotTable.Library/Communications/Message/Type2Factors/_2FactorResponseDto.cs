﻿using System;

namespace GotTable.Library.Communications.Message.Type2Factors
{
    [Serializable]
    public sealed class _2FactorResponseDto
    {
        public string Status { get; set; }
        public string Details { get; set; }
    }
}
