﻿using System;

namespace GotTable.Library.Communications.Message.Type2Factors
{
    [Serializable]
    public sealed class _2FactorRequestDto
    {
        public decimal LogId { get; set; }

        public string TemplateName { get; set; }

        public string ServiceUrl { get; set; }

        public string BaseUrl { get; set; }
    }
}
