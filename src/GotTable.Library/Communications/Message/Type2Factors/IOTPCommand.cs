﻿using GotTable.DAO.Communications.Message;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Message.Type2Factors
{
    /// <summary>
    /// IOTPCommand
    /// </summary>
    public interface IOTPCommand
    {
        /// <summary>
        /// New
        /// </summary>
        /// <param name="oTPDetail"></param>
        /// <returns></returns>
        Task New(OTPDetailDAO oTPDetail);

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="oTPDetail"></param>
        /// <returns></returns>
        Task Save(OTPDetailDAO oTPDetail);

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        Task<OTPDetailDAO> Get(decimal transactionId);
    }
}