﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Communications.Message.OTP;
using GotTable.Dal.Communications.Message.TwoFactor;
using GotTable.DAO.Communications.Message;
using GotTable.Library.ApplicationSettings;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Message.Type2Factors
{
    /// <summary>
    /// OTPCommand
    /// </summary>
    [Serializable]
    public sealed class OTPCommand : IOTPCommand
    {
        #region Factory method
        /// <summary>
        /// New
        /// </summary>
        /// <param name="oTPDetail"></param>
        /// <returns></returns>
        public async Task New(OTPDetailDAO oTPDetail)
        {
            string otpNumber = GenerateOTP();
            var dto = new TransactionDto()
            {
                ExternalId = oTPDetail.ExternalId,
                CreatedDate = DateTime.Now,
                TypeId = (int)Enum.Parse(typeof(Enumeration.OTPTransaction), oTPDetail.Type),
                IsUsed = false,
                PhoneNumber = oTPDetail.PhoneNumber,
                OTP = otpNumber
            };

            var dal = DalFactory.Create<ITransactionDal>();
            await dal.Insert(dto);
            if (dto.TransactionId != 0)
            {
                oTPDetail.TransactionId = dto.TransactionId;
                string apiURL = AppSettingKeys.TwoFactorAPIURl;
                string authKey = AppSettingKeys.Two2FactorApiAuthKey;
                var _2FactorDal = DalFactory.Create<ITwoFactorDal>();
                var messageTemplate = await _2FactorDal.Fetch(dto.TypeId);
                var twoFactorRequest = new _2FactorRequestDto()
                {
                    BaseUrl = apiURL,
                    ServiceUrl = string.Format(messageTemplate.QueryString, authKey, oTPDetail.PhoneNumber, otpNumber, messageTemplate.Name),
                    TemplateName = messageTemplate.Name,
                    LogId = 0
                };
                var logDto = new TwoFactorLogDto()
                {
                    LogId = 0,
                    CreatedDate = DateTime.Now,
                    ExternalId = oTPDetail.ExternalId,
                    PhoneNumber = oTPDetail.PhoneNumber,
                    TargetUrlWithParameters = twoFactorRequest.ServiceUrl,
                    TemplateId = messageTemplate.TemplateId
                };
                await _2FactorDal.Insert(logDto);
                twoFactorRequest.LogId = logDto.LogId;
                _2Factor.Send(twoFactorRequest);
            }
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="oTPDetail"></param>
        /// <returns></returns>
        public async Task Save(OTPDetailDAO oTPDetail)
        {
            var dto = new TransactionDto()
            {
                IsUsed = oTPDetail.IsUsed,
                TransactionId = oTPDetail.TransactionId
            };
            var dal = DalFactory.Create<ITransactionDal>();
            await dal.Update(dto);
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public async Task<OTPDetailDAO> Get(decimal transactionId)
        {
            var dal = DalFactory.Create<ITransactionDal>();
            var dto = await dal.Fetch(transactionId);
            return new OTPDetailDAO()
            {
                ExternalId = dto.ExternalId,
                CreatedDate = dto.CreatedDate,
                Type = dto.TypeName,
                IsUsed = (bool)dto.IsUsed,
                OTP = dto.OTP,
                TransactionId = dto.TransactionId
            };
        }

        #endregion

        #region Helper Methods
        private string GenerateOTP()
        {
            string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            string sOTP = String.Empty;
            string sTempChars = String.Empty;
            Random rand = new Random();
            for (int i = 0; i < 6; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;
            }
            return sOTP;
        }

        #endregion
    }
}
