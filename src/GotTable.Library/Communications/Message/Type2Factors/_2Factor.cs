﻿using GotTable.Dal;
using GotTable.Dal.Communications.Message.TwoFactor;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace GotTable.Library.Communications.Message.Type2Factors
{
    [Serializable]
    internal sealed class _2Factor
    {
        private _2Factor()
        {

        }

        public static bool Send(_2FactorRequestDto _2factorRequestDto)
        {
            bool returnStatus = false;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(_2factorRequestDto.BaseUrl);
                HttpResponseMessage responseMessage = client.GetAsync(_2factorRequestDto.ServiceUrl.Replace("#", "%23")).Result;
                if (responseMessage.IsSuccessStatusCode)
                {
                    string responseString = responseMessage.Content.ReadAsStringAsync().Result;
                    var responseModel = JsonConvert.DeserializeObject<_2FactorResponseDto>(responseString);
                    var dal = DalFactory.Create<ITwoFactorDal>();
                    dal.Update(new TwoFactorLogDto()
                    {
                        LogId = _2factorRequestDto.LogId,
                        TransactionId = responseModel.Details,
                        TransactionStatus = responseModel.Status
                    });
                    returnStatus = true;
                }
            }
            return returnStatus;
        }
    }
}

