﻿using GotTable.Common.Enumerations;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Message
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public interface IMessageTransmission<T1, T2>
    {
        /// <summary>
        /// InvokeMobile
        /// </summary>
        /// <param name="bookingType"></param>
        /// <param name="bookingDateTime"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="restaurantId"></param>
        /// <param name="selectedTable"></param>
        /// <param name="deliveryAddress"></param>
        /// <param name="userId"></param>
        /// <param name="bookingId"></param>
        /// <param name="rewardStatus"></param>
        /// <returns></returns>
        Task<T2> Execute(Enumeration.BookingType bookingType, DateTime bookingDateTime, decimal phoneNumber, decimal restaurantId, string selectedTable, string deliveryAddress, decimal userId, decimal bookingId, bool rewardStatus = false);
    }
}