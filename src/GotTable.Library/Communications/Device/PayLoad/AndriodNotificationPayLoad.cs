﻿using System;

namespace GotTable.Library.Communications.Device.PayLoad
{
    [Serializable]
    public sealed class AndriodNotificationPayLoad
    {
        public AndriodNotificationPayLoad(string title, string message, string categoryName, string offerName)
        {
            JsonString = "{\"title\": " + "\"" + title + "\"" + ", \"body\": " + "\"" + message + "\"" + ", \"category\": " + "\"" + categoryName + "\"" + ", \"purpose\": " + "\"" + offerName + "\"" + ", \"sound\": \"default\", \"icon\": \"myicon\" }";
        }

        public AndriodNotificationPayLoad(string title, string message, string categoryName, string offerName, string filePath)
        {
            JsonString = "{\"title\": " + "\"" + title + "\"" + ", \"body\": " + "\"" + message + "\"" + ", \"category\": " + "\"" + categoryName + "\"" + ", \"purpose\": " + "\"" + offerName + "\"" + ", \"sound\": \"default\", \"icon\": \"myicon\", \"image\": " + "\"" + filePath + "\"" + " }";
        }

        public string JsonString { get; private set; }
    }
}
