﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Notifications.DeviceNotifications;
using GotTable.Dal.PushNotifications;
using GotTable.DAO.PushNotifications;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Communications.Device.PayLoad;
using GotTable.Library.Communications.Jobs.Types;
using Newtonsoft.Json.Linq;
using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Jobs
{
    [Serializable]
    public sealed class AndroidPushNotificationJob : IJob<AndroidPushNotificationJob, PushNotificationDAO>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pushNotificationDAO"></param>
        /// <returns></returns>
        public async Task Execute(PushNotificationDAO pushNotificationDAO)
        {
            if (pushNotificationDAO.Active)
            {
                var dal = DalFactory.Create<INotificationDeviceListDal>();
                var dtos = new List<NotificationDeviceDto>();
                if (pushNotificationDAO.CityId != null)
                {
                    dtos = await dal.FetchList(pushNotificationDAO.CityId, Enumeration.NotificationType.Android);
                }
                else
                {
                    dtos = await dal.FetchList();
                }
                if (dtos != null && dtos.Any())
                {
                    Tuple<int, int, string> androidNotification = null;
                    var androiddDeviceList = dtos.Where(x => x.DeviceType == Enumeration.Device.Android.ToString()).ToList();
                    CreatingSubList(androiddDeviceList).ForEach(listItem =>
                    {
                        androidNotification = SendNotificationToAndriod(pushNotificationDAO.Id, listItem, pushNotificationDAO.Title, pushNotificationDAO.Message, pushNotificationDAO.CategoryTypeName, pushNotificationDAO.OfferTypeName, pushNotificationDAO.ImagePath);
                        pushNotificationDAO.SuccessCount += androidNotification.Item1;
                        pushNotificationDAO.FailureCount += androidNotification.Item2;
                        pushNotificationDAO.AndroidPayLoad = androidNotification.Item3;
                        pushNotificationDAO.CurrentStatusId = (int)Enumeration.NotificationStatusType.Completed;
                    });
                }
            }
            await this.Update(pushNotificationDAO);
        }

        #region Helpers method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pushNotificationDAO"></param>
        /// <returns></returns>
        private async Task Update(PushNotificationDAO pushNotificationDAO)
        {
            var dto = new PushNotificationDto()
            {
                NotificationId = pushNotificationDAO.Id,
                Completed = true,
                AndroidPayLoad = pushNotificationDAO.AndroidPayLoad,
                IosPayLoad = pushNotificationDAO.IosPayLoad,
                SuccessCount = pushNotificationDAO.SuccessCount,
                FailureCount = pushNotificationDAO.FailureCount,
                StatusId = pushNotificationDAO.CurrentStatusId
            };
            var dal = DalFactory.Create<IPushNotificationDal>();
            await dal.Update(dto);
        }

        private Tuple<int, int, string> SendNotificationToAndriod(int notificationId, List<NotificationDeviceDto> deviceDtos, string title, string message, string categoryName, string offerName, string imageName)
        {
            int failureCount = 0;
            int successCount = 0;
            string payLoad = string.Empty;
            var exceptionLogs = new List<NotificationExceptionLogDto>();

            var configuration = new GcmConfiguration(AppSettingKeys.FCMSenderId, AppSettingKeys.FCMServerKey, AppSettingKeys.ApplicationIdPackageName)
            {
                GcmUrl = AppSettingKeys.FCMUrl
            };

            var broker = new GcmServiceBroker(configuration);

            broker.OnNotificationFailed += (notification, exception) =>
            {
                failureCount = failureCount + 1;
                exceptionLogs.Add(new NotificationExceptionLogDto()
                {
                    DeviceId = notification.RegistrationIds[0],
                    DeviceTypeId = (int)Enumeration.NotificationType.IOS,
                    InnerException = exception.InnerException.ToString(),
                    Message = exception.Message,
                    NotificationId = notificationId,
                    Source = exception.Source,
                    StackTrace = exception.StackTrace
                });
            };

            broker.OnNotificationSucceeded += (notification) =>
            {
                successCount = successCount + 1;
            };

            if (string.IsNullOrEmpty(imageName))
            {
                payLoad = new AndriodNotificationPayLoad(title, message, categoryName, offerName).JsonString;
            }
            else
            {
                payLoad = new AndriodNotificationPayLoad(title, message, categoryName, offerName, imageName).JsonString;
            }

            broker.Start();

            deviceDtos.ForEach(device =>
            {
                broker.QueueNotification(new GcmNotification()
                {
                    RegistrationIds = new List<string> { device.DeviceId },
                    Data = JObject.Parse(payLoad)
                });
            });

            broker.Stop();

            this.InsertExceptionLog(exceptionLogs);

            return Tuple.Create(successCount, failureCount, payLoad);
        }

        private List<List<NotificationDeviceDto>> CreatingSubList(List<NotificationDeviceDto> deviceDtos)
        {
            int seprationCounter = 20;
            var listForDeviceList = new List<List<NotificationDeviceDto>>();
            if (deviceDtos.Count() > seprationCounter)
            {
                for (int deviceIndex = 0; deviceIndex <= deviceDtos.Count(); deviceIndex += seprationCounter)
                {
                    if (deviceIndex + seprationCounter < deviceDtos.Count())
                    {
                        listForDeviceList.Add(deviceDtos.GetRange(deviceIndex, seprationCounter));
                    }
                    else
                    {
                        listForDeviceList.Add(deviceDtos.GetRange(deviceIndex, deviceDtos.Count() - deviceIndex));
                    }
                }
            }
            else
            {
                listForDeviceList.Add(deviceDtos);
            }
            return listForDeviceList;
        }

        private void InsertExceptionLog(List<NotificationExceptionLogDto> exceptionLog)
        {
            var dal = DalFactory.Create<IPushNotificationDal>();
            dal.Insert(exceptionLog);
        }

        #endregion
    }
}
