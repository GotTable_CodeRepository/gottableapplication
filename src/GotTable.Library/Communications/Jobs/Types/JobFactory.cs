﻿using Hangfire;
using System;

namespace GotTable.Library.Communications.Jobs.Types
{
    [Serializable]
    public sealed class JobFactory : IJobFactory
    {
        #region Create Jobs

        public void CreateJob<T1, T2>(IJob<T1, T2> job, T2 jobContent, DateTime scheduleDateTime)
        {
            var timeSpan = scheduleDateTime - DateTime.Now;
            BackgroundJob.Schedule(() => job.Execute(jobContent), TimeSpan.FromSeconds(timeSpan.TotalSeconds));
        }

        #endregion
    }
}
