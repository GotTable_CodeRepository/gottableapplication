﻿using System.Threading.Tasks;

namespace GotTable.Library.Communications.Jobs.Types
{
    public interface IJob<T1, T2>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        Task Execute(T2 dao);
    }
}