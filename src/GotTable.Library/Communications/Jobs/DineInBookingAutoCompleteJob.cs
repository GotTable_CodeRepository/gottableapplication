﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.RestaurantBookings.DineIn;
using GotTable.DAO.Bookings.DineIn;
using GotTable.Library.Communications.Jobs.Types;
using System;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Jobs
{
    [Serializable]
    public sealed class DineInBookingAutoCompleteJob : IJob<DineInBookingAutoCompleteJob, DineInBookingDAO>
    {
        /// <summary>
        /// 
        /// </summary>
        private const string Comment = "System completed this booking.";

        #region Factory methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dineInbooking"></param>
        /// <returns></returns>
        public async Task Execute(DineInBookingDAO dineInbooking)
        {
            var dto = new DineInStatusDto()
            {
                AdminId = 0,
                Comment = Comment,
                UserId = null,
                BookingId = dineInbooking.Id,
                StatusId = (int)Enumeration.DineInStatusType.AutoComplete
            };
            var dal = DalFactory.Create<IDineInDal>();
            await dal.Insert(dto);
        }

        #endregion
    }
}
