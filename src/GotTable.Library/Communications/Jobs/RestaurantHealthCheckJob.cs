﻿using GotTable.Dal;
using System;
using System.Threading.Tasks;
using GotTable.Dal.Restaurants;
using GotTable.Dal.RestaurantConfigurations;
using GotTable.Dal.RestaurantInvoices;
using GotTable.Library.Communications.Jobs.Types;

namespace GotTable.Library.Communications.Jobs
{
    [Serializable]
    public sealed class RestaurantHealthCheckJob : IJob<RestaurantHealthCheckJob, decimal>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dao"></param>
        /// <returns></returns>
        public async Task Execute(decimal restaurantId)
        {
            await Task.FromResult(1);
            var restaurantDal = DalFactory.Create<IRestaurantDal>();
            var configurationDal = DalFactory.Create<IConfigurationDal>();
            var invoiceDal = DalFactory.Create<IInvoiceDal>();
            var restaurantDto = await restaurantDal.Fetch(restaurantId);
            var configurationDto = await configurationDal.Fetch(restaurantId);
            var invoiceDto = await invoiceDal.Fetch(restaurantId);
        }
    }
}
