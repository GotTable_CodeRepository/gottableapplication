﻿using GotTable.Common.Enumerations;
using GotTable.Dal;
using GotTable.Dal.Notifications.DeviceNotifications;
using GotTable.Dal.PushNotifications;
using GotTable.DAO.PushNotifications;
using GotTable.Library.ApplicationSettings;
using GotTable.Library.Communications.Device;
using GotTable.Library.Communications.Device.PayLoad;
using GotTable.Library.Communications.Jobs.Types;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GotTable.Library.Communications.Jobs
{
    [Serializable]
    public sealed class IOSPushNotificationJob : IJob<IOSPushNotificationJob, PushNotificationDAO>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pushNotificationDAO"></param>
        /// <returns></returns>
        public async Task Execute(PushNotificationDAO pushNotificationDAO)
        {
            if (pushNotificationDAO.Active)
            {
                var dal = DalFactory.Create<INotificationDeviceListDal>();
                var dtos = new List<NotificationDeviceDto>();
                if (pushNotificationDAO.CityId != null)
                {
                    dtos = await dal.FetchList(pushNotificationDAO.CityId, Enumeration.NotificationType.IOS);
                }
                else
                {
                    dtos = await dal.FetchList();
                }
                if (dtos != null && dtos.Any())
                {
                    Tuple<int, int, string> iosNotification = null;
                    var iosDeviceList = dtos.Where(x => x.DeviceType == Enumeration.Device.IOS.ToString()).ToList();
                    CreatingSubList(iosDeviceList).ForEach(async listItem =>
                    {
                        iosNotification = await SendNotificationToIOS(pushNotificationDAO.Id, listItem, pushNotificationDAO.Title, pushNotificationDAO.Message, pushNotificationDAO.CategoryTypeName, pushNotificationDAO.OfferTypeName, pushNotificationDAO.ImagePath);
                        pushNotificationDAO.SuccessCount += iosNotification.Item1;
                        pushNotificationDAO.FailureCount += iosNotification.Item2;
                        pushNotificationDAO.IosPayLoad = iosNotification.Item3;
                        pushNotificationDAO.CurrentStatusId = (int)Enumeration.NotificationStatusType.Completed;
                    });
                }
            }
            await Update(pushNotificationDAO);
        }

        #region Helpers method

        private async Task Update(PushNotificationDAO pushNotificationDAO)
        {
            var dto = new PushNotificationDto()
            {
                NotificationId = pushNotificationDAO.Id,
                Completed = true,
                AndroidPayLoad = pushNotificationDAO.AndroidPayLoad,
                IosPayLoad = pushNotificationDAO.IosPayLoad,
                SuccessCount = pushNotificationDAO.SuccessCount,
                FailureCount = pushNotificationDAO.FailureCount,
                StatusId = pushNotificationDAO.CurrentStatusId
            };
            var dal = DalFactory.Create<IPushNotificationDal>();
            await dal.Update(dto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationId"></param>
        /// <param name="deviceDtos"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="categoryName"></param>
        /// <param name="offerName"></param>
        /// <param name="imageName"></param>
        /// <returns></returns>
        private async Task<Tuple<int, int, string>> SendNotificationToIOS(int notificationId, List<NotificationDeviceDto> deviceDtos, string title, string message, string categoryName, string offerName, string imageName)
        {
            string payLoad = string.Empty;
            var exceptionLogs = new List<NotificationExceptionLogDto>();
            int successCount = 0;
            int failureCount = 0;

            ApnsServiceBrokerManager.Initialize(new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production,
                                                                      AppSettingKeys.ApnsCertificateContent,
                                                                      AppSettingKeys.ApnsCertificatePassword));

            var broker = ApnsServiceBrokerManager.GetApnsServiceBroker();

            broker.OnNotificationFailed += (notification, exception) =>
            {
                failureCount = failureCount + 1;

                exceptionLogs.Add(new NotificationExceptionLogDto()
                {
                    DeviceId = notification.DeviceToken,
                    DeviceTypeId = (int)Enumeration.NotificationType.IOS,
                    InnerException = exception.InnerException.ToString(),
                    Message = exception.Message,
                    NotificationId = notificationId,
                    Source = exception.Source,
                    StackTrace = exception.StackTrace
                });
            };

            broker.OnNotificationSucceeded += (notification) =>
            {
                successCount = successCount + 1;
            };

            if (string.IsNullOrEmpty(imageName))
            {
                payLoad = new IOSNotificationPayLoad(title, message, categoryName, offerName).JsonString;
            }
            else
            {
                payLoad = new IOSNotificationPayLoad(title, message, categoryName, offerName, imageName).JsonString;
            }

            broker.Start();

            deviceDtos.ForEach(device =>
            {
                broker.QueueNotification(new ApnsNotification
                {
                    DeviceToken = device.DeviceId,
                    Payload = JObject.Parse(payLoad)
                });
            });

            broker.Stop();

            await InsertExceptionLog(exceptionLogs);

            return Tuple.Create(successCount, failureCount, payLoad);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deviceDtos"></param>
        /// <returns></returns>
        internal List<List<NotificationDeviceDto>> CreatingSubList(List<NotificationDeviceDto> deviceDtos)
        {
            int seprationCounter = 20;
            var listForDeviceList = new List<List<NotificationDeviceDto>>();
            if (deviceDtos.Count() > seprationCounter)
            {
                for (int deviceIndex = 0; deviceIndex <= deviceDtos.Count(); deviceIndex += seprationCounter)
                {
                    if (deviceIndex + seprationCounter < deviceDtos.Count())
                    {
                        listForDeviceList.Add(deviceDtos.GetRange(deviceIndex, seprationCounter));
                    }
                    else
                    {
                        listForDeviceList.Add(deviceDtos.GetRange(deviceIndex, deviceDtos.Count() - deviceIndex));
                    }
                }
            }
            else
            {
                listForDeviceList.Add(deviceDtos);
            }
            return listForDeviceList;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="exceptionLog"></param>
        /// <returns></returns>
        internal async Task InsertExceptionLog(List<NotificationExceptionLogDto> exceptionLog)
        {
            var dal = DalFactory.Create<IPushNotificationDal>();
            await dal.Insert(exceptionLog);
        }

        #endregion
    }
}
