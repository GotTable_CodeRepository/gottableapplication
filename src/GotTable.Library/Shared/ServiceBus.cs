﻿using NServiceBus;
using System.Threading.Tasks;

namespace GotTable.Library.Shared
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class ServiceBus
    {
        /// <summary>
        /// 
        /// </summary>
        private static IEndpointInstance bus;

        /// <summary>
        /// 
        /// </summary>
        public static async void Configure()
        {
            var endpointConfiguration = new EndpointConfiguration("GotTableServiceBus");
            endpointConfiguration.MakeInstanceUniquelyAddressable("1");
            if (System.Diagnostics.Debugger.IsAttached)
            {
                endpointConfiguration.LimitMessageProcessingConcurrencyTo(1);
            }
            endpointConfiguration.UsePersistence<LearningPersistence>();
            endpointConfiguration.UseTransport<LearningTransport>();

            bus = await Endpoint.Start(endpointConfiguration).ConfigureAwait(false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="event"></param>
        /// <returns></returns>
        internal static async Task PublishAsync<T>(T @event)
        {
            await ServiceBus.bus.Publish(@event, null).ConfigureAwait(false);
        }
    }
}
