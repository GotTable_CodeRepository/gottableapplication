﻿using System;

namespace GotTable.DAO.Restaurants.PreferredRestaurants
{
    [Serializable]
    public sealed class PreferredRestaurantInfoDAO : ListModelDAO
    {
        public decimal RestaurantId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string TagLine { get; set; }

        public string SEOKeyword { get; set; }

        public string SEOTitle { get; set; }

        public string SEODescription { get; set; }

        public decimal Zipcode { get; set; }

        public decimal? Distance { get; set; }

        public string ImageUrl { get; set; }

        public int Rating { get; set; }
    }
}
