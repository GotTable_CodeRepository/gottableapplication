﻿
using GotTable.Common.Enumerations;

namespace GotTable.DAO.Restaurants
{
    [System.Serializable]
    public sealed class PipelineRestaurantDAO : ListModelDAO
    {
        public string SalesPerson { get; set; }

        public string SalesPersonEmailAddress { get; set; }

        public decimal? SalesPersonId { get; set; }

        public decimal RestaurantId { get; set; }

        public string RestaurantName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public decimal? AccountAdminId { get; set; }

        public string AccountAdminName { get; set; }

        public string AccountAdminEmailAddress { get; set; }

        public bool OpenForAccountAdmin { get; set; }

        public Enumeration.RestaurantCategories Category { get; set; }

        public int ContactCount { get; set; }

        public int CuisineCount { get; set; }

        public int TablesCount { get; set; }

        public int MenuCount { get; set; }

        public int TimingCount { get; set; }
    }
}
