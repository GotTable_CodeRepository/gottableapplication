﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Images;
using System.Collections.Generic;

namespace GotTable.DAO.Restaurants
{
    [System.Serializable]
    public sealed class ResuturantModelDAO : EditModelDAO
    {
        public ResuturantModelDAO()
        {
            this.ImageList = new List<ImageDAO>();
            this.SelectedTag = Enumeration.RestaurantTags.NoTag;
        }

        public decimal Id { get; set; }

        public string Name { get; set; }

        public string CuisineNames { get; set; }

        public int AvgRating { get; set; }

        public decimal Distance { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal ZipCode { get; set; }

        public string OfferName { get; set; }

        public bool IsOfferAvailable { get; set; }

        public string RestaurantTag { get; set; }

        public string SEOKeyword { get; set; }

        public string SEOTitle { get; set; }

        public string SEODescription { get; set; }

        public Enumeration.RestaurantTags SelectedTag { get; set; }

        public List<ImageDAO> ImageList { get; set; }

        public Enumeration.RestaurantCategories Category { get; set; }

        public int ActiveOffers { get; set; }

        public int CostForTwo { get; set; }

        public string Description { get; set; }

        public int MusicRating { get; set; }

        public int AmbienceRating { get; set; }

        public int PriceRating { get; set; }

        public int ServiceRating { get; set; }

        public int FoodRating { get; set; }
    }
}
