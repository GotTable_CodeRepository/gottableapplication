﻿namespace GotTable.DAO.Restaurants
{
    public sealed class AutoCompleteItemDAO
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
