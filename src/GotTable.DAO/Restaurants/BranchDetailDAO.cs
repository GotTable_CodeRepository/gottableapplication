﻿
namespace GotTable.DAO.Restaurants
{
    [System.Serializable]
    public sealed class BranchDetailDAO : EditModelDAO
    {
        public decimal BranchId { get; set; }

        public string BranchName { get; set; }

        public string TagLine { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public decimal SalesPersonId { get; set; }

        public decimal AccountAdminPersonId { get; set; }

        public bool Open4AccountAdmin { get; set; }

        public bool IsActive { get; set; }

        public string SEOKeyword { get; set; }

        public string SEOTitle { get; set; }

        public string SEODescription { get; set; }

        public string Category { get; set; }

        public decimal? LocalityId1 { get; set; }

        public string LocalityName1 { get; set; }

        public decimal? LocalityId2 { get; set; }

        public string LocalityName2 { get; set; }

        public decimal? LocalityId3 { get; set; }

        public string LocalityName3 { get; set; }

        public decimal? LocalityId4 { get; set; }

        public string LocalityName4 { get; set; }

        public string Description { get; set; }

        public int? CostForTwo { get; set; }
    }
}
