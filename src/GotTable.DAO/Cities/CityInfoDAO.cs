﻿using System;

namespace GotTable.DAO.Cities
{
    /// <summary>
    /// CityInfoDAO
    /// </summary>
    [Serializable]
    public sealed class CityInfoDAO : ListModelDAO
    {
        /// <summary>
        /// Id
        /// </summary>
        public decimal? Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
