﻿using System;

namespace GotTable.DAO.RestaurantAmenities
{
    [Serializable]
    public sealed class AmenityDAO : EditModelDAO
    {
        public AmenityDAO(decimal id = default, decimal branchId = default, int amenityId = default, string amenityName = default, bool checkedValue = default)
        {
            Id = id;
            BranchId = branchId;
            AmenityId = amenityId;
            AmenityName = amenityName;
            Checked = checkedValue;
        }

        public decimal Id { get; set; } 

        public decimal BranchId { get; set; }

        public int AmenityId { get; set; }

        public string AmenityName { get; set; }

        public bool Checked { get; set; }
    }
}
