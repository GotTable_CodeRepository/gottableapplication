﻿using System;

namespace GotTable.DAO.RestaurantRatings
{
    [Serializable]
    public sealed class BranchRatingDAO : EditModelDAO
    {
        public decimal Id { get; set; }

        public decimal BranchId { get; set; }

        public decimal UserId { get; set; }

        public string Title { get; set; }

        public string Comment { get; set; }

        public int? Rating { get; set; }

        public int? AmbienceRating { get; set; }

        public int? FoodRating { get; set; }

        public int? MusicRating { get; set; }

        public int? PriceRating { get; set; }

        public int? ServiceRating { get; set; }

        public bool IsActive { get; set; }

        public DateTime? CreationDate { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }
    }
}
