﻿using System;

namespace GotTable.DAO.Images
{
    [Serializable]
    public sealed class ImageDAO : EditModelDAO
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public string Url { get; set; }
    }
}
