﻿using GotTable.Common.Enumerations;
using System;

namespace GotTable.DAO.RestaurantTimings
{
    [Serializable]
    public sealed class TimingDAO : ListModelDAO
    {
        public decimal Id { get; set; }

        public decimal? BranchId { get; set; }

        public Enumeration.WeekDays SelectedDay { get; set; }

        public string DinnerStartTime { get; set; }

        public string DinnerStartTimeIn24HourFormat
        {
            get
            {
                return ReturnStringIn24HourFormat(this.DinnerStartTime);
            }
        }

        public int DinnerStartTimeIn24HourIntegerFormat
        {
            get
            {
                return ReturnIntIn24HourFormat(this.DinnerStartTime);
            }
        }

        public string DinnerEndTime { get; set; }

        public string DinnerEndTimeIn24HourFormat
        {
            get
            {
                return ReturnStringIn24HourFormat(this.DinnerEndTime);
            }
        }

        public int DinnerEndTimeIn24HourIntegerFormat
        {
            get
            {
                return ReturnIntIn24HourFormat(this.DinnerEndTime);
            }
        }

        public string LunchStartTime { get; set; }

        public string LunchStartTimeIn24HourFormat
        {
            get
            {
                return ReturnStringIn24HourFormat(this.LunchStartTime);
            }
        }

        public int LunchStartTimeIn24HourIntegerFormat
        {
            get
            {
                return ReturnIntIn24HourFormat(this.LunchStartTime);
            }
        }

        public string LunchEndTime { get; set; }

        
        public string LunchEndTimeIn24HourFormat
        {
            get
            {
                return ReturnStringIn24HourFormat(this.LunchEndTime);
            }
        }

        public int LunchEndTimeIn24HourIntegerFormat
        {
            get
            {
                return ReturnIntIn24HourFormat(this.LunchEndTime);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsClosed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputTime"></param>
        /// <returns></returns>
        private string ReturnStringIn24HourFormat(string inputTime)
        {
            if (inputTime == null)
            {
                return "00:00";
            }
            else if (inputTime == "None")
            {
                return "00:00";
            }
            else if (inputTime == "00:00")
            {
                return "00:00";
            }
            else
            {
                string dateTimeInStringFormat = DateTime.Now.ToString("MM-dd-yyyy") + " " + inputTime;
                DateTime dt = DateTime.ParseExact(dateTimeInStringFormat, "MM-dd-yyyy hh:mm tt", null);
                return dt.ToString("HH:mm");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputTime"></param>
        /// <returns></returns>
        private int ReturnIntIn24HourFormat(string inputTime)
        {
            if (inputTime == null)
            {
                return 0;
            }
            else if (inputTime == "None")
            {
                return 0;
            }
            else if (inputTime == "00:00")
            {
                return 0;
            }
            else
            {
                string dateTimeInStringFormat = DateTime.Now.ToString("MM-dd-yyyy") + " " + inputTime;
                DateTime dt = DateTime.ParseExact(dateTimeInStringFormat, "MM-dd-yyyy hh:mm tt", null);
                return int.Parse(dt.ToString("HHmm"));
            }
        }
    }
}
