﻿using GotTable.Common.Enumerations;
using System.Collections.Generic;

namespace GotTable.DAO.RestaurantTimings
{
    [System.Serializable]
    public sealed class BranchTimingDAO : EditModelDAO
    {
        public BranchTimingDAO()
        {
            this.DayTiming = new List<TimingDAO>();
        }

        public decimal BranchId { get; set; }

        public Enumeration.RestaurantTypes SelectedCategory { get; set; }

        public List<TimingDAO> DayTiming { get; set; }
    }
}
