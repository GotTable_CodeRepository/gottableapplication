﻿using System.Web;

namespace GotTable.DAO.RestaurantDocuments
{
    [System.Serializable]
    public sealed class FileModelDAO : EditModelDAO
    {
        public FileModelDAO()
        {

        }
        public FileModelDAO(decimal restaurantId, string logoName)
        {
            this.RestaurantId = restaurantId;
            this.LogoName = logoName;
        }

        public decimal RestaurantId { get; set; }

        public string LogoName { get; set; }

        public string FilePath { get; set; }

        //public HttpPostedFileBase Content { get; set; }
    }
}
