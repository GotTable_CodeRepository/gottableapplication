﻿using System;
using System.Web;

namespace GotTable.DAO.RestaurantDocuments
{
    [Serializable]
    public sealed class DocumentDAO : EditModelDAO
    {
        public int DocumentId { get; set; }

        public decimal BranchId { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public string Name { get; set; }

        public string Extension { get; set; }

        public bool Active { get; set; }

        public string ImagePath { get; set; }

        public bool Exist
        {
            get
            {
                return false;
            }
        }

        //public HttpPostedFileBase Content { get; set; }
    }
}
