﻿using System;
using System.Collections.Generic;

namespace GotTable.DAO.Communications.Message
{
    [Serializable]
    public sealed class OTPModelDAO : EditModelDAO
    {
        public OTPModelDAO()
        {
            this.sms = new List<SMSModelDAO>();
        }

        public string sender = "Socket";

        public string route = "4";

        public string country = "91";

        public List<SMSModelDAO> sms { get; set; }
    }
}
