﻿
namespace GotTable.DAO.Communications.Email
{
    [System.Serializable]
    public sealed class ForgetPasswordDAO : EditModelDAO
    {
        /// <summary>
        /// 
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ConfirmationMessage { get; set; }
    }
}
