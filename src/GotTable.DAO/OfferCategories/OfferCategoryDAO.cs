﻿

using System.Web;

namespace GotTable.DAO.OfferCategories
{
    public sealed class OfferCategoryDAO : EditModelDAO
    {
        public OfferCategoryDAO()
        {

        }

        public OfferCategoryDAO(int id = default, string name = default, bool active = default, string extension = default, string offerDirectory = default)
        {
            Id = id;
            Name = name;
            Active = active;
            Extension = extension;
            ImagePath = string.Format("{0}{1}{2}", offerDirectory, id, extension);
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        public string Extension { get; set; }

        //public HttpPostedFileBase Content { get; set; }

        public string ImagePath { get; set; }

        public int EngagedOffers { get; set; }
    }
}
