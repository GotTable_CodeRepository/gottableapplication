﻿using System;
using System.Collections.Generic;

namespace GotTable.DAO.Users
{
    [System.Serializable]
    public sealed class UserDetailDAO : EditModelDAO
    {
        public UserDetailDAO()
        {
            this.UserId = 0;
            this.AddressList = new List<UserAddressDAO>();
            this.PhoneNumberList = new List<PhoneNumberDAO>();
        }

        public decimal UserId { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public string UserType { get; set; }

        public string Prefix { get; set; }

        public bool IsActive { get; set; }

        public bool IsEmailVerified { get; set; }

        public bool IsEmailOptedForCommunication { get; set; }

        public int? RewardCount { get; set; }

        public List<UserAddressDAO> AddressList { get; set; }

        public List<PhoneNumberDAO> PhoneNumberList { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName + " " + EmailAddress;
            }
        }

        public decimal? DoubleTheDealBookingId { get; set; }
    }
}
