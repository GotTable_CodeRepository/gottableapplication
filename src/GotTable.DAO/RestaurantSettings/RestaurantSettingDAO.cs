﻿
using System;

namespace GotTable.DAO.RestaurantSettings
{
    [Serializable]
    public sealed class RestaurantSettingDAO : EditModelDAO
    {
        public decimal BranchId { get; set; }

        public decimal AccountAdminPersonId { get; set; }

        public bool OpenForAccountAdmin { get; set; }

        public bool ApplicationStatus { get; set; }


        #region Restaurant stats

        public int Contacts { get; set; }

        public int Cuisines { get; set; }

        public int Timings { get; set; }

        public int Tables { get; set; }

        public int Amenities { get; set; }

        public int Menus { get; set; }

        public bool Configuration { get; set; }

        public string MissingConfiguration { get; set; }

        public DateTime? InvoiceEndDate { get; set; }

        public bool DineInActive { get; set; }

        public bool DeliveryActive { get; set; }

        public bool TakeawayActive { get; set; }

        public int GalleyImages { get; set; }

        public string FrontImage { get; set; }

        public string InsideImage { get; set; }

        public string LogoImage { get; set; }

        #endregion
    }
}
