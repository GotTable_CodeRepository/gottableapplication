﻿using System;

namespace GotTable.DAO.Bookings.DeliveryandTakeaway
{
    /// <summary>
    /// 
    /// </summary>
    public sealed class DeliveryandTakeawayStatusDAO : EditModelDAO
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal BookingId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? AdminId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AdminName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}
