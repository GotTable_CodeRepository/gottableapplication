﻿
namespace GotTable.DAO.Bookings.DeliveryandTakeaway
{
    public sealed class DeliveryAddressDAO : EditModelDAO
    {
        public decimal Id { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Landmark { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public decimal Zip { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }
    }
}
