﻿using GotTable.Common.Enumerations;
using GotTable.DAO.Users;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GotTable.DAO.Bookings.DeliveryandTakeaway
{
    public sealed class DeliveryandTakeawayDAO : EditModelDAO
    {
        public DeliveryandTakeawayDAO()
        {
            this.StatusList = new List<DeliveryandTakeawayStatusDAO>();
            this.CartItem = new List<DeliveryandTakeawayCartItemDAO>();
            this.AddressDetail = new DeliveryAddressDAO();
            this.UserDetail = new UserDetailDAO();
        }

        public decimal BookingId { get; set; }

        public DateTime BookingDate { get; set; }

        public string BookingTime { get; set; }

        public int BookingTypeId { get; set; }

        public Enumeration.BookingType BookingType
        {
            get
            {
                return (Enumeration.BookingType)BookingTypeId;
            }
        }

        public decimal BranchId { get; set; }

        public decimal PhoneNumber { get; set; }

        public decimal UserId { get; set; }

        public decimal OfferId { get; set; }

        public string OfferTitle { get; set; }

        public string OfferDescription { get; set; }

        public string PromoCode { get; set; }

        public decimal CentralGST { get; set; }

        public decimal DeliveryCharges { get; set; }

        public decimal StateGST { get; set; }

        public int CurrentStatusId { get; set; }

        public string CurrentStatus { get; set; }

        public decimal Cartamount
        {
            get
            {
                if (CartItem.Count > 0)
                {
                    return CartItem.Sum(x => x.Amount);
                }
                else
                {
                    return 0;
                }
            }
        }

        public UserDetailDAO UserDetail { get; set; }

        public DeliveryandTakeawayStatusDAO NewStatus { get; set; }

        public List<DeliveryandTakeawayStatusDAO> StatusList { get; set; }

        public List<DeliveryandTakeawayCartItemDAO> CartItem { get; set; }

        public DeliveryAddressDAO AddressDetail { get; set; }

        public string ConfirmationMessage { get; set; }
    }
}
