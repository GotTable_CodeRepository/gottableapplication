﻿using System;

namespace GotTable.DAO.Bookings
{
    [Serializable]
    public sealed class BookingDAO : ListModelDAO
    {
        public decimal BookingId { get; set; }

        public string BookingType { get; set; }

        public decimal UserId { get; set; }

        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public decimal PhoneNumber { get; set; }

        public string DateTimeForOrder { get; set; }

        public DateTime BookingDateTime { get; set; }

        public string BookingDateTimeToString
        {
            get
            {
                return this.BookingDateTime.ToString("MMM dd yyyy, hh:mm tt");
            }
        }

        public string TableName { get; set; }

        public string CurrentStatus { get; set; }

        public bool IsRead { get; set; }
    }
}
