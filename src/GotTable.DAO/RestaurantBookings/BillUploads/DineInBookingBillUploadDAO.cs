﻿using System;

namespace GotTable.DAO.RestaurantBookings.BillUploads
{
    [Serializable]
    public class DineInBookingBillUploadDAO : EditModelDAO
    {
        public decimal UserId { get; set; }

        public decimal BookingId { get; set; }

        public double Amount { get; set; }

        public string ImagePath { get; set; }

        public DateTime? UploadedDate { get; set; }

        public int? AuthorizedRewardPoint { get; set; }

        public bool? Authorized { get; set; }

        public DateTime? AuthorizedDate { get; set; }

        public string AuthorizedComment { get; set; }

        public bool? Redeem { get; set; }

        public DateTime? RedeemDate { get; set; }

        public string RedeemComment { get; set; }

        public decimal? AdminId { get; set; }


        #region Connected Property

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public string RestaurantName { get; set; }

        public string RestaurantAddress { get; set; }

        public string OfferName { get; set; }

        public string OfferType { get; set; }

        public string BookingDate { get; set; }

        public string AdminName { get; set; }

        #endregion
    }
}
