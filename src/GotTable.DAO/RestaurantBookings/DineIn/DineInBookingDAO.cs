﻿using GotTable.DAO.Users;
using System;
using System.Collections.Generic;

namespace GotTable.DAO.Bookings.DineIn
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class DineInBookingDAO : EditModelDAO
    {
        public DineInBookingDAO()
        {
            this.StatusList = new List<DineInStatusDAO>();
            this.UserDetail = new UserDetailDAO();
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal BranchId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal TableId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal? OfferId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OfferType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OfferTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OfferDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PromoCode { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public decimal PhoneNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime BookingDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BookingTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SelectedTable { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public UserDetailDAO UserDetail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<DineInStatusDAO> StatusList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ConfirmationMessage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool RewardApplied { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BillUploadStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool DoubleTheDeal { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DeviceType { get; set; }
    }
}
