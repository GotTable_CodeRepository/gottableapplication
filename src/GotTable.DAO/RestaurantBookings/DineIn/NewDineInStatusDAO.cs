﻿using GotTable.Common;
using System.Collections.Generic;

namespace GotTable.DAO.Bookings.DineIn
{
    [System.Serializable]
    public sealed class NewDineInStatusDAO : EditModelDAO
    {
        public string BookingId { get; set; }

        public string Comments { get; set; }

        public string StatusName { get; set; }

        public string UserId { get; set; }

        public List<EnumDto> List { get; set; }
    }
}
