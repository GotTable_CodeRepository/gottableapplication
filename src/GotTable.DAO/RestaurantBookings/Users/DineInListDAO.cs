﻿using System.Collections.Generic;

namespace GotTable.DAO.Bookings.Users
{
    public sealed class DineInListDAO : ListModelDAO
    {
        public DineInListDAO()
        {
            this.RestaurantDetail = new RestaurantDAO();
            this.ContactList = new List<RestaurantContactDAO>();
        }

        public decimal BookingId { get; set; }

        public string SelectedOffer { get; set; }

        public string DateForBooking { get; set; }

        public string TimeForBooking { get; set; }

        public string SelectedTable { get; set; }

        public string Status { get; set; }

        public string BillUploadStatus { get; set; }

        public RestaurantDAO RestaurantDetail { get; set; }

        public List<RestaurantContactDAO> ContactList { get; set; }
    }
}
