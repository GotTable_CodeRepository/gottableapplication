﻿using System.Collections.Generic;

namespace GotTable.DAO.Bookings.Users
{
    public sealed class DeliveryandTakeawayListDAO : ListModelDAO
    {
        public DeliveryandTakeawayListDAO()
        {
            this.RestaurantDetail = new RestaurantDAO();
            this.CartList = new List<CartModelDAO>();
            this.DeliveryDetail = new DeliveryAddressDAO();
            this.ContactList = new List<RestaurantContactDAO>();
        }

        public decimal BookingId { get; set; }

        public string BookingType { get; set; }

        public string DateForBooking { get; set; }

        public string Status { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal CentralGST { get; set; }

        public decimal StateGST { get; set; }

        public decimal ServiceCharges { get; set; }

        public decimal DeliveryCharges { get; set; }

        public decimal PackingCharges { get; set; }

        public RestaurantDAO RestaurantDetail { get; set; }

        public List<CartModelDAO> CartList { get; set; }

        public DeliveryAddressDAO DeliveryDetail { get; set; }

        public List<RestaurantContactDAO> ContactList { get; set; }
    }
}
