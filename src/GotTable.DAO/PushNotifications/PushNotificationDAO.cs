﻿using GotTable.Common.Enumerations;
using System;
using System.Web;

namespace GotTable.DAO.PushNotifications
{
    [Serializable]
    public sealed class PushNotificationDAO : EditModelDAO
    {
        public PushNotificationDAO()
        {
            this.Id = 0;
        }

        public int Id { get; set; }

        public string Message { get; set; }

        public string Title { get; set; }

        public bool Active { get; set; }

        public int? CurrentStatusId { get; set; }

        public string CurrentStatusName { get { return CurrentStatusId != null ? ((Enumeration.NotificationStatusType)CurrentStatusId).ToString() : string.Empty; } }

        public decimal? CityId { get; set; }

        public decimal UserId { get; set; }

        public string ScheduleTime { get; set; }

        public DateTime CreatedDatetime { get; set; }

        public int NotificationTypeId { get; set; }

        public string CityName { get; set; }

        public string NotificationType { get; set; }

        public int CategoryTypeId { get; set; }

        public int OfferTypeId { get; set; }

        public string OfferTypeName { get; set; }

        public string CategoryTypeName { get; set; }

        //public HttpPostedFileBase Content { get; set; }

        public string Extension { get; set; }

        public int FailureCount { get; set; }

        public int SuccessCount { get; set; }

        public string IosPayLoad { get; set; }

        public string AndroidPayLoad { get; set; }

        public string ImagePath { get; set; }
    }
}
